function testHung() {
    // var personArray = [
    //     {name:"person1",code:"101011",mainDept:"mainD 1",dept:"dept1",SubDept:"Sub01"},
    //     {name:"person2",code:"201012",mainDept:"mainD 1",dept:"dept1",SubDept:"Sub11"},
    //     {name:"person3",code:"301013",mainDept:"mainD 1",dept:"dept2",SubDept:"Sub12"},
    //     {name:"person4",code:"401014",mainDept:"mainD 1",dept:"dept2",SubDept:"Sub12"},
    //     {name:"person5",code:"501015",mainDept:"mainD 1",dept:"dept2",SubDept:"Sub13"},
    //     {name:"person6",code:"601116",mainDept:"mainD 1",dept:"dept3",SubDept:"Sub21"},
    //     {name:"person7",code:"701117",mainDept:"mainD 1",dept:"dept3",SubDept:"Sub21"},
    //     {name:"person8",code:"801118",mainDept:"mainD 1",dept:"dept4",SubDept:"Sub22"},
    //     {name:"person9",code:"901119",mainDept:"mainD 2",dept:"dept12",SubDept:"Sub23"},
    //     {name:"person10",code:"101111",mainDept:"mainD 2",dept:"dept12",SubDept:"Sub24"},
    //     {name:"person12",code:"121012",mainDept:"mainD 2",dept:"dept13",SubDept:"Sub25"},
    //     {name:"person13",code:"131013",mainDept:"mainD 2",dept:"dept131",SubDept:"Sub26"},
    //     {name:"person14",code:"141014",mainDept:"mainD 3",dept:"dept132",SubDept:"Sub27"},
    //     {name:"person15",code:"151015",mainDept:"mainD 3",dept:"dept132",SubDept:"Sub27"},
    //     {name:"person16",code:"161116",mainDept:"mainD 4",dept:"dept141",SubDept:"Sub1"},
    //     {name:"person17",code:"171117",mainDept:"mainD 4",dept:"dept141",SubDept:"Sub1"},
    //     {name:"person18",code:"181118",mainDept:"mainD 4",dept:"dept141",SubDept:"Sub1"},
    //     {name:"person21",code:"211012",mainDept:"mainD 4",dept:"dept141",SubDept:"Sub1"},
    //     {name:"person22",code:"221013",mainDept:"mainD 4",dept:"dept141",SubDept:"Sub001"},
    //     {name:"person23",code:"231014",mainDept:"mainD 4",dept:"dept151",SubDept:"Sub002"},
    //     {name:"person24",code:"241015",mainDept:"mainD 5",dept:"dept161",SubDept:"Sub003"},
    //     {name:"person25",code:"251116",mainDept:"mainD 5",dept:"dept161",SubDept:"Sub003"},
    //     {name:"person26",code:"261117",mainDept:"mainD 5",dept:"dept161",SubDept:"Sub003"},
    //     {name:"person27",code:"271118",mainDept:"mainD 5",dept:"dept161",SubDept:"Sub003"},
    //     {name:"person28",code:"281119",mainDept:"mainD 5",dept:"dept161",SubDept:"Sub003"},
    //     {name:"person29",code:"291119",mainDept:"mainD 5",dept:"dept161",SubDept:"Sub003"}
    // ];

    // var items = Enumerable.From(personArray)
    //     .Let(function(e) {
    //         var roots = { '#': {}, mainDept: {}, dept: {}, SubDept: {} };
    //         e.ForEach(function(p) {
    //             roots['#'][p.mainDept] = '#';
    //             roots['mainDept'][p.dept] = p.mainDept;
    //             roots['dept'][p.SubDept] = p.dept;
    //             roots['SubDept'][p.name] = p.SubDept;
    //         });
    //         function makeNode(root) {
    //             return Enumerable.From(roots[root]).Select("{text: $.Key , id: $.Key, parentId: $.Value}");
    //         }
    //         return makeNode('#').Concat(makeNode('mainDept')).Concat(makeNode('dept')).Concat(makeNode('SubDept'));
    //     })
    //     .ToArray();

    // var items = [

    //     { "id": "hung 6", "text": "abc", "parentId": "hung 2" },
    //     { "id": "hung 7", "text": "abc", "parentId": "hung 6" },
    //     { "id": "hung 8", "text": "abc", "parentId": "hung 6" },
    //     { "id": "hung 1", "text": "abc", "parentId": "hung 2" },
    //     { "id": "hung 2", "text": "abc", "parentId": "#" },
    //     { "id": "hung 3", "text": "abc", "parentId": "hung 5" },
    //     { "id": "hung 4", "text": "abc", "parentId": "hung 2" },
    //     { "id": "hung 5", "text": "abc", "parentId": "#" }
    // ];

    var items = [

        {
            "text": "text 3",
            "id": "mainD 3",
            "parentId": "mainD 2"
        },
        {
            "text": "text 2",
            "id": "mainD 2",
            "parentId": "mainD 1"
        },
        {
            "text": "text 4",
            "id": "mainD 4",
            "parentId": "mainD 1"
        },
        {
            "text": "text 1",
            "id": "mainD 1",
            "parentId": "#"
        },
        {
            "text": "text 5",
            "id": "mainD 5",
            "parentId": "#"
        }
    ];

    var arr2 = [
        {
            "text": "text 3",
            "id": "mainD 3",
            "parentId": "mainD 2"
        },
        {
            "text": "text 2",
            "id": "mainD 2",
            "parentId": "mainD 1"
        },
        {
            "text": "text 4",
            "id": "mainD 4",
            "parentId": "mainD 1"
        },
        {
            "text": "text 1",
            "id": "mainD 1",
            "parentId": "#"
        },
        {
            "text": "text 5",
            "id": "mainD 5",
            "parentId": "#"
        },
        {
            "text": "mainD 1",
            "id": "mainD 1",
            "parentId": "#"
        },
        {
            "text": "mainD 2",
            "id": "mainD 2",
            "parentId": "#"
        },
        {
            "text": "mainD 3",
            "id": "mainD 3",
            "parentId": "#"
        },
        {
            "text": "mainD 4",
            "id": "mainD 4",
            "parentId": "#"
        },
        {
            "text": "mainD 5",
            "id": "mainD 5",
            "parentId": "#"
        },
        {
            "text": "dept1",
            "id": "dept1",
            "parentId": "mainD 1"
        },
        {
            "text": "dept2",
            "id": "dept2",
            "parentId": "mainD 1"
        },
        {
            "text": "dept3",
            "id": "dept3",
            "parentId": "mainD 1"
        },
        {
            "text": "dept4",
            "id": "dept4",
            "parentId": "mainD 1"
        },
        {
            "text": "dept12",
            "id": "dept12",
            "parentId": "mainD 2"
        },
        {
            "text": "dept13",
            "id": "dept13",
            "parentId": "mainD 2"
        },
        {
            "text": "dept131",
            "id": "dept131",
            "parentId": "mainD 2"
        },
        {
            "text": "dept132",
            "id": "dept132",
            "parentId": "mainD 3"
        },
        {
            "text": "dept141",
            "id": "dept141",
            "parentId": "mainD 4"
        },
        {
            "text": "dept151",
            "id": "dept151",
            "parentId": "mainD 4"
        },
        {
            "text": "dept161",
            "id": "dept161",
            "parentId": "mainD 5"
        },
        {
            "text": "Sub01",
            "id": "Sub01",
            "parentId": "dept1"
        },
        {
            "text": "Sub11",
            "id": "Sub11",
            "parentId": "dept1"
        },
        {
            "text": "Sub12",
            "id": "Sub12",
            "parentId": "dept2"
        },
        {
            "text": "Sub13",
            "id": "Sub13",
            "parentId": "dept2"
        },
        {
            "text": "Sub21",
            "id": "Sub21",
            "parentId": "dept3"
        },
        {
            "text": "Sub22",
            "id": "Sub22",
            "parentId": "dept4"
        },
        {
            "text": "Sub23",
            "id": "Sub23",
            "parentId": "dept12"
        },
        {
            "text": "Sub24",
            "id": "Sub24",
            "parentId": "dept12"
        },
        {
            "text": "Sub25",
            "id": "Sub25",
            "parentId": "dept13"
        },
        {
            "text": "Sub26",
            "id": "Sub26",
            "parentId": "dept131"
        },
        {
            "text": "Sub27",
            "id": "Sub27",
            "parentId": "dept132"
        },
        {
            "text": "Sub1",
            "id": "Sub1",
            "parentId": "dept141"
        },
        {
            "text": "Sub001",
            "id": "Sub001",
            "parentId": "dept141"
        },
        {
            "text": "Sub002",
            "id": "Sub002",
            "parentId": "dept151"
        },
        {
            "text": "Sub003",
            "id": "Sub003",
            "parentId": "dept161"
        },
        {
            "text": "person1",
            "id": "person1",
            "parentId": "Sub01"
        },
        {
            "text": "person2",
            "id": "person2",
            "parentId": "Sub11"
        },
        {
            "text": "person3",
            "id": "person3",
            "parentId": "Sub12"
        },
        {
            "text": "person4",
            "id": "person4",
            "parentId": "Sub12"
        },
        {
            "text": "person5",
            "id": "person5",
            "parentId": "Sub13"
        },
        {
            "text": "person6",
            "id": "person6",
            "parentId": "Sub21"
        },
        {
            "text": "person7",
            "id": "person7",
            "parentId": "Sub21"
        },
        {
            "text": "person8",
            "id": "person8",
            "parentId": "Sub22"
        },
        {
            "text": "person9",
            "id": "person9",
            "parentId": "Sub23"
        },
        {
            "text": "person10",
            "id": "person10",
            "parentId": "Sub24"
        },
        {
            "text": "person12",
            "id": "person12",
            "parentId": "Sub25"
        },
        {
            "text": "person13",
            "id": "person13",
            "parentId": "Sub26"
        },
        {
            "text": "person14",
            "id": "person14",
            "parentId": "Sub27"
        },
        {
            "text": "person15",
            "id": "person15",
            "parentId": "Sub27"
        },
        {
            "text": "person16",
            "id": "person16",
            "parentId": "Sub1"
        },
        {
            "text": "person17",
            "id": "person17",
            "parentId": "Sub1"
        },
        {
            "text": "person18",
            "id": "person18",
            "parentId": "Sub1"
        },
        {
            "text": "person21",
            "id": "person21",
            "parentId": "Sub1"
        },
        {
            "text": "person22",
            "id": "person22",
            "parentId": "Sub001"
        },
        {
            "text": "person23",
            "id": "person23",
            "parentId": "Sub002"
        },
        {
            "text": "person24",
            "id": "person24",
            "parentId": "Sub003"
        },
        {
            "text": "person25",
            "id": "person25",
            "parentId": "Sub003"
        },
        {
            "text": "person26",
            "id": "person26",
            "parentId": "Sub003"
        },
        {
            "text": "person27",
            "id": "person27",
            "parentId": "Sub003"
        },
        {
            "text": "person28",
            "id": "person28",
            "parentId": "Sub003"
        },
        {
            "text": "person29",
            "id": "person29",
            "parentId": "Sub003"
        }
    ];

    function buildHierarchy(arry) {

        var roots = [], children = new Map();

        // find the top level nodes and hash the children based on parent
        for (var i = 0, len = arry.length; i < len; ++i) {
            var item = arry[i],
                p = item.parentId;
            var target;
            //target = !p ? roots : (children[p] || (children[p] = []));
            if (p === "#") {
                target = roots;
            } else {
                if (!children.get(p)) {
                    children.set(p, []);
                }

                target = children.get(p);
            }

            //target.push({ value: item });
            target.push(item);
        }

        // function to recursively build the tree
        var findChildren = function (parent) {
            if (children.get(parent.id)) {
                parent.children = children.get(parent.id);

                parent.children.forEach(function (item, key, mapObj) {
                    findChildren(item);
                });
                /*  for (var i = 0, len = parent.children.length; i < len; ++i) {
                     findChildren(parent.children[i]);
                 } */
            }
        };

        // enumerate through to handle the case where there are multiple roots
        for (var i = 0, leng = roots.length; i < leng; ++i) {
            findChildren(roots[i]);
        }

        return roots;
    }

    console.log(JSON.stringify(buildHierarchy(arr2)));
    //alert(JSON.stringify(items).toString());
    //document.write('<pre>' + JSON.stringify(data, 0, 4) + '</pre>');

    //alert(JSON.stringify(items));
    // $("#root").html(items);
    //$('#root').jstree({core:{data:items}});


}