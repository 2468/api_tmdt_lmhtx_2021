var p = require('../package.json');
var version = p.version.split('.').shift();
module.exports = {
  restApiRoot: '/api' + (version > 0 ? '/v' + version : ''),
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 3200,
  FTP: {
     "config": {
          //"host": "cdn.smartlifevn.com",
          "host":"103.101.162.78",
          "port": 21,
          "user": "ftp_media",
          "password": "sml@!234",
          "secure": false
      },
      "rootPath": "cdn/upload/ecommerce",
      "cdn": "http://cdn.smartlifevn.com/upload/ecommerce/"   
        // "host": "171.244.49.164",
        // "port": 21,
        // "user": "admin",
        // "password": "clmm6789",
        // "secure": false
    // },
    // "rootPath": "images",
    // "cdn": "http://171.244.49.164/"
    //"host": ["103.101.162.65:2018"],
    //"host": ["118.70.118.130:2019"],
  },
  ElasticSearch: {
    "host": ["103.101.162.65:2018"],
    "log": "trace",
      "keepAlive": true,
      "maxRetries": 10,
      "maxSockets": 10,
      "minSockets": 10,
      "maxKeepAliveRequests": 0,
      "maxKeepAliveTime": 300000,
      "sniffOnStart": false 
  },
  SMS: {
      "host": "http://api.esms.vn/MainService.svc/xml/",
      "IRIS": {
          "host": "https://smsapi2.irismedia.vn/",
          "path": {
              "login": "oauth2/token",
              "sendSMS": "api/Sms"
          },
          "user": "sms_smartlife",
          "password": "sM@r1L!fe"
      }
  },
  Facebook: {
      "url": "https://graph.facebook.com/v2.12/"
  },
  SmartLifeTradeVerify: {
     
      "url": "http://api1.smartlifevn/api/user/passport?dev=1"
  },
  SMLSellerCenter: {
      "url": "http://chosachhathanh.com:4300"
  },
  LalamoveShiper:{
    host: 'https://sandbox-rest.lalamove.com',
    key: '2874276892104bd99f0c8db0ce475071',      // Obtained from Lalamove Sales Team
    secret: 'MCsCAQACBQCeQ4P7AgMBAAECBH21b6ECAwDR4wIDAMEJAgJ/eQICQukCAkkH',   // Obtained from Lalamove Sales Team
    country: 'VN_HAN'
  },
  MQTT:{
    port:1883,
    host:"103.101.162.65",
   // clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8), 
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
      
    //   host:"171.244.49.164",
    //   hostname:"171.244.49.164",
    //   keepalive:60,
    //   path:"",
    //   port:1883,
    //   protocol:"ws",
    //   protocolId:"ws",
    //   protocolVersion:4
      
  }
};