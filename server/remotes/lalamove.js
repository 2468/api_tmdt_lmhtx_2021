
'use strict';
/**
 * Lalamove ship connection
 * Author: Thiep Wong
 * Created: 12/11/2018
 * Connect to Lalamove shipping system 
 */
const http = require("request");
const app = require('../../server/server');
const LalamoveConfig = app.get('LalamoveShiper');
const Result = require('../middlewares/ResultHandle');
const Msg = require('../middlewares/message_constant');
var uuid = require('uuid');
const lalamove = require('lalamove-js')({
    host: LalamoveConfig.host,
    key: LalamoveConfig.key,      // Obtained from Lalamove Sales Team
    secret: LalamoveConfig.secret,   // Obtained from Lalamove Sales Team
    country: LalamoveConfig.country
})

module.exports = (Lalamove) => {


    //#region  Quotation Shipper
    Lalamove.remoteMethod('quotation', {
        description: '@Thiep Wong, create a quotation for shipping bill',
        http: {
            verb: 'POST'
        },
        accepts: [
            {
                arg: 'quotationData',
                type: 'object',
                http: {
                    source: 'body'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Lalamove.quotation = (quotationData, cb) => {
        let result = new Result();
        let _fromAdd = quotationData.fromAddress || '';
        let _toAdd = quotationData.toAddress || '';
        let _toContactName = quotationData.toContactName || null;
        let _toContactPhone = quotationData.toContactPhone || null;
        let _remarks = quotationData.remarks || null;
        if (!_fromAdd || !_toAdd || !_toContactName || !_toContactPhone) {
            result.setError(Msg.DATA_NOT_FOUND);
            return cb(null, result);
        }
        var body = {
            'serviceType': 'MOTORCYCLE',
            'scheduleAt': new Date(new Date().getTime() + 10 * 60000).toISOString(), //'2018-11-17T05:35:00:00Z', // Note: This is in UTC Timezone
            //  'specialRequests': [],
            'requesterContact': {
                'name': _toContactName,
                'phone': _toContactPhone
            },
            'stops': [
                {
                    // 'location': {'lat': '20.997696', 'lng': '105.841332'},
                    'addresses': {
                        'en_VN': {
                            'displayString': _fromAdd, //'Ngách 22 ngõ 61 lạc trung, vĩnh tuy, hai bà trưng, hà nội',
                            'country': 'VN_HAN'
                        }
                    }
                },
                {
                    // 'location': {'lat': '21.027381', 'lng': '105.896162'},
                    'addresses': {
                        'en_VN': {
                            'displayString': _toAdd, // 'Số 18 Lê Thanh Nghị, Hai Bà Trưng, Hà Nội',
                            'country': 'VN_HAN'
                        }
                    }
                }
            ],
            'deliveries': [
                {
                    'toStop': 1,
                    'toContact': {
                        'name': _toContactName,
                        'phone': _toContactPhone
                    },
                    'remarks': _remarks
                }
            ]
        }

        lalamove.quotation(body).then(res => {
            if (res.status != 200) {
                result.setError(JSON.parse(res.text,Msg.SHIP_QUOTATION_ERROR));
                return cb(null, result);
            }

            result.setSuccess(JSON.parse(res.text));
            cb(null, result);
            //   console.log(res.text);
            //   cb(null, JSON.parse(res.text));
        }, err => { 
            // console.log(err)
            // let _res = err?JSON.parse(err.response):null;
            result.setError({code: err.status, message: JSON.parse(err.response.text).message});
            return cb(null, result);
        })

        // http.post(LA.host);
    }

    //#endregion Quotation Shipper

    //#region  Place Order
    Lalamove.remoteMethod('order', {
        description: '@Thiep Wong, created a order to shipper',
        http: {
            verb: 'POST',
            path: '/order'
        },
        accepts: [
            {
                arg: 'orderData',
                type: 'object',
                http: {
                    source: 'body'
                }
            },
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    Lalamove.order = (orderData,ctx, cb) => { 

               
        let result = new Result();

        let _requestName = orderData.requestName || null;
        let _requestPhone = orderData.requestPhone || null;
        let _fromAdd = orderData.fromAddress || null;
        let _toAdd = orderData.toAddress || null;
        let _toContactName = orderData.toContactName || null;
        let _toContactPhone = orderData.toContactPhone || null;
        let _remarks = orderData.remarks || null;
        let _quotationPrice = orderData.quotationPrice || null;  
        let _orderID = orderData.orderID || null;  
        if (!_requestName || !_requestPhone || !_fromAdd || !_toAdd || !_toContactName || !_toContactPhone || !_quotationPrice) {
            result.setError(Msg.DATA_NOT_FOUND);
            return cb(null, result);
        }
  
        const request = async() => { 

            let Passport = Lalamove.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) { 
                result.setError(Msg.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            let body = {
                callerSideCustomerOrderId: _orderID,
                scheduleAt: new Date(new Date().getTime() + 10 * 60000).toISOString(),
                serviceType: 'MOTORCYCLE',
                // specialRequests: [] ["LALABAG", "HELP_BUY"],
                requesterContact: {
                    name: _requestName,
                    phone: _requestPhone,
                },
                stops: [
                    {
                        addresses: {
                            en_VN: {
                                displayString: _fromAdd,
                                country: "VN_HAN",
                            },
                        },
                    },
                    {
                        addresses: {
                            en_VN: {
                                displayString: _toAdd,
                                country: "VN_HAN",
                            },
                        },
                    },
                ],
                deliveries: [
                    {
                        toStop: 1,
                        toContact: {
                            name: _toContactName,
                            phone: _toContactPhone,
                        },
                        remarks: _remarks,
                    },
                ],
                quotedTotalFee: {
                    amount: _quotationPrice,
                    currency: "VND",
                }
            }
    
            lalamove.postOrder(body).then(res => {
                if (res.status != 200) {
                    result.setError(JSON.parse(res.text));
                    return cb(null, result);
                }
    
                result.setSuccess(JSON.parse(res.text));
                cb(null, result);
    
            }, err => {
                result.setError(err);
                return cb(null, result);
            });
        }

       request().catch(err=>{
        result.setError(err) ;
        return cb(null,result);
       })

    }

    //#endregion Place Order

    //#region  Cancel Order
    Lalamove.remoteMethod('CancelOrder',{
        description:'@Thiep Wong, cancel an order',
        http:{
            verb:'POST',
            path:'/order/:customerOrderId/cancel'
        },
        accepts:[
            {
                arg:'customerOrderId',
                type:'string'
            },
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns:{
            arg:'result',
            type:'string',
            root:true
        }
    })
    
    Lalamove.CancelOrder = (customerOrderId,ctx,cb) =>{
        let result = new Result();
        if(!customerOrderId) {
            result.setError(Msg.DATA_NOT_FOUND);
            return cb(null,result);
        }

        const request = async () =>{

            let Passport = Lalamove.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) { 
                result.setError(Msg.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            lalamove.cancelOrder(customerOrderId).then(data=>{
                if(data.status!= 200) {
                    result.setError(Msg.SHIP_CANCEL_ERROR);
                    return cb(null,result);
                } 
                result.setSuccess(Msg.SHIP_CANCEL_SUCCESS);
                cb(null,result);
    
            },err=>{
                result.setError(Msg.SHIP_CANCEL_ERROR);
                return cb(null,result);
            })

        }

        request().catch(err=>{
            result.setError(err);
            return cb(null,result);
        })


    }

    //#endregion Cancel Order

    //#region  Get Ship Order Details
    Lalamove.remoteMethod('GetOrderDetail',{
        description:'@Thiep Wong, get the detail of shipping order',
        http:{
            verb:'POST',
            path:'/order/:customerOrderId'
        },
        accepts:[
            {
                arg:'customerOrderId',
                type:'string'
            }
        ],
        returns:{
            arg:'result',
            type:'string',
            root:true
        }
    })

    Lalamove.GetOrderDetail = (customerOrderId,cb)=>{


        let result = new Result();
        if(!customerOrderId) {
            result.setError(Msg.DATA_NOT_FOUND);
            return cb(null,result);
        }

        lalamove.getOrderStatus(customerOrderId).then(data=>{

            if(data.status!=200) {
                result.setError(JSON.parse(data.text));
                return cb(null,result);
            }

            result.setSuccess( JSON.parse(data.text));
            cb(null,result);
            
        },err=>{
            result.setError(JSON.parse(err.text));
            return cb(null,result);
        })

    }

    //#endregion
}

/**
 * Test doc
 */

 // Quotation price
// {
//     "fromAddress":"hai ba trung, ha noi",
//     "toAddress":"Hoang cau, dong da, ha noi",
//     "toContactName":"Hoang van Thiep",
//     "toContactPhone":"0983851116",
//     "remarks":"Chuyen ngay va luon nhe!" 
//     }

// Place an order
// {
//     "requestName":"Hoang Cai",
//     "requestPhone":"+84983851116",
//     "fromAddress":"hai ba trung, ha noi",
//     "toAddress":"Hoang cau, dong da, ha noi",
//     "toContactName":"Hoang van Thiep",
//     "toContactPhone":"+84983851116",
//     "remarks":"Chuyen ngay va luon nhe!",
//      "quotationPrice":"25000" 
//     }


// customerSide order code: 6cfd7669-4c02-4c67-816e-6ac8e4801f74
// "customerOrderId": "c483517b-f39a-11e8-a723-06bff2d87e1b",
//     "orderRef": "5091"