/**
 * Relation Fetch
 * using to join details to master 
 * Author: Thiep Wong
 * Created: 13/05/2018
 * @param {*} Model 
 * @param {*} key 
 */
module.exports = function(MModel){
    if(!MModel)
    {
        return 0;
    }
 
    var prototype = Object.getPrototypeOf(MModel);

    prototype.FetchDetail = function(Model,key){

        if(!MModel[key]) {
            return   MModel[key] = Model;
        }

        if(MModel[key] && Array.isArray(MModel[key]))
        {
            Array.prototype.push.apply(MModel[key],Model);
        }
        else 
        {
            Model[key] = Model;
        }
      
    }
    
}