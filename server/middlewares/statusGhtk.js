module.exports = {
    HUY_DON_HANG: {
        status: -1,
        message: "Hủy đơn hàng"
    },
    CHUA_TIEP_NHAN: {
        status: 1,
        message: "Chưa tiếp nhận"
    },
    DA_TIEP_NHAN: {
        status: 2,
        message: "Đã tiếp nhận"
    }
}