var p = require('../../package.json');
/*
module.exports = function()
{
    this.success;
    this.errors;
    this.warning;
    this.api_version = p.version;
    this.server_time = new Date();
    this.results = {
      success: "this.success",
      errors: "this.errors",
      warning: this.warning,
      api_version: this.api_version,
      server_time:this.server_time
    };
}
*/
var Results =
    {
        success: null,
        errors: null,
        warning: null,
        api_version: p.version,
        server_time: new Date()
    }
    
module.exports = Results;
