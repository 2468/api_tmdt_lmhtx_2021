module.exports = function () {
  return function logError(err, req, res, next) {
    res.status(200).json(
      {
        errors: {
          Err_Code: err,
          Err_Class: "Loi: " + err.name,
          Err_Message: err.message
        },
        success: {
        },
        api_version: "1.0.0.1",
        server_time: new Date()
      }
    );
    return next();
  };
};