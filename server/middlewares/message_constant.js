/**
 * Message Constaint
 * Author: LuongPD
 * Last update: 12.3.2018 14:18
 */

module.exports = {

    //#region Success Code
    API_SUCCESSED: {
        code: "S0001",
        name: "API_SUCCESSED",
        message: "Đã xử lý thành công"
    },
    OTP_FOUND: {
        code: "S0002",
        name: "OTP_FOUND",
        message: "Mã xác thực OTP chính xác"
    },
    ORDER_SUCCESSED: {
        code: "S0003",
        name: "ORDER_SUCCESSED",
        message: "Đơn hàng đã đặt thành công. Thông tin đơn hàng sẽ được kiểm tra & xác nhận."
    },
    ITEM_DELETED:{
        code:'S0003',
        name:'ITEM_DELETED',
        message:'Đã xóa thành công bản ghi'
    },
    ITEM_EDITED:{
        code:'S0004',
        name:'ITEM_EDITED',
        message:'Đã sửa thành công bản ghi'
    },
    PASSWORD_RESET_SUCCESS:{
        code:'S0005',
        name:'PASSWORD_RESET_SUCCESS',
        message:'Mật khẩu thay đổi thành công, vui lòng đăng nhập lại!'
    },

    OTP_SENT:{
        code:'S0006',
        name:'OTP_SENT',
        message:'Mã OTP đã được gửi đi thành công!'
    },

    ADDED_SUCCESS:{
        code:'S0007',
        name:'ADDED_SUCCESS',
        message:'Thêm dữ liệu thành công!'
    },

    LOGOUT_SUCCESS:{
        code:'S0008',
        name:'LOGOUT_SUCCESS',
        message:'Đã đăng xuất thành công!'
    },
    SHIP_ORDER_SUCCESS:{
        code:'S0009',
        name:'SHIP_ORDER_SUCCESS',
        message:'Đã đặt vận đơn thành công!'
    },
    SHIP_CANCEL_SUCCESS:{
        code:'S0010',
        name:'SHIP_CANCEL_SUCCESS',
        message:'Đã hủy thành công vận đơn!'
    },

    //#endregion

    //#region Warning Code
    MOBILE_UNVERIFY: {
        code: "W0001",
        name: "MOBILE_UNVERIFY",
        message: "Số điện thoại chưa được xác thực, vui lòng xác thực số điện thoại"
    },
    //#endregion

    //#region Error Code
    API_TOKEN_INVALID: {
        code: "E0001",
        name: "API_TOKEN_INVALID",
        message: "Token API không đúng, không được phép thực hiện chức năng này"
    },
    LOGIN_FAILED: {
        code: "E0002",
        name: "LOGIN_FAILED",
        message: "Đăng nhập không thành công, vui lòng kiểm tra lại"
    },
    SIGNUP_FAILED: {
        code: "E0003",
        name: "SIGNUP_FAILED",
        message: "Đăng ký không thành công, vui lòng kiểm tra lại thông tin"
    },
    EMAIL_INVALID: {
        code: "E0004",
        name: "EMAIL_INVALID",
        message: "Email không đúng định dạng, vui lòng kiểm tra lại"
    },
    OTP_NOT_FOUND: {
        code: "E0005",
        name: "OTP_NOT_FOUND",
        message: "Không có mã OTP này, vui lòng kiểm tra lại",
    },
    OTP_EXPIRED: {
        code: "E0006",
        name: "OTP_EXPIRED",
        message: "Mã OTP này đã hết hạn, vui lòng gửi lại mã OTP khác",
    },
    USER_NOT_FOUND: {
        code: "E0007",
        name: "USER_NOT_FOUND",
        message: "Tài khoản này không tồn tại, vui lòng đăng ký tài khoản"
    },
    PASSWORD_NOT_MATCH: {
        code: "E0008",
        name: "PASSWORD_NOT_MATCH",
        message: "Mật khẩu không đúng, vui lòng nhập lại"
    },
    ADD_ITEM_FAILED: {
        code: "E0007",
        name: "ADD_ITEM_FAILED",
        message: "Thêm dữ liệu không thành công, vui lòng kiểm tra lại thông tin"
    },
    ITEM_NOT_FOUND: {
        code: "E0008",
        name: "ITEM_NOT_FOUND",
        message: "Không tìm thấy dữ liệu"
    },
    PRODUCT_STORE_DECREASE_ITEM_NOT_ENOUGH: {
        code: "E0009",
        name: "PRODUCT_STORE_DECREASE_ITEM_NOT_ENOUGH",
        message: "Số lượng hàng trong kho không đủ để giảm, vui lòng kiểm tra lại thông tin"
    },
    PRODUCT_STORE_BOOKED_ITEM_SMALLER_THAN_ZERO: {
        code: "E0010",
        name: "PRODUCT_STORE_BOOKED_ITEM_SMALLER_THAN_ZERO",
        message: "Số lượng đặt hàng phải lớn hơn 0, vui lòng kiểm tra lại thông tin"
    },
    PRODUCT_STORE_AVAILABE_ITEM_NOT_ENOUGH_TO_BOOK: {
        code: "E0011",
        name: "PRODUCT_STORE_AVAILABE_ITEM_NOT_ENOUGH_TO_BOOK",
        message: "Số lượng sản phẩm trong kho không đủ để đặt hàng, vui lòng kiểm tra lại thông tin"
    },
    PRODUCT_STORE_BOOKED_ITEM_NOT_ENOUGH_TO_SHIP: {
        code: "E0012",
        name: "PRODUCT_STORE_BOOKED_ITEM_NOT_ENOUGH_TO_SHIP",
        message: "Số lượng sản phẩm đặt hàng trong kho không đủ để vận chuyển, vui lòng kiểm tra lại thông tin"
    },
    MEMBER_MOBILE_INVALID: {
        code: "E0013",
        name: "MEMBER_MOBILE_INVALID",
        message: "Số điện thoại không tồn tại, vui lòng kiểm tra lại thông tin"
    },
    OTP_CREATE_ERR: {
        code: "E0014",
        name: "OTP_CREATE_ERR",
        message: "Không thể tạo được mã OTP, vui lòng kiểm tra lại thông tin"
    },
    SELLER_INFO_INVALID: {
        code: "E0015",
        name: "SELLER_INFO_INVALID",
        message: "Thông tin đăng ký không đầy đủ, vui lòng kiểm tra lại thông tin"
    },
    SEND_EMAIL_FALSE: {
        code: "E0016",
        name: "SEND_EMAIL_FALSE",
        message: "Không thể gửi thư đến email đã đăng ký, vui lòng kiểm tra lại thông tin"
    },
    EMAIL_VERIFYCODE_INVALID: {
        code: "E0017",
        name: "EMAIL_VERIFYCODE_INVALID",
        message: "Mã kích hoạt email không đúng, vui lòng kiểm tra lại thông tin"
    },
    UPDATE_ITEM_FAILED: {
        code: "E0018",
        name: "UPDATE_ITEM_FAILED",
        message: "Cập nhật dữ liệu không thành công, vui lòng kiểm tra lại thông tin"
    },
    MEMBER_INFO_INVALID: {
        code: "E0019",
        name: "MEMBER_INFO_INVALID",
        message: "Vui lòng nhập thông tin tài khoản, vui lòng kiểm tra lại thông tin"
    },
    PRODUCT_GET_LIST_ARGUMENTS_INVALID: {
        code: "E0020",
        name: "PRODUCT_GET_LIST_ARGUMENTS_INVALID",
        message: "Cần ít nhất một tham số có giá trị trong các tham số productName, brandIds, categoryId"
    },
    PRODUCT_PRICE_OVERLAP: {
        code: "E0021",
        name: "PRODUCT_PRICE_OVERLAP",
        message: "Khoảng thời gian sử dụng giá không được trùng nhau, vui lòng kiểm tra lại thông tin"
    },
    SYSTEM_ERROR: {
        code: "E0022",
        name: "SYSTEM_ERROR",
        message: "Hệ thống bị lỗi"
    },
    DATA_NOT_FOUND: {
        code: "E0023",
        name: "DATA_NOT_FOUND",
        message: "Dữ liệu không tồn tại [productId:'{0}', ordersId:'{1}', memberId:'{2}']"
    },
    VALIDATION_ERROR: {
        code: "E0024",
        name: "VALIDATION_ERROR",
        message: ""
    },
    ACCESS_TOKEN_INVALID: {
        code: 'E0025',
        name: 'ACCESS_TOKEN_INVALID',
        //message: 'Lỗi không được quyền truy cập chức năng này!'
        message: 'Phiên giao dịch đã hết hạn. Bạn cần phải đăng nhập'
    },
    FACEBOOK_TOKEN_INVALID: {
        code: 'E0026',
        name: 'FACEBOOK_TOKEN_INVALID',
        message: 'Lỗi đăng nhập từ facebook!'
    },
    SELLER_STORE_INFO_INVALID: {
        code: 'E0027',
        name: 'SELLER_STORE_INFO_INVALID',
        message: 'Thông tin kho hàng không đầy đủ!'
    },
    DELETE_ITEM_FAILS: {
        code: 'E0028',
        name: 'DELETE_ITEM_FAILS',
        message: 'Không xóa được dữ liệu này!'
    },
    EDIT_ITEM_FAILS: {
        code: 'E0029',
        name: 'EDIT_ITEM_FAILS',
        message: 'Không sửa được dữ liệu này!'
    },
    IMAGE_TYPE_INVALID: {
        code: 'E0030',
        name: 'IMAGE_TYPE_INVALID',
        message: 'Định dạng ảnh không đúng, vui lòng chọn đúng định dạng các ảnh được hỗ trợ!'
    },
    ID_INVALID:{
        code: 'E0030',
        name: 'ID_INVALID',
        message: 'Id không đúng, vui lòng kiểm tra lại!'
    },
    ORDERS_CANNOT_CANCEL: {
        code: "E0031",
        name: "ORDERS_CANNOT_CANCEL",
        message: "Đơn hàng đã được xác nhận, bạn không thể hủy."
    },
    ORDERS_ORDER_ERROR: {
        code: "E0032",
        name: "ORDERS_PRE_ORDER_ERROR",
        message: "Đơn hàng gặp sự cố, vui lòng kiểm tra lại."
    },
    NO_PERMISSTION_ERROR: {
        code: "E0033",
        name: "NO_PERMISSTION_ERROR",
        message: "Xin lỗi, bạn không có quyền thực hiện hành động này."
    },
    CANNOT_DELETE_DEFAULT_ADDRESS_ERROR: {
        code: "E0033",
        name: "CANNOT_DELETE_DEFAULT_ADDRESS_ERROR",
        message: "Xin lỗi, bạn không thể xoá địa chỉ mặc định."
    },
    SELLER_NO_ACTIVE_ERROR: {
        code: "E0034",
        name: "SELLER_NO_ACTIVE_ERROR",
        message: "Xin lỗi, shop này chưa được kích hoạt."
    },
    MEMBER_FOUND: {
        code: "E0035",
        name: "MEMBER_FOUND",
        message: "Tài khoản này đã tồn tại. Vui lòng đăng nhập để sử dụng dịch vụ!"
    },
    FACEBOOK_MOBILE_LINKED :{
        code: "E0036",
        name: "FACEBOOK_MOBILE_LINKED",
        message: "Số điện thoại này đã được kết nối với một tài khoản facebook khác, vui lòng đăng nhập bằng số điện thoại và mật khẩu để quản lý facebook đăng nhập"
    },
    REFRESH_TOKEN_EXPIRED:{
        code:'E0037',
        name:'REFRESH_TOKEN_EXPIRED',
        message:'Refresh token da het han, vui long dang nhap lai'
    },
    SHIP_CANCEL_ERROR: {
        code:'E0038',
        name:'SHIP_CANCEL_ERROR',
        message:'Hủy vận đơn không thành công do không tồn tại đơn hàng hoặc do đã bị hủy trước đó!'
    },
    SHIP_ORDER_ERROR :{
        code:'E0039',
        name:'SHIP_CANCEL_SUCCESS',
        message:'Đặt ship không thành công, vui lòng kiểm tra lại đơn hàng!'
    },
    SHIP_QUOTATION_ERROR:{
        code:'E0040',
        name:'SHIP_QUOTATION_ERROR',
        message:'Báo giá vận đơn không thành công, vui lòng kiểm tra lại đơn hàng!'
    },
    ARG_NOT_FOUND:{
        code:'E0041',
        name:'ARG_NOT_FOUND',
        message:'Vui lòng nhập tham số đầu vào!'
    },
    PRO_WEIGHT_ERR:{
        code:'E0042',
        name:'PRO_WEIGHT_ERR',
        message:'Vui lòng nhập thông tin khối lượng sản phẩm!'
    }
    ,
    DELETE_FAILS: {
        code: 'E0043',
        name: 'DELETE_ITEM_FAILS',
        message: 'Dữ liệu đang được sử dụng. Không được xóa!'
    },
    //#endregion
}