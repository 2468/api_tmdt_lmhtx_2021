/**
 * Search Engine for elasticsearch
 * Author: Thiep Wong
 * Created 14/05/2018
 */

var elasticsearch = require('elasticsearch');
var app = require('../../server/server');
var client = new elasticsearch.Client(app.get('ElasticSearch'));
var ApplicationError = require('../middlewares/application-error');

module.exports = function () {
  var prototype = Object.getPrototypeOf(this);

  /**
   * Search the models by keywords
   * @param {*} index 
   * @param {*} model 
   * @param {*} keyword 
   */
  prototype.SearchSuggestion = function (index, model, platformCode, keyword) {

    var promise = new Promise((resole, reject) => {    

      client.search({
        index: index,
        // type:model,
        body: {
          size: 10,
          from: 0,
          _source: {
            includes: [
              "productName"
            ]
          },
          sort: [
            {
              "productName.keyword": "asc"
            }
          ],
          query: {
            bool: {
              must: [
                {
                  match: {
                    productName: keyword
                  }
                }
              ], 
              filter:
              {
                bool: {
                  must: [
                    { terms : {"status" : [2]}},
                    { terms : {"productSellers.seller.sellerPlatform.status" : [1,2]}},
                    { term : {"productSellers.productSellerPlatform.displayOnShop" : 1}} ,
                    { term : {"productSellers.seller.sellerPlatform.platformCode.keyword" : platformCode}},
                    { term : {"productSellers.productSellerPlatform.platformCode.keyword" : platformCode}}
                  ]
                }
              }
            }
          }
        }
      }).then(function (resp) {

        var hits = resp.hits.hits;
        resole(hits);

      }, function (err) {
        reject('loi he thong');
      });
    });

    return promise;

  }

  prototype.SearchPaging = function (index, model, platformCode, keyword, sellerId, brandIds, categoryIds, sellerIds, fromPrice, toPrice, pageIndex, pageSize, pageOrder) {

    if (pageSize) {
      if (pageSize <= 0) {
        pageSize = 10;
      }
    } else {
      pageSize = 10;
    }

    if (pageIndex) {
      if (pageIndex < 1) {
        pageIndex = 1;
      }
    } else {
      pageIndex = 1;
    }
 //  { "terms" : {"productSellers.seller.status" :[2]}},  
 // { "terms" : {"productSellers.seller.status" :[1,2]}}, 
    let matchBody = [];
    let filterBody = [	
                        { "terms" : {"status" : [2]}},     
                       
                        { "terms" : {"productSellers.seller.sellerPlatform.status" : [1,2]}},                                            
                        { "term" : {"productSellers.productSellerPlatform.displayOnShop" : 1}} ,
                        { "term" : {"productSellers.seller.sellerPlatform.platformCode.keyword" : platformCode}},
                        { "term" : {"productSellers.productSellerPlatform.platformCode.keyword" : platformCode}}
                      ];

    let sortBody = [];

    if (keyword && keyword !== "undefined" && /\S/.test(keyword)) {
      matchBody.push({ match: { "productName": keyword } });
    }

    if (sellerId > 0) {
      filterBody.push({ term: { "productSellers.sellerId": sellerId } });
    }

    if (brandIds && brandIds.length > 0) {
      filterBody.push({ terms: { "brandId": brandIds } });
    }

    if (categoryIds && categoryIds.length > 0) {
      filterBody.push({ terms: { "categoryId": categoryIds } });
    }

    if (sellerIds && sellerIds.length > 0) {
      filterBody.push({ terms: { "productSellers.sellerId": sellerIds } });
    }

    if (fromPrice || toPrice) {

      let priceFilterBody = {};
      if (fromPrice && toPrice) {
        priceFilterBody = { gte: fromPrice, lte: toPrice }
      } else if (fromPrice) {
        priceFilterBody = { gte: fromPrice }
      } else if (toPrice) {
        priceFilterBody = { lte: toPrice }
      }

      filterBody.push({ range: { "productSellers.basePrice": priceFilterBody } });
    }

    if (pageOrder && pageOrder.length > 0) {
      for (let i = 0; i < pageOrder.length; i++) {
        let sortArr = pageOrder[i].match(/[^\s-]+-?/g);

        if (sortArr.length == 2) {
          let sortName = sortArr[0];
          let direction = sortArr[1];

          if ("id" === sortName.toLowerCase()) {

            if ("desc" === direction.toLowerCase()) {
              direction = "desc";
            } else {
              direction = "asc";
            }

            sortBody.push({ "id": direction })

          } else  if ("brandid" === sortName.toLowerCase()) {

            if ("desc" === direction.toLowerCase()) {
              direction = "desc";
            } else {
              direction = "asc";
            }

            sortBody.push({ "brandId": direction })

          } else if ("productname" === sortName.toLowerCase()) {

            if ("desc" === direction.toLowerCase()) {
              direction = "desc";
            } else {
              direction = "asc";
            }

            sortBody.push({ "productName.keyword": direction })

          } else if ("categoryid" === sortName.toLowerCase()) {

            if ("desc" === direction.toLowerCase()) {
              direction = "desc";
            } else {
              direction = "asc";
            }

            sortBody.push({ "categoryId": direction })
          }
          else if ("baseprice" === sortName.toLowerCase()) {

            if ("desc" === direction.toLowerCase()) {
              direction = "desc";
            } else {
              direction = "asc";
            }

            sortBody.push({ "productSellers.basePrice": direction })
          }
        }
      }
    } else {
        sortBody.push({ "id": "desc" })
    }

    let promise = new Promise((resole, reject) => {

      client.search({
        index: index,
        // type:model,
        body: {
          size: pageSize,
          from: (pageIndex - 1) * pageSize,
          _source: {
            includes: [
              "id",
              "productName"
            ]
          },
          sort: sortBody,
          query: {
            bool: {
              must: matchBody,
              filter:
              {
                bool: {
                  must: filterBody
                }
              }
            }
          }
        }
      }).then(function (resp) {

        //   var hits = resp.hits.hits;
        resole(resp);

      }, function (err) {
        reject('loi he thong');
      });
    });

    return promise;

  }

  prototype.SearchAggreation = function (index, model, platformCode, keyword, sellerUuid, categoryIds) {

    let matchBody = [];
    let filterBody = [	{ "terms" : {"status" : [2]}},
                        { "terms" : {"productSellers.seller.sellerPlatform.status" : [1,2]}},
                        { "term" : {"productSellers.productSellerPlatform.displayOnShop" : 1}} ,
                        { "term" : {"productSellers.seller.sellerPlatform.platformCode.keyword" : platformCode}},
                        { "term" : {"productSellers.productSellerPlatform.platformCode.keyword" : platformCode}}
                      ];

    if (keyword && keyword !== "undefined" && /\S/.test(keyword)) {
      matchBody.push({ match: { "productName": keyword } });
    }

    if (categoryIds && categoryIds.length > 0) {
      filterBody.push({ terms: { "categoryId": categoryIds } });
    }

    if (sellerUuid && sellerUuid !== "undefined" && /\S/.test(sellerUuid)) {
      filterBody.push({ term: { "productSellers.seller.uuid.keyword": sellerUuid } });
    }
    var promise = new Promise((resole, reject) => {
      client.search({
        index: index,
        // type:model,
        body: {
          size: 0,
          query: {
            bool: {
              must: matchBody,
              filter: filterBody
            }
          },
          aggs: {
            group_by_seller: {
              terms: {
                field: "productSellers.sellerId"
              }
            },
            group_by_brand: {
              terms: {
                field: "brandId"
              }
            },
            group_by_category: {
              terms: {
                field: "categoryId"
              }
            }
          }
        }
      }).then(function (resp) {

        //   var hits = resp.hits.hits;
        resole(resp);

      }, function (err) {
        reject('loi he thong');
      });
    });

    return promise;

  }

  /**
   * Update a document
   * @param {*} index 
   * @param {*} model 
   */
  prototype.UpdateDocument = async function (index, model) {
    let isExist = await client.indices.exists({
      index: index
    });

    if (!isExist) {
      throw ApplicationError("ElasticSearch Index 'products' không tồn tại");
    }

    let result = await client.index({
      index: index,
      type: '_doc',
      id: model.id,
      body: model
    });

    // await client.delete({
    //   index: index,
    //   type: '_doc',
    //   id: model.id
    // });

    // let result = await client.create({
    //   index: index,
    //   type: '_doc',
    //   id: model.id,
    //   body: model
    // });

    return result;
  }

  /**
   * thienvd 25/03/2020
   * thêm điều kiện status của ProductSeller
   * Create an index for models
   * @param {*} index 
   * @param {*} model 
   */
  prototype.Index = async function (index, models) {

    // console.log(index,model.constructor.name);
    // var promise = new Promise((resole, reject) => {

      let isExist = await client.indices.exists({
        index: index
      });

      if (isExist) {
        await client.indices.delete({
          index: index
        });
      }

      await client.indices.create({
          index: index,
          body: {
            aliases: {},
            mappings: {
              _doc: {
                properties: {
                  brandId: {
                    type: "long"
                  },
                  categoryId: {
                    type: "long"
                  },
                  id: {
                    type: "long"
                  },
                  status: {
                    type: "long"
                  },
                  productName: {
                    type: "text",
                    fields: {
                      keyword: {
                        type: "keyword",
                        ignore_above: 256
                      }
                    },
                    analyzer: "sml_analyzer"
                  },
                  productSellers: {
                    properties: {
                      basePrice: {
                        type: "long"
                      },
                      id: {
                        type: "long"
                      },
                      productId: {
                        type: "long"
                      },
                      seller: {
                        properties: {
                            shopName: {
                                type: "text",
                                fields: {
                                    keyword: {
                                        type: "keyword",
                                        ignore_above: 256
                                    }
                                }
                            },
                            status: {
                                "type": "long"
                            },
                            uuid: {
                                type: "text",
                                fields: {
                                    keyword: {
                                        type: "keyword",
                                        ignore_above: 256
                                    }
                                }
                            }
                        }
                    },
                      sellerId: {
                        type: "long"
                      }                      ,
                      status: {
                        type: "long"
                      }
                    }
                  }
                }
              }
            },
            settings: {
              index: {
                number_of_shards: "1",
                analysis: {
                  analyzer: {
                    sml_analyzer: {
                      filter: [
                        'icu_folding'
                      ],
                      char_filter: [
                        "html_strip"
                      ],
                      tokenizer: "icu_tokenizer"
                    }
                  }
                }
              }
            }
          }
        }
      );

      let body = [];
      models.forEach(function (row, id) {
        body.push({ index: { _index: index, _type: '_doc', _id: row.id } });
        body.push(row);
      });

      let result = await client.bulk({
        body: body
      });

      return result;

      // client.index({
      //     index: index,
      //     // type: model.constructor.name ,
      //     id: model.id,
      //     body: model

      //   }).then(function (resp) {

      //     //  var hits = resp.hits.hits;
      //       resole(resp);

      //   }, function (err) {
      //      reject('loi he thong');
      //   });

    // });

    // return promise;
  }

 /**
   * thienvd  06-12-2019 update trạng thái sản phẩm
   * @param {*} index 
   * @param {*} model 
   */
  prototype.UpdateProductStatus = async function(index, id, status){

    let result = await client.updateByQuery({
      index: index,
      type: '_doc',
      // id: id,    
      body: { 
        //  "query": { "match": { "animal": "bear" } }, 
        //  "script": { "inline": "ctx._source.status = '2'"}
         "query": { "term": {"_id": id}},
         "script": "ctx._source.status =" + status      
      }
   });
    return result;
  
  }



}