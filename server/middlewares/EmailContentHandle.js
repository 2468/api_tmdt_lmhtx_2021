var Email = 
{

    REGISTRATION_SUCCESS: {
        title: 'Tạo tài khoản thành công!',
        heading:'Xin chào ',
        content:   'Bạn đã đăng ký thành công tài khoản tại hệ thống thương mại điện tử SmartLife',
        link: 'http://chosachhathanh.com:4300/seller/registration-confirm/',
    },
    
    ACTIVE_SUCCESS: {
        title: 'Kích hoạt tài khoản thành công!',
        content:   'Bạn đã kích hoạt thành công tài khoản tại hệ thống thương mại điện tử SmartLife, vui lòng đăng nhập hệ thống để sử dụng dịch vụ',
        link: 'http://chosachhathanh.com:4300/seller/signin',
    } ,
    RESET_PASSWORD_REQUIRED: {
        title: 'Yêu cầu thay đổi mật khẩu!',
        content:   'Bạn đã yêu cầu thay đổi lại mật khẩu hệ thống, vui lòng click link dưới đây để thay đổi mật khẩu. Nếu yêu cầu này không phải do bạn đưa ra hãy bỏ qua thư này.',
        link: 'http://chosachhathanh.com:4300/seller/reset-password/',
    }     

};
module.exports = Email;


