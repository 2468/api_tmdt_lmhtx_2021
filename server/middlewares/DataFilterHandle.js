module.exports = function()
{
    var prototype = Object.getPrototypeOf(this);
    prototype.Where = function(primaryFilter, extfilter=null)
    {
        if(!extfilter || extfilter === undefined || extfilter === 'null')
        {
            return {'and': primaryFilter};
        }
        else
        {       
           primaryFilter.push({"or": JSON.parse(extfilter)});
            // console.log(primaryFilter);
            return {
                'and':   primaryFilter
            };
        }
    }

}