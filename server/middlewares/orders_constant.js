/**
 * Orders Constant
 * Author: LuongPD
 * Last update: 20.3.2018 14:18
 */

module.exports = {
    DA_DAT_HANG: {
        status: 0,
        message: "Đơn hàng đã đặt thành công. Thông tin đơn hàng sẽ được kiểm tra và xác nhận."
    },
    XAC_NHAN_DON_HANG: {
        status: 1,
        message: "Đơn hàng đã được SmartLife xác nhận. Thông tin đơn hàng đã gửi tới đối tác bán hàng."
    },
    CUA_HANG_XAC_NHAN: {
        status: 2,
        message: "Đơn hàng đã được cửa hàng xác nhận."
    },
    DANG_GUI_DI: {
        status: 3,
        message: "Đơn hàng đã được chuyển cho bộ phận giao nhận hàng."
    },
    DA_GIAO_THANH_CONG: {
        status: 4,
        message: "Đơn hàng đã giao thành công cho khách hàng."
    },
    DA_HUY: {
        status: 5,
        message: "Đơn hàng đã được ngưng thực hiện trên hệ thống. Chúng tôi xin lỗi vì những bất tiện quý khách đã gặp phải."
    },
    TU_CHOI_NHAN_HANG: {
        status: 6,
        message: "Khách hàng từ chối nhận đơn hàng."
    },
    KET_THUC: {
        status: 7,
        message: "Đơn hàng đã xử lý xong. Xin chân thành cảm ơn quý khách và hẹn gặp lại quý khách trong lần mua sắm tiếp theo."
    }
}