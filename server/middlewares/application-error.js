'use strict';
// require('util').inherits(module.exports, Error);


// module.exports = function ApplicationError(error) {
//   Error.captureStackTrace(this, this.constructor);
//   this.name = this.constructor.name;
//   this.error = error;
// };

class ApplicationError extends Error {
  constructor(error, status) {
    super();
    
    Error.captureStackTrace(this, this.constructor);
    
    this.name = this.constructor.name;
    
    this.error = error;
    this.status = status || 500;
  }
}
module.exports = ApplicationError;

// module.exports = class ApplicationError extends Error {
//     constructor (error) {
  
//     // Calling parent constructor of base Error class.
//     super(error.message);
//     this.error = error;

//     // Saving class name in the property of our custom error as a shortcut.
//     this.name = this.constructor.name;

//     // Capturing stack trace, excluding constructor call from it.
//     Error.captureStackTrace(this, this.constructor);
    
//     // You can use any additional properties you want.
//     // I'm going to use preferred HTTP status for this error types.
//     // `500` is the default value if not specified.
//     // this.status = status || 500;
    
//   }
// };