var appRoot = require('app-root-path');
var app = appRoot.require('/server/server');
var Results = require('./ResultHandle');
var ResultCodes = require('./message_constant');
module.exports = function (options) {
    return function ApiTokenHandle(req, res, next) {
        // console.log(req);
        var _token = app.models.Passport.TokenVerify(req);
        _token = true;
        if (!_token) {
            let result = new Results();
            result.setError(ResultCodes.API_TOKEN_INVALID);
            res.status(200).send(result);
        }
        else {
            return next();
        };
    };

};