/** 
 * MiddleWare module Result handler
 * Author: Thiep Wong
 * Created: 04.04/2018 11:00
 * Create new result instance for response from API
 * 
*/
var setting = require('../../package.json');

module.exports = function(){

    var prototype = Object.getPrototypeOf(this);

    prototype.setSuccess = function(success){
        if(success)  this.success =success;
    
    }
    
    prototype.setWarning = function(warning){
        if(warning)  this.warning =warning;
    }


    prototype.setError = function (errors) 
    {
        if(errors)  this.errors =errors;
    }


    this.api_version= setting.version;
    this.server_time= new Date();


}