/**
 * Model ProductSellerPlatform
 * Author: LuongPD
 * Last update: 23.5.2018 10:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
var ApplicationError = require('../middlewares/application-error');

'use strict';

module.exports = function(ProductSellerPlatform) {

    //#region Functions
    ProductSellerPlatform.CreateNew = function (productSellerId, platformCode, displayOnShop, createBy, cb) {
        app.dataSources.mysqld.transaction(async models => {
            const {Products} = models;
            await this.Add(models, productSellerId, platformCode, displayOnShop, createBy);

            // update ES document
            await Products.UpdateDocumentByProductSellerId(productSellerId, models);

            let result = new Results();
            result.setSuccess(true);
            return cb ? cb(null, result) : result;
            
        }).catch(function (e) {
            let result = new Results();

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }

    ProductSellerPlatform.Update = function (id,ctx,  cb) {

        let status = ctx.body.status || null; 
        let displayOnShop = ctx.body.displayOnShop || null; 

        app.dataSources.mysqld.transaction(async models => {

            const {Passport, ProductSellerPlatform, ProductSeller} = models;

            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                throw new ApplicationError(ResultsCodes.ACCESS_TOKEN_INVALID);
            }

            if (status != 0 && status != 1)  {
                let error = ResultsCodes.VALIDATION_ERROR;
                error.message = 'status không đúng';
                throw new ApplicationError(error);
            } 

            if (status != 0 && status != 1)  {
                let error = ResultsCodes.VALIDATION_ERROR;
                error.message = 'displayOnShop không đúng';
                throw new ApplicationError(error);
            } 

            let productSellerPlatform = await ProductSellerPlatform.findById(id);
            if (!productSellerPlatform) {
                let error = ResultsCodes.DATA_NOT_FOUND;
                error.message = 'Không tìm thấy ProductSellerPlatform với id = ' + id;
                throw new ApplicationError(error); 
            }

            let productSeller = await ProductSeller.findById(productSellerPlatform.productSellerId)
            if (!productSeller) {
                let error = ResultsCodes.DATA_NOT_FOUND;
                error.message = 'Không tìm thấy ProductSeller với id = ' + productSellerPlatform.productSellerId;
                throw new ApplicationError(error); 
            }

            if (productSeller.sellerId != token.userId) {
                let error = ResultsCodes.NO_PERMISSTION_ERROR;
                error.message = 'Bạn không có quyền làm việc này';
                throw new ApplicationError(error); 
            }

            await productSellerPlatform.updateAttributes({status: status, displayOnShop: displayOnShop});

            // update ES document
            await Products.UpdateDocumentByProductSellerId(productSeller.id, models);

            let result = new Results();
            result.setSuccess(productSellerPlatform);
            return cb ? cb(null, result) : result;
            
        }).catch(function (e) {
            let result = new Results();

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }

    ProductSellerPlatform.Add = async function(models, productSellerId, platformCode, displayOnShop, createBy) {
        const { ProductSellerPlatform } = models;
        var productSellerPlatform = new ProductSellerPlatform;
        productSellerPlatform.productSellerId = productSellerId;
        productSellerPlatform.platformCode = platformCode;
        productSellerPlatform.displayOnShop = displayOnShop;
        productSellerPlatform.status = 1;
        productSellerPlatform.approvedBy = null;
        productSellerPlatform.approvedTime = null;
        productSellerPlatform.createBy = createBy;
        productSellerPlatform.createTime = parseInt(new Date().getTime() / 1000.0);
        await ProductSellerPlatform.create(productSellerPlatform);
    }

    ProductSellerPlatform.Save = function (ctx, cb) {

        let productSellerPlatforms = ctx.body.productSellerPlatforms|| [];
        let productSellerId = ctx.body.productSellerId || 0;

        app.dataSources.mysqld.transaction(async models => {
            const {Passport, Products, ProductSellerPlatform, ProductSeller} = models;

            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                throw new ApplicationError(ResultsCodes.ACCESS_TOKEN_INVALID);
            }

            let productSeller = await ProductSeller.findById(productSellerId)
            if (!productSeller) {
                let error = ResultsCodes.DATA_NOT_FOUND;
                error.message = 'Không tìm thấy ProductSeller với id = ' + productSellerId;
                throw new ApplicationError(error); 
            }

            if (productSeller.sellerId != token.userId) {
                let error = ResultsCodes.NO_PERMISSTION_ERROR;
                error.message = 'Bạn không có quyền làm việc này';
                throw new ApplicationError(error); 
            }

            let newDate = new Date().getTime() / 1000.0;

            if (productSellerPlatforms.length > 0) {

                for (var index in productSellerPlatforms) {

                    let productSellerPlatform = productSellerPlatforms[index];

                    if (productSellerPlatform.productSellerPlatformStatus != undefined && productSellerPlatform.productSellerPlatformStatus != 0 && productSellerPlatform.productSellerPlatformStatus != 1) {
                            let error = ResultsCodes.VALIDATION_ERROR;
                            error.message = 'productSellerPlatformStatus phải là 0 hoặc 1';
                            throw new ApplicationError(error);
                    }

                    if (productSellerPlatform.displayOnShop != undefined && productSellerPlatform.displayOnShop != 0 && productSellerPlatform.displayOnShop != 1) {
                        let error = ResultsCodes.VALIDATION_ERROR;
                        error.message = 'displayOnShop phải là 0 hoặc 1';
                        throw new ApplicationError(error);
                    }

                    if (productSellerPlatform.productSellerPlatformId > 0) { // if exists => update status
                        let instance = await ProductSellerPlatform.findOne({where: {'id': productSellerPlatform.productSellerPlatformId, 'productSellerId': productSellerId}});
                        if (!instance) {
                            let error = ResultsCodes.DATA_NOT_FOUND;
                            error.message = 'Không tìm thấy ProductSellerPlatform với id = ' + productSellerPlatform.productSellerPlatformId + ', productSellerId = ' + productSellerId;
                            throw new ApplicationError(error);
                        } 

                        // if already exists and status = 1 => set status to 0
                        if (productSellerPlatform.productSellerPlatformStatus != undefined) {
                            await instance.updateAttributes({'status': productSellerPlatform.productSellerPlatformStatus, 'displayOnShop': productSellerPlatform.displayOnShop, 'modifyBy': productSeller.sellerId, 'modifyTime': newDate});
                        }
                    } else {
                        // if not exists => find by platformCode
                        if (productSellerPlatform.productSellerPlatformStatus != undefined && productSellerPlatform.productSellerPlatformStatus == 1) {
                        
                            let instance = await ProductSellerPlatform.findOne({where: {'platformCode': productSellerPlatform.platformCode, 'productSellerId': productSellerId}});
                            if (instance) {
                                // update status to 1
                                await instance.updateAttributes({'status': 1, 'displayOnShop': productSellerPlatform.displayOnShop, 'modifyBy': productSeller.sellerId, 'modifyTime': newDate});
                            
                            }  else {
                                // add new record with status = 1
                                await this.Add(models, productSellerId, productSellerPlatform.platformCode, productSellerPlatform.displayOnShop, productSeller.sellerId);
                            }
                        }
                    }
                }

                // update ES document
                await Products.UpdateDocumentByProductSellerId(productSeller.id, models);
            }

            let result = new Results();
            result.setSuccess(true);
            return cb(null, result);
        }).catch(function (e) {
            let result = new Results();

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }

    ProductSellerPlatform.Approve = function (id, cb) {

        app.dataSources.mysqld.transaction(async models => {
            const { ProductSellerPlatform } = models;

            var instance = await ProductSellerPlatform.findOne({
                // fields: ['id', 'price', 'from', 'to'],
                where: {
                    "id": id
                }
            });

            if (instance == null) {
                throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
            }

            instance.modifyTime = parseInt(new Date().getTime() / 1000.0);
            instance.modifyBy = 99;
            await instance.save();

            let result = new Results();
            result.setSuccess(true);
            return cb ? cb(null, result) : result;

        }).catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb ? cb(null, result) : result;
        });
        // }
        // makeRequest();
    }

    ProductSellerPlatform.List = function (ctx, cb) {

            let productSellerId = parseInt(ctx.query.productSellerId) || 0 ;
            let status = ctx.query.status || null;
             if(status.length<2) status = [status];

        const makeRequest = async () => { 
            let token = await ProductSellerPlatform.app.models.Passport.GetSellerToken(ctx);
            if (!token) {
                throw new ApplicationError(ResultsCodes.ACCESS_TOKEN_INVALID);
            }

           
            let productSeller = await ProductSellerPlatform.app.models.ProductSeller.findOne({fields: ['sellerId'], where: {'id': productSellerId}});

            if (!productSeller) {
                let error = ResultsCodes.DATA_NOT_FOUND;
                error.message = 'Không tìm thấy ProductSeller với id = ' + productSellerId;
                throw new ApplicationError(error);
            }

            if (productSeller.sellerId != token.userId) {
                let error = ResultsCodes.NO_PERMISSTION_ERROR;
                error.message = 'productSellerId không thuộc về sellerId này';
                throw new ApplicationError(error);
            }

            let productSellerPlatforms = await ProductSellerPlatform.find({
                fields: ['id', 'platformCode', 'displayOnShop'],
                include: [{
                    relation: 'platform',
                    scope: {
                        fields: ['id', 'platformCode', 'platformName']
                    }
                },
                {
                    relation: 'productSeller',
                    scope: {
                        fields: ['id', 'sellerId']
                    }
                }],
                where: {
                    'productSellerId': productSellerId,
                    'status': {inq : status}
                },
                order: 'platformCode asc'
            });

            let productSellerPlatformsFinal = [];
            if (productSellerPlatforms.length > 0) {
                for (var index in productSellerPlatforms) {
                    let productSellerPlatform = productSellerPlatforms[index];

                    let sellerPlatforms =  await ProductSellerPlatform.app.models.SellerPlatform.find({
                        fields: ['id', 'platformCode', 'status'],
                        where: {
                            'sellerId': token.userId,
                            'status': {inq : [1, 2]}
                        },
                        order: 'platformCode asc'
                    });

                    if (sellerPlatforms.length > 0) {
                        let idx = sellerPlatforms.findIndex(x => x.platformCode == productSellerPlatform.platformCode);
                        if (idx >= 0) {
                            productSellerPlatformsFinal.push(productSellerPlatform);
                        }
                    }
                }
            } 
            
            let result = new Results();
            result.setSuccess(productSellerPlatformsFinal);
            return  cb(null, result);
        }

        makeRequest().catch (function(e) {
            let result = new Results();

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }
    //#endregion

    //#region Expose Parameters
    ProductSellerPlatform.remoteMethod('CreateNew', {
        http: {
            verb: 'POST',
        },
        accepts: [{
                arg: 'productSellerId',
                type: 'number'
            }, {
                arg: 'platformCode',
                type: 'string'
            }, {
                arg: 'displayOnShop',
                type: 'number'
            }, {
                arg: 'createBy',
                type: 'number'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    ProductSellerPlatform.remoteMethod('Save', {
        http: {
            verb: 'POST',
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    
    ProductSellerPlatform.remoteMethod('Approve', {
        http: {
            verb: 'POST',
        },
        accepts: [{
                arg: 'id',
                type: 'number'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    ProductSellerPlatform.remoteMethod('List', {
        http: {
            verb: 'GET',
            path: '/list'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object', 
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    ProductSellerPlatform.remoteMethod('Update',{
        description:'@Hung Doan, update status of record',
        http:{
            verb:'POST',
            path:'/update/:id'
        },
        accepts:[
            {
                arg:'id',
                type:'number'
            },
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns:{
            arg:'result',
            type:'string',
            root:true
        }
    })
    //#endregion
};