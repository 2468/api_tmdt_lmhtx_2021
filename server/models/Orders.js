
'use strict';
/**
 * Model Orders
 * Author: LuongPD
 * Last update: 16.3.2018 13:49
 */
var Results = new require('../middlewares/ResultHandle');
const ResultsCodes = require('../middlewares/message_constant');
const ShippingCodes = require('../middlewares/shipping_constant');
const OrdersCodes = require('../middlewares/orders_constant');
const Filter = require('../middlewares/DataFilterHandle');
const Relation = require('../middlewares/RelationHandle');
const crypto = require('crypto');
const app = require('../../server/server');
const mqtt = require('mqtt');
const MQTT = app.get('MQTT');
var Config = require('../middlewares/GhtkApiConfig');
var http = require('request');

var statusGhtk = require('../middlewares/statusGhtk');

module.exports = function (Orders) {

    //#region Functions
    /**
     * Modified: Thiep Wong
     * Date: 7/12/2018
     */


    Orders.sharedClass.methods().forEach(function (method) {
        Orders.disableRemoteMethodByName(method.name, method.isStatic);
    });

    Orders.SendMQTT = (channel, order) => {
        const mk = async () => {
            MQTT.clientId = 'mqttjs_' + Math.random().toString(16).substr(2, 8)
            const client = await mqtt.connect(`ws://${MQTT.host}`, MQTT);
            client.publish(channel, order.toString());
            return true;

        }

        mk();

    }

    Orders.CreateNew = function (ctx, cb) {

        let preOrderKey = ctx.body.preOrderKey || null;
        let note = ctx.body.note || null;
        let buyerName = ctx.body.buyerName || null;
        let buyerPhone = ctx.body.buyerPhone || null;
        let buyerAddress = ctx.body.buyerAddress || null;
        let buyerLocationCode = ctx.body.buyerLocationCode || null;
        let totalPrice = ctx.body.totalPrice || 0;
        let paymentType = ctx.body.paymentType || 0;
        let transactionNumber = ctx.body.transactionNumber || 0;
        let shippingGroupJson = ctx.body.shippingGroupJson || null;
        let freeship = ctx.body.freeship || 0;
        //let freeship = ctx.body.shippingGroupJson.shipCost;
        let pickOption = ctx.body.pickOption || 'cod';
        let result = new Results();

        if (!preOrderKey || !buyerName || !buyerPhone || !buyerAddress || !buyerLocationCode || !totalPrice || !totalPrice || !shippingGroupJson) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }

        const makeCreateNew = async () => {
            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            app.dataSources.mysqld.transaction(async models => {
                const { PreOrder, Orders, ShippingBill, OrdersDetails, ShippingHistory } = models;
                let memberId = token.userId;

                let preOrder = await PreOrder.findOne({
                    // fields: ['id', 'preOrderKey', 'status', 'createBy', 'createTime'],
                    where: {
                        "preOrderKey": preOrderKey
                    }
                });

                if (preOrder == null || preOrder.status == 1) {
                    result.setError(ResultsCodes.ADD_ITEM_FAILED);
                    return cb(null, result);
                } else {
                    preOrder.status = 1;
                    await preOrder.save();
                }

                let currentTime = parseInt(new Date().getTime() / 1000.0);
                let order = new Orders;
                order.memberId = memberId;
                // order.orderCode = GenerateOrdersCode();  // using UUID from defaultFn in JOSN file. @Thiep Wong
                order.preOrderKey = preOrderKey;
                order.buyTime = currentTime;
                order.note = note;
                order.buyerName = buyerName;
                order.buyerPhone = buyerPhone;
                order.buyerAddress = buyerAddress;
                order.buyerLocationCode = buyerLocationCode;
                order.totalPrice = totalPrice;
                order.paymentType = paymentType;
                order.transactionNumber = transactionNumber;
                order.status = OrdersCodes.DA_DAT_HANG.status;
                order.createTime = currentTime;
                order.createBy = memberId;
                order = await Orders.create(order);
                // console.log(JSON.parse(JSON.stringify(order)));
                // split shipping bills
                var shippingGroup = parseListObject(shippingGroupJson);
                // for (var item in shippingGroup) {
                //     // var shipping = shippingGroup[0][item];
                //     await createShippingBill(order.id, item, memberId);
                // }

                for (let i = 0; i < shippingGroup.length; i++) {
                    await createShippingBill(order.id, shippingGroup[i], memberId);
                }

                let orders = await Orders.findOne({
                    fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status'],
                    include: [
                        {
                            relation: 'member',
                            scope: {
                                fields: ['id', 'fullname']
                            }
                        },
                        {
                            relation: 'shippingBills',
                            scope: {
                                fields: ['id', 'sellerId', 'sellerStoreId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note', 'label_id'],
                                include: [
                                    {
                                        relation: 'ordersDetails',
                                        scope: {
                                            fields: ['id', 'shippingBillId', 'productSellerId', 'affiliateId', 'price', 'unit', 'amount'],
                                            include: [
                                                {
                                                    relation: 'productSeller',
                                                    scope: {
                                                        fields: ['id', 'sellerId', 'productId', 'basePrice', 'total', 'aspect', 'status'],
                                                        include: [
                                                            {
                                                                relation: 'product',
                                                                scope: {
                                                                    fields: ['id', 'categoryId', 'brandId', 'productName', 'productImage', "weight"],
                                                                    include: [{
                                                                        relation: 'category',
                                                                        scope: {
                                                                            fields: ['id', 'title']
                                                                        }
                                                                    }]
                                                                }
                                                            }]
                                                    }
                                                }]
                                        }
                                    }, {
                                        relation: 'shippingHistories',
                                        scope: {
                                            fields: ['id', 'shippingBillId', 'status', 'shortDescription', 'createTime'],
                                            order: 'status DESC'
                                        }
                                    }, {
                                        relation: 'seller',
                                        scope: {
                                            fields: ['id', 'shopName']
                                        }
                                    }, {
                                        relation: 'shipUnit',
                                        scope: {
                                            fields: ['name', 'logoImage', 'logoImage']
                                        }
                                    }]
                            }
                        }],
                    where: {
                        "id": order.id
                    }
                });

                // console.log(JSON.parse(JSON.stringify(orders)));
                let sql = "DELETE FROM carts WHERE member_id = ?";
                let params = [memberId];
                let connector = Orders.dataSource.connector;
                await connector.query(sql, params, function (err, result) { });

                let _shipBills = JSON.parse(JSON.stringify(orders)).shippingBills;

                if (_shipBills.length) {

                    _shipBills.forEach(item => {
                        let _channel = item.sellerId;
                        Orders.SendMQTT(`orders-channel/${_channel}`, JSON.stringify({
                            orderId: orders.id,
                            orderCode: orders.orderCode,
                            orderDate: new Date().getTime(),
                            orderValue: item.billPrice
                        }));
                    })

                }

                //Shipment Order
                // Get list shippingBills
                let shippingBills = await ShippingBill.find({
                    include: [
                        {
                            relation: 'ordersDetails',
                            scope: {
                                include: {
                                    relation: 'productSeller',
                                    scope: {
                                        include: {
                                            relation: 'product'
                                        }
                                    }
                                }
                            }
                        },
                        {
                            relation: 'sellerStore',
                            scope: {
                                include: {
                                    relation: "seller"
                                }
                            }
                        }
                    ],
                    where: {
                        orderId: order.id
                    }
                });

                if (shippingBills) {

                    let customerLocation = await getLocationByCode(buyerLocationCode);
                    // console.log(buyerLocationCode);

                    shippingBills.forEach(async item => {
                        // calculate fee ship
                        var sellerLocation = await getLocationByCode(item.sellerStore().locationCode);

                        let pick_province = customerLocation.parent().parent().name;
                        let pick_district = customerLocation.parent().name;
                        let province = sellerLocation.parent().parent().name;
                        let district = sellerLocation.parent().name;
                        let address = buyerAddress;
                        let weight = 0;
                        let value = item.billPrice;

                        item.ordersDetails().forEach(od => {
                            // console.log(od);
                            weight += od.productSeller().product().weight ? od.productSeller().product().weight / 1000 : 1;
                        });

                        if (item.shippingType != 0) {
                            let fee = await getFeeTransport(item.id, pick_province, pick_district, province, district, address, weight, value);
                        }
                        //let shipment = await shipmentOrder(products, order, item.id);
                        // console.log(shipment);

                    });
                }
                // console.log(shippingBills);

                // Create notification
                let Notification = Orders.app.models.Notification;
                await Notification.createNotification(memberId, orders.id, null, orders.orderCode, OrdersCodes.DA_DAT_HANG.status);

                result.setSuccess(orders);
                result.setWarning(ResultsCodes.ORDER_SUCCESSED);
                return cb(null, result);
            }).catch(function (e) {
                console.log(e);
                result.setError(ResultsCodes.ORDERS_ORDER_ERROR);
                return cb(null, result);
            });
        }
        makeCreateNew().catch(function (e) {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    var getFeeTransport = async function (shippingBillId, pick_province, pick_district, province, district, address, weight, value) {

        let base_Url = Config.baseUrl;
        let access_token = Config.api_token;

        let options = {
            method: 'GET',
            // url: 'https://dev.ghtk.vn/services/shipment/fee',
            url: base_Url + 'services/shipment/fee',
            qs:
            {
                pick_province: pick_province,
                pick_district: pick_district,
                province: province,
                district: district,
                weight: weight,
                value: value,
                address: address
            },
            headers:
            {
                Connection: 'keep-alive',
                Host: 'dev.ghtk.vn',
                Accept: '*/*',
                'Content-Type': 'application/json',
                Token: access_token
            }
        };

        http(options, async function (error, response, body) {
            if (error) {
                return 0;
            }
            if (body) {
                if (body.success) {
                    var ShippingBill = Orders.app.models.ShippingBill;
                    let shippingBill = await ShippingBill.findOne({
                        where: {
                            id: shippingBillId
                        }
                    });
                    shippingBill.shipFee = body.fee.fee;
                    await shippingBill.save();
                    return shippingBill;
                }
            }
        });

    }

    var getLocationByCode = async function (locationCode) {
        var Location = Orders.app.models.Location;
        let location = await Location.findOne({
            include: {
                relation: 'parent',
                scope: {
                    include: {
                        relation: 'parent'
                    }
                }
            },
            where: {
                code: locationCode
            }
        });
        // console.log(location);
        return location;
    }

    var createShippingBill = async function (orderId, shipping, memberId) {

        var ShipUnit = Orders.app.models.ShipUnit;
        var shipUnit = await ShipUnit.findOne({
            fields: ['id', 'matchLocationLevelFee', 'timeDelivery'],
            where: {
                "id": shipping.shipUnitId
            }
        });

        var ShippingBill = Orders.app.models.ShippingBill;
        var shippingBill = new ShippingBill;
        shippingBill.orderId = orderId;
        shippingBill.sellerStoreId = shipping.sellerStoreId;
        shippingBill.sellerId = shipping.sellerId;
        //shippingBill.shippingType = 1;    
        //thienvd 03/01/2019 sửa  shippingType = 1 bằng para truyền vào
        shippingBill.shippingType = shipping.shipType;
        shippingBill.shipUnitId = shipping.shipUnitId;
        if (shipUnit != null && shipping.shipType == 1) {
            // thienvd 24/12/2019
            // shippingBill.shipFee = shipUnit.matchLocationLevelFee;
            shippingBill.shipFee = shipping.shipFree;
            shippingBill.shipTime = parseInt((new Date().getTime() + shipUnit.timeDelivery * 86400000) / 1000.0);
        } else {
            shippingBill.shipFee = 0;
            shippingBill.shipTime = 0;
        }

        shippingBill.billPrice = shipping.billPrice;
        shippingBill.note = null;
        shippingBill.status = ShippingCodes.DA_DAT_HANG.status;
        shippingBill.createTime = parseInt(new Date().getTime() / 1000.0);
        shippingBill.createBy = memberId;
        shippingBill = await ShippingBill.create(shippingBill);

        let _shippingHistory = Orders.app.models.ShippingHistory;
        await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.DA_DAT_HANG, null);

        for (var item in shipping.products) {
            await createOrderDetails(orderId, shippingBill, shipping.products[item], memberId);
        }

        // Create notification
        let Notification = Orders.app.models.Notification;
        await Notification.createNotification(memberId, orderId, shippingBill.id, null, OrdersCodes.DA_DAT_HANG.status);
    }
    var createOrderDetails = async function (orderId, shippingBill, item, memberId) {

        var ProductSeller = Orders.app.models.ProductSeller;
        var currentTime = parseInt(new Date().getTime() / 1000.0);

        var productSeller = await ProductSeller.findOne({
            fields: ['id', 'basePrice'],
            include: [{
                relation: 'productPrice',
                scope: {
                    fields: ['id', 'productSellerId', 'price', 'from', 'to'],
                    where: { and: [{ "from": { lte: currentTime } }, { "to": { gte: currentTime } }] }
                }
            }],
            where: {
                "id": item.productSellerId
            }
        });

        var jsonProductSeller = productSeller.toJSON();
        var productPrice = productSeller.basePrice;
        if (jsonProductSeller.productPrice != null) {
            productPrice = jsonProductSeller.productPrice.price;
        }

        var OrdersDetails = Orders.app.models.OrdersDetails;
        var ordersDetails = new OrdersDetails;
        ordersDetails.ordersId = orderId;
        ordersDetails.productSellerId = item.productSellerId;
        ordersDetails.shippingBillId = shippingBill.id;
        ordersDetails.affiliateId = item.affiliateId;
        ordersDetails.price = productPrice;
        ordersDetails.unit = item.unit;
        ordersDetails.amount = item.amount;
        ordersDetails.createTime = parseInt(new Date().getTime() / 1000.0);
        ordersDetails.createBy = memberId;
        await OrdersDetails.create(ordersDetails);
    }
    var parseListObject = function (json) {
        return JSON.parse(json);
    }

    // var createNotification = async function(memberId, orderId, shippingBillId, status) {
    //     let Notification = Orders.app.models.Notification;
    //     let notification = new Notification;
    //     notification.memberId = memberId;
    //     notification.orderId = orderId;
    //     notification.shippingBillId = shippingBillId;
    //     notification.status = 0;
    //     if (status == ShippingCodes.DA_DAT_HANG.status) {
    //         notification.shortDescription = 'Bạn đã đặt đơn hàng ' + orderId + ' thành công';
    //         notification.content = 'Bạn đã đặt đơn hàng ' + orderId + 'thành công';
    //     }
    //     if (status == ShippingCodes.CUA_HANG_XAC_NHAN.status) {
    //         notification.shortDescription = 'Cửa hàng đã xác nhận đơn hàng ' + orderId;
    //         notification.content = 'Cửa hàng đã xác nhận đơn hàng ' + orderId;
    //     }
    //     if (status == ShippingCodes.DANG_GUI_DI.status) {
    //         notification.shortDescription = 'Đơn hàng ' + orderId + ' của bạn đã được gửi đi';
    //         notification.content = 'Đơn hàng ' + orderId + ' của bạn đã được gửi đi';
    //     }
    //     if (status == ShippingCodes.DA_GIAO_THANH_CONG.status) {
    //         notification.shortDescription = 'Đơn hàng ' + orderId + ' đã giao thành công';
    //         notification.content = 'Đơn hàng ' + orderId + ' đã giao thành công';
    //     }
    //     if (status == ShippingCodes.DA_HUY.status) {
    //         notification.shortDescription = 'Đơn hàng ' + orderId + ' đã hủy thành công';
    //         notification.content = 'Đơn hàng ' + orderId + ' đã hủy thành công';
    //     }
    //     if (status == ShippingCodes.TU_CHOI_NHAN_HANG.status) {
    //         notification.shortDescription = 'Bạn đã từ chôi nhận đơn hàng ' + orderId;
    //         notification.content = 'Bạn đã từ chối nhận đơn hàng ' + orderId;
    //     }
    //     if (status == ShippingCodes.KET_THUC.status) {
    //         notification.shortDescription = 'Đơn hàng ' + orderId + ' kết thúc';
    //         notification.content = 'Đơn hàng ' + orderId + ' kết thúc';
    //     }
    //     await Notification.create(notification);
    // }
    /**
     * Count
     * @param {*} sql 
     * @param {*} params 
     */
    Orders.Count = function (sql, params) {

        let promise = new Promise((resolve, reject) => {
            var ds = Orders.dataSource;
            ds.connector.query(sql, params, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
        return promise;
    }

    /**
     * Get order list by sellers, using pagging
     * Created by Thiep Wong
     * Date: 12/05/2018
     * @param {*} pageIndex 
     * @param {*} pageSize 
     * @param {*} pageOrder 
     * @param {*} pageFilter 
     * @param {*} cb 
     */
    Orders.GetListBySeller = function (orderCode, productName, fromDate, toDate, status, ctx, cb) {

        //let pageIndex = ctx.query.pageIndex || 1;
        let pageIndex = ctx.query.pageIndex || 0;
        let pageSize = ctx.query.pageSize || 10;
        let pageOrder = ctx.query.pageOrder || null;
        let pageFilter = ctx.query.pageFilter || null;

        let result = new Results();
        const makeRequest = async () => {
            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            // if (pageSize) {
            //     if (pageSize <= 0) {
            //         pageSize = 10;
            //     }
            // } else {
            //     pageSize = 10;
            // }

            // if (pageIndex) {
            //     if (pageIndex < 1) {
            //         pageIndex = 1;
            //     }
            // } else {
            //     pageIndex = 1;
            // }

            if (pageSize) {
                if (pageSize <= 0) {
                    pageSize = 10;
                }
            } else {
                pageSize = 10;
            }

            if (pageIndex) {
                if (pageIndex < 1) {
                    pageIndex = 1;
                }
            } else {
                pageIndex = 1;
            }

            let orderBy = "";
            let offset = (pageIndex - 1) * pageSize;

            // if (!pageOrder || pageOrder == 'undefined' || pageOrder == 'null') pageOrder = ['id desc'];


            if (!pageOrder || pageOrder === 'undefined') {
                orderBy = "orderTime DESC"
            } else {

                for (let i = 0; i < pageOrder.length; i++) {
                    let sortArr = pageOrder[i].match(/[^\s-]+-?/g);

                    if (sortArr.length == 2) {
                        let sortName = sortArr[0];
                        let direction = sortArr[1];

                        if ("id" === sortName.toLowerCase()) {

                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "id" + direction;

                        } if ("ordertime" === sortName.toLowerCase()) {

                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "orderTime" + direction;

                        }

                        if (i < pageOrder.length - 1 && orderBy) {
                            orderBy += ", ";
                        }
                    }
                }
            }

            let sql = `SELECT o.id id, o.order_code orderCode, count(od.orders_id) items, sum(od.price*od.amount) amount, o.buy_time orderTime, o.status status, o.payment_type paymentType 
            FROM orders o left join orders_details od on od.orders_id = o.id
            left join product_seller ps on ps.id = od.product_seller_id
            left join products p on p.id = ps.product_id
            left join sellers s on s.id = ps.seller_id
            where s.id = ? `;

            let filterSQL = `select count(*) totalRecords from (SELECT o.id id, o.buy_time orderTime, o.status status FROM orders o left join orders_details od on od.orders_id = o.id
            left join product_seller ps on ps.id = od.product_seller_id
            left join products p on p.id = ps.product_id
            left join sellers s on s.id = ps.seller_id
            where s.id = ? `;

            let priceSQL = `SELECT sum(od.price*od.amount) totalPrice
            FROM orders o left join orders_details od on od.orders_id = o.id
            left join product_seller ps on ps.id = od.product_seller_id
            left join products p on p.id = ps.product_id
            left join sellers s on s.id = ps.seller_id
            where s.id = ? `;

            if (status !== undefined && status !== null && status != 99) {
                sql += `and o.status = ${status} `;
                filterSQL += `and o.status = ${status} `;
                priceSQL += `and o.status = ${status} `;
            }
            if (orderCode) {
                sql += `and o.order_code like '%${orderCode}%' `;
                filterSQL += `and o.order_code like '%${orderCode}%' `;
                priceSQL += `and o.order_code like '%${orderCode}%' `;
            }
            if (productName) {
                sql += `and p.product_name like '%${productName}%' `;
                filterSQL += `and p.product_name like '%${productName}%' `;
            }
            if (fromDate) {
                //console.log(fromDate);
                // let date = new Date(fromDate);
                // const from = date.getTime();
                // sql += `and datediff(date, FROM_UNIXTIME(o.buy_time), FROM_UNIXTIME('${fromDate}') >=0) `;
                // filterSQL += `and o.buy_time >= '${fromDate}' `;
                // priceSQL += `and o.buy_time >= '${fromDate}' `;

                sql += `and o.buy_time >= '${fromDate}' `;
                filterSQL += `and o.buy_time >= '${fromDate}' `;
                priceSQL += `and o.buy_time >= '${fromDate}' `;
            }
            if (toDate) {
                // let date = new Date(toDate);
                // date.setDate(date.getDate() + 1);
                // const to = date.getTime();
                // console.log(to);
                sql += `and o.buy_time <= '${toDate}' `;
                filterSQL += `and o.buy_time <= '${toDate}' `;
                priceSQL += `and o.buy_time <= '${toDate}' `;
            }
            sql += ` group by od.orders_id order by ${orderBy}  limit ${pageSize} offset ${offset}`;
            filterSQL += ` group by od.orders_id order by ${orderBy} ) as count`;



            // if (orderBy) {
            //     sql = sql + " ORDER BY " + orderBy + " LIMIT ? OFFSET ?";
            // } else {
            //     sql = sql + " LIMIT ? OFFSET ?";
            // }

            let params = [token.userId];

            // let filter = new Filter().Where([{ "sellerId": token.userId }, { "status": { gte: 2 } }], pageFilter);


            let count = await Orders.Count(filterSQL, params);
            //  let totalRecords = await Orders.count(filter);
            //console.log(sql);

            var ds = Orders.dataSource;

            let totalPrice = 0;

            await ds.connector.query(priceSQL, params, function (error, data) {
                totalPrice = data;

                ds.connector.query(sql, params, function (err, instance) {
                    if (err) {
                        result.setError(ResultsCodes.DATA_NOT_FOUND);
                        return cb(null, result);
                    }
                    result.setSuccess({ totalRecords: count[0].totalRecords, pageSize: pageSize, pageIndex: pageIndex, pageData: instance, totalPrice: totalPrice, message: 'Thanh cong' });
                    cb(null, result);
                });
            });


        }
        makeRequest().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    /**
     * Get order list for admin, using pagging
     * Updated by HungNV
     * Date: 14/09/2019
     * @param {*} pageIndex 
     * @param {*} pageSize 
     * @param {*} pageOrder 
     * @param {*} pageFilter 
     * @param {*} cb 
     */
    Orders.GetListAll = function (shopName, orderCode, productName, fromDate, toDate, status, ctx, cb) {

        let pageIndex = ctx.query.pageIndex || 1;
        let pageSize = ctx.query.pageSize || 10;
        let pageOrder = ctx.query.pageOrder || null;
        let pageFilter = ctx.query.pageFilter || null;

        let result = new Results();
        const makeRequest = async () => {
            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            if (pageSize) {
                if (pageSize <= 0) {
                    pageSize = 10;
                }
            } else {
                pageSize = 10;
            }

            if (pageIndex) {
                if (pageIndex < 1) {
                    pageIndex = 1;
                }
            } else {
                pageIndex = 1;
            }

            let orderBy = "";
            let offset = (pageIndex - 1) * pageSize;

            // if (!pageOrder || pageOrder == 'undefined' || pageOrder == 'null') pageOrder = ['id desc'];


            if (!pageOrder || pageOrder === 'undefined') {
                orderBy = "orderTime DESC"
            } else {

                for (let i = 0; i < pageOrder.length; i++) {
                    let sortArr = pageOrder[i].match(/[^\s-]+-?/g);

                    if (sortArr.length == 2) {
                        let sortName = sortArr[0];
                        let direction = sortArr[1];

                        if ("id" === sortName.toLowerCase()) {

                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "id" + direction;

                        } if ("ordertime" === sortName.toLowerCase()) {

                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "orderTime" + direction;

                        }

                        if (i < pageOrder.length - 1 && orderBy) {
                            orderBy += ", ";
                        }
                    }
                }
            }

            let sql = `SELECT s.shop_name shopName, o.id id, o.order_code orderCode, count(od.orders_id) items, sum(od.price*amount) amount, o.buy_time orderTime, o.status status FROM orders o left join orders_details od on od.orders_id = o.id
            left join product_seller ps on ps.id = od.product_seller_id
            left join products p on p.id = ps.product_id
            left join sellers s on s.id = ps.seller_id
            where 1 = 1 `;

            let filterSQL = `select count(*) totalRecords from (SELECT s.shop_name shopName, o.id id, o.buy_time orderTime, o.status status FROM orders o left join orders_details od on od.orders_id = o.id
            left join product_seller ps on ps.id = od.product_seller_id
            left join products p on p.id = ps.product_id
            left join sellers s on s.id = ps.seller_id
            where 1 = 1 `;

            let priceSQL = `SELECT sum(o.total_price) totalPrice
            FROM orders o left join orders_details od on od.orders_id = o.id
            left join product_seller ps on ps.id = od.product_seller_id
            left join products p on p.id = ps.product_id
            left join sellers s on s.id = ps.seller_id
            where 1 = 1 `;

            if (shopName) {
                sql += `and s.shop_name like '%${shopName}%' `;
                filterSQL += `and s.shop_name like '%${shopName}%' `;
                priceSQL += `and s.shop_name like '%${shopName}%' `;
            }
            if (status !== undefined && status !== null && status !== 99) {
                sql += `and o.status = ${status} `;
                filterSQL += `and o.status = ${status} `;
                priceSQL += `and o.status = ${status} `;
            }
            if (orderCode) {
                sql += `and o.order_code like '%${orderCode}%' `;
                filterSQL += `and o.order_code like '%${orderCode}%' `;
                priceSQL += `and o.order_code like '%${orderCode}%' `;
            }
            if (productName) {
                sql += `and p.product_name like '%${productName}%' `;
                filterSQL += `and p.product_name like '%${productName}%' `;
            }
            //console.log(fromDate);
            //console.log(toDate);
            if (fromDate) {
                // let date = new Date(fromDate);
                // const from = date.getTime();

                sql += `and o.buy_time >= '${fromDate}' `;
                filterSQL += `and o.buy_time >= '${fromDate}' `;
                priceSQL += `and o.buy_time >= '${fromDate}' `;
            }
            if (toDate) {
                // let date = new Date(toDate);
                // date.setDate(date.getDate() + 1);
                // const to = date.getTime();
                sql += `and o.buy_time <= '${toDate}' `;
                filterSQL += `and o.buy_time <= '${toDate}' `;
                priceSQL += `and o.buy_time <= '${toDate}' `;
            }
            sql += ` group by s.shop_name, o.id, o.buy_time, o.status order by ${orderBy}  limit ${pageSize} offset ${offset}`;
            filterSQL += ` group by s.shop_name, o.id, o.buy_time, o.status order by ${orderBy} ) as count`;



            // if (orderBy) {
            //     sql = sql + " ORDER BY " + orderBy + " LIMIT ? OFFSET ?";
            // } else {
            //     sql = sql + " LIMIT ? OFFSET ?";
            // }

            let params = [token.userId];

            // let filter = new Filter().Where([{ "sellerId": token.userId }, { "status": { gte: 2 } }], pageFilter);


            let count = await Orders.Count(filterSQL, params);
            //  let totalRecords = await Orders.count(filter);

            var ds = Orders.dataSource;

            let totalPrice = 0;

            await ds.connector.query(priceSQL, params, function (error, data) {
                totalPrice = data;

                ds.connector.query(sql, params, function (err, instance) {
                    if (err) {
                        result.setError(ResultsCodes.DATA_NOT_FOUND);
                        return cb(null, result);
                    }
                    result.setSuccess({ totalRecords: count[0].totalRecords, pageSize: pageSize, pageIndex: pageIndex, pageData: instance, totalPrice: totalPrice, message: 'Thanh cong' });
                    cb(null, result);
                });
            });

        }
        makeRequest().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    /**
     * Created by Thiep Wong
     * Get details of an order by orderId
     * @param {*} id 
     * @param {*} cb 
     */
    Orders.GetDetails = function (id, ctx, cb) {
        var order;
        const makeRequest = async () => {
            let Passport = Orders.app.models.Passport;

            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            order = await Orders.findById(id, {
                fields: ['id', 'orderCode', 'buyTime', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'status', 'paymentType', 'memberId'],
                include: [{
                    relation: "location",
                    scope: {
                        fields: ['id', 'code', 'path']
                    }
                }, {
                    relation: 'shippingBills',
                    scope: {
                        fields: ['id', 'sellerId', 'shipCode', 'shipTime', 'shipFee', 'billPrice', 'status',  'shipUnitId', 'shippingType','statusDelivery','label_id'],
                        where: {
                            sellerId: token.userId
                        },
                        order: 'createTime DESC'
                    }
                }]
                // include :["location","member","shippingBills"]
            });

            let Details = Orders.app.models.OrdersDetails;
            let _detail = await Details.GetDetailById(order.id, token.userId);

            let ServicesModel = Orders.app.models.Services;
            //  console.dir(_detail);
            // let location = await Orders.app.models.Location.findOne({ filter: { fields: ['id', 'path', 'code'], where: { 'code': order.buyerLocationCode } } })
            new Relation(order, 'id');
            order.FetchDetail(_detail, 'products');

            let _shipBills = JSON.parse(JSON.stringify(order)).shippingBills;
            // console.log(_shipBills);      
            let ships = []

            //Nếu khác gian hàng tự giao thì tinh Ship


            // if (_shipBills[0].shipUnitId == 2 && _shipBills[0].shippingType == 0) {
               order.FetchDetail(_shipBills, 'shippingBillss');
            //     //order.FetchDetail(order.shippingBills, 'shippingBillss')
            //     //console.log(order.shippingBillss);
            // }
            // else {
            //     if (_shipBills) {
            //         const allResult = await Promise.all(_shipBills.map(_element => ServicesModel.checkShipmentStatusTest(_element.label_id)));
            //         ships = await Promise.all(_shipBills.map(_element => Orders.ShippingBillsMap(_element, allResult)))
            //         order.FetchDetail(ships, 'shippingBillss');

            //     }
            // }

            // console.log(ships);  


            let result = new Results();
            result.setSuccess(order);
            console.log(order);
            cb(null, result);
        }
        makeRequest().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    // Orders.ShippingBillsMap = (ship, ghtkResult) => {
    //     return new Promise((res, rej) => {
    //         ghtkResult.forEach(item => {
    //             if (item['label_id'] == ship['label_id']) {
    //                 ship['ghtk_status'] = item['status_text']
    //                 res(ship)
    //             }
    //         })

    //         rej(null)
    //     })

    // }

    Orders.validateTrackingMyOrder = function (ctx, cb) {
        let buyerPhone = ctx.query.buyerPhone || null;
        let orderCode = ctx.query.orderCode || null;

        let result = new Results();
        if (!buyerPhone || !orderCode) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }

        if (orderCode.length < 5) {
            result.setError()
            let error = ResultsCodes.VALIDATION_ERROR;
            error.message = 'orderCode không được ít hơn 5 ký tự';
            result.setError(error);
            return cb(null, result);
        } else if (orderCode.length > 36) {
            let error = ResultsCodes.VALIDATION_ERROR;
            error.message = 'orderCode không được vượt quá 36 ký tự';
            result.setError(error);
            return cb(null, result);
        }


        orderCode = orderCode.toLowerCase().trim();
        const makeRequest = async () => {

            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            let where = '';
            if (!token) {
                where = { and: [{ 'orderCode': { like: (orderCode + '%') } }, { 'buyerPhone': buyerPhone }] };
            } else {
                where = { and: [{ 'orderCode': { like: (orderCode + '%') } }, { 'memberId': token.userId }] }
            }

            Orders.findOne({
                fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status'],
                include: [{
                    relation: 'member',
                    scope: {
                        fields: ['id', 'fullname']
                    }
                }, {
                    relation: 'shippingBills',
                    scope: {
                        fields: ['id', 'sellerId', 'sellerStoreId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'shipTime', 'billPrice', 'shipFee', 'status', 'note', 'label_id'],
                        include: [{
                            relation: 'ordersDetails',
                            scope: {
                                fields: ['id', 'shippingBillId', 'productSellerId', 'affiliateId', 'price', 'unit', 'amount'],
                                include: [{
                                    relation: 'productSeller',
                                    scope: {
                                        fields: ['id', 'sellerId', 'productId', 'basePrice', 'total', 'aspect', 'shortDescription', 'status', 'serviceAreaLevel'],
                                        include: [{
                                            relation: 'product',
                                            scope: {
                                                fields: ['id', 'categoryId', 'brandId', 'productName', 'productImage'],
                                                include: [{
                                                    relation: 'category',
                                                    scope: {
                                                        fields: ['id', 'title']
                                                    }
                                                }]
                                            }
                                        }]
                                    }
                                }]
                            }
                        }, {
                            relation: 'shippingHistories',
                            scope: {
                                fields: ['id', 'shippingBillId', 'status', 'shortDescription', 'createTime'],
                                order: 'status DESC'
                            }
                        }, {
                            relation: 'seller',
                            scope: {
                                fields: ['id', 'shopName', 'logoImage']
                            }
                        }, {
                            relation: 'shipUnit',
                            scope: {
                                fields: ['name', 'logoImage', 'logoImage']
                            }
                        }]
                    }
                }],
                where: where
            }, function (err, instance) {

                if (err) {
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                } else {

                    if (instance) {
                        result.setSuccess(instance);
                    } else {
                        result.setSuccess({});
                    }

                    return cb(null, result);
                }
            })
        }

        makeRequest().catch(function (e) {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }
    
    Orders.GetListByMember = function (ctx, cb) {

        let status = ctx.query.status || 99;
        let pageIndex = ctx.query.pageIndex || 1;
        let pageSize = ctx.query.pageSize || 10;
        let pageOrder = ctx.query.pageOrder || null;

        let result = new Results();
        const makeGetListByMember = async () => {
            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            var memberId = token.userId;
            var where = {
                "memberId": memberId
            };
            console.log(status);

            if (status == 99) {
                where = {
                    "memberId": memberId
                }
            }
            else {
                where = {
                    "memberId": memberId,
                    "status": status // cancel, finish
                }
            }
            console.log(where);
            // else {
            //     where = {
            //         "memberId": memberId,
            //         "status": { inq: [2, 5] } // cancel, finish
            //     }
            // }

            Orders.find({
                fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status'],
                include: [{
                    relation: 'member',
                    scope: {
                        fields: ['id', 'fullname']
                    }
                }, {
                    relation: 'shippingBills',
                    scope: {
                        fields: ['id', 'sellerId', 'sellerStoreId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'shipTime', 'billPrice', 'shipFee', 'status', 'note', 'label_id'],
                        include: [{
                            relation: 'ordersDetails',
                            scope: {
                                fields: ['id', 'shippingBillId', 'productSellerId', 'affiliateId', 'price', 'unit', 'amount'],
                                include: [{
                                    relation: 'productSeller',
                                    scope: {
                                        fields: ['id', 'sellerId', 'productId', 'basePrice', 'total', 'aspect', 'shortDescription', 'status', 'serviceAreaLevel'],
                                        include: [{
                                            relation: 'product',
                                            scope: {
                                                fields: ['id', 'categoryId', 'brandId', 'productName', 'productImage'],
                                                include: [{
                                                    relation: 'category',
                                                    scope: {
                                                        fields: ['id', 'title']
                                                    }
                                                }]
                                            }
                                        }]
                                    }
                                }]
                            }
                        }, {
                            relation: 'shippingHistories',
                            scope: {
                                fields: ['id', 'shippingBillId', 'status', 'shortDescription', 'createTime'],
                                order: 'status DESC'
                            }
                        }, {
                            relation: 'seller',
                            scope: {
                                fields: ['id', 'shopName', 'logoImage']
                            }
                        }, {
                            relation: 'shipUnit',
                            scope: {
                                fields: ['name', 'logoImage', 'logoImage']
                            }
                        }]
                    }
                }],
                where: where,
                limit: pageSize,
                skip: pageIndex * pageSize,
                order: pageOrder
            }, function (err, instance) {
                //console.log(instance);
                result.setSuccess(instance);
                //console.log(instance);
                return cb(null, result);
            })
        }
        makeGetListByMember().catch(function (e) {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });;
    }

    Orders.GetListBySmartLife = function (shopName, from, to, status, pageIndex, pageSize, pageOrder, cb) {

        const makeGetListBySmartLife = async () => {
            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetMemberToken(from);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            let memberId = token.userId;
            let where = null;

            if (pageSize) {
                if (pageSize <= 0) {
                    pageSize = 10;
                }
            } else {
                pageSize = 10;
            }

            if (pageIndex) {
                if (pageIndex < 1) {
                    pageIndex = 1;
                }
            } else {
                pageIndex = 1;
            }

            if (shopName && /\S/.test(shopName)) {

                let sql = `SELECT DISTINCT
                    orders.id, orders.create_time
                FROM
                    sellers
                        JOIN
                    product_seller ON sellers.id = product_seller.id
                        JOIN
                    orders_details ON orders_details.shipping_bill_id = orders_details.id
                        JOIN
                    orders ON orders.id = orders_details.orders_id
                WHERE
                    sellers.shop_name LIKE ?
                    AND orders.create_time > ?
                    AND orders.create_time < ?
                    AND orders.status = ?`;

                let params = [shopName + '%', from, to, status];

                sql = sql + " ORDER BY orders.create_time DESC LIMIT ? OFFSET ?";
                params.push(pageSize, (pageIndex - 1) * pageSize);

                let connector = Orders.dataSource.connector;

                let results = await new Promise((resolve, reject) => {

                    connector.query(sql, params, function (err, data) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(data);
                        }
                    });
                });

                if (results.length > 0) {
                    let sup = results.map(obj => obj.id);
                    if (status == 0) {
                        where = {
                            "status": { inq: [0] },
                            "id": { inq: [sup] }
                        }
                    } else {
                        where = {
                            "status": { inq: [1] },
                            "id": { inq: [sup] }
                        }
                    }
                } else {
                    let result = new Results();
                    result.setSuccess([]);
                    return cb(null, result);
                }
            } else {
                if (status == 0) {
                    where = {
                        // "memberId": memberId,
                        "status": { inq: [0] },
                        "createTime": { between: [from, to] }
                    }
                }
                else {
                    where = {
                        // "memberId": memberId,
                        "status": { inq: [1] },
                        "createTime": { between: [from, to] }
                    }
                }

            }

            Orders.find({
                fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
                include: [{
                    relation: 'member',
                    scope: {
                        fields: ['id', 'fullname', 'phone', 'address']
                    }
                }, {
                    relation: 'shippingBills',
                    scope: {
                        fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
                        include: [{
                            relation: 'seller',
                            scope: {
                                fields: ['id', 'shopName', 'fullname']
                            }
                        }, {
                            relation: 'ordersDetails',
                            scope: {
                                fields: ['id', 'shippingBillId', 'productSellerId', 'affiliateId', 'price', 'unit', 'amount'],
                                include: [{
                                    relation: 'productSeller',
                                    scope: {
                                        fields: ['id', 'sellerId', 'productId', 'basePrice', 'total', 'aspect', 'shortDescription', 'status', 'serviceAreaLevel'],
                                        include: [{
                                            relation: 'product',
                                            scope: {
                                                fields: ['id', 'productName', 'productImage']
                                            }
                                        }]
                                    }
                                }]
                            }
                        }, {
                            relation: 'shippingHistories',
                            scope: {
                                fields: ['id', 'shippingBillId', 'status', 'shortDescription', 'createTime'],
                                order: 'status DESC'
                            }
                        }, {
                            relation: 'shipUnit',
                            scope: {
                                fields: ['name', 'logoImage', 'logoImage']
                            }
                        }]
                    }
                }],
                where: where,
                limit: pageSize,
                skip: (pageIndex - 1) * pageSize,
                order: 'createTime DESC'
            }, function (err, instance) {
                let result = new Results();
                result.setSuccess(instance);
                return cb(null, result);
            })
        }
        makeGetListBySmartLife().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    Orders.ConfirmBySmartLife = function (orderId, cb) {
        // const makeConfirmBySmartLife = async () => {
        //     let Passport = Orders.app.models.Passport;
        //     let token = await Passport.GetMemberToken(ctx);
        //     if (!token) {
        //         let result = new Results();
        //         result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        //         return cb(null, result);
        //     }

        app.dataSources.mysqld.transaction(async models => {
            const { Orders, ShippingBill } = models;
            // var smartLifeId = token.userId;
            var smartLifeId = 999;
            var currentTime = parseInt(new Date().getTime() / 1000.0);

            const orders = await Orders.findOne({
                // fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
                include: [{
                    relation: 'shippingBills',
                    scope: {
                        fields: ['id']
                    }
                }],
                where: {
                    "id": orderId
                }
            })

            if (orders == null) {
                throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
            }

            if (orders.status == ShippingCodes.DA_DAT_HANG.status) {
                var shippingBills = orders.toJSON().shippingBills;

                for (var item in shippingBills) {
                    const shippingBill = await ShippingBill.findOne({
                        // fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
                        where: {
                            "id": shippingBills[item].id
                        }
                    });

                    if (shippingBill == null) {
                        throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
                    }

                    shippingBill.status = ShippingCodes.XAC_NHAN_DON_HANG.status;
                    shippingBill.modifyTime = currentTime;
                    shippingBill.modifyBy = smartLifeId;
                    await shippingBill.save();

                    let _shippingHistory = Orders.app.models.ShippingHistory;
                    await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.XAC_NHAN_DON_HANG, null);
                }

                orders.status = OrdersCodes.XAC_NHAN_DON_HANG.status;
                orders.modifyTime = currentTime;
                orders.modifyBy = smartLifeId;
                await orders.save();
            }

            let result = new Results();
            result.setSuccess(true);
            return cb(null, result);

        }).catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
            return cb(null, result);
        });
        // }
        // makeConfirmBySmartLife();
    }

    Orders.CancelOrders = function (ctx, cb) {
        let orderId = ctx.body.orderId || 0;
        let result = new Results();
        if (!orderId) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }
        const makeCancelOrders = async () => {
            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            app.dataSources.mysqld.transaction(async models => {
                const { Orders, ShippingBill, OrdersDetails, ShippingHistory } = models;
                var memberId = token.userId;
                var currentTime = parseInt(new Date().getTime() / 1000.0);

                const orders = await Orders.findOne({
                    // fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
                    include: [{
                        relation: 'shippingBills',
                        scope: {
                            fields: ['id']
                        }
                    }],
                    where: {
                        "id": orderId
                    }
                })

                if (orders == null) {
                    throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
                }

                if (orders.status < ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                    var shippingBills = orders.toJSON().shippingBills;

                    for (var item in shippingBills) {
                        const shippingBill = await ShippingBill.findOne({
                            fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note', 'createTime', 'createBy', 'modifyTime', 'modifyBy', 'label_id'],
                            where: {
                                "id": shippingBills[item].id
                            }
                        });

                        if (shippingBill == null) {
                            throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
                        }

                        // call api cancel shipment
                        await cancelShipping(shippingBill.shippingBillId, shippingBill.label_id);

                        shippingBill.status = ShippingCodes.DA_HUY.status;
                        shippingBill.modifyTime = currentTime;
                        shippingBill.modifyBy = memberId;
                        await shippingBill.save();

                        let _shippingHistory = Orders.app.models.ShippingHistory;
                        await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.DA_HUY, null);
                    }

                    orders.status = OrdersCodes.DA_HUY.status;
                    orders.modifyTime = currentTime;
                    orders.modifyBy = memberId;
                    await orders.save();

                    // Create notification
                    //let Notification = Orders.app.models.Notification;
                    //await Notification.createNotification(memberId, orders.id, shippingBill.id, null, OrdersCodes.DA_HUY.status);

                    var Notification = ShippingBill.app.models.Notification;

                    Notification.destroyAll({ memberId: orders.memberId }, { orderId: orders.id });
                    //let notification = await Notification.find();
                    //console.log(notification);
                    await Notification.createNotification(orders.memberId, orders.id, shippingBills.id, orders.orderCode, OrdersCodes.DA_HUY.status);



                    result.setSuccess(true);
                    return cb(null, result);
                }
                else {
                    result.setSuccess(false);
                    result.setWarning(ResultsCodes.ORDERS_CANNOT_CANCEL);
                    return cb(null, result);
                }
            }).catch(function (e) {
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                return cb(null, result);
            });
        }
        makeCancelOrders().catch(function (e) {
            result.setError(ResultsCodes.ORDERS_CANNOT_CANCEL);
            return cb(null, result);
        });;
    }

    // huy ship đơn hàng
    var cancelShipping = async function (shippingBillId, labelId) {
        let options = {
            method: 'POST',
            url: Config.baseUrl + `/services/shipment/cancel/${labelId}`,
            headers:
            {
                Connection: 'keep-alive',
                Host: Config.domain,
                Accept: '*/*',
                'Content-Type': 'application/json',
                Token: Config.api_token
            }
        };
        // console.log(options);

        var res = http(options, async (err, res, body) => {
            if (err) {
                return 0;
            }
            if (body) {
                if (body.success) {
                    return 1;
                }
            }
        });
    }

    Orders.GetOrderReportBySeller = (ctx, cb) => {

        let t = ctx.query.t || new Date().getTime();
        let result = new Results();

        const makeRequest = async () => {
            let Passport = Orders.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token.userId) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            let ShippingBill = Orders.app.models.ShippingBill;

            let _order = await ShippingBill.find({
                where: {
                    status: 0,
                    sellerId: token.userId
                }
            });

            let _orderReport = {

                total: 0,
                in24h: 0,
                in12h: 0,
                in1h: 0
            }
            _orderReport.total = _order.length;
            _order.forEach(item => {

                let _time = new Date().getTime() / 1000 - item.createTime;

                if (_time <= 3600) {

                    _orderReport.in1h++;
                }

                if (_time <= 3600 * 12) {
                    _orderReport.in12h++;
                }

                if (_time <= 3600 * 24) {
                    _orderReport.in24h++;
                }
            })

            result.setSuccess(_orderReport);
            cb(null, result);
        }

        makeRequest().catch(err => {
            let result = new Results();
            result.setError(err);
            return cb(null, result);
        })


    }


    Orders.GetListNotifications = function (memberUuid, ctx, cb) {
        let result = new Results();
        if (!memberUuid || !ctx) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }
        const makeGetListNotifications = async () => {
            let Members = Orders.app.models.Members;
            let member = await Members.findOne({
                where: { uuid: memberUuid }
            });

            // console.log(member);

            let Notification = Orders.app.models.Notification;
            let notifications = await Notification.find({
                // where: { memberId: member.id }
            });

            for (let item of notifications) {
                if (member.id === item.memberId) {
                    var arrayFilter = notifications.filter(item => item.memberId === member.id);
                    var count = arrayFilter.length;
                }
                // console.log(count);
            };

            // console.log(notifications);

            notifications = arrayFilter;

            // result["count"] = count;

            // result.setSuccess(notifications);

            result.setSuccess({ totalCount: count, pageData: notifications });
            // result.setSuccess(count);


            return cb(null, result);
        }
        makeGetListNotifications().catch(function (e) {
            console.log(e);
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    Orders.RemoveNotification = function (id, ctx, cb) {
        let result = new Results();
        if (!id || !ctx) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }
        const makeGetListNotifications = async () => {
            let Notification = Orders.app.models.Notification;
            let rs = await Notification.removeNotification(id);

            if (rs.errors) {
                cb(null, rs);
            }
            result.setSuccess(true);
            return cb(null, result);
        }
        makeGetListNotifications().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }



    // Orders.GetNotificationById = function (id, ctx, cb) {
    //     let result = new Results();
    //     if (!id || !ctx) {
    //         result.setError(ResultsCodes.ARG_NOT_FOUND);
    //         return cb(null, result);
    //     }
    //    // const makeGetListNotifications = async () => {
    //         let Notification = Orders.app.models.Notification;
    //         //let rs = await Notification.findById(id);

    //         Notification.findById(id, (err, resBanner) => {
    //             if (err) {
    //                 result.setError(Msg.SYSTEM_ERROR);
    //                 return cb(null, result);
    //             }

    //             if (!resBanner) {
    //                 result.setError(Msg.DATA_NOT_FOUND);
    //                 return cb(null, result);
    //             }
    //             else
    //             {
    //                 result.setSuccess(resBanner);
    //                 return cb(null, result);
    //             }
    //         })

    //         // if (rs.errors) {
    //         //     cb(null, rs);
    //         // }
    //         // result.setSuccess(true);
    //         // return cb(null, result);
    //     // }
    //     // makeGetListNotifications().catch(function (e) {
    //     //     let result = new Results();
    //     //     result.setError(ResultsCodes.SYSTEM_ERROR);
    //     //     return cb(null, result);
    //     // });
    // }


    //#endregion

    //#region Expose Parameters

    Orders.remoteMethod('sendMSG', {
        http: {
            verb: 'GET'
        },
        accepts: [{
            arg: 'content',
            type: 'string'
        }],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Orders.sendMSG = (content, cb) => {
        let a = Orders.SendMQTT(content);
        cb(null, a);
    }

    Orders.remoteMethod('CreateNew', {
        description: '@Luong, unstable, in developing, do not change it if unclear! Update by Thiep',
        http: {
            verb: 'POST',
            path: '/createnew'
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    Orders.remoteMethod('GetListByMember', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    Orders.remoteMethod('validateTrackingMyOrder', {
        description: '@Hung Doan, validate tracking my order by order code',
        http: {
            verb: "GET",
            path: '/validate-tracking-my-order'
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Orders.remoteMethod('GetListBySmartLife', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: "shopName",
                type: "string"
            }, {
                arg: "from",
                type: "number",
                required: true
            }, {
                arg: "to",
                type: "number",
                required: true
            }, {
                arg: "status",
                type: "number",
                required: true
            }, {
                arg: "pageIndex",
                type: "number"
            }, {
                arg: "pageSize",
                type: "number"
            }, {
                arg: "pageOrder",
                type: "string"
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    Orders.remoteMethod('ConfirmBySmartLife', {
        accepts: [{
            arg: "orderId",
            type: "number",
            required: true
        }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }
        ]
    })

    Orders.remoteMethod('CancelOrders', {
        http: {
            verb: 'POST'
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    Orders.remoteMethod('GetListBySeller', {
        description: '@Thiep Wong, get orders list by sellers id',
        http: {
            verb: 'GET',
            path: '/seller-orders'
        },
        accepts: [
            {
                arg: 'orderCode',
                type: 'string'
            },
            {
                arg: 'productName',
                type: 'string'
            },
            {
                arg: 'fromDate',
                type: 'number'
            },
            {
                arg: 'toDate',
                type: 'number'
            },
            {
                arg: 'status',
                type: 'number'
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }

        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Orders.remoteMethod('GetListAll', {
        description: '@HungNV, get all orders for admin',
        http: {
            verb: 'GET',
            path: '/all-orders'
        },
        accepts: [
            {
                arg: 'shopName',
                type: 'string'
            },
            {
                arg: 'orderCode',
                type: 'string'
            },
            {
                arg: 'productName',
                type: 'string'
            },
            {
                arg: 'fromDate',
                type: 'number'
            },
            {
                arg: 'toDate',
                type: 'number'
            },
            {
                arg: 'status',
                type: 'number'
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }

        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Orders.remoteMethod('GetOrderReportBySeller', {
        description: '@Thiep Wong, get orders reports for seller home page ',
        http: {
            verb: 'GET',
            path: '/orders-report'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Orders.remoteMethod('GetDetails', {
        description: '@Thiep Wong, get details of an order for sellers',
        http: {
            verb: 'GET',
            path: '/details/:id'
        },
        accepts: [
            {
                arg: 'id',
                type: 'number'
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }

        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Orders.remoteMethod('GetListNotifications', {
        description: '@HungNV, get list notifications of orders for members',
        http: {
            verb: 'POST',
            path: '/notifications/:memberUuid'
        },
        accepts: [
            {
                arg: 'memberUuid',
                type: 'string'
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Orders.remoteMethod('RemoveNotification', {
        description: '@HungNV, remove a notification of orders for members',
        http: {
            verb: 'GET',
            path: '/notifications/:id'
        },
        accepts: [
            {
                arg: 'id',
                type: 'string'
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    var checkShipmentStatus1111 = function (labelId) {
        var options = {
            method: 'GET',
            url: 'https://dev.ghtk.vn/services/shipment/v2/' + labelId,
            headers:
            {
                Connection: 'keep-alive',
                Host: Config.domain,
                Accept: '*/*',
                'Content-Type': 'application/json',
                Token: 'Efb1cEf6f783Fe16f7b2625409785aDcCE70345b'
            }
        };

        return new Promise((resolve, reject) => {
            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                }
                resolve(JSON.parse(body).order);
            });

        })
    }


    // Orders.remoteMethod('GetNotificationById', {
    //     description: '@ThienVD, get a notification by id',
    //     http: {
    //         verb: 'GET',
    //         path: '/getNoti/:id'
    //     },
    //     accepts: [
    //         {
    //             arg: 'id',
    //             type: 'string'
    //         },
    //         {
    //             arg: 'ctx',
    //             type: 'object',
    //             http: {
    //                 source: 'req'
    //             }
    //         }
    //     ],
    //     returns: {
    //         arg: 'result',
    //         type: 'string',
    //         root: true
    //     }
    // })



    // #endregion
    // test
}