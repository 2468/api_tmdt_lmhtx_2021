/** Model OTP 
 * Create, Compaire, Send OTP
 * Author: Thiep Wong
 * Last update: 9.3.2018 - 21:07
**/

var http = require('request');
var json_result = require("xml2json");
var Results = require('../middlewares/ResultHandle');
var ResultCodes = require('../middlewares/message_constant'); 
var app = require('../../server/server');
var Sms = app.get('SMS');
var code = require('base-64');
module.exports = function (OTP) {

                 
    OTP.sharedClass.methods().forEach(function (method) {
        OTP.disableRemoteMethodByName(method.name, method.isStatic);
      });

    /**
     * @function CreateNewOTP create an otp code
     * @param {String} user the user instance of model member
     * @param {Object} cb callback function for return result
     */
    OTP.createNewOTP = function (user, cb) {
        if (!user) {
            var e = new Error('Uid do not exist! ');
            cb(e);
        }

        // Created a random otp code 
        var token = ("" + Math.random()).substring(2, 8);
        var res = {
            id: token,
            phone: user.phone,
            userid: user.id,
            created: parseInt(new Date().getTime() / 1000),
            expired: parseInt((new Date().getTime() / 1000) + 3000),  // 5 mins from now
            ttl: 300 // 5 mins
        } 
        OTP.create(res, function (err, otpInstance) {

            if (err) { 
                cb(err);
            } 
            cb(null,otpInstance);
        });  
    }


    OTP.destroyOTP = function (id, cb) {
        if (!id) {
            Results.success = {};
            Results.errors = {
                code: "E0004",
                name: "OTP_ID_NOT_VALID",
                message: "Mã OTP không đúng"
            }
            Results.warning = {};
            cb(null, Results);
        }
        else {
            OTP.deleteById(id, function (err) {
                if (err) {
                    Results.success = {};
                    Results.errors = {
                        code: "E0005",
                        name: "CANNOT_DESTROY_OTP",
                        message: "Không thể hủy mã OTP này"
                    }
                    Results.warning = {};
                    cb(null, Results);

                }
                else {
                    cb();
                }
            });
        }


    }


    OTP.IRISSend = function (smsPakage, cb) {
        if (!smsPakage) {
            return cb(null, 'Error!');
        }

        const makeRequest = async () => {

            let SMS = OTP.app.models.SMS;
            let _token;
            SMS.checkToken().then(data => {
                _token = data;
                let _now = parseInt(new Date().getTime() / 1000);
                if (_token && _token['timelife'] - _now > 0) {
                    let options = {
                        url: `${Sms.IRIS.host}${Sms.IRIS.path.sendSMS}`,
                        "rejectUnauthorized": false,
                        method: 'POST',
                        port: 80,
                        headers: {
                            'Authorization': `Bearer ${_token.token}`,
                            'Content-Type': 'application/json; charset=utf-8',
                        },
                        form:
                            {
                                "Brandname": "SMARTLIFE",
                                "SendingList": [
                                    {
                                        "SmsId": "SMS" + _now,
                                        "PhoneNumber": smsPakage.mobile,
                                        "Content": "Ma xac nhan OTP la: " + smsPakage.otp + "\r\nMa xac nhan chi su dung mot lan, co hieu luc trong vong 5 phut",
                                        "ContentType": "30"
                                    }
                                ]
                            }
                    };

                    var req = http(options, (err, res, body) => {

                        if (err) {
                            return cb(null, ResultCodes.SYSTEM_ERROR);
                        }
                        let data = JSON.parse(body);
                        cb(null, data);
                    });

                } else {
                    OTP.IRISLogin((err, res) => {
                        cb(null, res);
                    });
                }

            }).then(reject => {
                if (reject) {
                    cb(null, ResultCodes.SYSTEM_ERROR);
                }

            });

        }

        makeRequest();

    }


    OTP.IRISLogin = function (cb) {
        const makeRequest = async () => {

            let SMS = OTP.app.models.SMS;
            let encode = code.encode(Sms.IRIS.user + ':' + Sms.IRIS.password);
            let options = {
                url: `${Sms.IRIS.host}${Sms.IRIS.path.login}`,
                "rejectUnauthorized": false,
                method: 'POST',
                port: 80,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Basic ' + encode
                },
                form: { 'grant_type': 'password' }
            };

            var req = http(options, (err, res, body) => {

                if (err) {
                    let result = new Results();
                    result.setError(ResultCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }
                let data = JSON.parse(body);
                let sms = new SMS();
                // sms.uuid = 'SML-DFK-VVFS';
                sms.timelife = parseInt(new Date().getTime() / 1000) + data.expires_in;
                sms.token = data.access_token;
                sms.tokenType = data.token_type;

                SMS.upsert(sms, (err, res) => {
                    cb(null, res);
                });
            });

            req.on('error', function (e) {
                cb(null, e);
            });


        }
        makeRequest();
    }

}

  //"./middlewares/smart-errors-handle": {}