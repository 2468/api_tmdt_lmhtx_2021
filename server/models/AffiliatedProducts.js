'use strict';
var Results = require('../middlewares/smart-result-handle');
var ResultsCodes = require('../middlewares/message_constant');

module.exports = function (AffiliatedProducts) {



    //#region GetAllproductForAffiliate
    /**
     * @param {Number} pageIndex 
     * @param {Number} pageSize 
     * @param {Number} pageOrder 
     * @param {Function} cb 
     */
    AffiliatedProducts.GetAllProductsForAffiliate = function (pageIndex, pageSize, pageOrder, cb) {

        var sql = `SELECT 
          distinct ps.id , 
            pd.id pd_id,
            pd.category_id,
            pd.product_name,
            pd.product_image,
            pd.short_description,
            pd.description,
            pd.article,
            pd.unit,
            pd.weight,
            pd.dimension,
            br.id br_id,
            br.brand_name,
            ps.display_on_shop,
            ps.seller_id,
            ps.base_price,
            ps.affiliate_reward_rate,
            ps.aspect,
            ps.short_description,
            ps.service_area_level,
            sl.id sl_id,
            sl.shop_name,
            sl.fullname,
            sl.seller_type,
            sl.shop_address,
            sl.shop_location_id,
            sl.logo_image,
            sl.cover_image,
            pp.id pp_id,
            pp.product_seller_id,
            pp.price,
            pp.from,
            pp.to
    
        FROM
            products pd
                JOIN
            brands br ON br.id = pd.brand_id
                JOIN
            product_seller ps ON ps.product_id = pd.id
                JOIN
            sellers sl ON sl.id = ps.seller_id
                JOIN
            product_price pp ON pp.product_seller_id = ps.id
                
        WHERE
        ps.affiliate_reward_rate >0 
        and    
        ps.display_on_shop  =1
            
                AND pp.from <= ?
                AND pp.to >= ? `;


        var currentTime = parseInt(new Date().getTime() / 1000.0);
        var params = [currentTime, currentTime];

        if (pageSize) {
            if (pageSize <= 0) {
                pageSize = 10;
            }
        } else {
            pageSize = 10;
        }

        if (pageIndex) {
            if (pageIndex < 0) {
                pageIndex = 0;
            }
        } else {
            pageIndex = 0;
        }

        var orderBy = "";

        if (!pageOrder) {
            orderBy = "product_name desc"
        } else {

            for (var i = 0; i < pageOrder.length; i++) {
                var sortArr = pageOrder[i].match(/[^\s-]+-?/g);

                if (sortArr.length == 2) {
                    var sortName = sortArr[0];
                    var direction = sortArr[1];

                    if ("id" === sortName.toLowerCase()) {

                        if ("desc" === direction.toLowerCase()) {
                            direction = " desc";
                        } else {
                            direction = " asc";
                        }

                        orderBy += "pd.id" + direction;

                    } if ("brandId" === sortName.toLowerCase()) {

                        if ("desc" === direction.toLowerCase()) {
                            direction = " desc";
                        } else {
                            direction = " asc";
                        }

                        orderBy += "pd.brand_id" + direction;

                    } else if ("productname" === sortName.toLowerCase()) {

                        if ("desc" === direction.toLowerCase()) {
                            direction = " desc";
                        } else {
                            direction = " asc";
                        }

                        orderBy += "pd.product_name" + direction;

                    } else if ("categoryid" === sortName.toLowerCase()) {
                        if ("desc" === direction.toLowerCase()) {
                            direction = " desc";
                        } else {
                            direction = " asc";
                        }

                        orderBy += "pd.category_id" + direction;
                    }

                    if (i < pageOrder.length - 1 && orderBy) {
                        orderBy += ", ";
                    }
                }
            }
        }

        sql = sql + " ORDER BY " + orderBy + " LIMIT ? OFFSET ?";
        params.push(pageSize, pageIndex);

        var connector = AffiliatedProducts.dataSource.connector
        connector.query(sql, params, function (err, resultObjects) {

            if (err) {
                console.log(err);
                cb(err);
            }
            else {
                Results.errors = {};
                Results.warning = {};
                Results.success = resultObjects;
                cb(null, Results);
            }
        });

    }
    //#endregion

    //#region GetAffiliatedProducts
    AffiliatedProducts.GetAffiliatedProducts = function (pageIndex, pageSize, pageOrder, memberId, collectionId, cb) {

        if (memberId) {
            AffiliatedProducts.find({
                //    fields: ["id", 'productSellerId', 'sellerId'],
                include: {

                    relation: 'productSeller',

                    scope: {
                        //  fields: ['id', 'sellerId', 'productId']
                        include: [{
                            relation: 'product'
                        },
                        {
                            relation: 'productPrices',
                            scope: {
                                where: {
                                    id: 9
                                }
                            }
                        }
                        ]
                    }
                },
                where: {
                    memberId: memberId,
                    "or":
                        [
                            {
                                collectionId: 0
                            },
                            {
                                collectionId: null
                            }
                        ]

                },
                limit: pageSize,
                skip: pageIndex * pageSize,
                order: pageOrder

            }, function (err, _affProductInstance) {

                Results.errors = {};
                Results.warning = {};
                Results.success = {
                    data: _affProductInstance,
                    message: ResultsCodes.API_SUCCESSED,
                };
                cb(null, Results);

            });

        }
        else {
            Results.success = {};
            Results.warning = {};
            Results.errors = ResultsCodes.MEMBER_INFO_INVALID;

        }
    }
    //#region Remote Method define
    /**
     * 
     */
    AffiliatedProducts.remoteMethod('GetAllProductsForAffiliate', {
        http: {
            verb: 'GET',

        },
        accepts: [{
            arg: "pageIndex",
            type: "number"
        },
        {
            arg: "pageSize",
            type: "number"
        },
        {
            arg: "pageOrder",
            type: "array"
        }

        ],
        returns: {
            arg: "result",
            root: true
        }
    })

    AffiliatedProducts.remoteMethod('GetAffiliatedProducts', {
        http: {
            verb: "GET"
        },
        accepts: [{
            arg: "pageIndex",
            type: "number"
        },
        {
            arg: "pageSize",
            type: "number"
        },
        {
            arg: "pageOrder",
            type: "array"
        },
        {
            arg: "memberId",
            type: "number"
        },
        {
            arg: "collectionId",
            type: "number"
        }
        ],
        returns: {
            arg: "result",
            root: true
        }

    });

};
//#endregion

// pageIndex, pageSize, pageOrder