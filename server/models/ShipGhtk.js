var Results = new require('../middlewares/ResultHandle.js');
var ResultsCodes = require('../middlewares/message_constant.js');
var Config = require('../middlewares/GhtkApiConfig');
var request = require('request');
var statusGhtk = require('../middlewares/statusGhtk');
'use strict';

module.exports = function (Services) {

    //#region Functions

    Services.GetFeeTransport = function (pick_province, pick_district, province, district, address, weight, value, transport, cb) {

        let base_Url = Config.baseUrl;
        let access_token = Config.api_token;

        if (!pick_district || !pick_province || !province || !district || !weight) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }

        const makeInfoRequest = async () => {
            var options = {
                method: 'GET',
                // url: 'https://dev.ghtk.vn/services/shipment/fee',
                url: base_Url + 'services/shipment/fee',
                qs:
                {
                    pick_province: pick_province,
                    pick_district: pick_district,
                    province: province,
                    district: district,
                    weight: weight,
                    value: value,
                    address: address
                },
                headers:
                {
                    Connection: 'keep-alive',
                    Host: 'dev.ghtk.vn',
                    Accept: '*/*',
                    'Content-Type': 'application/json',
                    Token: access_token
                }
            };

            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                console.log(body);
                let result = new Results();
                let json = JSON.parse(body);
                result = json;
                return cb(null, result);
            });
        }

        makeInfoRequest().catch(function (e) {
            console.log(e);
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        })

    }

    Services.ShippingBillStatus = function (orderId, cb) {
        let result = new Results();

        const makeInfoRequest = async () => {
            var ShippingBill = Services.app.models.ShippingBill;
            let shippingBills = await ShippingBill.find({
                where: {
                    orderId: orderId
                }
            });
            // console.log(shippingBills);
            if (shippingBills) {
                const allResults = await Promise.all(shippingBills.map(_element => checkShipmentStatus(_element.label_id)));
                // console.log(allResults);

                for (let i = 0; i < shippingBills.length; i++) {
                    const item = shippingBills[i];
                    console.log(item);
                    let shipment = allResults.find(result => {
                        // console.log(result);
                        // console.log(item);
                        return result.label_id == item.label_id;
                    });
                    console.log(shipment);
                    let shippingBill = await ShippingBill.findOne({
                        where: {
                            id: item.id
                        }
                    });
                    if (shippingBill) {
                        shippingBill.statusDelivery = Number(shipment.status);
                        // console.log(shipment);
                        await shippingBill.save();
                    }
                }
                
                result.setSuccess(allResults);
                return cb(null, result);
            }
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cb(null, result);

        }
        makeInfoRequest();
    }

    var checkShipmentStatus = function (labelId) {
        var options = {
            method: 'GET',
            url: Config.baseUrl + 'services/shipment/v2/' + labelId,
            headers:
            {
                Connection: 'keep-alive',
                Host: Config.domain,
                Accept: '*/*',
                'Content-Type': 'application/json',
                Token: Config.api_token
            }
        };

        return new Promise((resolve, reject) => {
            request(options, function (error, response, body) {
                if (error) {
                    reject(error);

                }
                resolve(JSON.parse(body).order);
            });

        })
    }
    //#endregion

    //#region Expose Parameters

    Services.remoteMethod('GetFeeTransport', {
        http: {
            path: '/shipment/fee',
            verb: 'GET'
        },
        accepts: [
            {
                arg: "pick_province",
                type: "string",
            },
            {
                arg: "pick_district",
                type: "string"
            },
            {
                arg: "province",
                type: "string"
            },
            {
                arg: "district",
                type: "string"
            },
            {
                arg: "address",
                type: "string"
            },
            {
                arg: "weight",
                type: "number"
            },
            {
                arg: "value",
                type: "number"
            },
            {
                arg: "transport",
                type: "string"
            }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    Services.remoteMethod('ShippingBillStatus', {
        http: {
            path: '/shipment/:orderId',
            verb: 'GET'
        },
        accepts: [
            {
                arg: 'orderId',
                type: 'string'
            }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })
    //#endregion
}