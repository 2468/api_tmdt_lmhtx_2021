/**
 * Banner manager module
 * Author: Thiep Wong
 * Created: 08/12/2018
 * Manager all banner in EC of SmartLife
 */
'use strict';
const Result = require('../middlewares/ResultHandle');
const Msg = require('../middlewares/message_constant');

module.exports = function (BannerManager) {
    //#region RegisterBanner
    BannerManager.remoteMethod('RegisterBanner', {
        description: '@Thiep Wong, register images to platform',
        http: {
            verb: 'POST',
            path: '/register'
        },
        accepts: [
            {
                arg: 'bannerData',
                type: 'BannerManager',
                http: {
                    source: "body"
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    BannerManager.RegisterBanner = (bannerData, cb) => {
        let result = new Result();
        if (!bannerData) {
            result.setError(Msg.ARG_NOT_FOUND);
            return cb(null, result);
        }

        BannerManager.create(bannerData, (err, res) => {
            if (err) {
                result.setError(Msg.SYSTEM_ERROR);
                return cb(null, result);
            }

            result.setSuccess(res);
            cb(null, result);
        })


    }
    //#endregion

    //#region  Getbanner
    BannerManager.remoteMethod('GetBanner', {
        description: '@Thiep Wong, get list of banners',
        http: {
            verb: 'GET',
            path: '/list'
        },
        accepts: [
            {
                arg: 'platform',
                type: 'string'
            },
            {
                arg: 'type',
                type: 'number'
            },
            {
                arg: 'position',
                type: 'number'
            },
            {
                arg: 'cateId',
                type: 'number'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    BannerManager.GetBanner = (platform, type, position, cateId, cb) => {
        let result = new Result();
        let _platform = platform || 'E-SML';
        let _type = type || 1;
        let _position = position;
        let _cateId = cateId || 0;
        let status;

        let _where = _cateId > 1 ? { platform: _platform, type: _type, position: _position, categoryId: _cateId, status:1 } : { platform: _platform, type: _type, position: _position,status:1 };
        BannerManager.find({ where: _where, order: 'priority asc' }, (err, resBanner) => {
            if (err) {
                result.setError(Msg.DATA_NOT_FOUND);
                return cb(null, result);
            }

            result.setSuccess(resBanner);
            cb(null, result);
        })

    }

    //#endregion

    
    //#region  EditBanner  
    BannerManager.remoteMethod('Edit', {
        description: '@Thiep Wong, edit the banner',
        http: {
            verb: 'POST',
            path: '/edit/:id'
        },
        accepts: [
            {
                arg: 'id',
                type: 'string'
            },
            {
                arg: 'bannerData',
                type: 'object',
                http: {
                    source: 'body'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    BannerManager.Edit = (id, bannerData, cb) => {
        let result = new Result();
        if (!id) {
            result.setError(Msg.ARG_NOT_FOUND);
            return cb(null, result);
        }

        BannerManager.findById(id, (err, resBanner) => {
            if (err) {
                result.setError(Msg.SYSTEM_ERROR);
                return cb(null, result);
            }

            if (!resBanner) {
                result.setError(Msg.DATA_NOT_FOUND);
                return cb(null, result);
            }

            resBanner.updateAttributes(bannerData, (err, resData) => {
                if (err) {
                    result.setError(Msg.SYSTEM_ERROR);
                    return cb(null, result);
                }

                result.setSuccess(resData);
                cb(null, result);
            })


        })
    }

    //#endregion

    //#region  DeleteBanner
    BannerManager.remoteMethod('Delete', {
        description: '@Thiep Wong, delete banners ',
        http: {
            verb: 'delete',
            path: '/:id'
        },
        accepts: [
            {
                arg: 'id',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    BannerManager.Delete = (id, cb) => {
        let result = new Result();
        if (!id) {
            result.setError(Msg.ARG_NOT_FOUND);
            return cb(null, result);
        }
        BannerManager.findById(id, (err, resBanner) => {
            if (err) {
                result.setError(Msg.SYSTEM_ERROR);
                return cb(null, result);
            }

            if (!resBanner) {
                result.setError(Msg.DATA_NOT_FOUND);
                return cb(null, result);
            }

            resBanner.updateAttribute('status', -1, (err, resData) => {
                if (err) {
                    result.setError(Msg.SYSTEM_ERROR);
                    return cb(null, result);
                }
                result.setSuccess(Msg.ITEM_DELETED);
                cb(null, result);
            }) 
        }) 
    }

    //#endregion

    //#region GetOneBanner
    BannerManager.remoteMethod('GetOne', {
        description: '@Thiep Wong, get one of banner',
        http: {
            verb: 'GET',
            path: '/:id'
        },
        accepts: [
            {
                arg: 'id',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    BannerManager.GetOne = (id, cb) => {
        let result = new Result();
        if (!id) {
            result.setError(Msg.ARG_NOT_FOUND);
            return cb(null, result);
        }

        BannerManager.findById(id, (err, resBanner) => {
            if (err) {
                result.setError(Msg.SYSTEM_ERROR);
                return cb(null, result);
            }
            result.setSuccess(resBanner);
            cb(null, result);
        })

    }

    //#endregion
}