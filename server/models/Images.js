/**
 * Images upload process
 * Author: Thiep Wong
 * Created: 18/04/2018
 * Updated: 24/10/2018
 * Update images and save temporarily in uploads folder, then delete image after upload success
 */

var fs = require('fs')
var uuid = require('uuid/v4');
var fileUpload = require('formidable').IncomingForm;
var Results = require('./../middlewares/ResultHandle');
var ResultCodes = require('./../middlewares/message_constant');
var fs = require('fs');
var path = require('path');

var app = require('../../server/server');
var FTPConnect = app.get('FTP');

module.exports = function (Images) {

    /**
     * Rename the uploaded file
     * @param {*} file file after save to temp folder
     * @param {*} cb 
     */
    Images.rename = function (file, cb) {
        if (file.type.split('/')[0] != 'image') {
            return;
        }
        let extention = file.name.split('.');
        let result = uuid();
        result += '.';
        result += extention[extention.length - 1];
        return result;
    }

    /**
     * Upload image file method
     * @param {*} req request package from client
     * @param {*} cb 
     */
    Images.uploadImage = function (req, cb) {
        let result = new Results();
        const request = async () => {
            var data = new fileUpload();
            data.multiples = true;
            let response = {
                fileName: null,
                fileSize: 0,
                fileUrl: null
            }

            data.on('fileBegin', function (name, file) {
                let file_name = Images.rename(file, cb);
                if (file_name) {
                    file.path = path.resolve(__dirname, `../../server/uploads/${file_name}`);  // Edited@Thiep Wong, create uploads folder under root
                    var ftp = Images.app.models.FTP;
                    let dir = [new Date().getFullYear().toString(), ("0" + (new Date().getMonth() + 1)).slice(-2), ("0" + new Date().getDate()).slice(-2)];
                    let resPromise = ftp.upload(file.path, file_name, dir);
                    response.fileName = dir[0] + '/' + dir[1] + '/' + dir[2] + '/' + file_name;
                    response.fileUrl = FTPConnect.cdn + dir[0] + '/' + dir[1] + '/' + dir[2] + '/' + file_name;
                    resPromise.then((res) => {
                        fs.unlinkSync(file.path);
                        // Delete temporarily image after uploaded!
                        result.setSuccess(response);
                        cb(null, result);
                    }, (err) => {
                        result.setError(err);
                        return cb(null, result);
                    })
                }
                else {
                    result.setError(ResultCodes.IMAGE_TYPE_INVALID);
                    return cb(null, result);
                }
            });

            let _data = await data.parse(req);
            if (!_data.bytesExpected) {
                result.setError(ResultCodes.ARG_NOT_FOUND);
                return cb(null, result);
            }
            response.fileSize = _data.bytesExpected

        }

        request().catch(err => {
            result.setError(err);
            return result;
        });
    }
 
    Images.remoteMethod('uploadImage', {
        description: '@Thiep Wong, upload the images from client to cdn servers',
        http: {
            verb: 'POST',
            path: '/upload'
        },
        accepts: [
            {
                arg: 'req',
                type: 'object',
                http: {
                    source: 'req'
                }
            }],
        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    })


    Images.remoteMethod('uploadImageBase64', {
        description: '@Thiep Wong, upload image as base 64 code',
        http: {
            verb: 'POST',
            path: '/base64-upload'
        },
        accepts: [
            {
                arg: 'data',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })


    Images.uploadImageBase64 = (data, cb) => { 
        const ftp = Images.app.models.FTP;
        let result = new Results();
        if (!data || data.indexOf('data:image') < 0) {
            result.setError(ResultCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }
        var base64Data, response = {}; 
        base64Data = data.replace(/^data:image\/[a-z]+;base64,/, "");
        base64Data += base64Data.replace('+', ' ');
        let _buff = new Buffer(base64Data, 'base64');
        let file_name = `${uuid()}.jpg`;
        let dir = [new Date().getFullYear().toString(), ("0" + (new Date().getMonth() + 1)).slice(-2), ("0" + new Date().getDate()).slice(-2)];
        let resPromise = ftp.upload(_buff, file_name, dir);        
        response.fileName = dir[0] + '/' + dir[1] + '/' + dir[2] + '/' + file_name;
        response.fileUrl = FTPConnect.cdn + dir[0] + '/' + dir[1] + '/' + dir[2] + '/' + file_name;      
        response.fileSize = _buff.byteLength;
        resPromise.then((res) => {
            result.setSuccess(response);
            cb(null, result);
        }, (err) => {
            result.setError(err);
            return cb(null, result);
        }) 
    }

}