/**
 * Model ShippingHistory
 * Author: LuongPD
 * Last update: 14.3.2018 13:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var Filter = require('../middlewares/DataFilterHandle');
var app = require('../../server/server');
'use strict';

module.exports = function(ShippingHistory) {

    //#region Functions
    ShippingHistory.GetHistoryByShippingId = function(id){

        let promise = new Promise((resolve,reject) => {

            ShippingHistory.find({
                where:{
                    shippingBillId:id
                },
                order: 'createTime DESC'
            }, function(err,instance) {

                if (err) {
                    reject(err);
                }
                else
                {
                    resolve(instance);
                }
            });
        });
        return promise;
    }

    ShippingHistory.CreateNew = function (shippingBillId, shippingCode, cb) {
        app.dataSources.mysqld.transaction(async models => {
            const { ShippingHistory } = models;

            var shippingHistory = new ShippingHistory;
            shippingHistory.shippingBillId = shippingBillId;
            shippingHistory.status = shippingCode.status;
            shippingHistory.shortDescription = shippingCode.message;
            shippingHistory.createTime = parseInt(new Date().getTime() / 1000.0);
            await ShippingHistory.create(shippingHistory);

            let result = new Results();
            result.setSuccess(true);
            return cb ? cb(null, result) : result;
            
        }).catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb ? cb(null, result) : result;
        });
    }
    //#endregion

    //#region Expose Parameters
    //#endregion
};