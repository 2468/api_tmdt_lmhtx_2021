/**
 * Model ProductSeller
 * Author: LuongPD
 * Last update: 14.3.2018 13:49
 */
var Results = new require("../middlewares/ResultHandle");
var ResultsCodes = require("../middlewares/message_constant");
var Filter = require("../middlewares/DataFilterHandle");
var ApplicationError = require("../middlewares/application-error");
var app = require("../../server/server");
("use strict");

module.exports = function(ProductSeller) {
  // validate model data
  ProductSeller.validatesInclusionOf("status", { in: [0, 1] });
  ProductSeller.validatesInclusionOf("activeAgencyLink", { in: [0, 1] });
  ProductSeller.validatesLengthOf("aspect", { min: 0, max: 100 });
  ProductSeller.validatesLengthOf("affiliateRewardRate", { min: 0, max: 100 });
  ProductSeller.validatesInclusionOf("serviceAreaLevel", { in: [1, 2, 3, 4] });

  //#region Functions
  /**
   * Add new product for seller, included warehouse list and prices list
   * Created by Thiep Wong
   * Date: 5/7/2018
   * @param {*} productId
   * @param {*} productAspect
   * @param {*} productShowOnShop
   * @param {*} productStatus
   * @param {*} productDescription
   * @param {*} productAffiliateActive
   * @param {*} productAffRate
   * @param {*} productServiceLevel service location level
   * @param {*} selectedWarehouses product warehouses list
   * @param {*} productBaseprice  product base price
   * @param {*} productPrices product prices list
   * @param {*} cb
   */
  ProductSeller.NewProduct = function(ctx, cb) {
    let productId = ctx.body.productId || 0;
    let productAspect = ctx.body.productAspect || 0;
    let productShowOnShop = ctx.body.productShowOnShop || 0; 
    let productStatus = ctx.body.productStatus || 0;
    let productDescription = ctx.body.productDescription || null;
    let productAffiliateActive = ctx.body.productAffiliateActive || 0;
    let productAffRate = ctx.body.productAffRate || 0;
    let productServiceLevel = ctx.body.productServiceLevel || 0;
    let selectedWarehouses = ctx.body.selectedWarehouses || [];
    let selectedSellerPlatforms = ctx.body.selectedSellerPlatforms || [];
    let productBaseprice = ctx.body.productBaseprice || 0;
    let productPrices = ctx.body.productPrices || [];
    app.dataSources.mysqld
      .transaction(async models => {
        const {
          ProductSeller,
          Passport,
          ProductPrice,
          ProductStore,
          ProductSellerPlatform,
          Products
        } = models;
        let token = await Passport.GetSellerToken(ctx);
        if (!token) {
          let result = new Results();
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }

        let sellerProduct = new ProductSeller();
        sellerProduct.sellerId = token.userId;
        sellerProduct.productId = productId;
        sellerProduct.basePrice = productBaseprice;
        sellerProduct.aspect = productAspect ? productAspect : 100;
        sellerProduct.shortDescription = productDescription;
        sellerProduct.activeAgencyLink = productAffiliateActive;
        sellerProduct.status = 1;
        //sellerProduct.status = productStatus;
        sellerProduct.affiliateRewardRate = productAffRate;
        sellerProduct.serviceAreaLevel = productServiceLevel;
        sellerProduct.createBy = token.userId;
        sellerProduct.createTime = new Date() / 1000;

        let _productInstance = await ProductSeller.create(sellerProduct);

        if (!_productInstance) {
          let result = new Results();
          result.setError(ResultsCodes.SYSTEM_ERROR);
          return cb(null, result);
        }

        if (selectedWarehouses.length > 0) {
          let _store = ProductStore;
          await selectedWarehouses.forEach(function(item, index) {
            _store.Add(
              models,
              _productInstance.sellerId,
              _productInstance.id,
              item.id,
              item.quantity,
              item.quantity
            );
          });
        }

        if (selectedSellerPlatforms.length > 0) {
          let _productSellerPlatform = ProductSellerPlatform;

          await selectedSellerPlatforms.forEach(function(item, index) {
            _productSellerPlatform.Add(
              models,
              _productInstance.id,
              item.platformCode,
              item.isDisplayOnShop,
              token.userId
            );
          });
        }

        if (productPrices.length > 0) {
          let _prices = ProductPrice;
          await productPrices.forEach(function(item, index) {
            _prices.Add(
              models,
              _productInstance.id,
              item.price,
              item.from,
              item.to,
              _productInstance.sellerId,
              null
            );
          });
        }

        // update ES document
        await Products.UpdateDocumentByProductId(
          _productInstance.productId,
          models
        );

        let result = new Results();
        result.setSuccess(true);
        return cb(null, result);
      })
      .catch(function(e) {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });
  };

  /**
   * Update product for seller
   * Created by Hung Doan
   * Date: 17/10/2018
   * @param {*} id
   * @param {*} aspect
   * @param {*} status
   * @param {*} shortDescription
   * @param {*} productAffiliateActive
   * @param {*} productAffRate
   * @param {*} serviceAreaLevel
   * @param {*} basePrice
   * @param {*} cb
   */
  ProductSeller.UpdateProduct = function(id, ctx, cb) {
    let aspect = ctx.body.aspect || 0;
    let status = ctx.body.status || 0;
    let shortDescription = ctx.body.shortDescription || null;
    let activeAgencyLink = ctx.body.activeAgencyLink || null;
    let affiliateRewardRate = ctx.body.affiliateRewardRate || 0;
    let serviceAreaLevel = ctx.body.serviceAreaLevel || 0;
    let basePrice = ctx.body.basePrice || 0;
    app.dataSources.mysqld
      .transaction(async models => {
        const { ProductSeller, Passport, Products } = models;
        let token = await Passport.GetSellerToken(ctx);
        if (!token) {
          let result = new Results();
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }

        let productSeller = await ProductSeller.findById(id);

        if (!productSeller) {
          result.setError(ResultsCodes.ITEM_NOT_FOUND);
          return cb(null, result);
        }

        if (productSeller.sellerId != token.userId) {
          let error = ResultsCodes.NO_PERMISSTION_ERROR;
          error.message = "Bạn không có quyền làm việc này";
          throw new ApplicationError(error);
        }

        productSeller.updateAttributes({
          basePrice: basePrice,
          status: status,
          aspect: aspect >= 0 ? aspect : 100,
          shortDescription: shortDescription,
          activeAgencyLink: activeAgencyLink || false,
          affiliateRewardRate:
            affiliateRewardRate > 0 ? affiliateRewardRate : 0,
          serviceAreaLevel: serviceAreaLevel,
          modifyBy: token.userId,
          modifyTime: new Date() / 1000
        });

        // update ES document
        await Products.UpdateDocumentByProductId(
          productSeller.productId,
          models
        );

        let result = new Results();
        result.setSuccess(productSeller);
        return cb(null, result);
      })
      .catch(function(e) {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });
  };

  /**
   * Get products list of seller by id with filters
   * Modified by Thiep Wong
   * Last updated: 11:11 01/12/2018
   * @param {*} pageIndex
   * @param {*} pageSize
   * @param {*} pageOrder
   * @param {*} pageFilter
   * @param {*} cb
   */
  ProductSeller.GetListBySeller1 = function(ctx, cb) {
    let pageIndex = ctx.query.pageIndex || 0;
    let pageSize = ctx.query.pageSize || 10;
    let pageOrder = ctx.query.pageOrder || null;
    let pageFilter = ctx.query.pageFilter || null;
    let result = new Results();

    if (!pageSize) {
      pageSize = 10;
      pageIndex = 0;
    }

    const makeRequestProductList = async () => {
      let Passport = this.app.models.Passport;
      let token = await Passport.GetSellerToken(ctx);
      if (!token) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      let _productNameFilter =
        pageFilter && pageFilter != null && pageFilter != "null"
          ? {
              productName: {
                like: `%${pageFilter}%`
              }
            }
          : {};

      if (!pageOrder || pageOrder == "undefined" || pageOrder == "null")
        pageOrder = ["id desc"];
      let filter = new Filter().Where([{ sellerId: token.userId }], null);

      let totalRecords = await this.count(filter);
      console.log(totalRecords);
      ProductSeller.find(
        {
          fields: [
            "id",
            "sellerId",
            "productId",
            "basePrice",
            "total",
            "aspect",
            "serviceAreaLevel",
            "activeAgencyLink",
            "status",
            "displayOnShop",
            "affiliateRewardRate",
            "shortDescription"
          ],
          include: {
            relation: "product",
            scope: {
              fields: ["id", "categoryId", "productName", "productImage"],
              include: {
                relation: "category",
                scope: {
                  fields: ["id", "title"]
                }
              },
              where: _productNameFilter
            }
          },
          where: filter,
          limit: pageSize,
          skip: pageIndex * pageSize,
          order: pageOrder
        },
        function(err, instance) {
          if (err) {
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cbn(null, result);
          }

          let _sellerProducts = [];
          let _jsonData;
          if (instance.length > 0) {
            _jsonData = JSON.parse(JSON.stringify(instance));
            _jsonData.forEach((item, index) => {
              if (item.product) {
                _sellerProducts.push(item);
              }
            });
          }

          result.setSuccess({
            totalRecords: totalRecords,
            pageSize: pageSize,
            pageIndex: pageIndex,
            pageData: _sellerProducts
          });
          cb(null, result);
        }
      );
    };
    makeRequestProductList().catch(err => {
      ResultsCodes.Detail = err;
      result.setError(ResultsCodes.DATA_NOT_FOUND);
      return cbn(null, result);
    });
  };






  ProductSeller.GetListBySeller = (ctx, cb) => {
    let result = new Results();
    let pageIndex = ctx.query.pageIndex;
    let pageSize = ctx.query.pageSize;
    let pageOrder = ctx.query.pageOrder;   
    let productName = ctx.query.productName;

    app.dataSources.mysqld
      .transaction(async models => {
        const { ProductSeller, Passport, Products } = models;
        let token = await Passport.GetSellerToken(ctx);
        if (!token) {
          let result = new Results();
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }
      

        if (pageSize) {
            if (pageSize <= 0) {
                pageSize = 10;
            }
        } else {
            pageSize = 10;
        }

        if (pageIndex) {
            if (pageIndex < 1) {
                pageIndex = 1;
            }
        } else {
            pageIndex = 1;
        }

        let offset = (pageIndex - 1) * pageSize;
        //let limit = pageSize;           
    
        let sql = `SELECT  sp.id ,sp.seller_id AS sellerId,sp.product_id AS productId,sp.base_price AS basePrice ,sp.aspect AS aspect,sp.short_description AS shortDescription,sp.status,sp.active_agency_link AS activeAgencyLink, sp.service_area_level AS serviceAreaLevel,sp.display_on_shop AS displayOnShop, sp.affiliate_reward_rate AS affiliateRewardRate,
                           p.product_name  AS productName, p.product_image AS productImage,  c.title
                    FROM 
                    product_seller sp    
                    JOIN 
                    sellers s ON sp.seller_id  = s.id             
                    JOIN
                    products p ON sp.product_id = p.id       
                    JOIN
                    category c ON  p.category_id  = c.id                    
                    WHERE  1=1  AND sp.seller_id= '${token.userId}'`
                    // thienvd bỏ sp.status=1 WHERE  1=1 AND sp.status =1 AND sp.seller_id= '${token.userId}'`

        if (productName != "undefined" && productName != null) {
            sql += ` AND p.product_name like  '%${productName}%' `

        }
         let count = await ProductSeller.Count(sql,null);
      
         sql += `  GROUP BY sp.id ORDER BY sp.id DESC  limit ${pageSize} offset ${offset} `;
        
        var params = [pageSize, offset];
        // var params =[platformCode, sellerUuid, pageSize, offset];

      

        let connector = ProductSeller.dataSource.connector;
        connector.query(sql, params, function (err, resultObjects) {
            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {
                let result = new Results();
                result.setSuccess({ itemCount: count, rows: resultObjects });
                console.log(result);
                return cb(null, result);
            }
        });

    }

    )
      .catch(function(e) {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });  
}

ProductSeller.Count = function (sql, params) {
  let promise = new Promise((resolve, reject) => {
      var ds = ProductSeller.dataSource;
      ds.connector.query(sql, params, function (err, data) {
          if (err) {
              reject(err);
          } else {
              resolve(data.length);
          }
      });
  });
  return promise;
}

  /**
   * Update status of product's seller
   * Created by Thiep Wong
   * Date 11/05/2018
   * @param {*} id
   * @param {*} status
   * @param {*} cb
   */
  ProductSeller.UpdateStatus = function(id, status, cb) {
    let result = new Results();
    if (!id) {
      result.setError(ResultsCodes.ID_INVALID);
      return cb(null, result);
    }

    ProductSeller.findById(id, function(err, instance) {
      if (err) {
        result.setError(ResultsCodes.SYSTEM_ERROR);
        return cb(null, result);
      }

      instance.updateAttribute("status", status, function(err, instance) {
        if (err) {
          result.setError(ResultsCodes.SYSTEM_ERROR);
          return cb(null, result);
        }

        result.setSuccess(instance);
        return cb(null, result);
      });
    });
  };

  /**
     * check service_area_level and the delivery location
     * Created by Hung Doan
     * Date 24/05/2018
     * example
     * {"ids":[
        {"productSellerId":14, "sellerStoreId":1},
        {"productSellerId":26, "sellerStoreId":4}
        ],
        "locationCodeLevel1":"vn",
        "locationCodeLevel2":"01",
        "locationCodeLevel3":"005",
        "locationCodeLevel4":"00167"}
     * @param {*} id 
     * @param {*} status 
     * @param {*} cb 
     */
  ProductSeller.CheckLocation = function(data, cb) {
    data = parseObject(data)[0];
    var arr = data.ids;

    if (
      !arr ||
      arr.length <= 0 ||
      !data.locationCodeLevel1 ||
      !data.locationCodeLevel2 ||
      !data.locationCodeLevel3 ||
      !data.locationCodeLevel4
    ) {
      let result = new Results();
      result.setError(ResultsCodes.VALIDATION_ERROR);
      return cb(null, result);
    }

    let productSellerSql = `SELECT 
            pd.product_name AS productName,
            ps.id productSellerId,
            ss.id sellerStoreId,
        CASE
            WHEN ps.service_area_level = 1 and  lo1.code = ? THEN '1'
            WHEN ps.service_area_level = 2 and  lo2.code = ? THEN '1'
            WHEN ps.service_area_level = 3 and  lo3.code = ? THEN '1'
            WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
            else '0'
        END  saleable
        FROM product_seller ps
            JOIN
        sellers sl ON sl.id = ps.seller_id
            JOIN
        products pd ON pd.id = ps.product_id
            JOIN
        product_store pst ON pst.product_seller_id = ps.id
            JOIN
        seller_store ss ON pst.seller_store_id = ss.id
            JOIN
        location lo4 ON lo4.code = ss.location_code
            JOIN
        location lo3 ON lo3.code = lo4.parent_code
            JOIN
        location lo2 ON lo2.code = lo3.parent_code
            JOIN
        location lo1 ON lo1.code = lo2.parent_code  
        WHERE (ps.id, ss.id)  IN (`;

    let connector = ProductSeller.dataSource.connector;
    let params = [
      data.locationCodeLevel1,
      data.locationCodeLevel2,
      data.locationCodeLevel3,
      data.locationCodeLevel4
    ];

    for (var i = 0; i < arr.length; ++i) {
      var item = arr[i];
      if (
        !item.productSellerId ||
        !Number.isInteger(item.productSellerId) ||
        !item.sellerStoreId ||
        !Number.isInteger(item.sellerStoreId)
      ) {
        let result = new Results();
        result.setError(ResultsCodes.VALIDATION_ERROR);
        return cb(null, result);
      }

      if (i == arr.length - 1) {
        productSellerSql += "(?,?))";
      } else {
        productSellerSql += "(?,?),";
      }

      params.push(item.productSellerId);
      params.push(item.sellerStoreId);
    }
    //let params = [locationCodeLevel1, locationCodeLevel2, locationCodeLevel3, locationCodeLevel4];
    connector.query(productSellerSql, params, function(err, data) {
      let result = new Results();

      if (err) {
        result.setError(ResultsCodes.SYSTEM_ERROR);
        return cb(null, result);
      } else {
        result.setSuccess(data);
        return cb(null, result);
      }
    });
  };

  var parseObject = function(json) {
    json =
      "[" + // enclose with []
      json
        .replace(/(\w+)(?=:)/g, '"$1"') // search for words followed by a colon
        .replace(/,$/, "") // trim the ending comma
        .replace(/:([\w.]+)/g, function(match, value) {
          // grab the values
          return (
            ":" + // ensure preceding colon is in place
            (isNaN(value) || value % 1 !== 0 // is it a non-float number?
              ? '"' + value + '"' // enclose with "" if not a non-float number
              : parseFloat(value)) // parse if is number
          );
        }) +
      "]"; // enclose with []
    return JSON.parse(json);
  };
  //#endregion

  //#region Expose Parameters
  ProductSeller.remoteMethod("NewProduct", {
    description:
      "@Thiep Wong, add a new product to seller products list, included product stores and product prices",
    http: {
      verb: "POST",
      path: "/add"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  ProductSeller.remoteMethod("UpdateProduct", {
    description: "@Hung Doan, update product seller",
    http: {
      verb: "POST",
      path: "/update/:id"
    },
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true
      },
      {
        arg: "ctx",
        type: "object",
        http:{
            source:'req'
        }
      }
      
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  ProductSeller.remoteMethod("GetListBySeller", {
    description: "Modified by Thiep Wong, get list of seller's products ",
    http: {
      verb: "GET",
      path: "/list"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: [
      {
        arg: "result",
        type: "string",
        root: true
      }
    ]
  });

  ProductSeller.remoteMethod("UpdateStatus", {
    description:
      "@Thiep Wong, update the status of seller's products, using to approve status",
    http: {
      verb: "POST",
      path: "/status/:id"
    },
    accepts: [
      {
        arg: "id",
        type: "number"
      },
      {
        arg: "status",
        type: "number"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  ProductSeller.remoteMethod("CheckLocation", {
    http: {
      verb: "POST",
      path: "/check-location"
    },
    // ,
    // accepts: [{
    //     arg: 'data',
    //     type: 'string',
    //     required: true
    // }
    accepts: [
      {
        arg: "data",
        type: "string",
        // type: "object",
        // http: { source: 'body' },
        required: true
      }

      // , {
      //     arg: 'locationCodeLevel1',
      //     type: 'string',
      //     required: true
      // }, {
      //     arg: 'locationCodeLevel2',
      //     type: 'string',
      //     required: true
      // }, {
      //     arg: 'locationCodeLevel3',
      //     type: 'string',
      //     required: true
      // }, {
      //     arg: 'locationCodeLevel4',
      //     type: 'string',
      //     required: true
      // }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });



  ProductSeller.remoteMethod("CheckWeight", {
    description:
    "@ThienVD tính khối lượng đơn hàng.",
    http: {
      verb: "POST",
      path: "/check-weight"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });




  ProductSeller.CheckWeight =(ctx,cb) => {   
    let mproductID =  ctx.body.productId;
    let result = new Results();          
        let sql = `SELECT SUM(a.weight) as weight FROM products a, product_seller b WHERE a.id =b.product_id `;
                        let params = [];
                        if(mID != undefined && mID !=null) 
                        {
                            sql = sql + ` AND b.id  IN =  '${mproductID}'`;
                        }                
        let connector = ProductSeller.dataSource.connector;
        connector.query(sql, params, function (err, res) {
            if (err) {
              result.setSuccess(false);
              return cb(null, result);;
            }
            else {
              if(res != null || res !=undefined)
              {
                result.setSuccess(true);
                return cb(null,result);
              }
              else
              {
                result.setSuccess(false);
                return cb(null,result);
              }
              
            }
        });   
    }


  //#endregion
};
