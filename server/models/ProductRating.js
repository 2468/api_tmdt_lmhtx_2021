/**
 * Model ProductRating
 * Signup, signin, update infomation for ProductRating model.
 * Author: HungDX
 * Last update: 13.3.2018 13:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var async = require('async');
module.exports = function (ProductRating) {

    // validate model data
    ProductRating.validatesInclusionOf('rating', { in: [1, 2, 3, 4, 5] });
    ProductRating.validatesInclusionOf('status', { in: [0, 1] });

    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };

    /**
    * Reviews is used to get all ProductRating by productId
    * @param {number} productId id of product from frontend
    * @param {number} ordersId id of order from frontend
    * @param {number} rating rating for product from frontend
    * @param {string} comment comment for product from frontend
    * @param {*} cb Callback function for result returns
    */
    ProductRating.CreateNew = function (ctx, cb) {

        let productSellerId = ctx.body.productSellerId || 0;
        let ordersId = ctx.body.ordersId || 0;
        let rating = ctx.body.rating || 0;
        let comment = ctx.body.comment || null;

        if (comment != null) {
            ProductRating.validatesLengthOf('comment', { max: 500, message: { max: 'Nhận xét đánh giá không được quá 500 kí tự' } });
        }
        let result = new Results();

        if (!productSellerId || !ordersId || !rating) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }

        const makeCreateNew = async () => {

            let Passport = ProductRating.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            var memberId = token.userId;
            let ProductSeller = ProductRating.app.models.ProductSeller;
            ProductSeller.findOne({
                fields: ['productId'],
                where: {
                    "id": productSellerId
                }
            }, function (err, productSellerInstance) {

                var productId = productSellerInstance.productId;
                var ds = ProductRating.dataSource;
                var sql = `
                SELECT 
                    1
                FROM
                    orders od
                        JOIN
                    orders_details odd ON odd.orders_id = od.id
                        JOIN
                    product_seller ps ON ps.id = odd.product_seller_id
                        JOIN
                    products pd ON pd.id = ps.product_id
                WHERE
                    pd.id = ? AND od.id = ?
                        AND od.member_id = ?
                `;

                var params = [productId, ordersId, memberId];

                // validate input data
                ds.connector.query(sql, params, function (err, data) {

                    if (err) {
                        result.setError(ResultsCodes.SYSTEM_ERROR);
                        return cb(null, result);
                    }
                    else {

                        // data does not exists
                        if (data.length == 0) {
                            var error = ResultsCodes.DATA_NOT_FOUND;
                            error.message = error.message.format(productId, ordersId, memberId);
                            result.setError(error);
                            return cb(null, result);
                        } else {

                            var currentTime = parseInt(new Date().getTime() / 1000.0);
                            ProductRating.findOne({
                                //fields: ["id"],
                                where: {
                                    "productId": productId,
                                    "memberId": memberId,
                                    "orderId": ordersId
                                }
                            }, function (err, productRatingInstace) {
                                if (err) {
                                    result.setError(ResultsCodes.SYSTEM_ERROR);
                                    return cb(null, result);
                                } else if (productRatingInstace == null) {

                                    var productRating = ProductRating();
                                    productRating.productId = productId;
                                    productRating.productSellerId = productSellerId;
                                    productRating.orderId = ordersId;
                                    productRating.rating = rating;
                                    productRating.comment = comment ? comment : "";
                                    productRating.memberId = memberId;
                                    productRating.status = 1;
                                    productRating.createTime = currentTime;
                                    productRating.createBy = memberId;
                                    productRating.modifyTime = currentTime;
                                    productRating.modifyBy = memberId;

                                    // Add new
                                    ProductRating.create(productRating, function (err, instance) {
                                        if (err) {
                                            if (err.name === "ValidationError") {
                                                // var error = ResultsCodes.VALIDATION_ERROR;
                                                // error.message = err.messages;
                                                err.code = ResultsCodes.VALIDATION_ERROR.code;
                                                err.name = ResultsCodes.VALIDATION_ERROR.name;
                                                result.setError(err);
                                            } else {
                                                result.setError(ResultsCodes.SYSTEM_ERROR);
                                            }

                                            return cb(null, result);
                                        } else {
                                            result.setSuccess(true);
                                            return cb(null, result);
                                        }
                                    });
                                } else {

                                    // Update
                                    productRatingInstace.rating = rating;
                                    productRatingInstace.comment = comment ? comment : "";
                                    productRatingInstace.modifyTime = currentTime;
                                    productRatingInstace.modifyBy = memberId;

                                    productRatingInstace.save(function (err, instance) {
                                        if (err) {
                                            if (err.name === "ValidationError") {
                                                // var error = ResultsCodes.VALIDATION_ERROR;
                                                // error.message = err.messages;
                                                err.code = ResultsCodes.VALIDATION_ERROR.code;
                                                err.name = ResultsCodes.VALIDATION_ERROR.name;
                                                result.setError(err);
                                            } else {
                                                result.setError(ResultsCodes.SYSTEM_ERROR);
                                            }

                                            return cb(null, result);
                                        } else {
                                            result.setSuccess(true);
                                            return cb(null, result);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            });
        }
        makeCreateNew().catch(err => {
            err.code = ResultsCodes.VALIDATION_ERROR.code;
            err.name = ResultsCodes.VALIDATION_ERROR.name;
            result.setError(err);
            return cb(null, result0);
        });
    }

    /**
    * Reviews is used to get all ProductRating by productId
    * @param {number} productId id of product from frontend
    * @param {number} status status of rating
    * @param {number} pageSize pageSize from frontend (default is 10)
    * @param {number} pageIndex pageIndex from frontend (default is 0)
    * @param {array} pageOrder pageOrder from frontend (an array, example ["id DESC", "rating ASC"])
    * @param {*} cb Callback function for result returns
    */
    ProductRating.Reviews = function (productId, status, pageIndex, pageSize, order, cb) {

        // var orderParamTest = JSON.parse(orderParam);

        let whereClause;
        if (status != undefined) {
            whereClause = {
                "productId": productId,
                "status": status
            };
        } else {
            whereClause = { "productId": productId };
        }

        ProductRating.find({
            fields: ["id", "createTime", "modifyTime", 'rating', 'comment', "memberId", "status"],
            include: {
                relation: 'members', // include the owner object
                scope: { // further filter the owner object
                    fields: ['username', 'fullname']
                }
            },
            where: whereClause,
            limit: pageSize,
            skip: pageIndex,
            order: order
        }, function (err, instance) {

            if (err) {

                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {

                let result = new Results();
                result.setSuccess(instance);
                return cb(null, result);
            }
        })
    };

    /**
    * Deactive productRating record
    * @param {number} id id of record
    * @param {*} cb Callback function for result returns
    */
    ProductRating.Deactive = function (id, ctx, cb) {

        const makeDeactive = async () => {
            let Passport = ProductRating.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            let productRating = await ProductRating.findOne({
                where: {
                    "id": id
                }
            });

            if (productRating == null) {
                let result = new Results();
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            } else {
                productRating.status = 0;
                await productRating.save();

                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);
            }
        }

        makeDeactive().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb(null, result);
        });
    }

    /**
    * Change status productRating record
    * @param {number} id id of record
    * @param {*} cb Callback function for result returns
    */
    ProductRating.ChangeStatus = function (id, ctx, cb) {
        let status = ctx.body.status || 0;

        let result = new Results();
        const make = async () => {
            let Passport = ProductRating.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            // if (status != 0 && status != 1) {
            //     let result = new Results();
            //     let error = ResultsCodes.VALIDATION_ERROR;
            //     error.message = "status is invalid, must be 0 or 1";
            //     result.setError(error);
            //     return cb(null, result);
            // }

            let productRating = await ProductRating.findOne({
                where: {
                    "id": id
                }
            });
            console.log(productRating);

            if (productRating == null) {
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            } else {
                productRating.status = 1;
                await productRating.save();

                result.setSuccess(true);
                return cb(null, result);
            }
        }

        make().catch(function (e) {
            if (e.name === "ValidationError") {
                let err = {
                    code: ResultsCodes.VALIDATION_ERROR.code,
                    name: ResultsCodes.VALIDATION_ERROR.name,
                    message: e.message
                };
                result.setError(err);
            } else {
                result.setError(ResultsCodes.SYSTEM_ERROR);
            }

            return cb(null, result);
        });
    }

    /**
    * Aggregate is used to get aggregation of ProductRating by productid
    * @param {number} productId id of product from frontend
    * @param {*} cb Callback function for result returns
    */
    ProductRating.Aggregate = function (productId, cb) {

        async.parallel({
            ratingAverage: async.apply(ProductRating.average.bind(null, productId)),
            ratingCountByStar: async.apply(ProductRating.countRatingByStar.bind(null, productId))
        }, function (err, results) {
            if (err) {

                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {

                let result = new Results();
                result.success = { ratingAverage: results.ratingAverage, ratingCountByStar: results.ratingCountByStar }
                return cb(null, result);
            }
        });
    };

    ProductRating.average = function (productId, cb) {

        var ds = ProductRating.dataSource;
        var sql = "SELECT ifnull(avg(rating),0) as ratingAverage, count(rating) as ratingCount FROM product_rating where product_id = ?";
        var params = [productId];
        ds.connector.query(sql, params, function (err, data) {

            if (err) {
                cb(err);
            }
            else {
                cb(null, data);
            }
        });
    };

    ProductRating.countRatingByStar = function (productId, cb) {

        var ds = ProductRating.dataSource;
        var sql = "SELECT rating, count(rating) count FROM product_rating  where product_id = ?  group by rating";
        var params = [productId];
        ds.connector.query(sql, params, function (err, data) {

            if (err) {
                cb(err);
            }
            else {
                cb(null, data);
            }
        });
    };

    ProductRating.countRatingByStarAsync = function (productId) {

        let promise = new Promise((resolve, reject) => {
            var ds = ProductRating.dataSource;
            var sql = "SELECT rating, count(rating) count FROM product_rating  where product_id = ?  group by rating";
            var params = [productId];

            ds.connector.query(sql, params, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });

        // .catch(err => {throw err});

        return promise;

        // ds.connector.query(sql, params, function (err, data) {

        //     if (err) {
        //         cb(err);
        //     }
        //     else {
        //         cb(null, data);
        //     }
        // });
    };

    ProductRating.RatingDetails = function (productSellerId, ordersId, cb) {

        // var memberId = 5;

        ProductRating.findOne({
            fields: ["id", 'productSellerId', 'orderId', "rating", "comment"],
            where: {
                // "memberId": memberId,
                "productSellerId": productSellerId,
                "orderId": ordersId
            }
        }, function (err, instance) {

            if (err) {

                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {

                let result = new Results();
                result.setSuccess(instance);
                return cb(null, result);
            }
        })
    };

    ProductRating.GetListRating = function (cb) {
        ProductRating.find({
            fields: ['id', 'memberId', 'productId', 'productSellerId', 'orderId', 'rating', 'comment'],
        }, function (err, instance) {

            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {

                let result = new Results();
                result.setSuccess(instance);
                return cb(null, result);

            }
        })
    }

    ProductRating.remoteMethod("CreateNew", {
        http: {
            "verb": "post",
            path: '/createnew'
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],

        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    });

    ProductRating.remoteMethod("Reviews", {
        http: {
            "verb": "get"
        },
        accepts: [
            {
                arg: "productId",
                type: "number",
                required: true
            },
            {
                arg: "status",
                type: "number"
            },
            {
                arg: "pageIndex",
                type: "number"
            },
            {
                arg: "pageSize",
                type: "number"
            }
            ,
            {
                arg: "order",
                type: "array"
            }
        ],

        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    });

    ProductRating.remoteMethod("ChangeStatus", {
        description: '@Hung Doan, change status of record to 0 or 1',
        http: {
            verb: "POST",
            path: '/:id/change-status'
        },
        accepts: [
            {
                arg: "id",
                type: "number",
                required: true
            },
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],

        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    });

    ProductRating.remoteMethod("Aggregate", {
        http: {
            "verb": "get"
        },
        accepts: [
            {
                arg: "productId",
                type: "number",
                required: true
            }
        ],
        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    });

    ProductRating.remoteMethod("RatingDetails", {
        http: {
            "verb": "get"
        },
        accepts: [
            {
                arg: "productSellerId",
                type: "number",
                required: true
            },
            {
                arg: "ordersId",
                type: "number",
                required: true
            }
        ],
        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    });

    ProductRating.remoteMethod("GetListRating", {
        http: {
            "verb": "get"
        },
        returns: {
            arg: "result",
            type: "string",
            root: true
        }

    })
};