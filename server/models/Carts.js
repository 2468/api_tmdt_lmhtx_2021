/**
 * Model Carts
 * Author: LuongPD
 * Last update: 22.3.2018 13:49
 */
var Results = new require("../middlewares/ResultHandle");
var ResultsCodes = require("../middlewares/message_constant");
var app = require("../../server/server");
("use strict");

module.exports = function(Carts) {
  //#region Functions
  Carts.GetListByMember = function(ctx, cb) {
    let result = new Results();
    const makeGetListByMember = async () => {
      let Passport = Carts.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (!token) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      var memberId = token.userId;
      var currentTime = parseInt(new Date().getTime() / 1000.0);

      Carts.find(
        {
          fields: [
            "id",
            "productSellerId",
            "productStoreId",
            "sellerStoreId",
            "sellerId",
            "memberId",
            "amount",
            "affiliateId",
            "status"
          ],
          include: [
            {
              relation: "productSeller",
              scope: {
                fields: ["id", "productId", "basePrice"],
                include: [
                  {
                    relation: "product",
                    scope: {
                      fields: ["id", "productName", "productImage", "brandId"],
                      include: {
                        relation: "brand",
                        scope: {
                          fields: ["id", "brandName"]
                        }
                      }
                    }
                  },
                  {
                    relation: "productPrice",
                    scope: {
                      fields: ["id", "productSellerId", "price", "from", "to"],
                      where: {
                        and: [
                          { from: { lte: currentTime } },
                          { to: { gte: currentTime } }
                        ]
                      }
                    }
                  }
                ]
              }
            },
            {
              relation: "seller",
              scope: {
                fields: ["id", "shopName"]
              }
            },
            {
              relation: "productStore",
              scope: {
                fields: ["id", "available"]
              }
            }
          ],
          where: {
            memberId: memberId
          }
        },
        function(err, results) {
          result.setSuccess(results);
          return cb(null, result);
        }
      );
    };
    makeGetListByMember().catch(err => {
      result.setError(ResultsCodes.ARG_NOT_FOUND);
      return cb(null, result);
    });
  };

  Carts.CreateNew = function( ctx, cb) {

    let   productSellerId = ctx.body.productSellerId || 0;
    let productStoreId = ctx.body.productStoreId || 0;
    let sellerId = ctx.body.sellerId || 0;
    let    sellerStoreId = ctx.body.sellerStoreId || 0;
    let amount = ctx.body.amount || 0;
    let affiliateId = ctx.body.affiliateId || 0;

    let result = new Results();
    if(!productSellerId || !productStoreId || !sellerId || !sellerStoreId || !amount) {
        result.setError(ResultsCodes.ARG_NOT_FOUND);
        return cb(null,result);
    }
    
    const makeCreateNew = async () => {
      let Passport = Carts.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (!token) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      app.dataSources.mysqld
        .transaction(async models => {
          CreateCart(
            models,
            token.userId,
            productSellerId,
            productStoreId,
            sellerId,
            sellerStoreId,
            amount,
            affiliateId
          );
          // const {Carts} = models;
          // var memberId = token.userId;

          // var cart = await Carts.findOne({
          //     fields: ['id', 'productSellerId', 'productStoreId', 'sellerStoreId', 'sellerId', 'memberId', 'amount', 'affiliateId', 'status', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
          //     where: {
          //         "memberId": memberId,
          //         "productSellerId": productSellerId,
          //         "sellerStoreId": sellerStoreId
          //     }
          // });

          // if (cart == null) {
          //     var newCart = new Carts();
          //     newCart.productSellerId = productSellerId;
          //     newCart.productStoreId = productStoreId;
          //     newCart.sellerStoreId = sellerStoreId;
          //     newCart.sellerId = sellerId;
          //     newCart.memberId = memberId;
          //     newCart.amount = amount;
          //     newCart.affiliateId = affiliateId;
          //     newCart.status = 0;
          //     newCart.createTime = parseInt(new Date().getTime() / 1000.0);
          //     newCart.createBy = memberId;
          //     await Carts.create(newCart);
          // } else {
          //     cart.amount += amount;
          //     cart.modifyTime = parseInt(new Date().getTime() / 1000.0);
          //     cart.modifyBy = memberId;
          //     await cart.save();
          // }
 
          result.setSuccess(true);
          return cb(null, result);
        })
        .catch(function(e) { 
          result.setError(ResultsCodes.ADD_ITEM_FAILED);
          return cb(null, result);
        });
    };
    makeCreateNew().catch(err=>{
        result.setError(ResultsCodes.ADD_ITEM_FAILED);
        return cb(null, result);
    });
  };

  async function CreateCart(
    models,
    memberId,
    productSellerId,
    productStoreId,
    sellerId,
    sellerStoreId,
    amount,
    affiliateId
  ) {
    const { Carts } = models;

    var cart = await Carts.findOne({
      fields: [
        "id",
        "productSellerId",
        "productStoreId",
        "sellerStoreId",
        "sellerId",
        "memberId",
        "amount",
        "affiliateId",
        "status",
        "createTime",
        "createBy",
        "modifyTime",
        "modifyBy"
      ],
      where: {
        memberId: memberId,
        productSellerId: productSellerId,
        sellerStoreId: sellerStoreId
      }
    });

    if (cart == null) {
      var newCart = new Carts();
      newCart.productSellerId = productSellerId;
      newCart.productStoreId = productStoreId;
      newCart.sellerStoreId = sellerStoreId;
      newCart.sellerId = sellerId;
      newCart.memberId = memberId;
      newCart.amount = amount;
      newCart.affiliateId = affiliateId;
      newCart.status = 0;
      newCart.createTime = parseInt(new Date().getTime() / 1000.0);
      newCart.createBy = memberId;
      await Carts.create(newCart);
    } else {
      cart.amount += amount;
      cart.modifyTime = parseInt(new Date().getTime() / 1000.0);
      cart.modifyBy = memberId;
      await cart.save();
    }
  }

  Carts.PreOrder = function(ctx,cb) {

    let result = new Results();
    const makePreOrder = async () => {
      let Passport = Carts.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (!token) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      app.dataSources.mysqld
        .transaction(async models => {
          const { Carts, ShipUnit, Sellers } = models;
          var memberId = token.userId;
          var currentTime = parseInt(new Date().getTime() / 1000.0);

          const productsInCart = await Carts.find({
            fields: [
              "id",
              "productSellerId",
              "productStoreId",
              "sellerStoreId",
              "sellerId",
              "memberId",
              "amount",
              "affiliateId",
              "status"
            ],
            include: [
              {
                relation: "productSeller",
                scope: {
                  fields: ["id", "productId", "basePrice"],
                  include: [
                    {
                      relation: "product",
                      scope: {
                        fields: [
                          "id",
                          "productName",
                          "productImage",
                          "brandId"
                        ],
                        include: {
                          relation: "brand",
                          scope: {
                            fields: ["id", "brandName"]
                          }
                        }
                      }
                    },
                    {
                      relation: "productPrice",
                      scope: {
                        fields: [
                          "id",
                          "productSellerId",
                          "price",
                          "from",
                          "to"
                        ],
                        where: {
                          and: [
                            { from: { lte: currentTime } },
                            { to: { gte: currentTime } }
                          ]
                        }
                      }
                    },
                    {
                      relation: "productStores",
                      scope: {
                        include: [
                          {
                            relation: "stores",
                            scope: {
                              where: {},
                              include: [
                                {
                                  relation: "location"
                                }
                              ]
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ],
            where: {
              memberId: memberId
            }
          });

          var groupProductsShip = [];
          var groupPrice = [];
          var sellers = [];
          var groupShipping = [];

          for (var item in productsInCart) {
            var product = productsInCart[item];

            // check valid cart amount
            if (product.status == 0) {
              result.setError(
                ResultsCodes.PRODUCT_STORE_AVAILABE_ITEM_NOT_ENOUGH_TO_BOOK
              );
              return cb(null, result);
            }

            var groupProducts = getValue(
              groupProductsShip,
              product.sellerStoreId
            );

            if (groupProducts == null) {
              groupProducts = [];
              groupProducts.push(product);

              groupProductsShip.push({
                key: product.sellerStoreId,
                value: groupProducts
              });

              sellers.push({
                key: product.sellerStoreId,
                value: product.sellerId
              });
            } else {
              groupProducts.push(product);
            }

            // obtain shipping price
            var itemPrice = null;
            var price = 0;

            for (var item in groupPrice) {
              var itemKey = groupPrice[item].key;
              if (itemKey == product.sellerStoreId) {
                itemPrice = groupPrice[item];
                break;
              }
            }

            var jsonProduct = product.toJSON();

            if (itemPrice == null) {
              if (jsonProduct.productSeller.productPrice != null) {
                price =
                  product.amount * jsonProduct.productSeller.productPrice.price;
              } else {
                price = product.amount * jsonProduct.productSeller.basePrice;
              }

              groupPrice.push({
                key: product.sellerStoreId,
                value: price
              });
            } else {
              if (jsonProduct.productSeller.productPrice != null) {
                price =
                  product.amount * jsonProduct.productSeller.productPrice.price;
              } else {
                price = product.amount * jsonProduct.productSeller.basePrice;
              }

              itemPrice.value += price;
            }
          }

          for (var item in groupProductsShip) {
            var sellerStoreId = groupProductsShip[item].key;
            var sellerId = getValue(sellers, sellerStoreId);

            var shipUnits = await ShipUnit.find({
              fields: [
                "id",
                "name",
                "type",
                "sellerId",
                "matchLocationLevelFee",
                "notMatchLocationLevelFee",
                "logoImage",
                "timeDelivery"
              ],
              where: {
                sellerId: { inq: [0, sellerId] },
                status: 1
              },
              order: "orders asc"
            });

            var seller = await Sellers.findOne({
              fields: ["id", "shopName", "bankAccountHolder", "bankAccountNumber", "bankName", "bankBranch"],
              where: {
                id: sellerId
              }
            });

            var shipping = {};
            shipping.sellerId = seller.id;
            shipping.shopName = seller.shopName;
            shipping.sellerStoreId = sellerStoreId;
            shipping.shipUnits = shipUnits;
            shipping.bankAccountHolder = seller.bankAccountHolder;
            shipping.bankAccountNumber = seller.bankAccountNumber;
            shipping.bankName = seller.bankName;
            shipping.bankBranch = seller.bankBranch;
            shipping.shipping_type  = 0;
            var products = new Map();
            for (var i in groupProductsShip[item].value) {
              var ll = groupProductsShip[item].value;
              products.set(groupProductsShip[item].value[i].key, ll);
            }
            shipping.products = Array.from(products.values())[0];

            var price = getValue(groupPrice, sellerStoreId);
            shipping.price = price;

            groupShipping.push(shipping);

          }

          var preOrderResults = {};
          console.log( JSON.stringify(groupShipping));
          preOrderResults.preOrderKey = await savePreOrder(
            JSON.stringify(groupShipping),
            memberId
          );
          preOrderResults.groupShips = groupShipping;

          result.setSuccess(preOrderResults);
          return cb(null, result);
        })
        .catch(function(e) {
          result.setError(ResultsCodes.ORDERS_ORDER_ERROR);
          return cb(null, result);
        });
    };
    makePreOrder().catch(function(e) {
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  var savePreOrder = async function(metadata, memberId) {
    let PreOrder = Carts.app.models.PreOrder;
    var preOrder = new PreOrder();
    preOrder.metadata = metadata;
    preOrder.status = 0;
    preOrder.createBy = memberId;
    preOrder.createTime = parseInt(new Date().getTime() / 1000.0);
    await PreOrder.create(preOrder);
    return preOrder.preOrderKey;
  };

  var getValue = function(arrs, key) {
    for (var item in arrs) {
      var itemKey = arrs[item].key;
      if (itemKey == key) {
        return arrs[item].value;
      }
    }
    return null;
  };

  Carts.UpdateAmount = function(ctx, cb) {
      let cartId = ctx.body.cartId || 0;
      let amount = ctx.body.amount || 0;

    let result = new Results();
    // if(!cartId || !amount) {
    if(!cartId) {
        result.setError(ResultsCodes.ARG_NOT_FOUND);
        return cb(null,result);
    }

    const makeUpdateAmount = async () => {
      let Passport = Carts.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (!token) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }
      app.dataSources.mysqld
        .transaction(async models => {
          const { Carts, ProductStore } = models;
          var memberId = token.userId;

          var cart = await Carts.findOne({
            fields: [
              "id",
              "productSellerId",
              "productStoreId",
              "sellerStoreId",
              "sellerId",
              "memberId",
              "amount",
              "affiliateId",
              "status",
              "createTime",
              "createBy",
              "modifyTime",
              "modifyBy"
            ],
            where: {
              id: cartId
            }
          });

          if (cart != null) {
            // delete
            if (amount == 0) {
              await cart.delete();
            } else 
            {
              cart.amount += amount;       
              if (cart.amount < 1) {
                cart.amount = 1;
              }

              const productStore = await ProductStore.findOne({
                fields: ["available"],
                where: {
                  id: cart.productStoreId
                }
              });

              var isError = false;
              if (cart.amount > productStore.available) {
                cart.amount = productStore.available;
                isError = true;
              }

              cart.modifyTime = parseInt(new Date().getTime() / 1000.0);
              cart.modifyBy = memberId;
              await cart.save();

              if (isError) { 
                result.setError(
                  ResultsCodes.PRODUCT_STORE_AVAILABE_ITEM_NOT_ENOUGH_TO_BOOK
                );
                return cb(null, result);
              }
            }
 
            result.setSuccess(true);
            return cb(null, result);
          } else { 
            result.setError(ResultsCodes.ITEM_NOT_FOUND);
            return cb(null, result);
          }
        })
        .catch(function(e) { 
          result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
          return cb(null, result);
        });
    };
    makeUpdateAmount().catch(err=>{
        result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
        return cb(null, result);
    });
  };



  Carts.UpdateAmountCustomer = function(ctx, cb) {
    let cartId = ctx.body.cartId || 0;
    let amount = ctx.body.amount || 0;

  let result = new Results();
  // if(!cartId || !amount) {
  if(!cartId) {
      result.setError(ResultsCodes.ARG_NOT_FOUND);
      return cb(null,result);
  }

  const makeUpdateAmount = async () => {
    let Passport = Carts.app.models.Passport;
    let token = await Passport.GetMemberToken(ctx);
    if (!token) {
      result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
      return cb(null, result);
    }
    app.dataSources.mysqld
      .transaction(async models => {
        const { Carts, ProductStore } = models;
        var memberId = token.userId;

        var cart = await Carts.findOne({
          fields: [
            "id",
            "productSellerId",
            "productStoreId",
            "sellerStoreId",
            "sellerId",
            "memberId",
            "amount",
            "affiliateId",
            "status",
            "createTime",
            "createBy",
            "modifyTime",
            "modifyBy"
          ],
          where: {
            id: cartId
          }
        });

        if (cart != null) {
          // delete
          if (amount == 0) {
            await cart.delete();
          } else 
          {
            //cart.amount += amount;
            cart.amount = amount;

            if (cart.amount < 1) {
              cart.amount = 1;
            }

            const productStore = await ProductStore.findOne({
              fields: ["available"],
              where: {
                id: cart.productStoreId
              }
            });

            var isError = false;
            if (cart.amount > productStore.available) {
              cart.amount = productStore.available;
              isError = true;
            }

            cart.modifyTime = parseInt(new Date().getTime() / 1000.0);
            cart.modifyBy = memberId;
            await cart.save();

            if (isError) { 
              result.setError(
                ResultsCodes.PRODUCT_STORE_AVAILABE_ITEM_NOT_ENOUGH_TO_BOOK
              );
              return cb(null, result);
            }
          }

          result.setSuccess(true);
          return cb(null, result);
        } else { 
          result.setError(ResultsCodes.ITEM_NOT_FOUND);
          return cb(null, result);
        }
      })
      .catch(function(e) { 
        result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
        return cb(null, result);
      });
  };
  makeUpdateAmount().catch(err=>{
      result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
      return cb(null, result);
  });
};


  Carts.CountItem = function(ctx,cb) {
      
    let result = new Results(); 
    const makeCountItem = async () => {
      let Passport = Carts.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (!token) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      var memberId = token.userId;
      var ds = Carts.dataSource;
      var sql =
        "SELECT IFNULL(SUM(amount), 0) As CountItem FROM carts WHERE member_id = ?";
      var params = [memberId];
      ds.connector.query(sql, params, function(err, data) {
  
        if (err) {
          result.setError(ResultsCodes.SYSTEM_ERROR);
          return cb(null, result);
        } else { 
          result.setSuccess(data.length > 0 ? data[0].CountItem : 0);
          return cb(null, result);
        }
      });
    };
    makeCountItem().catch(function(e) { 
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  Carts.CheckCart = function(ctx,cb) {
      
    let result = new Results();
    const makeCheckCart = async () => {
      let Passport = Carts.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (!token) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      var memberId = token.userId;
      // var memberId = 167;
      var products = await Carts.find({
        fields: [
          "id",
          "productSellerId",
          "productStoreId",
          "sellerStoreId",
          "sellerId",
          "memberId",
          "amount",
          "affiliateId",
          "status",
          "createTime",
          "createBy",
          "modifyTime",
          "modifyBy"
        
        ],
        include: [
          {
            relation: "productStore",
            scope: {
              fields: ["id", "available"]
            }
          }
        ],
        where: {
          memberId: memberId
        }
      });

      var isError = false;
      for (var item in products) {
        var product = products[item];
        var productStore = product.toJSON().productStore;

        if (product.amount > productStore.available) {
          product.status = 0;

          if (!isError) {
            isError = true;
          }
        } else {
          product.status = 1;
        }

        product.modifyTime = parseInt(new Date().getTime() / 1000.0);
        product.modifyBy = memberId;
        await product.save();
      }
 
      if (isError) {
        result.setError(
          ResultsCodes.PRODUCT_STORE_AVAILABE_ITEM_NOT_ENOUGH_TO_BOOK
        );
      } else {
        result.setSuccess(true);
      }
      return cb(null, result);
    };
    makeCheckCart().catch(function(e) { 
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  /**
   * Add multple items to carts table (Web use only)
   * Created by Thiep Wong
   * Created 20/06/2018
   * @param {*} carts
   * @param {*} cb
   */
  Carts.CartSync = function(ctx, cb) {
      let carts = ctx.body.carts || [];

      let result = new Results();
    app.dataSources.mysqld
      .transaction(async models => {
        let Passport = Carts.app.models.Passport;
        let token = await Passport.GetMemberToken(ctx);
        if (!token) {
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        } 
        for (let index in carts) {
          let cart = carts[index];
          await CreateCart(
            models,
            token.userId,
            cart.productSellerId,
            cart.productStoreId,
            cart.sellerId,
            cart.sellerStoreId,
            cart.amount,
            0
          );
          // await Carts.CreateNew(cart.sellerProductId, cart.productStoreId, cart.sellerId, cart.sellerStoreId,cart.amount,0);
        }
 
        result.setSuccess(true);
        return cb(null, result);
      })
      .catch(function(e) { 
        result.setError(ResultsCodes.SYSTEM_ERROR);
        return cb(null, result);
      });
  };
  //#endregion

  //#region Expose Parameters
  Carts.remoteMethod("CartSync", {
    description: "@Thiep Wong, created multiple items from website frontend",
    http: {
      verb: "POST",
      path: "/cart-sync"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http:{
            source:'req'
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Carts.remoteMethod("GetListByMember", {
    http: {
      verb: "GET",
      path: "/getlistbymember"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Carts.remoteMethod("CreateNew", {
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http:{
            source:'req'
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Carts.remoteMethod("PreOrder", {
    http: {
      verb: "GET",
      path:'/preorder'
    },
    accepts:[
        {
            arg:'ctx',
            type:'object',
            http:{
                source:'req'
            }
        }
    ],
    returns: 
      {
        arg: "result",
        type: "string",
        root: true
      }
    
  });

  Carts.remoteMethod("UpdateAmount", {
      description:'@Updated by Thiep Wong, ong lam nao tu dau khong ro????!!!',
      http:{
          verb:'POST',
          path:'/updateamount'
      },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http:{
            source:'req'
        }
      } 
    ],
    returns: 
      {
        arg: "result",
        type: "string",
        root: true
      }
    
  });

  Carts.remoteMethod("UpdateAmountCustomer", {
    description:'@Updated by ThienVD update khi nhập tay so luong sp',
    http:{
        verb:'POST',
        path:'/updateamount'
    },
  accepts: [
    {
      arg: "ctx",
      type: "object",
      http:{
          source:'req'
      }
    } 
  ],
  returns: 
    {
      arg: "result",
      type: "string",
      root: true
    }
  
});

  Carts.remoteMethod("CountItem", {
    http: {
      verb: "GET"
    },
    accepts:[
        {
            arg:'ctx',
            type:'object',
            http:{
                source:'req'
            }
        }
    ],
    returns: 
      {
        arg: "result",
        type: "string",
        root: true
      }
    
  });

  Carts.remoteMethod("CheckCart", {
    http: {
      verb: "GET"
    },
    accepts:[
        {
            arg:'ctx',
            type:'object',
            http:{
                source:'req'
            }
        }
    ],
    returns: 
      {
        arg: "result",
        type: "string",
        root: true
      }
    
  });
  //#endregion
};
