/**
 * Model Category
 * Author: LuongPD
 * Last update: 13.3.2018 13:49
 */
var Results = require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');

module.exports = function (Category) {

    Category.sharedClass.methods().forEach(function (method) {   
        Category.disableRemoteMethodByName(method.name, method.isStatic);
      });


    //#region Functions
    /**
     * Edited by Thiep Wong, catch err and convert to result format
     * @param {*} cb  callback 
     */
    Category.GetListFull = function (cb) {
        Category.find({
            fields: ['id', 'title', 'level', 'icon', 'parentId', 'arrChildId', 'order'],
            include: {
                relation: 'menus',
                scope: {
                    fields: ['id', 'title', 'level', 'icon', 'parentId', 'arrChildId', 'order'],
                    include: {
                        relation: 'menus',
                        scope: {
                            fields: ['id', 'title', 'level', 'icon', 'parentId', 'arrChildId', 'order'],
                            include: {
                                relation: 'menus',
                                scope: {
                                    fields: ['id', 'title', 'level', 'icon', 'parentId', 'arrChildId', 'order'], 
                                    where: {
                                        'status': 1
                                    }
                            },
                                order: 'order asc'
                            },
                            where: {
                                'status': 1
                            },
                            order: 'order asc'
                        }
                    },
                    where: {
                        'status': 1
                    },
                    order: 'order asc'
                }
            },
            where: {
                'parentId': 0,
                'status': 1
            },
            order: 'order asc'
        }, function (err, instance) {
            let result = new Results();

            if (err) {
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }

            result.setSuccess(instance);
            cb(null, result);
        });
    }

    Category.ListManage = function (pageIndex, pageSize, pageOrder, cb) {
        if (!pageSize) {
            pageSize = 10;
            pageIndex = 0;
        }

        if (!pageOrder || pageOrder == 'undefined' || pageOrder == 'null') pageOrder = ['id desc'];

        Category.find({
            fields: ['id', 'title', 'level', 'icon', 'parentId', 'arrChildId', 'order'],
            limit: pageSize,
            skip: pageIndex * pageSize,
            order: pageOrder
        }, function (err, instance) {
            let result = new Results();

            if (err) {
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }

            result.setSuccess(instance);
            cb(null, result);
        });
    }

    /**
     * Get subcategory by its parent id.
     * Edited by Thiep Wong, check pid argument when it's invalid null, undefine, minus number etc but zero. Return result format
     * @param {*} pid parent id of category
     * @param {*} cb callback function
     */
    Category.SubCategory = function (pid, cb) {
        if (pid == undefined || pid == null || pid < 0) {
            let result = new Results();
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cb(null, result);
        }

        Category.find({ where: { parentId: pid } }, function (err, catInstance) {
            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }
            else {
                let result = new Results();
                result.setSuccess(catInstance);
                cb(null, result);
            }
        });
    }

    Category.CategoryFilter = function (filter, cb) {
        if (!filter) {
            return Category.GetListFull();
        }

        Category.find({ where: { title: { like: `%${filter}%` } } }, function (err, cInstant) {
            let result = new Results();
            if (err) {

                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }

            result.setSuccess(cInstant);
            cb(null, result);
        })
    }

    Category.AddNew = function (jsonData, cb) {

        const makeRequest = async () => {
            app.dataSources.mysqld.transaction(async models => {
                const { Category } = models;

                var newCategories = JSON.parse(jsonData);
                for (let i = 0; i < newCategories.length; i++) {
                    var newCategory = newCategories[i];
                    var categoryObj = new Category;
                    categoryObj.title = newCategory.title;
                    categoryObj.icon = newCategory.icon;
                    categoryObj.order = newCategory.order;
                    categoryObj.parentId = -1;
                    categoryObj.level = 0;
                    categoryObj.status = 1;
                    await Category.create(categoryObj);

                    categoryObj.arrChildId = categoryObj.id;
                    await categoryObj.save();
                }

                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);

            }).catch(function (e) {
                let result = new Results();
                result.setError(ResultsCodes.ADD_ITEM_FAILED);
                return cb(null, result);
            });
        }
        makeRequest();
    }

    Category.UpdateCat = function (id, title, icon, status, cb) {

        const makeRequest = async () => {
            app.dataSources.mysqld.transaction(async models => {
                const { Category } = models;

                var category = await Category.findOne({
                    where: {
                        "id": id
                    }
                });

                category.title = title;
                category.icon = icon;
                category.status = status;

                await categoryObj.save();

                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);

            }).catch(function (e) {
                let result = new Results();
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                return cb(null, result);
            });
        }
        makeRequest();
    }

    Category.UpdateTree = function (jsonData, cb) {

        const makeRequest = async () => {
            app.dataSources.mysqld.transaction(async models => {
                const { Category } = models;

                var categories = JSON.parse(jsonData);
                for (let i = 0; i < categories.length; i++) {

                    let categoryJson = categories[i];

                    if (categoryJson.mode == 1) { // add
                        await addCategoryTree(categoryJson);
                    } else if (categoryJson.mode == 2) { // remove
                        await removeCategoryTree(categoryJson);
                    }
                }

                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);

            }).catch(function (e) {
                let result = new Results();
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                return cb(null, result);
            });
        }
        makeRequest();
    }

    var addCategoryTree = async function (categoryJson) {

        // update category
        var category = await Category.findOne({
            where: {
                "id": categoryJson.id
            }
        });

        category.parentId = categoryJson.parentId;
        await category.save();

        // update parents
        var parentCategory = await Category.findOne({
            where: {
                "id": categoryJson.parentId
            }
        });

        if (parentCategory.arrChildId == null) {
            parentCategory.arrChildId = category.arrChildId;
        } else if (parentCategory.arrChildId.indexOf(category.id) == -1) {
            parentCategory.arrChildId += "," + category.arrChildId;
        }

        await parentCategory.save();

        // update grant parents
        if (categoryJson.arrParentId != "") {
            var arr = categoryJson.arrParentId.split(",").map(async function (val) {

                var grantCategory = await Category.findOne({
                    where: {
                        "id": Number(val)
                    }
                });

                if (grantCategory.arrChildId == null) {
                    grantCategory.arrChildId = category.id;
                } else if (grantCategory.arrChildId.indexOf(category.id) == -1) {
                    grantCategory.arrChildId += "," + category.id;
                }

                await grantCategory.save();
            });
        }
    }

    var removeCategoryTree = async function (categoryJson) {

        // update category
        var category = await Category.findOne({
            where: {
                "id": categoryJson.id
            }
        });

        category.parentId = -1;
        await category.save();

        // find list to remove
        let removeList = [];
        if (category.arrChildId != null) {
            var arr = category.arrChildId.split(",").map(async function (val) {
                removeList.push(Number(val));
            });
        }

        // update parents
        await parentRemoveChilds(categoryJson.parentId, removeList);

        // update grant parents
        if (categoryJson.arrParentId != "") {
            var arr = categoryJson.arrParentId.split(",").map(async function (val) {
                await parentRemoveChilds(Number(val), removeList);
            });
        }
    }

    var parentRemoveChilds = async function (parentCategoryId, removeList) {

        var parentCategory = await Category.findOne({
            where: {
                "id": parentCategoryId
            }
        });

        if (parentCategory.arrChildId != null) {
            for (var item in removeList) {
                var removeId = removeList[item];
                parentCategory.arrChildId = parentCategory.arrChildId.replace(',' + removeId, '');
            }
        }

        await parentCategory.save();
    }

    Category.ListFree = function (cb) {
        Category.find({
            fields: ['id', 'title', 'level', 'icon', 'parentId', 'arrChildId', 'order'],
            where: {
                'parentId': -1,
                'status': 1
            },
            order: 'order asc'
        }, function (err, instance) {
            let result = new Results();

            if (err) {
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }

            result.setSuccess(instance);
            cb(null, result);
        });
    }


    /**
     *  Get BreadCrumb for categoies
     * @param {*} id  
     * @param {*} cb 
     */
    Category.BreadCrumb = (id, cb) => {
        const makeRequest = async () => {
            let _category;
            let result = new Results();
            _category = await Category.findOne({
                fields: ['id', 'title', 'level', 'parent', 'parentId'],
                include: {
                    relation: 'parent',
                    scope: {
                        fields: ['id', 'title', 'level', 'parent', 'parentId'],
                        include: {
                            relation: 'parent',
                            scope: {
                                fields: ['id', 'title', 'level', 'parent', 'parentId'],
                                include: {
                                    relation: 'parent',
                                    scope: {
                                        fields: ['id', 'title', 'level', 'parent', 'parentId'],
                                    }
                                }
                            }
                        }
                    }
                },
                where: {
                    id: id
                }
            });

            let _result = await Category.ParentFetch(JSON.parse(JSON.stringify(_category)));

            if (!_result) {
                result.setError(ResultsCodes.ITEM_NOT_FOUND);
                return cb(null, result);
            }

            if (_result) {
                result.setSuccess(_result);

                cb(null, result);

            }
        }

        makeRequest();

    }

    Category.ParentFetch = (data) => { 

        if(!data) {
            return null;
        }

        // console.log(JSON.parse(data));
        let _cateArray = [];
        let _parent = data;
        for (let i = 0; i < data.level; i++) {

            _cateArray[(1 - _parent.level) * -1] = { title: _parent.title, id: _parent.id, level: _parent.level }
            // _cateArray.push();
            _parent = _parent.parent;
        }
        // console.log(_cateArray);
        return _cateArray;
    }

    //#endregion

    //#region Expose Parameters
    Category.remoteMethod('GetListFull', {
        http: {
            verb: "GET",
            path: '/list'
        },
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    Category.remoteMethod('SubCategory', {
        http: {
            verb: "GET",
            path: '/subs'
        },
        accepts: {
            arg: "pid",
            type: "number"
        },
        returns: {
            arg: "result",
            type: "object",
            root: true
        }
    })

    Category.remoteMethod('CategoryFilter', {
        description: '@Thiep Wong, get categories with filter',
        http: {
            verb: 'GET',
            path: '/filter'
        },
        accepts: [
            {
                arg: 'filter',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Category.remoteMethod('AddNew', {
        accepts: [
            {
                arg: 'jsonData',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Category.remoteMethod('UpdateCat', {
        accepts: [
            {
                arg: 'id',
                type: 'number'
            },
            {
                arg: 'title',
                type: 'string'
            },
            {
                arg: 'icon',
                type: 'string'
            },
            {
                arg: 'status',
                type: 'number'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Category.remoteMethod('UpdateTree', {
        accepts: [
            {
                arg: 'jsonData',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Category.remoteMethod('ListFree', {
        http: {
            verb: "GET"
        },
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Category.remoteMethod('ListManage', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: 'pageIndex',
                type: 'number'
            },
            {
                arg: 'pageSize',
                type: 'number'
            },
            {
                arg: 'pageOrder',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Category.remoteMethod('BreadCrumb', {
        description: '@Thiep Wong, get breadcrumb of category by leaf node id',
        http: {
            verb: 'GET',
            path: '/breadcrumb/:id'
        },
        accepts: [
            {
                arg: 'id',
                type: 'number'
            }
        ],
        returns: {
            type: 'string',
            root: true
        }
    })
    //#endregion
};