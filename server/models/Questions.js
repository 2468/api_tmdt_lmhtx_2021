/**
 * Model Questions
 * Author: LuongPD
 * Last update: 16.3.2018 13:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
'use strict';

module.exports = function (Questions) {

    //#region Functions
    Questions.GetListBySeller = function (ctx, cb) {

        let isAnwered = ctx.query.isAnwered || false;
        let pageIndex = ctx.query.pageIndex || 1;
        let pageSize = ctx.query.pageSize || 10;
        let pageOrder = ctx.query.pageOrder || null;

        const makeGetListBySeller = async () => {
            let Passport = Questions.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var sellerId = token.userId;

            let Answers = Questions.app.models.Answers;
            Answers.find({
                fields: ['id', 'questionId', 'sellerId', 'answer', 'modifyTime'],
                include: [{
                    relation: 'question',
                    scope: {
                        fields: ['id', 'productId', 'memberId', 'question', 'createTime'],
                        include: {
                            relation: 'product',
                            scope: {
                                fields: ['id', 'productName', 'productImage'],
                            }
                        }
                    }
                }],
                where: {
                    "sellerId": sellerId,
                    "answer": isAnwered == true ? { neq: null } : null
                },
                limit: pageSize,
                skip: pageIndex * pageSize,
                order: pageOrder
            }, function (err, results) {
                let result = new Results();
                result.setSuccess(results);
                return cb(null, result);
            })
        }
        makeGetListBySeller();
    }

    Questions.GetList = function (productId, sellerUuid, pageIndex, pageSize, pageOrder, cb) {

        const makeInfoRequest = async () => {
            var sellerId = 0;

            if (sellerUuid) {

                let seller = await Questions.app.models.Sellers.findOne({
                    fields: ['id'],
                    where: {
                        "uuid": sellerUuid
                    }
                });

                sellerId = seller.id;
            }

            if (sellerUuid && sellerId == 0) {
                let result = new Results();
                result.setError(ResultsCodes.ITEM_NOT_FOUND);
                return cb(null, result);
            } else {
                let questions = await Questions.find({
                    fields: ['id', 'productId', 'memberId', 'question', 'createTime'],
                    include: [{
                        relation: 'member',
                        scope: {
                            fields: ['id', 'fullname']
                        }
                    }, {
                        relation: 'answers',
                        scope: {
                            fields: ['id', 'sellerId', 'answer', 'modifyTime'],
                            include: {
                                relation: 'seller',
                                scope: {
                                    fields: ['id', 'shopName']
                                }
                            },
                            where: {
                                sellerId: sellerUuid ? sellerId : { "neq": null }
                                // sellerId: 34
                            }
                        }
                    }],
                    where: {
                        "productId": productId
                    },
                    limit: pageSize,
                    skip: pageIndex * pageSize,
                    order: pageOrder
                });

                // const questionFiltered = questions.filter(x => x.__data.answers.length > 0);

                let result = new Results();
                result.setSuccess(questions);
                return cb(null, result);
            }
        }

        makeInfoRequest().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    Questions.CreateNew = function (ctx, cb) {
        let productId = ctx.body.productId || 0;
        let sellerIds = ctx.body.sellerIds || 0;
        let question = ctx.body.question || null;

        let result = new Results();
        if (!productId || !sellerIds || !question) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }

        const makeCreateNew = async () => {
            let Passport = Questions.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var memberId = token.userId;

            app.dataSources.mysqld.transaction(async models => {
                const { Questions, Answers } = models;
                var questions = new Questions;
                questions.productId = productId;
                questions.memberId = memberId;
                questions.question = question;
                questions.createTime = parseInt(new Date().getTime() / 1000.0);
                questions.createBy = memberId;
                await Questions.create(questions);

                var sellerIdsArr = sellerIds.split(",").map(function (val) {
                    return Number(val);
                });

                for (var item in sellerIdsArr) {
                    var sellerId = sellerIdsArr[item];
                    if (sellerId == 0)
                        continue;

                    var answers = new Answers;
                    answers.questionId = questions.id;
                    answers.sellerId = sellerId;
                    answers.answer = null;
                    answers.createTime = parseInt(new Date().getTime() / 1000.0);
                    answers.createBy = memberId;
                    await Answers.create(answers);
                }
                result.setSuccess(true);
                return cb(null, result);

            }).catch(function (e) {
                result.setError(ResultsCodes.ADD_ITEM_FAILED);
                return cb(null, result);
            });
        }
        makeCreateNew().catch(err => {
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb(null, result);
        });
    }

    Questions.SearchQuestions = function (sellerUuid, productName, customerName, customerPhone, content, fromDate, toDate, pageIndex, pageSize, cb) {

        const makeInfoRequest = async () => {
            var sellerId = 0;
            let result = new Results();

            if (sellerUuid) {

                let seller = await Questions.app.models.Sellers.findOne({
                    fields: ['id'],
                    where: {
                        "uuid": sellerUuid
                    }
                });
                if (seller) {
                    sellerId = seller.id;
                } else {
                    result.setError(ResultsCodes.ITEM_NOT_FOUND);
                    return cb(null, result);
                }
            }

            let sql = `select distinct q.id, p.product_name productName, m.fullname, m.phone, q.question, q.create_time createTime, q.status from questions q
                inner join members m on m.id = q.member_id
                inner join products p on p.id = q.product_id
                inner join product_seller ps on ps.product_id = q.product_id
                where 1 = 1 `;

            let countSql = `select count(distinct q.id) totalRecords from questions q
                inner join members m on m.id = q.member_id
                inner join products p on p.id = q.product_id
                inner join product_seller ps on ps.product_id = q.product_id
                where 1 = 1 `;

            let params = [];

            if (sellerUuid) {
                sql += 'and ps.seller_id = ? ';
                countSql += 'and ps.seller_id = ? ';
                params.push(sellerId);
            }
            if (productName) {
                sql += `and p.product_name like '%${productName}%' `;
                countSql += `and p.product_name like '%${productName}%' `;
            }
            if (customerName) {
                sql += `and m.fullname like '%${customerName}%' `;
                countSql += `and m.fullname like '%${customerName}%' `;
            }
            if (customerPhone) {
                sql += `and m.phone like '%${customerPhone}%'`;
                countSql += `and m.phone like '%${customerPhone}%' `;
            }
            if (content) {
                sql += `and q.question like '%${content}%' `;
                countSql += `and q.question like '%${content}%' `;
            }
            if (fromDate) {
                // let date = new Date(fromDate);
                // const from = date.getTime();
                sql += `and q.create_time >= '${fromDate}' `;
                countSql += `and q.create_time >= '${fromDate}' `;

               
            }
            if (toDate) {
                // let date = new Date(toDate);
                // date.setDate(date.getDate() + 1);
                // const to = date.getTime();
                sql += `and q.create_time < '${toDate}' `;
                countSql += `and q.create_time < '${toDate}' `;
            }
            let limit = pageSize ? pageSize : 10;
            let offset = pageIndex ? (pageIndex * pageSize) : 0;
            sql += `order by q.id desc    limit ${limit} offset ${offset}`;

            let totalRecords = 0;
            var ds = Questions.dataSource;

            await ds.connector.query(countSql, params, function (error, data) {
                console.log(data);
                if (error) {
                    result.setError(ResultsCodes.DATA_NOT_FOUND);
                    return cb(null, result);
                }
                totalRecords = data[0].totalRecords;
                console.log(data);
            });
            console.log(totalRecords);

            await ds.connector.query(sql, params, function (error, data) {
                if (error) {
                    result.setError(ResultsCodes.DATA_NOT_FOUND);
                    return cb(null, result);
                }
                result.setSuccess({ totalRecords: totalRecords, pageSize: pageSize, pageIndex: pageIndex, data: data, message: 'Thanh cong' });
                cb(null, result);
            });

        }

        makeInfoRequest().catch(function (e) {
            console.log(e);
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }
    //#endregion

    //#region Expose Parameters
    Questions.remoteMethod('GetListBySeller', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    Questions.remoteMethod('GetList', {
        http: {
            verb: "GET"
        },
        accepts: [{
            arg: "productId",
            type: "number"
        }, {
            arg: "sellerUuid",
            type: "string"
        }, {
            arg: "pageIndex",
            type: "number"
        }, {
            arg: "pageSize",
            type: "number"
        }, {
            arg: "pageOrder",
            type: "string"
        }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    Questions.remoteMethod('CreateNew', {
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Questions.remoteMethod('SearchQuestions', {
        http: {
            verb: "GET",
            path: '/search-questions'
        },
        accepts: [
            {
                arg: "sellerUuid",
                type: "string"
            },
            {
                arg: "productName",
                type: "string"
            },
            {
                arg: "customerName",
                type: "string"
            },
            {
                arg: "customerPhone",
                type: "string"
            },
            {
                arg: "content",
                type: "string"
            },
            {
                arg: "fromDate",
                type: "number"
            },
            {
                arg: "toDate",
                type: "number"
            },
            {
                arg: "pageIndex",
                type: "number"
            },
            {
                arg: "pageSize",
                type: "number"
            }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })
    //#endregion
};