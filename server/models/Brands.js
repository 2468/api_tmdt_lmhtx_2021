var Results = require('./../middlewares/ResultHandle');
var ResultCodes = require('./../middlewares/message_constant');
'use strict'
module.exports = function(Brands) {

    Brands.RegisterBrand = (ctx,cb)=>{

        const makeRequest = async() =>{
         let   brandName = ctx.body.brandName || null;
         let brandGCP = ctx.body.brandGCP || null; 
         let brandKeySearch = ctx.body.brandKeySearch || null; 
         let email = ctx.body.email || null;
         let phone = ctx.body.email || null; 
         let logo = ctx.body.logo || null; 
         let status = ctx.body.status || 0;
         let seekOriginId = ctx.body.seekAnOriginId || 0;

            let result = new Results();
            let Passport = Brands.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);

            if(!token.userId) {
                result.setError(ResultCodes.ACCESS_TOKEN_INVALID);
                return cb(null,result);
            }
            Brands.create({
                brandName:brandName,
                brandGCP:brandGCP,
                keySearch:brandKeySearch,
                email:email,
                phone:phone,
                logo:logo,
                status:status,
                seekAnOriginId:seekOriginId,
                createTime:new Date().getTime()/1000,
                createBy: token.userId
            },(err,res)=>{
                if(err) {
                    result.setError(ResultCodes.ACCESS_TOKEN_INVALID);
                    return cb(null,result);
                } 
                //result.setSuccess(ResultCodes.ADDED_SUCCESS);
                //return cb(null,result); 
                result.setSuccess(res);
                return cb(null,result); 
            })
        }

        makeRequest();

    }

   

    Brands.GetBrands = (brandName,cb)=>{

        const makeRequest = async()=>{
            let result = new Results();
           
            let where = brandName?{
                where: {'brandName':{like: '%'+brandName+'%'},} 
            }: null;    
            Brands.find(where,(err,res)=>{
                if(err) {
                result.setError(err);
                return cb(null,result);
                }
    
                result.setSuccess(res);
                cb(null,result);
            });
        }
      makeRequest();
    }
  // ThienVD
  // Lấy thương hiệu >-1
    Brands.GetBrandsFilter = function (brandName, cb) 
    {        
        let result = new Results();
        let sql = `SELECT id, brand_name AS  brandName, logo, email, phone,status      FROM  brands WHERE status > 0`;
                        let params = [];
                        if(brandName != undefined && brandName !=null)
                        {
                            sql = sql + ` AND brand_name like  '%${brandName}%'`;
                        }                      

         
        let connector = Brands.dataSource.connector;
        connector.query(sql, params, function (err, res) {
            if (err) {
                result.setError(err);
                return cb(null,result);
            }
            else {
                result.setSuccess(res);
                cb(null,result);
            }
        });

    }









    

  


    Brands.GetListBrands = (cb)=>{
        let result = new Results();
        Brands.find({
            fields: {}          
        },(err,res)=>{
            if(err) {
                result.setError(err);
                return cb(null,result);
            }

            result.setSuccess(res);
            cb(null,result);           
 
        })
    }




  

    Brands.remoteMethod('RegisterBrand',{
        description:'@Thiep Wong, create a new brand',
        http:{
            verb:'POST',
            path:'/register-brand'
        },
        accepts:[
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns:{
            arg:'result',
            type:'string',
            root:true
        }
    })

    Brands.remoteMethod('GetBrandsFilter',{
        description:'@Thien getlistBrandsFilter status >-1',
        http:{
            verb:'GET',
            path:'/listfilter'
        },
        accepts:[
            {
                arg:'brandName',
                type:'string'
            }
        ],
        returns:{
            type:'string',
            root:true
        }
    })

    Brands.remoteMethod('GetBrands',{
        description:'@Thiep Wong, get all brands as list',
        http:{
            verb:'GET',
            path:'/list'
        },
        accepts:[
            {
                arg:'brandName',
                type:'string'
            }
        ],
        returns:{
            type:'string',
            root:true
        }
    })

    Brands.remoteMethod('GetListBrands',{
        description:'@Thien VD, get all brands as list ',
        http:{
            verb:'GET',
            path:'/get-list'
        },
        accepts:[],
        returns: {
            type:'string',
            root:true
        }

    })
};