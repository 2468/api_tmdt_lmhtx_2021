/**
 * Model ProductStore
 * Author: LuongPD
 * Last update: 15.3.2018 13:49
 */
var Results = require("../middlewares/ResultHandle");
var ResultsCodes = require("../middlewares/message_constant");
var app = require("../../server/server");
("use strict");

module.exports = function(ProductStore) { 
  //#region Functions

  ProductStore.AddProduct = function(
    sellerId,
    productId,
    sellerStoreId,
    total,
    available,
    cb
  ) {
    app.dataSources.mysqld
      .transaction(async models => {
        await this.Add(
          models,
          sellerId,
          productId,
          sellerStoreId,
          total,
          available
        );

        let result = new Results();
        result.setSuccess(true);
        return cb(null, result);
      })
      .catch(function(e) {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });
  };

  ProductStore.Add = function(
    models,
    sellerId,
    productId,
    sellerStoreId,
    total,
    available
  ) {
    const { ProductStore } = models;

    let _store = new ProductStore();
    _store.productSellerId = productId;
    _store.sellerStoreId = sellerStoreId;
    _store.total = total;
    _store.available = available;
    _store.createBy = sellerId;
    _store.createTime = new Date() / 1000;

    ProductStore.create(_store);
  };

  ProductStore.GetListByProductSeller = function(productSellerId, cb) {
    ProductStore.find(
      {
        fields: [
          "id",
          "productSellerId",
          "sellerStoreId",
          "total",
          "available",
          "booked"
        ],
        include: {
          relation: "stores",    
      
            scope: {
                fields: ["id", "storeName", "address","status"],      
        },          
       
        },
        where: {
          productSellerId: productSellerId
          
        }
      },
      function(err, instance) {
        if (err) {
          let result = new Results();
          result.setError(ResultsCodes.SYSTEM_ERROR);
          return cb(null, result);
        }

       // console.log(instance);
        let result = new Results();
        result.setSuccess(instance);
        cb(null, result);
      }
    );
  };

  /**
   * Added by Hung Doan
   * Update amount of total from seller
   * @param {*} threshold
   * @param {*} cb
   */
  ProductStore.GetListByThreshold = function(ctx, cb) {
    let threshold = parseInt(ctx.query.threshold) || 0;
    let pageSize = parseInt(ctx.query.pageSize )|| 0;
    let pageIndex = parseInt(ctx.query.pageIndex) || 0;
    let productName = ctx.query.productName || null;
    let result = new Results();
    const start = async () => {
      let token = await ProductStore.app.models.Passport.GetSellerToken(ctx);
      if (!token) { 
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }
      let sql = `SELECT 
                pd.id id ,
                pd.product_name productName,
                ps.id productSellerId,
                ps.seller_id sellerId,
                pst.id productStoreId,
                ss.store_name storeName,
                pst.available
                FROM
                    product_store pst
                      LEFT  JOIN
                    product_seller ps ON ps.id = pst.product_seller_id
                      LEFT    JOIN
                    products pd ON pd.id = ps.product_id
                      LEFT  JOIN
                    seller_store ss ON ss.id = pst.seller_store_id
                WHERE
                        pst.available <= ?
                        AND ps.seller_id = ?`
                
     // if(productName != "undefined" || productName !=null)
      if(productName && productName !== "undefined" && /\S/.test(productName))
      {
        sql = sql + ` AND pd.product_name like '%${productName}%' ORDER BY pd.product_name ASC, ss.store_name ASC  LIMIT ? OFFSET ?`;       
      }
      else
      {
        sql = sql + ` ORDER BY pd.product_name ASC, ss.store_name ASC  LIMIT ? OFFSET ?`;  
      }
      // if (productName && /\S/.test(productName)) {
      //   sql = sql + " AND pd.product_name LIKE ? ";
      //   params.push("%" + productName + "%");
      // }

      if (pageSize) {
        if (pageSize <= 0) {
          pageSize = 10;
        }
      } else {
        pageSize = 10;
      }
      if (pageIndex) {
        if (pageIndex < 1) {
          pageIndex = 1;
        }
      } else {
        pageIndex = 1;
      }
      let filterSql = `SELECT 
            COUNT(*) totalRecords
            FROM
                (SELECT 
                    pd.id id,
                        pd.product_name productName,
                        ps.id productSellerId,
                        ps.seller_id sellerId,
                        pst.id productStoreId,
                        ss.store_name storeName,
                        pst.available
                FROM
                    product_store pst
                LEFT JOIN product_seller ps ON ps.id = pst.product_seller_id
                LEFT  JOIN products pd ON pd.id = ps.product_id
                LEFT  JOIN seller_store ss ON ss.id = pst.seller_store_id
                WHERE
                        pst.available <= ?
                        AND ps.seller_id = ?`
    if(productName && productName !== "undefined" && /\S/.test(productName))
      {
        filterSql = filterSql + `   AND pd.product_name like '%${productName}%'   ORDER BY pd.product_name ASC , ss.store_name ASC) AS COUNT`;
      }
      else
      {
        filterSql = filterSql + ` ORDER BY pd.product_name ASC , ss.store_name ASC) AS COUNT`

      }


             

      let filterParams = [threshold, token.userId];
      let count = await ProductStore.Count(filterSql, filterParams);

      let params = [
        threshold,
        token.userId,
        pageSize,
        (pageIndex - 1) * pageSize
      ];
      let connector = ProductStore.dataSource.connector;
      let stores = await new Promise((resolve, reject) => {
        connector.query(sql, params, function(err, data) {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });
 
      result.setSuccess({
        totalRecords: count[0].totalRecords,
        pageSize: pageSize,
        pageIndex: pageIndex,
        pageData: stores 
      });
      return cb(null, result);
    };

    start().catch(function(e) { 
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  /**
   * Count
   * @param {*} sql
   * @param {*} params
   */
  ProductStore.Count = function(sql, params) {
    let promise = new Promise((resolve, reject) => {
      var ds = ProductStore.dataSource;
      ds.connector.query(sql, params, function(err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
    return promise;
  };

  /**
   * Edited by Thiep Wong
   * Update amount of total from seller
   * @param {*} productStoreId
   * @param {*} amount
   * @param {*} cb
   */
  ProductStore.UpdateAvailable = function(productStoreId, amount, cb) {
    app.dataSources.mysqld
      .transaction(async models => {
        let result = new Results();
        const { ProductStore, StoreHistory } = models;

        const productStore = await ProductStore.findOne({
          fields: [
            "id",
            "productSellerId",
            "sellerStoreId",
            "total",
            "available",
            "booked",
            "createBy",
            "createTime",
            "modifyBy",
            "modifyTime"
          ],
          where: {
            id: productStoreId
          }
        });

        if (productStore == null) {
          result.setError(ResultsCodes.ITEM_NOT_FOUND);
          return cb(null, result);
        }

        // giảm hàng trong kho
        if (amount < 0 && productStore.available < -1 * amount) {
          result.setError(ResultsCodes.PRODUCT_STORE_DECREASE_ITEM_NOT_ENOUGH);
          return cb(null, result);
        }

        productStore.total += amount;
        productStore.available += amount;
        productStore.modifyTime = parseInt(new Date().getTime() / 1000.0);
        productStore.modifyBy = 0;
        await productStore.save();
        await CreateStoreHistory(productStore.id, 0, amount, 0);
        result.setSuccess(true);
        cb(null, result);
      })
      .catch(function(e) {
        result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
        cb(null, result);
      });
  };

  // ProductStore.UpdateShipping = function (productStoreId, ordersDetailsId, cb) {
  //     app.dataSources.mysqld.transaction(async models => {
  //         const { OrdersDetails, ProductStore, StoreHistory } = models;

  //         const ordersDetails = await OrdersDetails.findOne({
  //             fields: ['id', 'amount'],
  //             where: {
  //                 "id": ordersDetailsId
  //             }
  //         });

  //         if (ordersDetails == null) {
  //             throw new Error(JSON.stringify(ResultsCodes.PRODUCT_STORE_BOOKED_ITEM_SMALLER_THAN_ZERO));
  //         }

  //         if (amount < 0) {
  //             throw new Error(JSON.stringify(ResultsCodes.PRODUCT_STORE_BOOKED_ITEM_SMALLER_THAN_ZERO));
  //         }

  //         const productStore = await ProductStore.findOne({
  //             fields: ['id', 'productSellerId', 'sellerStoreId', 'total', 'available', 'booked', 'createBy', 'createTime', 'modifyBy', 'modifyTime'],
  //             where: {
  //                 "id": productStoreId
  //             }
  //         });

  //         if (productStore == null) {
  //             throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
  //         }

  //         if (productStore.booked < amount) {
  //             throw new Error(JSON.stringify(ResultsCodes.PRODUCT_STORE_BOOKED_ITEM_NOT_ENOUGH_TO_SHIP));
  //         }

  //         productStore.booked -= amount;
  //         productStore.total -= amount;
  //         productStore.modifyTime = parseInt(new Date().getTime() / 1000.0);
  //         await productStore.save();
  //         await CreateStoreHistory(productStore.id, orderDetailsId, 0, (-1 * amount));

  //         Results.success = {
  //             data: true,
  //             message: ResultsCodes.API_SUCCESSED,
  //         };
  //         Results.warning = null;
  //         Results.errors = null;
  //         cb(null, Results);

  //     }).catch(function (e) {
  //         Results.success = null;
  //         Results.warning = null;
  //         Results.errors = {
  //             message: e.message
  //         };
  //         cb(null, Results);
  //     });
  // }

  var CreateStoreHistory = async function(
    productStoreId,
    orderDetailsId,
    itemAvailable,
    itemBooked
  ) {
    var StoreHistory = ProductStore.app.models.StoreHistory;
    var storeHistory = new StoreHistory();
    storeHistory.productStoreId = productStoreId;
    storeHistory.orderDetailsId = orderDetailsId;
    storeHistory.itemAvailable = itemAvailable;
    storeHistory.itemBooked = itemBooked;
    storeHistory.createTime = parseInt(new Date().getTime() / 1000.0);
    await StoreHistory.create(storeHistory);
  };
  //#endregion
 
  //#region Expose Parameters
  ProductStore.remoteMethod("GetListByProductSeller", {
    http: {
      verb: "GET",
      path: "/product/:productSellerId"
    },
    accepts: [
      {
        arg: "productSellerId",
        type: "number"
      }
    ],
    returns: [
      {
        arg: "result",
        type: "string",
        root: true
      }
    ]
  });

  ProductStore.remoteMethod("GetListByThreshold", {
    http: {
      verb: "GET",
      path: "/list"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
      //   {
      //     arg: "threshold",
      //     type: "number",
      //     required: true
      //   },
      //   {
      //     arg: "pageSize",
      //     type: "number"
      //   },
      //   {
      //     arg: "pageIndex",
      //     type: "number"
      //   }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  ProductStore.remoteMethod("UpdateAvailable", {
    http: {
      verb: "POST",
      path: "/update/:productStoreId"
    },
    accepts: [
      {
        arg: "productStoreId",
        type: "number",
        required: true
      },
      {
        arg: "amount",
        type: "number",
        required: true
      }
    ],
    returns: [
      {
        arg: "result",
        type: "string",
        root: true
      }
    ]
  });

  // ProductStore.remoteMethod('UpdateShipping', {
  //     accepts: [{
  //         arg: "productStoreId",
  //         type: "number",
  //         required: true
  //     }, {
  //         arg: "ordersDetailsId",
  //         type: "number",
  //         required: true
  //     }
  //     ],
  //     returns: [{
  //         arg: 'result',
  //         type: 'string',
  //         root: true
  //     }]
  // })
  //#endregion
};
