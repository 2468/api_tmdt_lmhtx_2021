/**
 * Model ShippingBill
 * Author: LuongPD
 * Last update: 19.3.2018 13:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var ShippingCodes = require('../middlewares/shipping_constant');
var OrdersCodes = require('../middlewares/orders_constant');
var app = require('../../server/server');
var Config = require('../middlewares/GhtkApiConfig');
var http = require('request');
'use strict';

module.exports = function (ShippingBill) {

    //#region Functions
    ShippingBill.GetListBySeller = function (ctx, cb) {

        let status = ctx.query.status || 0;
        let pageIndex = ctx.query.pageIndex || 1;
        let pageSize = ctx.query.pageSize || 10;
        let pageOrder = ctx.query.pageOrder || null;

        let result = new Results();
        const makeGetListBySeller = async () => {
            let Passport = ShippingBill.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var sellerId = token.userId;

            ShippingBill.find({
                fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note', 'label_id'],
                include: [{
                    relation: 'orders',
                    scope: {
                        fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerAddress', 'buyerLocationId', 'totalPrice', 'totalShipFee', 'paymentType', 'transactionNumber', 'status'],
                    }
                }, {
                    relation: 'ordersDetails',
                    scope: {
                        fields: ['id', 'shippingBillId', 'productSellerId', 'affiliateId', 'price', 'unit', 'amount']
                    }
                }, {
                    relation: 'sellerStore',
                    scope: {
                        fields: ['id', 'sellerId', 'managerName', 'managerPhone', 'address', 'locationId', 'locationLevel'],
                        include: [{
                            relation: 'seller',
                            scope: {
                                fields: ['id', 'shopName', 'fullname']
                            }
                        }]
                    }
                }, {
                    relation: 'shippingHistories',
                    scope: {
                        fields: ['id', 'shippingBillId', 'status', 'shortDescription', 'createTime']
                    }
                }],
                where: {
                    "sellerId": sellerId,
                    "status": status
                },
                limit: pageSize,
                skip: pageIndex * pageSize,
                order: pageOrder
            }, function (err, instance) {
                Results.success = {
                    data: instance,
                    message: ResultsCodes.API_SUCCESSED,
                };
                Results.warning = null;
                Results.errors = null;
                cb(null, Results);
            })
        }
        makeGetListBySeller();
    }

    ShippingBill.ConfirmBySeller = function (shippingBillId, ctx, cb) {

        const makeConfirmBySeller = async () => {

            let result = new Results();

            let Passport = ShippingBill.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var sellerId = token.userId;

            app.dataSources.mysqld.transaction(async models => {
                const { ShippingBill, ProductStore, ShippingHistory, StoreHistory } = models;

                let shippingBill = await ShippingBill.findOne({
                    // fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note'],
                    include: [
                        {
                            relation: 'ordersDetails',
                            scope: {
                                include: {
                                    relation: 'productSeller',
                                    scope: {
                                        include: {
                                            relation: 'product'
                                        }
                                    }
                                }
                            }
                        },
                        {
                            relation: 'sellerStore',
                            scope: {
                                include: {
                                    relation: "seller"
                                }
                            }
                        },
                        {
                            relation: 'orders'
                        }
                    ],
                    where: {
                        "id": shippingBillId
                    }
                });
                // console.log(shippingBill);

                if (shippingBill == null) {
                    result.setError(ResultsCodes.ITEM_NOT_FOUND);
                    return cb(null, result);
                }

                if (shippingBill.status == ShippingCodes.DA_DAT_HANG.status) {
                    var ordersDetails = shippingBill.toJSON().ordersDetails;

                    for (var item in ordersDetails) {
                        var productSellerId = ordersDetails[item].productSellerId;
                        var sellerStoreId = shippingBill.sellerStoreId;
                        var orderDetailsId = ordersDetails[item].id;
                        var amount = ordersDetails[item].amount;

                        if (amount < 0) {
                            result.setError(ResultsCodes.PRODUCT_STORE_BOOKED_ITEM_SMALLER_THAN_ZERO);
                            return cb(null, result);
                        }
                        else {
                            const productStore = await ProductStore.findOne({
                                fields: ['id', 'productSellerId', 'sellerStoreId', 'total', 'available', 'booked', 'createBy', 'createTime', 'modifyBy', 'modifyTime'],
                                where: {
                                    "productSellerId": productSellerId,
                                    "sellerStoreId": sellerStoreId
                                }
                            })

                            if (productStore == null) {
                                result.setError(ResultsCodes.ITEM_NOT_FOUND);
                                return cb(null, result);
                            }
                            else {
                                if (productStore.available < amount) {
                                    let result = new Results();
                                    result.setError(ResultsCodes.PRODUCT_STORE_AVAILABE_ITEM_NOT_ENOUGH_TO_BOOK);
                                    return cb(null, result);
                                }
                                else {
                                    productStore.booked += amount;
                                    productStore.available -= amount;
                                    productStore.modifyTime = parseInt(new Date().getTime() / 1000.0);
                                    await productStore.save();

                                    await StoreHistory.CreateNew(productStore.id, orderDetailsId, 0, amount, null);
                                }
                            }
                        }
                    }

                    shippingBill.status = ShippingCodes.CUA_HANG_XAC_NHAN.status;
                    shippingBill.modifyTime = parseInt(new Date().getTime() / 1000.0);
                    // shippingBill.modifyBy = sellerId;
                    let shipping = await shippingBill.save();
                    // console.log(shippingBill);

                    // memberId
                    var memberId = 0;

                    await ShippingHistory.CreateNew(shippingBill.id, ShippingCodes.CUA_HANG_XAC_NHAN, null);
                    console.log(shipping);
                    // count orderDetail is not shipped
                    let sql = `select count(1) as countBillNotShipped from shipping_bill sb
                                where sb.order_id = ? and sb.status < ?`;
                    let params = [shippingBill.orderId, ShippingCodes.CUA_HANG_XAC_NHAN.status];
                    let connector = ShippingBill.dataSource.connector;
                    await connector.query(sql, params, async function (err, resultObjects) {
                        // console.log(sql);
                        let countBillNotShipped = -1;
                        if (resultObjects.length > 0) {
                            countBillNotShipped = resultObjects[0].countBillNotShipped;
                        }
                        // console.log(resultObjects);
                        if (countBillNotShipped === 0 || countBillNotShipped === 1) {
                            let _orders = await ShippingBill.app.models.Orders;

                            try {
                                var order = await _orders.findOne({
                                    where: { id: shippingBill.orderId }
                                });
                                console.log(order);

                                // if (order && order.status < ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                                //     memberId = order.memberId;
                                //     order.status = ShippingCodes.CUA_HANG_XAC_NHAN.status;
                                //     await order.save();
                                // }
                            } catch (ex) {
                                console.error(ex);
                            }

                            if (order && order.status < ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                                memberId = order.memberId;
                                order.status = ShippingCodes.CUA_HANG_XAC_NHAN.status;
                                await order.save();
                            }


                            var Notification = ShippingBill.app.models.Notification;

                            Notification.destroyAll({ memberId: order.memberId }, { orderId: order.id });

                            //let notification = await Notification.find();
                            //console.log(notification);

                            await Notification.createNotification(order.memberId, shippingBill.orderId, shippingBill.id, order.orderCode, OrdersCodes.CUA_HANG_XAC_NHAN.status);
                        }
                    });

                    // shipmment order
                    let products = [];
                    shippingBill.ordersDetails().forEach(od => {
                        //console.log(od);
                        // thienvd 24-02-2020
                        // Sua weight phai nhan them vs so luong
                        let weightPro = 0;

                        if(od.productSeller.product.weight !=null && od.productSeller.product.weight != undefined && od.productSeller.product.weight>0)
                        {
                           //if(od.productSeller.product.weight<1000)
                           if(od.productSeller.product.weight<1)
                           {
                            //weightPro= 1000 * od.amount;
                            weightPro=  od.amount; 
                           }
                           else
                           {
                            weightPro = od.productSeller.product.weight * od.amount;
                           }
                        }
                        else
                        {
                           // weightPro = 1000;
                           weightPro = 1;

                        }
                        let product = {
                            name: od.productSeller.product.productName,
                            weight: weightPro,
                            //  (od.productSeller.product.weight  ? od.productSeller.product.weight / 1000 : 1),
                            quantity: od.amount
                        }
                        products.push(product);
                    });

                    var sellerLocation = await getLocationByCode(shippingBill.sellerStore().locationCode);
                    let customerLocation = await getLocationByCode(shippingBill.orders().buyerLocationCode);

                    let order = {
                        id: shippingBill.id,
                        pick_name: shippingBill.sellerStore().storeName,
                        pick_money: shippingBill.billPrice,
                        pick_address: shippingBill.sellerStore().address,
                        pick_province: customerLocation.parent().parent().name,
                        pick_district:sellerLocation.parent().name,
                        //pick_district: customerLocation.parent().name,
                        pick_tel: shippingBill.sellerStore().seller.mobile,
                        tel: shippingBill.orders().buyerPhone,
                        email: shippingBill.orders().buyerPhone + '@gmail.com',
                        name: shippingBill.orders().buyerName,
                        address: shippingBill.orders().buyerAddress,
                        province: sellerLocation.parent().parent().name,
                        //district: sellerLocation.parent().name,
                        district: customerLocation.parent().name,
                        is_freeship: shippingBill.shipFee > 0 ? 0 : 1,
                        note: shippingBill.note ? shippingBill.note : shippingBill.sellerStore().storeName,
                        // cod
                        pick_option: shippingBill.orders().paymentType = 0 ? 'cod' : 'post'

                    };

                    //thienvd check khác cửa hàng tự giao thì mới tạo vận đơn bên GHTK
                    // 09-01-2020
                    if (shippingBill.shippingType != 0) {
                        let shipment = await shipmentOrder(products, order, shippingBill.id);
                    }
                }

                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);

            }).catch(function (err) {
                console.log(err);
                let result = new Results();
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
                return cb(null, result);
            });

        }
        makeConfirmBySeller()
    }

    ShippingBill.ReceivedByMember = function (ctx, cb) {
        let shippingBillId = ctx.body.shippingBillId || 0;

        const makeReceivedByMember = async () => {
            let Passport = ShippingBill.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);

            let result = new Results();
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var memberId = token.userId;

            app.dataSources.mysqld.transaction(async models => {
                const { Orders, ShippingBill, ShippingHistory } = models;

                const shippingBill = await ShippingBill.findOne({
                    fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note'],
                    include: [{
                        relation: 'ordersDetails',
                        scope: {
                            fields: ['id', 'shippingBillId', 'productSellerId', 'amount']
                        }
                    }],
                    where: {
                        "id": shippingBillId
                    }
                });

                if (shippingBill == null) {
                    throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
                }

                if (shippingBill.status == ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                    // add step delivery if missing
                    if (shippingBill.status == ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                        var ordersDetails = shippingBill.toJSON().ordersDetails;

                        for (var item in ordersDetails) {
                            var productSellerId = ordersDetails[item].productSellerId;
                            var sellerStoreId = shippingBill.sellerStoreId;
                            var orderDetailsId = ordersDetails[item].id;
                            var amount = ordersDetails[item].amount;
                            await UpdateShipping(productSellerId, sellerStoreId, orderDetailsId, amount)
                        }

                        let _shippingHistory = ShippingBill.app.models.ShippingHistory;
                        await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.DANG_GUI_DI, null);
                    }

                    shippingBill.status = ShippingCodes.KET_THUC.status;
                    shippingBill.modifyTime = parseInt(new Date().getTime() / 1000.0);
                    await shippingBill.save();

                    let _shippingHistory = ShippingBill.app.models.ShippingHistory;
                    await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.DA_GIAO_THANH_CONG, null);
                    await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.KET_THUC, null);

                    var sql = "SELECT COUNT(1) as CountBill FROM shipping_bill WHERE order_id=? AND status != ?";
                    var params = [shippingBill.orderId, ShippingCodes.KET_THUC.status];
                    var connector = ShippingBill.dataSource.connector;
                    connector.query(sql, params, async function (err, resultObjects) {

                        var countBill = 0;
                        if (resultObjects.length > 0) {
                            countBill = resultObjects[0].CountBill;
                        }

                        if (countBill == 1) {
                            var Orders = ShippingBill.app.models.Orders;
                            const orders = await Orders.findOne({
                                // fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
                                where: {
                                    "id": shippingBill.orderId
                                }
                            })

                            orders.status = OrdersCodes.DA_GIAO_THANH_CONG.status;
                            orders.modifyTime = parseInt(new Date().getTime() / 1000.0);
                            orders.modifyBy = memberId;
                            await orders.save();

                            // Create notification
                            let Notification = ShippingBill.app.models.Notification;
                            await Notification.createNotification(order.memberId, shippingBill.orderId, shippingBill.id, null, OrdersCodes.DA_GIAO_THANH_CONG.status);

                            result.setSuccess(true);
                            return cb(null, result);
                        }
                        else {
                            result.setSuccess(true);
                            return cb(null, result);
                        }
                    })
                }
                else {
                    result.setSuccess(true);
                    return cb(null, result);
                }
            }).catch(function (err) {
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
                return cb(null, result);
            });
        }
        makeReceivedByMember().catch(err => {
            result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
            return cb(null, result);
        });
    }

    ShippingBill.DeliveryBySeller = function (shippingBillId, cb) {

        // const makeDeliveryBySeller = async () => {
        //     let Passport = ShippingBill.app.models.Passport;

        //     if (!token) {
        //         let result = new Results();
        //         result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        //         return cb(null, result);
        //     }
        //     var sellerId = token.userId;

        app.dataSources.mysqld.transaction(async models => {
            const { ShippingBill, ProductStore, ShippingHistory, StoreHistory } = models;

            const shippingBill = await ShippingBill.findOne({
                // fields: ['id', 'sellerStoreId', 'sellerId', 'status'],
                include: [{
                    relation: 'ordersDetails',
                    scope: {
                        fields: ['id', 'shippingBillId', 'productSellerId', 'amount']
                    }
                }],
                where: {
                    "id": shippingBillId
                }
            });

            if (shippingBill == null) {
                throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
            }

            if (shippingBill.status == ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                var ordersDetails = shippingBill.toJSON().ordersDetails;

                for (var item in ordersDetails) {
                    var productSellerId = ordersDetails[item].productSellerId;
                    var sellerStoreId = shippingBill.sellerStoreId;
                    var orderDetailsId = ordersDetails[item].id;
                    var amount = ordersDetails[item].amount;
                    await UpdateShipping(productSellerId, sellerStoreId, orderDetailsId, amount)
                }

                shippingBill.status = ShippingCodes.DANG_GUI_DI.status;
                shippingBill.modifyTime = parseInt(new Date().getTime() / 1000.0);
                // shippingBill.modifyBy = sellerId;
                await shippingBill.save();

                let _shippingHistory = ShippingBill.app.models.ShippingHistory;
                await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.DANG_GUI_DI, null);
            }

            let result = new Results();
            result.setSuccess(true);
            return cb(null, result);

        }).catch(function (err) {
            let result = new Results();
            result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
            return cb(null, result);
        });
        // }
        // makeDeliveryBySeller();
    }

    ShippingBill.ReceivedBySeller = function (shippingBillId, cb) {

        // const makeReceivedBySeller = async () => {
        //     let Passport = ShippingBill.app.models.Passport;

        //     if (!token) {
        //         let result = new Results();
        //         result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        //         return cb(null, result);
        //     }
        //     var sellerId = token.userId;

        app.dataSources.mysqld.transaction(async models => {
            const { Orders, ShippingBill, ShippingHistory } = models;

            const shippingBill = await ShippingBill.findOne({
                fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note'],
                where: {
                    "id": shippingBillId
                }
            });

            if (shippingBill == null) {
                throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
            }

            if (shippingBill.status == ShippingCodes.DANG_GUI_DI.status) {
                shippingBill.status = ShippingCodes.KET_THUC.status;
                shippingBill.modifyTime = parseInt(new Date().getTime() / 1000.0);
                await shippingBill.save();

                let _shippingHistory = ShippingBill.app.models.ShippingHistory;
                await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.DA_GIAO_THANH_CONG, null);
                await _shippingHistory.CreateNew(shippingBill.id, ShippingCodes.KET_THUC, null);

                var sql = "SELECT COUNT(1) as CountBill FROM shipping_bill WHERE order_id=? AND status != ?";
                var params = [shippingBill.orderId, ShippingCodes.KET_THUC.status];
                var connector = ShippingBill.dataSource.connector;
                connector.query(sql, params, async function (err, resultObjects) {

                    var countBill = 0;
                    if (resultObjects.length > 0) {
                        countBill = resultObjects[0].CountBill;
                    }

                    if (countBill == 0) {
                        var Orders = ShippingBill.app.models.Orders;
                        const orders = await Orders.findOne({
                            // fields: ['id', 'memberId', 'orderCode', 'buyTime', 'note', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode', 'totalPrice', 'paymentType', 'transactionNumber', 'status', 'createTime', 'createBy', 'modifyTime', 'modifyBy'],
                            where: {
                                "id": shippingBill.orderId
                            }
                        })

                        orders.status = OrdersCodes.DA_GIAO_THANH_CONG.status;
                        orders.modifyTime = parseInt(new Date().getTime() / 1000.0);
                        // orders.modifyBy = sellerId;
                        await orders.save();

                        // Create notification
                        let Notification = ShippingBill.app.models.Notification;
                        await Notification.createNotification(order.memberId, shippingBill.orderId, shippingBill.id, null, OrdersCodes.DA_GIAO_THANH_CONG.status);

                        let result = new Results();
                        result.setSuccess(true);
                        return cb(null, result);
                    }
                    else {
                        let result = new Results();
                        result.setSuccess(true);
                        return cb(null, result);
                    }
                })
            }
            else {
                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);
            }
        }).catch(function (err) {
            let result = new Results();
            result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
            return cb(null, result);
        });
        // }
        // makeReceivedBySeller();
    }

    /**
     * GetShippingBull by orderId to fetch info
     * Created by Thiep Wong
     * Date 17/05/2018
     * @param {*} sellerId 
     * @param {*} orderId 
     */

    ShippingBill.GetShippingBillDetails = function (id, ctx, cb) {

        let result = new Results();
        //  let ShipHistory = ShippingBill.app.models.ShippingHistory;
        let makeRequest = async () => {
            let Passport = ShippingBill.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            let shippingBill = await ShippingBill.findOne({
                fields: ['id', 'shipCode', 'orderId', 'sellerStoreId', 'addresss', 'shipUnitId', 'status', 'shippingType', 'label_id'],
                include: [
                    {
                        relation: 'orders',
                        scope: {
                            fields: ['id', 'buyerName', 'buyerPhone', 'buyerAddress', 'buyerLocationCode'],
                            include: [
                                {
                                    relation: 'location',
                                    scope: {
                                        fields: ['id', 'path']
                                    }
                                }
                            ]

                        }
                    }, {
                        relation: 'sellerStore',
                        scope: {
                            fields: ['id', 'storeName', 'managerPhone', 'address', 'locationCode', 'locationLevel'],
                            include: [
                                {
                                    relation: 'location',
                                    scope: {
                                        fields: ['id', 'path']
                                    }
                                }
                            ]
                        }
                    }, {
                        relation: 'shipUnit',
                        scope: {
                            fields: ['id', 'name', 'sellerId', 'matchLocationLevelFee', 'notMatchLocationLevelFee', 'logoImage', 'timeDelivery']
                        }
                    }
                ],
                where: {
                    sellerId: token.userId,
                    id: id
                }
            });

            let shippingHistoryInstance = await ShippingBill.app.models.ShippingHistory.GetHistoryByShippingId(shippingBill.id);
            shippingBill["shippingHistory"] = shippingHistoryInstance;
            result.setSuccess(shippingBill);
            return cb(null, result);
        };
        makeRequest();
    }

    var UpdateShipping = async function (productSellerId, sellerStoreId, orderDetailsId, amount) {

        if (amount < 0) {
            throw new Error(JSON.stringify(ResultsCodes.PRODUCT_STORE_BOOKED_ITEM_SMALLER_THAN_ZERO));
        }

        let ProductStore = ShippingBill.app.models.ProductStore;
        const productStore = await ProductStore.findOne({
            fields: ['id', 'productSellerId', 'sellerStoreId', 'total', 'available', 'booked', 'createBy', 'createTime', 'modifyBy', 'modifyTime'],
            where: {
                "productSellerId": productSellerId,
                "sellerStoreId": sellerStoreId
            }
        });

        if (productStore == null) {
            throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
        }

        if (productStore.booked < amount) {
            throw new Error(JSON.stringify(ResultsCodes.PRODUCT_STORE_BOOKED_ITEM_NOT_ENOUGH_TO_SHIP));
        }

        productStore.booked -= amount;
        productStore.total -= amount;
        productStore.modifyTime = parseInt(new Date().getTime() / 1000.0);
        await productStore.save();

        let _storeHistory = ShippingBill.app.models.StoreHistory;
        await _storeHistory.CreateNew(productStore.id, orderDetailsId, 0, (-1 * amount), null);


    }

    ShippingBill.ChangeOrdersStatus = function (shippingBillId, status, ctx, cb) {

        const makeConfirmBySeller = async () => {

            let result = new Results();

            let Passport = ShippingBill.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            if (!shippingBillId || !status || status <= ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                result.setError(ResultsCodes.VALIDATION_ERROR);
                return cb(null, result);
            }
            var sellerId = token.userId;

            app.dataSources.mysqld.transaction(async models => {
                //const { ShippingBill } = models;
                const { ShippingBill, ProductStore, ShippingHistory, StoreHistory, OrdersDetails } = models;

                const shippingBill = await ShippingBill.findOne({
                    fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note'],
                    include: [{
                        relation: 'ordersDetails',
                        scope: {
                            fields: ['id', 'shippingBillId', 'ordersId', 'productSellerId', 'affiliateId', 'price', 'unit', 'amount']
                        }
                    }],
                    where: {
                        "id": shippingBillId
                    }
                });
                // console.log(shippingBill);
                const orDetails = shippingBill.toJSON().ordersDetails;

                if (shippingBill == null) {
                    result.setError(ResultsCodes.ITEM_NOT_FOUND);
                    return cb(null, result);
                }

                if (shippingBill.status >= status) {
                    result.setError(ResultsCodes.VALIDATION_ERROR);
                    return cb(null, result);
                }

                shippingBill.status = status;
                shippingBill.modifyTime = parseInt(new Date().getTime() / 1000.0);
                shippingBill.modifyBy = sellerId;
                await shippingBill.save();
                // console.log(shippingBill);

                let shippingCode = {};
                if (status == ShippingCodes.DANG_GUI_DI.status) {
                    shippingCode = ShippingCodes.DANG_GUI_DI;
                }
                if (status == ShippingCodes.DA_GIAO_THANH_CONG.status) {
                    shippingCode = ShippingCodes.DA_GIAO_THANH_CONG;
                }
                if (status == ShippingCodes.DA_HUY.status) {
                    shippingCode = ShippingCodes.DA_HUY;
                }
                if (status == ShippingCodes.TU_CHOI_NHAN_HANG.status) {
                    shippingCode = ShippingCodes.TU_CHOI_NHAN_HANG;
                }
                if (status == ShippingCodes.KET_THUC.status) {
                    shippingCode = ShippingCodes.KET_THUC;
                }

                let _shippingHistory = ShippingBill.app.models.ShippingHistory;
                await _shippingHistory.CreateNew(shippingBill.id, shippingCode, null);

                // member
                let memberId = 0;

                // count orderDetail is not shipped
                let sql = `select count(1) as countBillNotShipped from shipping_bill sb
                            where sb.order_id = ? and sb.status < ?`;
                let params = [shippingBill.orderId, status];
                let connector = ShippingBill.dataSource.connector;
                await connector.query(sql, params, async function (err, resultObjects) {
                    // console.log(resultObjects);
                    let countBillNotShipped = -1;
                    if (resultObjects.length > 0) {
                        countBillNotShipped = resultObjects[0].countBillNotShipped;
                    }
                    //console.log(countBillNotShipped);
                    if (countBillNotShipped == 0 || countBillNotShipped == 1) {
                        let _orders = ShippingBill.app.models.Orders;

                        try {
                            var order = await _orders.findOne({
                                where: { id: shippingBill.orderId }
                            });
                            console.log(order);
                        } catch (ex) {
                            console.error(ex);
                        }

                        if (order && order.status < status) {
                            // memberId = order.memberId;
                            // order.status = status;
                            // await order.save();


                            //Update lại số lượng khi khách hủy hoặc từ chối nhận hàng
                            // thienvd
                            // 27-11-2019
                            if (status == ShippingCodes.DA_HUY.status || status == ShippingCodes.TU_CHOI_NHAN_HANG.status) {
                                if (order.status == 5 && status == 6) {
                                    memberId = order.memberId;
                                    order.status = status;
                                    await order.save();
                                    return cb(null, result);
                                }

                                //let sStoreId = shippingBill.sellerStoreId;
                                for (var item in orDetails) {
                                    var productSellerId = orDetails[item].productSellerId;
                                    var sellerStoreId = shippingBill.sellerStoreId;
                                    var orderDetailsId = orDetails[item].id;
                                    var amount = orDetails[item].amount;

                                    let ProductStore = ShippingBill.app.models.ProductStore;
                                    const productStore = await ProductStore.findOne({
                                        fields: ['id', 'productSellerId', 'sellerStoreId', 'total', 'available', 'booked', 'createBy', 'createTime', 'modifyBy', 'modifyTime'],
                                        where: {
                                            "productSellerId": productSellerId,
                                            "sellerStoreId": sellerStoreId
                                        }
                                    });
                                    if (productStore == null) {
                                        result.setError(ResultsCodes.ITEM_NOT_FOUND);
                                        return cb(null, result);
                                    }
                                    else {
                                        let StoreHistory = ShippingBill.app.models.StoreHistory;
                                        productStore.booked -= amount;
                                        productStore.available += amount;
                                        productStore.modifyTime = parseInt(new Date().getTime() / 1000.0);
                                        await productStore.save();
                                        await StoreHistory.CreateNew(productStore.id, orderDetailsId, 0, amount, null);
                                    }

                                }

                            }

                            memberId = order.memberId;
                            order.status = status;
                            await order.save();
                        }


                    }

                    var Notification = ShippingBill.app.models.Notification;

                    Notification.destroyAll({ memberId: order.memberId }, { orderId: order.id });

                    //let notification = await Notification.find();
                    //console.log(notification);

                    await Notification.createNotification(order.memberId, shippingBill.orderId, shippingBill.id, order.orderCode, status);
                }
                );

                // Create notification
                // let Notification = ShippingBill.app.models.Notification;
                // await Notification.createNotification(order.memberId, shippingBill.orderId, shippingBill.id, order.orderCode, status);
                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);

            }).catch(function (err) {
                let result = new Results();
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
                return cb(null, result);
            });

        }
        makeConfirmBySeller()
    }

    /// thienvd
    /// 22/02/2020
    ///Buyer Huy đơn hàng

    ShippingBill.ChangeShipStatus = function (  shippingBillId, status, orId, ctx, cb) {
        const makeConfirmBuyer = async () => {
            let result = new Results();
            let Passport = ShippingBill.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            if (!shippingBillId || !status || status <= ShippingCodes.CUA_HANG_XAC_NHAN.status) {
                result.setError(ResultsCodes.VALIDATION_ERROR);
                return cb(null, result);
            }
            //var sellerId = token.userId;

            app.dataSources.mysqld.transaction(async models => {
                //const { ShippingBill } = models;
                const { ShippingBill, ProductStore, ShippingHistory, StoreHistory, OrdersDetails } = models;

                const shippingBill = await ShippingBill.findOne({
                    fields: ['id', 'orderId', 'sellerStoreId', 'sellerId', 'address', 'shippingType', 'shipCode', 'shipUnitId', 'billPrice', 'shipFee', 'shipTime', 'status', 'note'],
                    include: [{
                        relation: 'ordersDetails',
                        scope: {
                            fields: ['id', 'shippingBillId', 'ordersId', 'productSellerId', 'affiliateId', 'price', 'unit', 'amount']
                        }
                    }],
                    where: {
                        "id": shippingBillId
                    }
                });
                // console.log(shippingBill);
                const orDetails = shippingBill.toJSON().ordersDetails;

                if (shippingBill == null) {
                    result.setError(ResultsCodes.ITEM_NOT_FOUND);
                    return cb(null, result);
                }

                if (shippingBill.status >= status) {
                    result.setError(ResultsCodes.VALIDATION_ERROR);
                    return cb(null, result);
                }



                shippingBill.status = status;
                shippingBill.modifyTime = parseInt(new Date().getTime() / 1000.0);
                //shippingBill.modifyBy = sellerId;
                await shippingBill.save();
                // console.log(shippingBill);

                let shippingCode = {};
                if (status == ShippingCodes.DANG_GUI_DI.status) {
                    shippingCode = ShippingCodes.DANG_GUI_DI;
                }
                if (status == ShippingCodes.DA_GIAO_THANH_CONG.status) {
                    shippingCode = ShippingCodes.DA_GIAO_THANH_CONG;
                }
                if (status == ShippingCodes.DA_HUY.status) {
                    shippingCode = ShippingCodes.DA_HUY;
                }
                if (status == ShippingCodes.TU_CHOI_NHAN_HANG.status) {
                    shippingCode = ShippingCodes.TU_CHOI_NHAN_HANG;
                }
                if (status == ShippingCodes.KET_THUC.status) {
                    shippingCode = ShippingCodes.KET_THUC;
                }

                let _shippingHistory = ShippingBill.app.models.ShippingHistory;
                await _shippingHistory.CreateNew(shippingBill.id, shippingCode, null);

                // member
                let memberId = 0;


               //          count orderDetail is not shipped
                let sql = `select count(1) as countBillNotShipped from shipping_bill sb
                            where sb.order_id = ? and sb.status != 2      and  sb.status != 5`;
                            // count orderDetail is not shipped

                // let sql = `select count(1) as countBillNotShipped, status from shipping_bill sb
                // where sb.order_id = ? and sb.status < ?`;

                let params = [shippingBill.orderId];
                let connector = ShippingBill.dataSource.connector;
                await connector.query(sql, params, async function (err, resultObjects) {
                    // console.log(resultObjects);
                     let countBillNotShipped = -1;
                    if (resultObjects.length > 0) {
                       countBillNotShipped = resultObjects[0].countBillNotShipped;
                     }
                    //console.log(countBillNotShipped);
                    if (countBillNotShipped == 0 || countBillNotShipped == 1)
                    {        
                                                   
                        const shipCheck = await ShippingBill.findOne({
                        fields: ['id', 'status'],                   
                            where: {
                                "orderId": shippingBill.orderId,
                                "status":2
                            }
                        })

                        if(shipCheck !=null && shipCheck != undefined)
                        {
                            return;
                        }
                        else
                        {
                        let _orders = ShippingBill.app.models.Orders;
                        try {
                            var order = await _orders.findOne({
                                where: { id: shippingBill.orderId }
                            });
                            console.log(order);
                        } catch (ex) {
                            console.error(ex);
                        }
                        if (order && order.status < status) {                           
                            order.status = status;
                            await order.save();
                        }

                            // ordersCheck.status = status;
                            // await ordersCheck.save();


                        }
                          
                        


                        // let _orders = ShippingBill.app.models.Orders;

                        // try {
                        //     var order = await _orders.findOne({
                        //         where: { id: shippingBill.orderId }
                        //     });
                        //     console.log(order);
                        // } catch (ex) {
                        //     console.error(ex);
                        // }
                        // if (order && order.status < status) {                           
                        //     order.status = status;
                        //     await order.save();
                        // }
                        var Notification = ShippingBill.app.models.Notification;

                        Notification.destroyAll({ memberId: order.memberId }, { orderId: order.id });               
    
                        await Notification.createNotification(order.memberId, shippingBill.orderId, shippingBill.id, order.orderCode, status);

                        
                    }

                    // var Notification = ShippingBill.app.models.Notification;

                    // Notification.destroyAll({ memberId: order.memberId }, { orderId: order.id });               

                    // await Notification.createNotification(order.memberId, shippingBill.orderId, shippingBill.id, order.orderCode, status);
                }
                );
                
               let result = new Results();
                result.setSuccess(true);
                return cb(null, result);

            }).catch(function (err) {
                let result = new Results();
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED)
                return cb(null, result);
            });           
        }
        makeConfirmBuyer()
    }


    var shipmentOrder = async function (products, order, shippingBillId) {
        let req = {
            products: products,
            order: order
        };
        console.log(req);
        var options = {
            method: 'POST',
            url: Config.baseUrl + 'services/shipment/order/?ver=1.5',
            headers:
            {
                Connection: 'keep-alive',
                Host: Config.domain,
                Accept: '*/*',
                'Content-Type': 'application/json',
                Token: Config.api_token
            },
            json: req
        };
        // console.log(options);

        var res = http(options, async (err, res, body) => {
            if (err) {
                return 0;
            }
            if (body) {
                if (body.success) {
                    let shippingBill = await ShippingBill.findOne({
                        where: {
                            id: shippingBillId
                        }
                    });
                    shippingBill.label_id = body.order.label;
                    shippingBill.shipFee = body.order.fee;
                    shippingBill.statusDelivery = 1;
                    // console.log(body.order);
                    await shippingBill.save();
                    return shippingBill;
                }
            }
        });
    }

    var getLocationByCode = async function (locationCode) {
        var Location = ShippingBill.app.models.Location;
        let location = await Location.findOne({
            include: {
                relation: 'parent',
                scope: {
                    include: {
                        relation: 'parent'
                    }
                }
            },
            where: {
                code: locationCode
            }
        });
        // console.log(location);
        return location;
    }

    ShippingBill.ReportShippment = function (uuid, from, to, status, shipCode, orderCode, pageIndex, pageSize, cb) {
        let result = new Results();
        if (!uuid) {
            result.setError(ResultsCodes.VALIDATION_ERROR);
            return cb(null, result);
        }
        let makeRequest = async () => {
            let index = pageIndex || 1;
            let size = pageSize || 10;
            let offset = (index - 1) * size;

            let sql = `select sb.id, sb.ship_code shipCode, sb.order_id orderId, o.order_code orderCode, group_concat(p.product_name) as productName, o.buyer_address buyerAddress, ss.address shopAddress, o.buy_time buyTime,
            sb.modify_time modifyTime, o.buyer_name buyerName, o.buyer_phone buyerPhone, sb.status, sb.status_delivery statusDelivery, sb.ship_fee shipFee, sb.bill_price billPrice
            from shipping_bill sb
            inner join orders o on sb.order_id = o.id
            inner join orders_details od on od.orders_id = o.id
            inner join product_seller ps on od.product_seller_id = ps.id
            inner join sellers s on s.id = ps.seller_id
            inner join products p on p.id = ps.product_id
            inner join seller_store ss on sb.seller_store_id = ss.id
            where s.uuid = '${uuid}' `;
            let sqlPrice = `select SUM(sb.bill_price) totalPrice, SUM(sb.ship_fee) totalFee
            from shipping_bill sb
            inner join orders o on sb.order_id = o.id
            inner join orders_details od on od.orders_id = o.id
            inner join product_seller ps on od.product_seller_id = ps.id
            inner join sellers s on s.id = ps.seller_id
            inner join products p on p.id = ps.product_id
            inner join seller_store ss on sb.seller_store_id = ss.id
            where s.uuid = '${uuid}' `;
            let sqlCount = `select count(1) as totalRecords from (select sb.id, sb.ship_code shipCode, sb.order_id orderId, o.order_code orderCode, group_concat(p.product_name) as productName, o.buyer_address buyerAddress, ss.address shopAddress, o.buy_time buyTime,
            sb.modify_time modifyTime, o.buyer_name buyerName, o.buyer_phone buyerPhone, sb.status, sb.status_delivery statusDelivery
            from shipping_bill sb
            inner join orders o on sb.order_id = o.id
            inner join orders_details od on od.orders_id = o.id
            inner join product_seller ps on od.product_seller_id = ps.id
            inner join sellers s on s.id = ps.seller_id
            inner join products p on p.id = ps.product_id
            inner join seller_store ss on sb.seller_store_id = ss.id
            where s.uuid = '${uuid}' `;

            if (from) {
                sql += `and o.buy_time >= '${from}' `;
                sqlPrice += `and o.buy_time >= '${from}' `;
                sqlCount += `and o.buy_time >= '${from}' `;
            }
            if (to) {
                sql += `and o.buy_time <= '${to}' `;
                sqlPrice += `and o.buy_time <= '${to}' `;
                sqlCount += `and o.buy_time <= '${to}' `;
            }
            if (status !== undefined && status !== null && status !== 99) {
                sql += `and sb.status = ${status} `;
                sqlPrice += `and sb.status = ${status} `;
                sqlCount += `and sb.status = ${status} `;
            }
            if (shipCode) {
                sql += `and sb.ship_code like '%${shipCode}%' `;
                sqlPrice += `and sb.ship_code like '%${shipCode}%' `;
                sqlCount += `and sb.ship_code like '%${shipCode}%' `;
            }
            if (orderCode) {
                sql += `and o.order_code like '%${orderCode}%' `;
                sqlPrice += `and o.order_code like '%${orderCode}%' `;
                sqlCount += `and o.order_code like '%${orderCode}%' `;
            }
            sql += `group by sb.id, sb.ship_code, sb.order_id, o.order_code, o.buyer_address, ss.address, o.buy_time,
            sb.modify_time, o.buyer_name, o.buyer_phone, sb.status, sb.status_delivery, sb.ship_fee, sb.bill_price 
            order by o.buy_time DESC  limit ${size} offset ${offset} `;
            sqlCount += `group by sb.id, sb.ship_code, sb.order_id, o.order_code, o.buyer_address, ss.address, o.buy_time,
            sb.modify_time, o.buyer_name, o.buyer_phone, sb.status, sb.status_delivery ) as count`;

            let params = [];
            var ds = ShippingBill.dataSource;

            let count = await ShippingBill.Count(sqlCount, params);
            console.log(count[0]);

            await ds.connector.query(sqlPrice, params, function (error, data) {
                console.log(data[0]);
                let totalPrice = data[0].totalPrice;
                let totalFee = data[0].totalFee;
                ds.connector.query(sql, params, function (err, instance) {
                    if (err) {
                        // console.log(err);
                        result.setError(ResultsCodes.DATA_NOT_FOUND);
                        return cb(null, result);
                    }
                    result.setSuccess({ totalRecords: count[0].totalRecords, pageSize: size, pageIndex: index, pageData: instance, totalPrice: totalPrice, totalFee: totalFee, message: 'Thanh cong' });
                    cb(null, result);
                });
            });
            
        }

        makeRequest().catch(_err => {
            console.log(_err);
            result.setError(ResultsCodes.SYSTEM_ERROR)
            return cb(null, result);
        });
    }

    ShippingBill.Count = function (sql, params) {

        let promise = new Promise((resolve, reject) => {
            var ds = ShippingBill.dataSource;
            ds.connector.query(sql, params, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
        return promise;
    }
    //#endregion

    //#region Expose Parameters
    ShippingBill.remoteMethod('GetListBySeller', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    ShippingBill.remoteMethod('ConfirmBySeller', {
        http: {
            verb: "GET",
            path: '/update/:shippingBillId'

        },
        accepts: [
            {
                arg: "shippingBillId",
                type: "number"
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    
    

    ShippingBill.remoteMethod('DeliveryBySeller', {
        http: {
            verb: "POST"
        },
        accepts: [{
            arg: "shippingBillId",
            type: "number"
        }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }
        ]
    })

    ShippingBill.remoteMethod('ReceivedByMember', {
        description: 'Updated by Thiep Wong. Nguoi lam ban dau lai ai??!',
        http: {
            verb: "POST"
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    ShippingBill.remoteMethod('ReceivedBySeller', {
        http: {
            verb: "POST"
        },
        accepts: [{
            arg: "shippingBillId",
            type: "number"
        }],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    ShippingBill.remoteMethod('GetShippingBillDetails', {
        description: '@Thiep Wong, get the details of a shipping bill from sellers',
        http: {
            verb: 'GET',
            path: '/details/:id'
        },
        accepts: [
            {
                arg: 'id',
                type: 'number'
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    });

    ShippingBill.remoteMethod('ChangeOrdersStatus', {
        http: {
            verb: "POST",
            path: '/change-orders-status'

        },
        accepts: [
            {
                arg: "shippingBillId",
                type: "number"
            },
            {
                arg: "status",
                type: "number"
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })


    ShippingBill.remoteMethod('ChangeShipStatus', {
        http: {
            verb: "POST",
            path: '/change-ship-status'

        },
        accepts: [
            {
                arg: "shippingBillId",
                type: "number"
            },
            {
                arg: "status",
                type: "number"
            },
            {
                arg: "orderId",
                type: "number"
            },
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    ShippingBill.remoteMethod('ReportShippment', {
        description: '@NgoHung, report shippment',
        http: {
            verb: 'GET',
            path: '/report-shippment'
        },
        accepts: [
            {
                arg: 'uuid',
                type: 'string'
            },
            {
                arg: 'from',
                type: 'number'
            },
            {
                arg: 'to',
                type: 'number'
            },
            {
                arg: 'status',
                type: 'number'
            },
            {
                arg: 'shipCode',
                type: 'string'
            },
            {
                arg: 'orderCode',
                type: 'string'
            },
            {
                arg: 'pageIndex',
                type: 'number'
            },
            {
                arg: 'pageSize',
                type: 'number'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    // #endregion
};