/**
 * Model ProductPrice
 * Author: LuongPD
 * Last update: 15.3.2018 13:49
 */
var Results = require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
var ApplicationError = require('../middlewares/application-error');

'use strict';

module.exports = function (ProductPrice) {

    //#region Functions
    ProductPrice.CreateNew = function (productSellerId, price, from, to, sellerId, cb) {
        app.dataSources.mysqld.transaction(async models => {
          
            await this.Add(models, productSellerId, price, from, to, sellerId);

            let result = new Results();
            result.setSuccess(true);
            return cb ? cb(null, result) : result;

        }).catch(function (e) {
            let result = new Results();

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }

    ProductPrice.Add = async function(models, productSellerId, price, from, to, sellerId) {
        
        const { ProductPrice } = models;

        const productPrices = await ProductPrice.find({
            fields: ['id', 'price', 'from', 'to'],
            where: {
                "productSellerId": productSellerId
            }
        });

        let isOk = true;

        for (var item in productPrices) {
            var itemPrice = productPrices[item];

            if (from < itemPrice.from && itemPrice.from < to) { // 1 < a < 3 
                isOk = false;
            }
            else if (from < itemPrice.to && itemPrice.to < to) { // 5 < b < 7
                isOk = false;
            }
            else if (itemPrice.from < from && to < itemPrice.to) { // a < 5,6 < b
                isOk = false;
            }
            else if (itemPrice.from == from && to == itemPrice.to) { // a < 5,6 < b
                isOk = false;
            }
        }

        if (!isOk) {
            throw new ApplicationError(ResultsCodes.PRODUCT_PRICE_OVERLAP);
        }

        var productPrice = new ProductPrice;
        productPrice.productSellerId = productSellerId;
        productPrice.price = price;
        productPrice.from = from;
        productPrice.to = to;
        productPrice.createTime = parseInt(new Date().getTime() / 1000.0);
        productPrice.createBy = sellerId;
        await ProductPrice.create(productPrice);
    }

    ProductPrice.GetListByProductSeller = function (productSellerId, cb) {
        ProductPrice.find({
            fields: ['id', 'productSellerId', 'price', 'from', 'to'],
            where: {
                "productSellerId": productSellerId
            }
        }, function (err, instance) {

            if(err){
                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
            }

            let result = new Results();
            result.setSuccess(instance)
      
            cb(null, result);
        })
    }


    /**
     * Update record of product price
     * Created by Thiep Wong
     * Date 11/05/2018
     * @param {*} id 
     * @param {*} from 
     * @param {*} to 
     * @param {*} price 
     * @param {*} cb 
     */
    ProductPrice.UpdatePrice = function(id,from,to,price,cb){
    let result = new Results();
        if(!id || !from || !to || !price){
            result.setError(ResultsCodes.VALIDATION_ERROR);
            return cb(null,result);
        }

        ProductPrice.findById(id,function(err,instance){
            if(err){
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                return result;
            }
 
            instance.updateAttributes({
                from:from,to:to,price:price,modifyTime:  new Date().getTime()/1000
            }, function(err,instance){
                if(err){
                    result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                    return result;
                }

                result.setSuccess(true);
                cb(null,result);
            })

            result.setError

        });

    }
    //#endregion

    //#region Expose Parameters
    ProductPrice.remoteMethod('CreateNew', {
        http: {
            verb: 'POST'
        },
        accepts: [{
            arg: "productSellerId",
            type: "number"
        }, {
            arg: "price",
            type: "number"
        }, {
            arg: "from",
            type: "number"
        }, {
            arg: "to",
            type: "number"
        }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    ProductPrice.remoteMethod('GetListByProductSeller', {
        http: {
            verb: "GET",
            path:'/product/:productSellerId'
        },
        accepts: [{
            arg: "productSellerId",
            type: "number"
        }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    ProductPrice.remoteMethod('UpdatePrice',{
        description:'@Thiep Wong, update information of price record',
        http:{
            verb:'POST',
            path:'/update/:id'
        },
        accepts:[
            {
                arg:'id',
                type:'number'
            },
            {
                arg:'from',
                type:'number'
            },
            {
                arg:'to',
                type:'number'
            },
            {
                arg:'price',
                type:'number'
            }
        ],
        returns:{
            arg:'result',
            type:'string',
            root:true
        }
    })
    //#endregion


    //#region CreatePrice
    // Author: Thien VD
    // 17.01.2019
    // tạo giá
    ProductPrice.CreatePrice = function (productSellerId, price, from, to, sellerId, cb) {
        app.dataSources.mysqld.transaction(async models => {          
            await this.Add(models, productSellerId, price, from, to, sellerId);

            let result = new Results();
            result.setSuccess(true);
            return cb ? cb(null, result) : result;

        }).catch(function (e) {
            let result = new Results();

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }

    ProductPrice.remoteMethod('CreatePrice', {
        http: {
            verb: 'POST',
            path:'/createprice'

        },
        accepts: [{
            arg: "productSellerId",
            type: "number"
        }, {
            arg: "price",
            type: "number"
        }, {
            arg: "from",
            type: "number"
        }, {
            arg: "to",
            type: "number"
        },
        {
            arg: "sellerId",
            type: "number"
        }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    //#endregion
};