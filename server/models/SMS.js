module.exports = function (SMS) {

    SMS.checkToken = () => {

        const promise = new Promise((resolve, reject) => {

            let OTP = SMS.app.models.OTP;
            let _timelife = new Date().getTime() / 1000;
            SMS.findOne({
                fields: ['uuid', 'token', 'tokenType', 'timelife'],
                order: 'timelife ASC',
                skip: 1,
                where: {
                    timelife: {
                        gte: _timelife
                    }
                }
            }, (err, tokenPackage) => {

                if (err) {
                    return reject(err);
                }

                if (!tokenPackage) {
                    OTP.IRISLogin(function (err, res) {
                        if (err) {
                            return reject(err);
                        }
                        // console.log(res);
                        return resolve(res);
                    })
                }


                if (tokenPackage) {
                    // console.log(tokenPackage);
                    return resolve(tokenPackage);
                }
            });


        });

        return promise;

    }

    SMS.getOne = async function (cb) {
        let _timelife = new Date().getTime() / 1000;
        SMS.findOne({
            fields: ['uuid', 'token', 'tokenType', 'timelife'],
            order: 'timelife ASC',
            skip: 1,
            where: {
                timelife: {
                    gt: _timelife
                }

            }
        }, function (err, res) {
            if (err) {
                return cb(null, { err: 'Error!' });
            }
            cb(null, res);
        });
    }

    SMS.LoggedCheck = (cb)=>{

        SMS.checkToken().then(data=>{
            cb(null,data);

        }).then(rej=>{
            cb(null,rej);
        })

    }

    SMS.remoteMethod('checkToken', {
        http: {
            verb: 'GET'
        },
        accepts: [],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    });

    SMS.remoteMethod('LoggedCheck',{
        http:{
            verb:'GET',
            path:'/da-log-chua'
        },
        accepts:[],
        returns:{
            type:'string',
            root:true
        }
    })
};