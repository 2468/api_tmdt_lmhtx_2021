/**
 * Refresh Token model
 * Create an frefresh token after access token expired
 * Author: Thiep Wong
 * Created Date: 26/10/2018
 */
const crypto = require('crypto');
module.exports = (RefreshToken)=>{

    /**
     * Sign a token with user infomation
     * iss: user uuid
     * aid as account id
     * type as type of account, mem for member and sel for seller
     */
    RefreshToken.sign = (iss,aid,type,cb)=>{ 
            if(!iss || !aid || !type) {
              return  cb({
                    err: 'Khong day du du lieu!'
                });
            }
            let hash = crypto.createHash('RSA-SHA512'); 
            hash.update(Math.random().toString(), "ascii");
            hash.update("smartlife"); // Secret keyword
            const _tkId = hash.digest('hex'); 
        RefreshToken.create({
                id:_tkId,
                iss:iss,
                aid:aid,
                typ:type,
                iat: Math.floor(new Date().getTime()/1000),
                exp:  Math.floor(new Date().getTime()/1000) + 6*30*24*60*60 
        
        },(err,res)=>{
            if(err){
                return cb(err)
            }
            cb(null,res)
        })  
    } 

    RefreshToken.delete = (id)=> {
        if(!id){
            return cb({err:'No id for delete'});
        }

        RefreshToken.destroyById(id,(err,res)=>{
            if(err){
                return cb({err:'Cannot delete token'});
            }

            cb(null,res);
        })

    }

    RefreshToken.getToken = (id,cb)=>{
        if(!id){
            return cb({err:'No id for delete'});
        }

     return   RefreshToken.findById(id);
    }
}