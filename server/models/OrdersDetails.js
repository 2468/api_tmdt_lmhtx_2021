module.exports = function(OrdersDetails) {

             
    OrdersDetails.sharedClass.methods().forEach(function (method) {
        OrdersDetails.disableRemoteMethodByName(method.name, method.isStatic);
      });


    OrdersDetails.GetDetailById = function(id){
        var promise = new Promise((resolve,reject) =>{

            OrdersDetails.find({
                fields:['id','ordersId','productSellerId','price'],
                include:{
                    relation:'productSeller',
                    scope:{
                        fields:['id','productId','sellerId'],
                        include:{
                            relation:'product',
                            scope: {
                                fields:['id','productName','sellerId']
                            }
                        },

                        where:{
                            sellerId: 1000009
                        }
                    },
             
                },
                where:{
                    ordersId:id
                }
            },function(err, instance){
                if(err){
                    return reject('System Error1');
                }

                resolve(instance);
            })

        });

        return promise
    }

    OrdersDetails.GetDetailById = function(id,sellerId){

        if(!id || !sellerId) return null;

        let promise = new Promise((resole,reject)=>{

            let connector = OrdersDetails.dataSource.connector;
            let params = [id,sellerId];
            let sql =`
                SELECT  od.id, od.orders_id orderId, p.id productId, p.product_name productName,
                p.product_image productImage, ps.id productSellerId, s.id sellerId,
                od.shipping_bill_id shipBillId,od.affiliate_id affId,od.price,od.unit,od.amount quantity, (od.price*od.amount) total  
                FROM  orders_details od 
                left join product_seller ps  on ps.id = od.product_seller_id 
                left join products p on p.id = ps.product_id
                left join sellers s on s.id = ps.seller_id 
                where od.orders_id = ? and ps.seller_id = ?
            `;

            connector.query(sql,params,function(err,instance){
                if(err){
                    reject('System Error!');
                }
                else{
                    resole(instance);
                }
            });
        });

        return promise;
    }
};