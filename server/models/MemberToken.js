module.exports = function(MemberToken){
    
    MemberToken.sharedClass.methods().forEach(function (method) {
        MemberToken.disableRemoteMethodByName(method.name, method.isStatic);
      });
}