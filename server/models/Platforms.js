/**
 * Model Location
 * Author: LuongPD
 * Last update: 22.5.2018 10:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');

module.exports = function (Platforms) {

    Platforms.sharedClass.methods().forEach(function (method) {
        Platforms.disableRemoteMethodByName(method.name, method.isStatic);
      });


    //#region Functions
    Platforms.GetList = function (cb) {
        Platforms.find({
            fields: ['id', 'platformCode', 'platformName', 'platformDomain', 'logoImage']
        }, function (err, instance) {
            let result = new Results();
            result.setSuccess(instance);
            return cb(null, result);
        })
    }


    Platforms.GetPlatformCode = function (domain, cb) {

        let result = new Results();
        if (!domain) {
            result.setError(ResultsCodes.ITEM_NOT_FOUND);
            return cb(null, result);
        }

        Platforms.findOne({
            where: {
                platformDomain: domain
            }
        }, (err, res) => {
            if (err) {
                result.setError(ResultsCodes.ITEM_NOT_FOUND);
                return cb(null, result);
            }

            result.setSuccess(res);
            cb(null,result);

        })
    }

    //#endregion

    //#region Expose Parameters
    Platforms.remoteMethod('GetList', {
        http: {
            verb: "GET",
            path:'/getlist'
        },
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })


    Platforms.remoteMethod('GetPlatformCode', {
        description: '@Thiep Wong, get platform code by domain',
        http: {
            verb: 'GET',
            path: '/platform'
        },
        accepts: [
            {
                arg: 'domain',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    //#endregion
};