'use strict';
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');

module.exports = function(BannerHome) {

    BannerHome.sharedClass.methods().forEach(function (method) {
        BannerHome.disableRemoteMethodByName(method.name, method.isStatic);
    });

    BannerHome.GetList = function(type, cb) {
        BannerHome.find({
            fields: ['id', 'path', 'priority', 'status', 'type'],
            where: {
                "status": 1,
                "type": type
            },
            order: 'priority asc'
         }, function (err, results) {

            let result = new Results();
            if (err) {
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            } else {
                result.setSuccess(results);
                return  cb(null,result);
            }
        })
    }

    BannerHome.remoteMethod('GetList', {
        http: {
            verb: "GET"
        }, accepts: [{
            arg: "type",
            type: "number",
            required: true
        }],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })
};