/**
 * Model ProductFavorites
 * Author: LuongPD
 * Last update: 18.5.2018 10:30
 */
var Results =  require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
'use strict';

module.exports = function(ProductFavorites) {

    //#region Functions
    ProductFavorites.CreateNew = function (ctx, cb) {
        let productId = ctx.body.productId || 0;

        let result = new Results();
        if(!productId){
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null,result);
        }

        const makeCreateNew = async () => {
            let Passport = ProductFavorites.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var memberId = token.userId;

            app.dataSources.mysqld.transaction(async models => {
                const { ProductFavorites } = models;

                var productFavorites = await ProductFavorites.findOne({
                    // fields: ['id', 'basePrice'],
                    where: {
                        "productId": productId,
                        "memberId": memberId
                    }
                });

                if (productFavorites == null)
                {
                    var productFavorites = new ProductFavorites();
                    productFavorites.productId = productId;
                    productFavorites.memberId = memberId;
                    productFavorites.createTime = parseInt(new Date().getTime() / 1000.0);
                    productFavorites.createBy = memberId;
                    await ProductFavorites.create(productFavorites);
                }
                                
                result.setSuccess(true);
                return cb(null,result);
                
            }).catch(function (e) { 
                result.setError(ResultsCodes.ADD_ITEM_FAILED);
                return cb(null, result);
            });
        }
        makeCreateNew().catch (function(e) { 
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return  cb(null, result);
        });
    }

    ProductFavorites.GetList = function (ctx,cb) { 
        let result = new Results();

        const makeGetList = async () => {
            let Passport = ProductFavorites.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var memberId = token.userId;

            let sql = `SELECT
                    pd.id,
                    pd.product_name productName,
                    pd.product_image productImage,
                    IFNULL(MIN(pp.price), MIN(ps.base_price)) minPrice, 
                    IFNULL(MAX(pp.price), MAX(ps.base_price)) maxPrice
                FROM
                    products pd
                JOIN
                    product_favorites pf ON pf.product_id = pd.id
                JOIN
                    product_seller ps ON ps.product_id = pd.id
                LEFT JOIN
                    product_price pp ON pp.product_seller_id = ps.id
                    AND pp.from <= ?
                    AND pp.to >= ?
                WHERE
                    pf.member_id = ?
                GROUP BY pd.id`;
                
            var currentTime = parseInt(new Date().getTime() / 1000.0);
            let params = [currentTime, currentTime, memberId];
            let connector = ProductFavorites.dataSource.connector
            connector.query(sql, params, function (err, resultObjects) {

                if (err) { 
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }
                else { 
                    result.setSuccess(resultObjects);
                    return cb(null, result);
                }
            });
        }
        makeGetList().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }

    ProductFavorites.Delete = function (ctx, cb) {
        
        let result = new Results();
        let productId = ctx.body.productId || 0;
        if(!productId) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null,result);
        }
        const makeDelete = async () => {
            let Passport = ProductFavorites.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
          //  var memberId = token.userId;

            ProductFavorites.destroyAll({productId:productId,memberId:token.userId},(err)=>{
                if (err) {
                    result.setError("Không hủy được yêu thích sản phẩm")
                    return cb(null,result)
                }

                result.setSuccess(true) 
                return cb(null,result)
            })

            // app.dataSources.mysqld.transaction(  models => {
            //     const { ProductFavorites } = models;

            //     var sql = "DELETE FROM product_favorites WHERE product_id = ? AND member_id = ?";
            //     var params = [productId, memberId];
            //     var connector = ProductFavorites.dataSource.connector;
            //     connector.query(sql, params, function (err, result) {
            //         //let result = new Results(); 
            //         if (err) { 
            //             result.setError('Error!');
            //             return cb(null, result);
            //         }
            //         else {
            //             result.setSuccess(true);
            //             return cb(null, result);
            //         }
            //     }); 
                
            // }).catch(function (e) { 
            //     result.setError(ResultsCodes.ADD_ITEM_FAILED);
            //     return cb(null, result);
            // });
        }
        
        makeDelete().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }

    ProductFavorites.DeleteAll = function (ctx,cb) {
             let result = new Results();
        const makeDeleteAll = async () => {
            let Passport = ProductFavorites.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            // var memberId = token.userId;

            // app.dataSources.mysqld.transaction(async models => {
            //     const { ProductFavorites } = models;

            //     var sql = "DELETE FROM product_favorites WHERE member_id = ?";
            //     var params = [memberId];
            //     var connector = ProductFavorites.dataSource.connector;
            //     await connector.query(sql, params, function (err, result) {
            //         if (err == null) { 
            //             result.setSuccess(true);
            //             return cb(null, result);
            //         }
            //         else {
            //             throw err;
            //         }
            //     });
                
            // }).catch(function (e) { 
            //     result.setError(ResultsCodes.ADD_ITEM_FAILED);
            //     return cb(null, result);
            // });

          //thienvd - 15/08/2019
          //Cập nhật hàm bỏ thích
          ProductFavorites.destroyAll({memberId:token.userId},(err)=>{
            if (err) {
                result.setError("Không xóa được yêu thích sản phẩm")
                return cb(null,result)
            }

            result.setSuccess(true) 
            return cb(null,result)
        })

        }
        makeDeleteAll().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }

    ProductFavorites.CountItem = function(ctx, cb) {

                let result = new Results();
        const makeCountItem = async () => {
            let Passport = ProductFavorites.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            var memberId = token.userId;
            var ds = ProductFavorites.dataSource;
            var sql = "SELECT COUNT(*) As CountItem FROM product_favorites WHERE member_id = ?";
            var params = [memberId];
            ds.connector.query(sql, params, function (err, data) {
 
                if (err) {
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                } else { 
                    result.setSuccess(data.length > 0 ? data[0].CountItem : 0);
                    return cb(null, result);
                } 
            });
        }
        makeCountItem().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }
    //#endregion

    //#region Expose Parameters
    ProductFavorites.remoteMethod('CreateNew', {
        http:{
            verb:'POST'
        },
        accepts: [
        {
            arg: "ctx",
            type: "object",
            http:{
                source:'req'
            }
        }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
        
    })

    ProductFavorites.remoteMethod('GetList', {
        http: {
            verb: 'GET'
        },
        accepts:[
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
        
    })

    ProductFavorites.remoteMethod('Delete', {
        http:{
            verb:'POST'
        },
        accepts: [
            {
            arg: "ctx",
            type: "object",
            http:{
                source:'req'
            }
        }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }
        ]
    })

    ProductFavorites.remoteMethod('DeleteAll', {
        http:{
            verb:'POST'
        },
        accepts: [
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    ProductFavorites.remoteMethod('CountItem', {
        http: {
            verb: "GET"
        },
        accepts:[
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    // #endregion
};