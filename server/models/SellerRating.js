/**
 * Model SellerRating
 * Author: LuongPD
 * Last update: 26.4.2018 11:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var async = require('async');

module.exports = function(SellerRating) {

    SellerRating.ratingPercent = function(sellerId, cb) {
        var ds = SellerRating.dataSource;
        var sql = `SELECT IFNULL((SELECT 
                (COUNT(*) / (SELECT 
                    COUNT(*)
                FROM
                    seller_rating
                WHERE
                    seller_id = ?)) * 100
            FROM
                seller_rating
            WHERE
                seller_id = ?
                AND rating = 3
            GROUP BY rating), 0) AS 'ratingPercent'`;

        var params = [sellerId, sellerId];
        ds.connector.query(sql, params, function (err, data) {
            if (err) {
                cb(err);
            }
            else {
                cb(null, data);
            }
        });
    };

    SellerRating.countRatingByStar = function(sellerId, cb) {
        var ds = SellerRating.dataSource;
        var sql = "SELECT rating, count(rating) count FROM seller_rating  where seller_id = ? GROUP BY rating";
        var params = [sellerId];
        ds.connector.query(sql, params, function (err, data) {
            if (err) {
                cb(err);
            }
            else {
                cb(null, data);
            }
        });
    };

    SellerRating.CreateNew = function (ctx, cb) { 
        let sellerId = ctx.body.sellerId || 0;
        let ordersId = ctx.body.ordersId || 0 ;
        let rating = ctx.body.rating || 0; 
        let comment       = ctx.body.comment || null;
        let result = new Results();
        if(!sellerId || !ordersId || !rating) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null, result);
        }
 
        const makeCreateNew = async () => {
            let Passport = SellerRating.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) { 
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            
            var memberId = token.userId;
            let Orders = SellerRating.app.models.Orders;
            Orders.findOne({
                fields: ['id'],
                where: {
                    "id": ordersId,
                    "memberId": memberId
                }
            }, function (err, ordersInstance) {

                if (err) {
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }  


                if (ordersInstance == null) { 
                    var error = ResultsCodes.DATA_NOT_FOUND;
                    error.message = error.message.format(sellerId, ordersId, memberId);
                    result.setError(error);
                    return cb(null, result);
                } else {

                    var currentTime = parseInt(new Date().getTime() / 1000.0);
                    SellerRating.findOne({
                        //fields: ["id"],
                        where: { 
                            "sellerId": sellerId,
                            "memberId": memberId
                        }
                    }, function (err, sellerRatingInstace) {
                        if (err) {
                            result.setError(ResultsCodes.SYSTEM_ERROR);
                            return cb(null, result);
                        }  else if (sellerRatingInstace == null) {

                            var sellerRating = SellerRating();
                            sellerRating.memberId = memberId;
                            sellerRating.sellerId = sellerId;
                            sellerRating.orderId = ordersId;
                            sellerRating.rating = rating;
                            sellerRating.comment = comment;
                            sellerRating.status = 1;
                            sellerRating.createTime = currentTime;
                            sellerRating.createBy = memberId;
                            sellerRating.modifyTime = currentTime;
                            sellerRating.modifyBy = memberId;

                            // Add new
                            SellerRating.create(sellerRating, function (err, instance) {
                                if (err) { 
                                    if (err.name === "ValidationError") {
                                        // var error = ResultsCodes.VALIDATION_ERROR;
                                        // error.message = err.messages;
                                        err.code = ResultsCodes.VALIDATION_ERROR.code;
                                        err.name = ResultsCodes.VALIDATION_ERROR.name;
                                        result.setError(err);
                                    } else {
                                        result.setError(ResultsCodes.SYSTEM_ERROR);
                                    }
        
                                    return cb(null, result);
                                } else {
                                    let result = new Results();
                                    result.setSuccess(true);
                                    return cb(null, result);
                                }
                            });
                        } else {

                            // Update
                            sellerRatingInstace.rating = rating;
                            sellerRatingInstace.comment = comment;
                            sellerRatingInstace.modifyTime = currentTime;
                            sellerRatingInstace.modifyBy = memberId;

                            sellerRatingInstace.save(function (err, instance) {
                                if (err) { 
                                    if (err.name === "ValidationError") {
                                        // var error = ResultsCodes.VALIDATION_ERROR;
                                        // error.message = err.messages;
                                        err.code = ResultsCodes.VALIDATION_ERROR.code;
                                        err.name = ResultsCodes.VALIDATION_ERROR.name;
                                        result.setError(err);
                                    } else {
                                        result.setError(ResultsCodes.SYSTEM_ERROR);
                                    }

                                    return cb(null, result);
                                } else { 
                                    result.setSuccess(true);
                                    return cb(null, result);
                                }
                            });
                        }
                    });
                }
            });
        }
        makeCreateNew().catch(err=>{
            result.setError(err);
            return cb(null, result);
        });
    }

    SellerRating.RatingDetails = function (sellerId, ordersId, cb) {

        // var memberId = 5;

        SellerRating.findOne({
            fields: ["id", 'sellerId', 'orderId', "rating", "comment"],
            where: { 
                // "memberId": memberId,
                "sellerId": sellerId,
                "orderId": ordersId
            }
        }, function (err, instance) {

            if (err) {

                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {

                let result = new Results();
                result.setSuccess(instance);
                return cb(null, result);
            }
        })
    };

    SellerRating.remoteMethod("CreateNew", {
        http: {
            verb: "post",
            path: '/createnew'
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http:{
                    source:'req'
                }
            } 
        ],

        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    });

    SellerRating.remoteMethod("RatingDetails", {
        http: {
            "verb": "get"
        },
        accepts: [
            {
                arg: "sellerId",
                type: "number",
                required: true
            },
            {
                arg: "ordersId",
                type: "number",
                required: true
            }
        ],
        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    });
};