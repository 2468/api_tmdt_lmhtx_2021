/**
 * Model MemberAddress
 * Author: LuongPD
 * Last update: 2.4.2018 10:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
'use strict';

module.exports = function(MemberAddress) {

    //#region Functions
    MemberAddress.CreateNew = function(ctx, cb) {
        let fullName = ctx.body.fullName || null;
        let  address = ctx.body.address || null;
        let locationCodeLevel2 = ctx.body.locationCodeLevel2 || null;
        let  locationCodeLevel3 = ctx.body.locationCodeLevel3 || null;
        let  locationCodeLevel4 = ctx.body.locationCodeLevel4 || null; 
        let mobile = ctx.body.mobile || null;
        
        let result = new Results();
        if(!fullName || !address || !locationCodeLevel2 || !locationCodeLevel3 || !locationCodeLevel4 || !mobile) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null,result);
        }
        
        const makeCreateNew = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            var memberId = token.userId;
            var defaultFlag = 0;
            var sql = "SELECT COUNT(1) as CountAddress FROM member_address WHERE member_id=?";
            var params = [memberId];
            var connector = MemberAddress.dataSource.connector;
            connector.query(sql, params, function (err, resultObjects) {
                if (resultObjects.length > 0) {
                    var countRaw = resultObjects[0].CountAddress;
                    if (countRaw == 0) {
                        defaultFlag = 1;
                    }
                }

                var memberAddress = new MemberAddress;
                memberAddress.memberId = memberId;
                memberAddress.fullName = fullName;
                memberAddress.address = address;
                memberAddress.locationCodeLevel2 = locationCodeLevel2;
                memberAddress.locationCodeLevel3 = locationCodeLevel3;
                memberAddress.locationCodeLevel4 = locationCodeLevel4;
                memberAddress.mobile = mobile;
                memberAddress.status = 1;
                memberAddress.defaultFlag = defaultFlag;
                memberAddress.createTime = parseInt(new Date().getTime() / 1000.0);
                memberAddress.createBy = memberId;
                MemberAddress.create(memberAddress, function (err, response) {
                
                    if (err) { 
                        result.setError(ResultsCodes.ADD_ITEM_FAILED);
                        return cb(null,result);
                    }
                    else { 
                        result.setSuccess(true);
                        return cb(null,result);
                    }
                });
            });
        }

        makeCreateNew().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });;
    }

    MemberAddress.Edit = function(ctx, cb) {
        let id = ctx.body.id ;
        let  fullName = ctx.body.fullName || null;
        let  mobile = ctx.body.mobile || null; 
        let  address = ctx.body.address || null;
        let  locationCodeLevel2 = ctx.body.locationCodeLevel2 || null; 
        let locationCodeLevel3 = ctx.body.locationCodeLevel3 || null;
        let  locationCodeLevel4 = ctx.body.locationCodeLevel4 || null;
         
        let result = new Results();
        if(!fullName || !mobile || !address || !locationCodeLevel2 || !locationCodeLevel3 || !locationCodeLevel4){
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null,result);
        }
        const makeEdit = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            var memberId = token.userId;

            let memberAddress = await MemberAddress.findOne({
                where: {
                    "id": id
                }
            });

            if (!memberAddress) { 
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }

            if (memberId != memberAddress.memberId) { 
                result.setError(ResultsCodes.NO_PERMISSION);
                return cb(null, result);
            }

            memberAddress.fullName = fullName;
            memberAddress.mobile = mobile;
            memberAddress.address = address;
            memberAddress.locationCodeLevel2 = locationCodeLevel2;
            memberAddress.locationCodeLevel3 = locationCodeLevel3;
            memberAddress.locationCodeLevel4 = locationCodeLevel4;
            memberAddress.modifyTime = parseInt(new Date().getTime() / 1000.0);
            memberAddress.save(); 
            result.setSuccess(true);
            return cb(null,result);
        }

        makeEdit().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }

    MemberAddress.GetList = function(ctx, cb) {
        let pageIndex = ctx.query.pageIndex || 1;
        let  pageSize = ctx.query.pageSize || 10;
        let pageOrder = ctx.query.pageOrder || null;
        
        let result = new Results(); 
        const makeGetList = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
        
            var memberId = token.userId;
            MemberAddress.find({
                fields: ['id', 'fullName', 'address', 'locationCodeLevel2', 'locationCodeLevel3', 'locationCodeLevel4', 'mobile', 'status', 'defaultFlag'],
                where: {
                    "memberId": memberId,
                    "status": 1
                },
                limit: pageSize,
                skip: pageIndex * pageSize,
                order: pageOrder
            }, function (err, instance) { 
                result.setSuccess(instance);
                return  cb(null,result);
            })
        }

        makeGetList().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }

    MemberAddress.GetListIncludeLocationPaths = function(ctx, cb) {

        let pageIndex = ctx.body.pageIndex || 1;
        let  pageSize= ctx.body.pageSize || 20;
        let  pageOrder = ctx.body.pageOrder || null;

        let result = new Results();
        const makeGetList = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
        
            var memberId = token.userId;
            var sql = `SELECT 
                ma.id ma_id,
                full_name,
                address,
                location_code_level2,
                location_code_level3,
                location_code_level4,
                mobile,
                status,
                default_flag,
                location.path
            FROM
                member_address ma
                 LEFT JOIN
                location ON location.code = ma.location_code_level4
            WHERE
                status = 1
                AND member_id = ?`;

            var params = [memberId];

            if (pageSize) {
                if (pageSize <= 0) {
                    pageSize = 20;
                }
            } else {
                pageSize = 20;
            }

            if (pageIndex) {
                if (pageIndex < 1) {
                    pageIndex = 1;
                }
            } else {
                pageIndex = 1;
            }

            let orderBy = "";

            if (!pageOrder) {
                orderBy = "full_name ASC"
            } else {

                for (let i = 0; i < pageOrder.length; i++) {
                    let sortArr = pageOrder[i].match(/[^\s-]+-?/g);

                    if (sortArr.length == 2) {
                        let sortName = sortArr[0];
                        let direction = sortArr[1];

                        if ("id" === sortName.toLowerCase()) {

                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "ma.id" + direction;

                        } if ("address" === sortName.toLowerCase()) {

                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "ma.address" + direction;

                        } else if ("mobile" === sortName.toLowerCase()) {

                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "ma.mobile" + direction;

                        } else if ("defaultflag" === sortName.toLowerCase()) {
                            
                            if ("DESC" === direction.toUpperCase()) {
                                direction = " DESC";
                            } else {
                                direction = " ASC";
                            }

                            orderBy += "ma.default_flag" + direction;
                        }

                        if (i < pageOrder.length - 1 && orderBy) {
                            orderBy += ", ";
                        }
                    }
                }
            }

            if (orderBy) {
                sql = sql + " ORDER BY " + orderBy;
            }

            sql = sql + " LIMIT ? OFFSET ?";
            params.push(pageSize, (pageIndex - 1) * pageSize);

            var connector = MemberAddress.dataSource.connector;
            let memberAddressRaw = {};

            let results = await new Promise((resolve, reject) => {

                connector.query(sql, params, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            });

            let memberAddresses = [];

            let sellerId = 0;

            if (results.length > 0) {

                results.map(resultRaw => {

                    let memberAddressRaw =  connector.fromRow('MemberAddress', resultRaw);
                    memberAddressRaw.id = resultRaw.ma_id; // remap id from alias
                    memberAddressRaw.path = resultRaw.path;
                    memberAddresses.push(memberAddressRaw);
                });
            }
 
            result.setSuccess(memberAddresses);
            return  cb(null,result);
        }
        
        makeGetList().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }

    MemberAddress.GetDefaultAddress = function(ctx,cb) {
        
        let result = new Results();
        const makeGetDefaultAddress = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token || !token.id) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
        
            var memberId = token.userId;
            MemberAddress.findOne({
                fields: ['id', 'fullName', 'address', 'locationCodeLevel2', 'locationCodeLevel3', 'locationCodeLevel4', 'mobile', 'status', 'defaultFlag'],
                where: {
                    "memberId": memberId,
                    "defaultFlag": 1
                }
            }, function (err, instance) { 
                result.setSuccess(instance);
                return cb(null,result);
            })
        }
        makeGetDefaultAddress().catch(err=>{
            result.setError(err);
            return cb(null,result);
        });
    }

    MemberAddress.GetDefaultAddressIncludeLocationPath = function(ctx,cb) {
        
        let result = new Results();
        const makeGetDefaultAddress = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
        
            var memberId = token.userId;
            var sql = `SELECT 
                ma.id ma_id,
                full_name,
                address,
                location_code_level2,
                location_code_level3,
                location_code_level4,
                mobile,
                status,
                default_flag,
                location.path
            FROM
                member_address ma
                  LEFT JOIN
                location ON location.code = ma.location_code_level4
            WHERE
                status = 1
                AND default_flag = 1
                AND member_id = ?`;

            var params = [memberId];

            var connector = MemberAddress.dataSource.connector;

            let results = await new Promise((resolve, reject) => {

                connector.query(sql, params, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            });

            let memberAddressRaw = {};
            if (results.length > 0) {

                let resultRaw = results[0];
                memberAddressRaw =  connector.fromRow('MemberAddress', resultRaw);
                memberAddressRaw.id = resultRaw.ma_id; // remap id from alias
                memberAddressRaw.path = resultRaw.path;
            }
 
            result.setSuccess(memberAddressRaw);
            return  cb(null,result);
        }
        makeGetDefaultAddress().catch(err=>{
            result.setError(ResultsCodes.SYSTEM_ERROR);
            cb(null,result);
        });
    }

    MemberAddress.SetDefaultAddress = function(ctx, cb) {
        let addressId = ctx.body.addressId || 0;
        
        let result = new Results();
        if(!addressId) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null,result);
        }
        
        const makeSetDefaultAddress = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
        
            var memberId = token.userId;
            var sql = "UPDATE member_address SET default_flag = 0 WHERE member_id = ?";
            var params = [memberId];
            var connector = MemberAddress.dataSource.connector;
            connector.query(sql, params, function (err, resultObjects) {
                
                if (err) { 
                    result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                    return  cb(null,result);
                }
                else {
                    sql = "UPDATE member_address SET default_flag = 1 WHERE member_id = ? AND id = ?";
                    params.push(addressId);
                    connector.query(sql, params, function (err, resultObjects) {

                        if (err) { 
                            result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                            return  cb(null,result);
                        }
                        else { 
                            result.setSuccess(true);
                            return  cb(null,result);
                        }
                    });
                }
            });
        }
        makeSetDefaultAddress().catch(err=>{
            result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
            return cb(null,result);
        });
    }

    MemberAddress.Delete = function(ctx, cb) { 
        let id = ctx.body.addressId || 0;
        let result = new Results();
        const makeDelete = async () => {
            let Passport = MemberAddress.app.models.Passport;
            let token = await Passport.GetMemberToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
        
            var memberId = token.userId;

            let memberAddress = await MemberAddress.findOne({
                where: {
                    "id": id
                }
            });

            if (!memberAddress) { 
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }

            if (memberAddress.defaultFlag) { 
                result.setError(ResultsCodes.CANNOT_DELETE_DEFAULT_ADDRESS_ERROR);
                return cb(null, result);
            }

            if (memberId != memberAddress.memberId) { 
                result.setError(ResultsCodes.NO_PERMISSION);
                return cb(null, result);
            }

         await memberAddress.delete(); 
            result.setSuccess(true);
            return cb(null,result);

        }

        makeDelete().catch (function(e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }
    //#endregion

    //#region Expose Parameters
    MemberAddress.remoteMethod('CreateNew', {
        http:{
            verb:'POST',
            path:'/CreateNew'
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        
    })

    MemberAddress.remoteMethod('Edit', {
        description:'@Hung Doan, edit member address information',
        http:{
            verb:'POST',
            path:'/edit'
        },
        accepts: [
           {
                arg: "ctx",
                type: "object",
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        
    })

    MemberAddress.remoteMethod('GetList', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        
    })

    MemberAddress.remoteMethod('GetListIncludeLocationPaths', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        
    })


    MemberAddress.remoteMethod('GetDefaultAddress', {
        http: {
            verb: "GET"
        },
        accepts:[
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        
    })

    MemberAddress.remoteMethod('GetDefaultAddressIncludeLocationPath', {
        http: {
            verb: "GET"
        },
        accepts:[
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        
    })

    MemberAddress.remoteMethod('SetDefaultAddress', {
        http: {
            verb: "POST"
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        
    })

    MemberAddress.remoteMethod('Delete', {
        http: {
            verb: "POST"
        },
        accepts: [
            {
                arg: "ctx",
                type: "object",
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
    })
    //#endregion
};