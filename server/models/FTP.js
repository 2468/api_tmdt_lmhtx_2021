/**
 * FTP connect process
 * Author: Thiep Wong
 * Created: 18/04/2018
 */
var Client = require('ftp');
var app = require('../../server/server');
var FTPConnect = app.get('FTP');

module.exports = function (FTP) {


    /**
     * upload file to ftp server
     * @param {*} sourceFile the source file 
     * @param {*} fileName   name of file to save
     * @param {*} path path to save file
     */
    FTP.upload = async function (sourceFile, fileName, path) {
        let client = new Client();
        var res = false;
        let _promise = new Promise((resolve, reject) => {
            client.on('ready', function () {

                client.cwd(FTPConnect.rootPath, function (err, a) {
                    if (err) { 
                        return reject(err);
                    }
                    client.list(function (err, list) {
                        if (err) { 
                            return reject(err);
                        }
                        if (FTP.dirExist(list, path[0])) {
                            client.cwd(path[0], function (err, dir) {
                                if (err) { 
                                    return reject(err);
                                }
                                client.list(function (err, lst) {
                                    if (err) { 
                                        return reject(err);
                                    }
                                    if (FTP.dirExist(lst, path[1])) {
                                        client.cwd(path[1], function (err, dir) {
                                            if (err) { 
                                                return reject(err);
                                            }
                                            client.list(function (err, lst) {
                                                if (err) { 
                                                    return reject(err);
                                                }
                                                if (FTP.dirExist(lst, path[2])) {
                                                    client.put(sourceFile, path[2] + '/' + fileName, function (err) {
                                                        if (err) {
                                                           // throw err;
                                                            return reject(err);
                                                        };
                                                        client.end();

                                                        return resolve(true);

                                                    });
                                                }
                                                else {
                                                    client.mkdir(path[2], function (err) {
                                                        if (err) { 
                                                            return reject(err);
                                                        }
                                                        client.put(sourceFile, path[2] + '/' + fileName, function (err) {
                                                            if (err) {
                                                               // throw err;
                                                                return reject(err);
                                                            }
                                                            client.end();
                                                            return resolve(true);

                                                        });
                                                    })
                                                }


                                            })
                                        })
                                    }
                                    else {
                                        // Create new folder by month
                                        client.mkdir(path[1], function (err) {
                                            if (err) { 
                                                return reject(err);
                                            }
                                            client.cwd(path[1], function (err, dir) {
                                                if (err) { 
                                                    return reject(err);
                                                }
                                                client.list(function (err, lst) {
                                                    if (err) { 
                                                          return reject(err);
                                                      }
                                                    if (FTP.dirExist(lst, path[2])) {
                                                        client.cwd(path[2], function (err, dir) {
                                                            // Create the file here!
                                                            client.put(sourceFile, path[2] + '/' + fileName, function (err) {
                                                                if (err) {
                                                                  //  throw err;
                                                                    return reject(err);
                                                                }
                                                                client.end();
                                                                return resolve(true);

                                                            });

                                                        });
                                                    }
                                                    else {
                                                        client.mkdir(path[2], function (err) {
                                                            client.put(sourceFile, path[2] + '/' + fileName, function (err) {
                                                                if (err) {
                                                                  //  throw err;
                                                                    return reject(err);
                                                                }
                                                                client.end();
                                                                return resolve(true);

                                                            });

                                                        })
                                                    }


                                                })
                                            })
                                        })
                                    }


                                })
                            })
                        }
                        else {

                            client.mkdir(path[0], function (err) {
                                client.cwd(path[0], function (err, dir) {
                                    client.mkdir(path[1], function (err) {
                                        client.cwd(path[1], function (err, dir) {
                                            client.mkdir(path[2], function (err) {
                                                client.put(sourceFile, path[2] + '/' + fileName, function (err) {
                                                    if (err) {
                                                      //  throw err;
                                                        return reject(err);
                                                    }
                                                    client.end();
                                                    return resolve(true);
                                                });
                                            })
                                        })

                                    })
                                })
                            })

                        }
                        // console.dir(list);


                    });


                });

            });


            client.connect(FTPConnect.config);


        });

        return _promise;


    }

    FTP.put = async function (sourceFile, fileName, path) {
        let client = new Client();
        var res = false;
        client.on('ready', function () {
            let level = path.length;
            client.cwd(FTPConnect.rootPath, function (err, cDir) {
                for (let i = 0; i < level; i++) {
                    let _dir = FTP.dirExist(path[i])
                    if (_dir) {
                        client.cwd(path[i]);
                    }

                }


            })
        });
        client.connect(FTPConnect.config);

    }

    /**
     * Check dir exist
     * @param {*} list list of directory 
     * @param {*} path path to save file
     */
    FTP.dirExist = function (list, path) {
        let res = false;
        list.forEach(item => {
            if (item.name == path && item.type == 'd') res = true;
        });

        return res;
    }

}