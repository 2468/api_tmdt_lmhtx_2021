/**
 * Model Products
 * Author: LuongPD
 * Last update: 14.3.2018 13:49
 */
"use strict";
var SEngine = require("../middlewares/SearchEngine");
var Results = new require("../middlewares/ResultHandle");
var ResultsCodes = require("../middlewares/message_constant");
var Filter = require("../middlewares/DataFilterHandle");
var path = require("path");
var ApplicationError = require("../middlewares/application-error");
var app = require("../../server/server");
var shippingStatus = require("../middlewares/shipping_constant");

// var async = require('async');

module.exports = function(Products) {
  //#region Functions
  /**
   * GetDetails is used to get all information of product by id
   * @param {number} id id of product from frontend
   * @param {number} locationCodeLevel1 country lelve
   * @param {number} locationCodeLevel2 city level
   * @param {number} locationCodeLevel3 district level
   * @param {number} locationCodeLevel4 ward level
   * @param {array} pageOrder pageOrder from frontend (an array, example ["productName DESC", "categoryId ASC"])
   * @param {*} cb Callback function for result returns
   */
  Products.GetDetails = function(ctx, cb) {
    let   id = ctx.query.id || 0;
    let sellerUuid = ctx.query.sellerUuid || null;
    let locationCodeLevel1 = ctx.query.locationCodeLevel1 || null;
    let locationCodeLevel2 = ctx.query.locationCodeLevel2 || null;
    let locationCodeLevel3 = ctx.query.locationCodeLevel3 || null;
    let locationCodeLevel4 = ctx.query.locationCodeLevel4 || null;
    let platformCode = ctx.query.platformCode || 'E-SML';   
    let pageOrder = ctx.query.pageOrder || null;

    const start = async () => {
      var memberId = 0;
      let Passport = Products.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (token && token.id) {
        memberId = token.userId;
      }

      let productSql = `SELECT 
                pd.id pd_id,
                pd.category_id,
                pd.product_name,
                pd.product_image,
                pd.short_description,
                pd.description,
                pd.article,
                pd.certificate_images,
                pd.youtube_video_link,
                pd.unit,
                pd.weight,
                pd.dimension,
                pd.certificate_images,
                pd.youtube_video_link,
                br.id br_id,
                br.brand_name,
                br.logo,
                IFNULL(AVG(rating), 0) AS ratingAverage,
                COUNT(rating) AS ratingCount,
                EXISTS(SELECT 1 FROM product_favorites WHERE product_id = pd.id AND member_id = ?) AS favorite
            FROM
                products pd
                   LEFT JOIN
                brands br ON br.id = pd.brand_id
                   LEFT JOIN
                product_rating pr ON pr.product_id = pd.id
            WHERE
                pd.id = ?;`;

      let productSellerSql = `SELECT 
                ps.product_id ps_product_id,
                ps.id ps_id,
                ps.seller_id,
                ps.base_price,
                ps.aspect,
                ps.short_description,
                ps.service_area_level,
                sl.id sl_id,
                sl.uuid sl_uuid,
                sl.shop_name,
                sl.fullname,
                sl.seller_type,
                sl.shop_address,
                sl.shop_location_code,
                sl.logo_image,
                sl.cover_image,
                sl.create_time,         
                GROUP_CONCAT(CONCAT(pp.id, ':', pp.price, ':', pp.from, ':', pp.to)
                SEPARATOR ';') pp_arr,
                GROUP_CONCAT(CONCAT(pst.id , ':' , pst.product_seller_id, ':',pst.available, ':', pst.seller_store_id, ':',  CASE
                WHEN ps.service_area_level = 1 and lo1.code = ? THEN '1'
                WHEN ps.service_area_level = 2 and lo2.code = ? THEN '1'
                WHEN ps.service_area_level = 3 and lo3.code = ? THEN '1'
                WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
                    else '0'
                END)
                SEPARATOR ';') pst_arr,
                IFNULL((SELECT 
                        (COUNT(*) / (SELECT 
                                    COUNT(*)
                                FROM
                                    seller_rating
                                WHERE
                                    seller_id = sl.id)) * 100
                    FROM
                        seller_rating
                    WHERE
                        seller_id = sl.id AND rating = 3
                    GROUP BY rating),
                0) AS 'ratingPercentage'
            FROM
                product_seller ps
                   LEFT JOIN
                product_seller_platform psp ON psp.product_seller_id = ps.id
                LEFT JOIN
                sellers sl ON sl.id = ps.seller_id
                    LEFT JOIN
                product_price pp ON pp.product_seller_id = ps.id
                    AND pp.from <= ?
                    AND pp.to >= ?
                    LEFT  JOIN
                product_store pst ON pst.product_seller_id = ps.id
                LEFT  JOIN
                seller_store ss ON pst.seller_store_id = ss.id
                LEFT   JOIN
                location lo4 ON lo4.code = ss.location_code
                LEFT   JOIN
                location lo3 ON lo3.code = lo4.parent_code
                LEFT   JOIN
                location lo2 ON lo2.code = lo3.parent_code
                LEFT   JOIN
                location lo1 ON lo1.code = lo2.parent_code            
         

            WHERE
                ps.product_id = ?
                AND sl.status IN (1,2)
                AND ps.status = 1
                AND psp.platform_code = ?
                AND psp.status = 1
                AND psp.display_on_shop = 1
                AND pst.available > 0
                `;

      let productSellerGroupClause = ` GROUP BY ps.ID 
                HAVING
                FIND_IN_SET ('1',  GROUP_CONCAT(CASE
                    WHEN ps.service_area_level = 1 and  lo1.code = ? THEN '1'
                    WHEN ps.service_area_level = 2 and  lo2.code = ? THEN '1'
                    WHEN ps.service_area_level = 3 and  lo3.code = ? THEN '1'
                    WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
                    else '0'
                END 
                SEPARATOR ',')) >  0`;

      let params = [memberId, id];

      let connector = Products.dataSource.connector;
      // let product = await connector.query(productSql, params);

      // Obtain Product, Brand
      // let productRaw = await Products.dataSource.connector.query(productSql, params);
      let productRaw = {};

      let productResults = await new Promise((resolve, reject) => {
        connector.query(productSql, params, function(err, data) {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });

      if (productResults.length > 0) {
        // convert result raws to JSON tree
        let brand = null; // we only have one record for brand

        // resultObjects.map(resultRaw => {
        productRaw = connector.fromRow("Products", productResults[0]);
        productRaw.id = productResults[0].pd_id; // remap id from alias
        productRaw.ratingAverage = productResults[0].ratingAverage;
        productRaw.ratingCount = productResults[0].ratingCount;
        productRaw.favorite = productResults[0].favorite;

        brand = connector.fromRow("Brands", productResults[0]);
        brand.id = productResults[0].br_id;
        productRaw.brand = brand;
        // });

        // Rating
        let ratings = await Products.app.models.ProductRating.find({
          fields: ["id", "modifyTime", "rating", "comment", "memberId"],
          include: {
            relation: "members", // include the owner object
            scope: {
              // further filter the owner object
              fields: ["username", "fullname"]
            }
          },
          where: { productId: id },
          limit: 5,
          skip: 0,
          order: "modifyTime desc"
        });

        productRaw.ratings = ratings;

        let countRatingByStar = await Products.app.models.ProductRating.countRatingByStarAsync(
          productRaw.id
        );
        productRaw.ratingCountByStar = countRatingByStar;

        // Obtain Product Sellers, Product Price, Product Stores, Sellers
        let currentTime = parseInt(new Date().getTime() / 1000.0);

        let sellerId = 0;
        if (sellerUuid && sellerUuid !== "undefined" && /\S/.test(sellerUuid)) {
          let seller = await Products.app.models.Sellers.findOne({
            fields: ["id"],
            where: {
              uuid: sellerUuid
            }
          });

          if (seller && seller.id > 0) {
            sellerId = seller.id;

            productSellerSql += " AND sl.uuid = ?";
            params = [
              locationCodeLevel1,
              locationCodeLevel2,
              locationCodeLevel3,
              locationCodeLevel4,
              currentTime,
              currentTime,
              id,
              platformCode,
              sellerUuid,
              locationCodeLevel1,
              locationCodeLevel2,
              locationCodeLevel3,
              locationCodeLevel4
            ];
          } else {
            let result = new Results();
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cb(null, result);
          }
        } else {
          params = [
            locationCodeLevel1,
            locationCodeLevel2,
            locationCodeLevel3,
            locationCodeLevel4,
            currentTime,
            currentTime,
            id,
            platformCode,
            locationCodeLevel1,
            locationCodeLevel2,
            locationCodeLevel3,
            locationCodeLevel4
          ];
        }

        productSellerSql += productSellerGroupClause;

        let orderBy = "";

        if (!pageOrder) {
          orderBy = "GROUP_CONCAT(pp.price SEPARATOR ',') ASC";
        } else {
          for (let i = 0; i < pageOrder.length; i++) {
            let sortArr = pageOrder[i].match(/[^\s-]+-?/g);

            if (sortArr.length == 2) {
              let sortName = sortArr[0];
              let direction = sortArr[1];

              if ("price" === sortName.toLowerCase()) {
                if ("DESC" === direction.toUpperCase()) {
                  direction = " DESC";
                } else {
                  direction = " ASC";
                }

                orderBy += "GROUP_CONCAT(pp.price SEPARATOR ',')" + direction;
              }
              if ("shopname" === sortName.toLowerCase()) {
                if ("DESC" === direction.toUpperCase()) {
                  direction = " DESC";
                } else {
                  direction = " ASC";
                }

                orderBy += "sl.shop_name" + direction;
              }

              if (i < pageOrder.length - 1 && orderBy) {
                orderBy += ", ";
              }
            }
          }
        }

        productSellerSql = productSellerSql + " ORDER BY " + orderBy;

        let productSellers = await new Promise((resolve, reject) => {
          connector.query(productSellerSql, params, function(err, data) {
            if (err) {             
            
              reject(err);
            } else {
             
              resolve(data);
              
            }
          });
        });

        // let productSellers = await connector.query(productSellerSql, params, function (err, resultProductSellers) {
        let productSellersRaw = [];

        if (productSellers.length > 0) {
          productSellers.map(resultRaw => {

            let productSellerRaw = connector.fromRow(
              "ProductSeller",
              resultRaw
            );
            productSellerRaw.id = resultRaw.ps_id; // remap id from alias

            // Seller
            let sellerRaw = connector.fromRow("Sellers", resultRaw);
            sellerRaw.id = resultRaw.sl_id; // remap id from alias
            sellerRaw.uuid = resultRaw.sl_uuid; // remap id from alias

            sellerRaw.ratingPercentage = Math.ceil(resultRaw.ratingPercentage);

            // Assign Seller
            productSellerRaw.seller = sellerRaw;

            // Price
            if (resultRaw.pp_arr) {
              let productPriceArrRaw = resultRaw.pp_arr;

              let firstElement = productPriceArrRaw.split(";")[0];
              let productPriceRaw = {
                id: firstElement.id,
                price: firstElement.price,
                from: firstElement.fro,
                to: firstElement.to
              };

              // Assign Price
              productSellerRaw.productPrice = productPriceRaw;
            }

            // Product Price
            let productPrices =
              resultRaw.pp_arr != null
                ? resultRaw.pp_arr.split(";").map(x => {
                    let item = x.split(":");
                    if (item != null && item.length >= 4) {
                      return {
                        id: parseNumber(item[0]),
                        price: parseNumber(item[1]),
                        from: parseNumber(item[2]),
                        to: parseNumber(item[3])
                      };
                    }
                  })
                : null;

            // Product Store
            let productStores =
              resultRaw.pst_arr != null
                ? resultRaw.pst_arr.split(";").map(x => {
                    let item = x.split(":");
                    if (item != null && item.length >= 5) {
                      return {
                        id: parseNumber(item[0]),
                        productSellerId: parseNumber(item[1]),
                        available: parseNumber(item[2]),
                        sellerStoreId: parseNumber(item[3]),
                        saleable: parseNumber(item[4])
                      };
                    }
                  })
                : null;

            if (productPrices != null && productPrices.length > 0) {
              productSellerRaw.productPrice = productPrices[0];
            }

            if (productStores != null && productStores.length > 0) {
              let productStoresByProductSellerId = productStores.filter(
                x => x.productSellerId == productSellerRaw.id && x.saleable == 1
              );
              if (
                productStoresByProductSellerId != null &&
                productStoresByProductSellerId.length > 0
              ) {
                productStoresByProductSellerId = productStoresByProductSellerId.sort(
                  compare
                );
                productSellerRaw.productStore =
                  productStoresByProductSellerId[0];

                if (sellerId > 0) {
                  if (productSellerRaw.sellerId == sellerId) {
                    productSellersRaw.push(productSellerRaw);
                  }
                } else {
                  productSellersRaw.push(productSellerRaw);
                }
              }
            }
          });
        }

        productRaw.productSellers = productSellersRaw;

        let questions = await Products.app.models.Questions.find({
          fields: ["id", "productId", "memberId", "question", "createTime"],
          include: [
            {
              relation: "member",
              scope: {
                fields: ["id", "fullname"]
              }
            },
            {
              relation: "answers",
              scope: {
                fields: ["id", "sellerId", "answer", "modifyTime"],
                include: {
                  relation: "seller",
                  scope: {
                    fields: ["id", "shopName"]
                  }
                },
                where: {
                  sellerId: sellerUuid ? sellerId : { neq: null }
                }
              }
            }
          ],
          where: {
            productId: productRaw.id
          },
          limit: 5,
          order: "createTime DESC"
        });

        // const questionFiltered = questions.filter(question => question.answers.length > 0);

        const questionFiltered = questions.filter(
          x => x.__data.answers.length > 0
        );
        productRaw.questions = questionFiltered;

        // check allow comment
        let checkCommentSql = `select count(1) as countOrder from shipping_bill sb
        inner join orders_details od on sb.id = od.shipping_bill_id
        inner join orders o on od.orders_id = o.id
        inner join product_seller ps on od.product_seller_id = ps.id
        where ps.product_id = ? and o.member_id = ? and (sb.status = ? or sb.status = ?)`;
        let cParams = [productRaw.id, memberId, shippingStatus.DA_GIAO_THANH_CONG.status, shippingStatus.KET_THUC.status];
        let countOrder = await new Promise((resolve, reject) => {
          connector.query(checkCommentSql, cParams, function(err, data) {
            if (err) {             
              reject(err);
            } else {
              resolve(data);
            }
          });
        });
        // console.log(countOrder[0].countOrder);
        productRaw.allowComment = countOrder[0].countOrder > 0 ? 1 : 0;

        let result = new Results();
        result.setSuccess(productRaw);
        return cb(null, result);
      } else {
        let result = new Results();
        result.setError(ResultsCodes.DATA_NOT_FOUND);
        return cb(null, result);
      }
    };

    start().catch(function(e) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  function compare(a, b) {
    if (parseInt(a.available) < parseInt(b.available)) return 1;
    if (parseInt(a.available) > parseInt(b.available)) return -1;
    return 0;
  }

  /**
   * GetList is used to search all items in Products table by productNam or brandIds
   * @param {String} productName productName from frontend
   * @param {*} brandIds brandIds from frontend
   * @param {*} locationLevel1 country lelve
   * @param {*} locationLevel2 city level
   * @param {*} locationLevel3 district level
   * @param {*} locationLevel4 ward level
   * @param {*} pageSize pageSize from frontend (default is 10)
   * @param {*} pageIndex pageIndex from frontend (default is 0)
   * @param {*} pageOrder pageOrder from frontend (an array, example ["productName DESC", "categoryId ASC"])
   * @param {*} cb Callback function for result returns
   */
  Products.GetList = function(
    uuid,
    productName,
    brandIds,
    categoryId,
    platformCode,
    locationLevel1,
    locationLevel2,
    locationLevel3,
    locationLevel4,
    keySearch,
    fromPrice,
    toPrice,
    pageSize,
    pageIndex,
    pageOrder,
    cb
  ) {
    const start = async () => {
      // validate input arguments

      // if ((!uuid || !/\S/.test(uuid)) && (!productName || !/\S/.test(productName)) && (!brandIds || brandIds.length == 0) && (!categoryId || categoryId == 0) && (!keySearch || !/\S/.test(keySearch))) {

      //     let result = new Results();
      //     result.setError(ResultsCodes.PRODUCT_GET_LIST_ARGUMENTS_INVALID);
      //     return cb(null, result);
      // } else {

      let sql = `SELECT 
                FIND_IN_SET ('1',  GROUP_CONCAT(CASE
                    WHEN ps.service_area_level = 1 and  lo1.code = ? THEN '1'
                    WHEN ps.service_area_level = 2 and  lo2.code = ? THEN '1'
                    WHEN ps.service_area_level = 3 and  lo3.code = ? THEN '1'
                    WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
                    else '0'
                END 
                SEPARATOR ',')) > 0 saleable,
                pd.id,
                pd.category_id categoryId,
                pd.weight weight,
                pd.unit unit,
                pd.brand_id brandId,
                pd.product_name productName,
                pd.product_image productImage,
                CAST(IFNULL(MIN(pp.price), MIN(ps.base_price)) AS UNSIGNED) minPrice,
                CAST(IFNULL(MAX(pp.price), MAX(ps.base_price)) AS UNSIGNED) maxPrice,
                IFNULL(AVG(rating),0) AS ratingAverage, COUNT(DISTINCT pr.id) AS ratingCount
                FROM
                    products pd
                        JOIN
                    product_seller ps ON ps.product_id = pd.id
                        JOIN
                    product_seller_platform psp ON psp.product_seller_id = ps.id
                        JOIN
                    sellers sl ON ps.seller_id = sl.id
                        LEFT JOIN
                    product_price pp ON pp.product_seller_id = ps.id
                        AND pp.from <= ?
                        AND pp.to >= ?
                        JOIN
                    product_store pst ON pst.product_seller_id = ps.id
                        JOIN
                    seller_store ss ON pst.seller_store_id = ss.id
                        JOIN
                    location lo4 ON lo4.code = ss.location_code
                        JOIN
                    location lo3 ON lo3.code = lo4.parent_code
                        JOIN
                    location lo2 ON lo2.code = lo3.parent_code
                        JOIN
                    location lo1 ON lo1.code = lo2.parent_code
                        LEFT JOIN
                    product_rating pr ON pr.product_id = pd.id
                WHERE
                ps.status = 1
                AND sl.status IN (1,2)
                AND psp.platform_code = ?
                AND psp.display_on_shop = 1`;

      let currentTime = parseInt(new Date().getTime() / 1000.0);
      let params = [
        locationLevel1,
        locationLevel2,
        locationLevel3,
        locationLevel4,
        currentTime,
        currentTime,
        platformCode
      ];

      if (uuid && /\S/.test(uuid)) {
        sql = sql + " AND sl.uuid = ? ";
        params.push(uuid);
      }

      if (productName && /\S/.test(productName)) {
        sql = sql + " AND pd.product_name LIKE ? ";
        params.push("%" + productName + "%");
      }

      if (brandIds && brandIds.length > 0) {
        sql = sql + " AND pd.brand_id in (?)";
        params.push(brandIds);
      }

      if (categoryId && categoryId > 0) {
        sql =
          sql +
          " AND FIND_IN_SET(category_id, (SELECT arr_child_id FROM category where id = ?)) > 0";
        // sql = sql + " AND pd.category_id IN (SELECT arr_child_id FROM category where id = ?)";
        params.push(categoryId);
      }

      let idList = null;
      if (keySearch) {
        let s = new SEngine();
        let hits = await s.Search("smartlife-eco", "Products", keySearch);
        idList = hits.map(x => x._source.id).join();

        sql = sql + " AND FIND_IN_SET(pd.id, ? ) > 0";
        params.push(idList);

        // sql = sql + " AND FIND_IN_SET(pd.id, (select product_id_arr from product_key_search where key_search = ?)) > 0";
        // params.push(keySearch);
      }

      sql = sql + ` GROUP BY pd.id `;
      // params.push.apply(params, [locationLevel1, locationLevel2, locationLevel3, locationLevel4]);

      if (fromPrice) {
        sql =
          sql +
          " HAVING (CAST(IFNULL(MIN(pp.price), MIN(ps.base_price)) AS UNSIGNED) >= ? OR  CAST(IFNULL(MAX(pp.price), MIN(ps.base_price)) AS UNSIGNED) >= ?)";
        params.push(fromPrice);
        params.push(fromPrice);
      }

      if (toPrice) {
        if (!fromPrice) {
          sql = sql + " HAVING";
        } else {
          sql = sql + " AND";
        }

        sql =
          sql +
          " (CAST(IFNULL(MIN(pp.price), MIN(ps.base_price)) AS UNSIGNED) <= ? OR  CAST(IFNULL(MAX(pp.price), MIN(ps.base_price)) AS UNSIGNED) <= ?)";
        params.push(toPrice);
        params.push(toPrice);
      }

      if (pageSize) {
        if (pageSize <= 0) {
          pageSize = 10;
        }
      } else {
        pageSize = 10;
      }

      if (pageIndex) {
        if (pageIndex < 1) {
          pageIndex = 1;
        }
      } else {
        pageIndex = 1;
      }

      let orderBy = "";

      if (!pageOrder) {
        if (idList) {
          // in case ElastichSearch => use order by the id list order return by ElastichSearch
          orderBy = "FIND_IN_SET(pd.id, ?)";
          params.push(idList);
        } else {
          orderBy = "product_name DESC";
        }
      } else {
        for (let i = 0; i < pageOrder.length; i++) {
          let sortArr = pageOrder[i].match(/[^\s-]+-?/g);

          if (sortArr.length == 2) {
            let sortName = sortArr[0];
            let direction = sortArr[1];

            if ("id" === sortName.toLowerCase()) {
              if ("DESC" === direction.toUpperCase()) {
                direction = " DESC";
              } else {
                direction = " ASC";
              }

              orderBy += "pd.id" + direction;
            }
            if ("brandid" === sortName.toLowerCase()) {
              if ("DESC" === direction.toUpperCase()) {
                direction = " DESC";
              } else {
                direction = " ASC";
              }

              orderBy += "pd.brand_id" + direction;
            } else if ("productname" === sortName.toLowerCase()) {
              if ("DESC" === direction.toUpperCase()) {
                direction = " DESC";
              } else {
                direction = " ASC";
              }

              orderBy += "pd.product_name" + direction;
            } else if ("categoryid" === sortName.toLowerCase()) {
              if ("DESC" === direction.toUpperCase()) {
                direction = " DESC";
              } else {
                direction = " ASC";
              }

              orderBy += "pd.category_id" + direction;
            } else if ("minprice" === sortName.toLowerCase()) {
              if ("DESC" === direction.toUpperCase()) {
                direction = " DESC";
              } else {
                direction = " ASC";
              }

              orderBy +=
                "CAST(IFNULL(MIN(pp.price), MIN(ps.base_price)) AS UNSIGNED)" +
                direction;
            } else if ("maxprice" === sortName.toLowerCase()) {
              if ("DESC" === direction.toUpperCase()) {
                direction = " DESC";
              } else {
                direction = " ASC";
              }

              orderBy +=
                "CAST(IFNULL(MAX(pp.price), MAX(ps.base_price)) AS UNSIGNED)" +
                direction;
            }

            if (i < pageOrder.length - 1 && orderBy) {
              orderBy += ", ";
            }
          }
        }
      }

      if (orderBy) {
        sql = sql + " ORDER BY " + orderBy + " LIMIT ? OFFSET ?";
      } else {
        sql = sql + " LIMIT ? OFFSET ?";
      }

      params.push(pageSize, (pageIndex - 1) * pageSize);

      //sql = sql.replace(/%s/g, orderBy);
      let connector = Products.dataSource.connector;
      connector.query(sql, params, function(err, resultObjects) {
        // Products.dataSource.disconnect();

        if (err) {
          let result = new Results();
          result.setError(ResultsCodes.SYSTEM_ERROR);
          return cb(null, result);
        } else {
          let result = new Results();
          result.setSuccess(resultObjects);
          return cb(null, result);
        }
      });
      // }
    };

    start().catch(function(e) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  function parseNumber(number) {
    return Number(number) || 0;
  }

  /**
   * GetListHotSale
   * * @param {*} platformCode country lelve
   * @param {*} locationLevel1 country lelve
   * @param {*} locationLevel2 city level
   * @param {*} locationLevel3 district level
   * @param {*} locationLevel4 ward level
   * @param {*} cb Callback function for result returns
   */
  Products.GetListHotSale = function(
    platformCode,
    locationLevel1,
    locationLevel2,
    locationLevel3,
    locationLevel4,
    cb
  ) {
    let sql = `SELECT 
        FIND_IN_SET ('1',  GROUP_CONCAT(CASE
            WHEN ps.service_area_level = 1 and  lo1.code = ? THEN '1'
            WHEN ps.service_area_level = 2 and  lo2.code = ? THEN '1'
            WHEN ps.service_area_level = 3 and  lo3.code = ? THEN '1'
            WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
            else '0'
        END 
        SEPARATOR ',')) > 0 saleable,
        pd.id,
        pd.weight weight,
        pd.unit unit,
        pd.category_id categoryId,
        pd.brand_id brandId,
        pd.product_name productName,
        pd.product_image productImage,
        IFNULL(MIN(pp.price), MIN(ps.base_price)) minPrice,
        IFNULL(MAX(pp.price), MAX(ps.base_price)) maxPrice,
        IFNULL(AVG(rating),0) AS ratingAverage, COUNT(DISTINCT pr.id) AS ratingCount
        FROM
            products pd
                JOIN
            product_seller ps ON ps.product_id = pd.id
                JOIN
            product_seller_platform psp ON psp.product_seller_id = ps.id
                JOIN
            sellers sl ON ps.seller_id = sl.id
                LEFT JOIN
            product_price pp ON pp.product_seller_id = ps.id
                AND pp.from <= ?
                AND pp.to >= ?
                JOIN
            product_store pst ON pst.product_seller_id = ps.id
                JOIN
            seller_store ss ON pst.seller_store_id = ss.id
                JOIN
            location lo4 ON lo4.code = ss.location_code
                JOIN
            location lo3 ON lo3.code = lo4.parent_code
                JOIN
            location lo2 ON lo2.code = lo3.parent_code
                JOIN
            location lo1 ON lo1.code = lo2.parent_code
                LEFT JOIN
            product_rating pr ON pr.product_id = pd.id
        WHERE
        ps.status = '1'
        AND pd.status = '2'
        AND psp.platform_code = ?
        AND psp.display_on_shop = '1'`;

    let currentTime = parseInt(new Date().getTime() / 1000.0);
    let params = [
      locationLevel1,
      locationLevel2,
      locationLevel3,
      locationLevel4,
      currentTime,
      currentTime,
      platformCode
    ];

    sql = sql + ` GROUP BY pd.id `;
    sql = sql + ` ORDER BY RAND() LIMIT 5`;

    let connector = Products.dataSource.connector;
    connector.query(sql, params, function(err, resultObjects) {
      if (err) {
        let result = new Results();
        result.setError(ResultsCodes.DATA_NOT_FOUND);
        return cb(null, result);
      } else {
        let result = new Results();
        result.setSuccess(resultObjects);
        return cb(null, result);
      }
    });
  };

  /**
   * Created by Thiep Wong, get products list by multiple categories for search using
   * 25/4/2018
   * @param {*} categories categories array, [1,2,3,4,5] for where filter
   * @param {*} cb  callback result
   */
  Products.GetProductsByCategories = function(categories, cb) {
    if (!categories) {
      let result = new Results();
      result.setError(ResultsCodes.DATA_NOT_FOUND);
      return cb(null, result);
    }

    Products.find(
      {
        fields: {
          id: true,
          categoryId: true,
          productName: true,
          brandId: true,
          productImage: true
        },
        where: {
          categoryId: {
            inq: categories.split(",")
          }
        }
      },
      function(err, proInstance) {
        let result = new Results();
        if (err) {
          result.setError(ResultsCodes.DATA_NOT_FOUND);
          return cb(null, result);
        }

        result.setSuccess(proInstance);
        cb(null, result);
      }
    );
  };

  /**
   * Search products by keywords
   * Created by Hung Doan
   * Date: 24/08/2018
   * @param {*} keyword
   * @param {*} cb
   */
  Products.SearchSuggestion = function(platformCode, keyword, cb) {
    let s = new SEngine();
    let result = new Results();

    const makeRequest = async () => {
      let hits = await s.SearchSuggestion(
        "products",
        "_doc",
        platformCode,
        keyword
      );
      let results = hits.map(x => x._source.productName);
      result.setSuccess(results);
      cb(null, result);
    };

    makeRequest().catch(function(e) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  /**
   * Search products by keywords
   * Created by Hung Doan
   * Date: 24/08/2018
   * @param {*} keyword
   * @param {*} cb
   */
  Products.SearchAggreation = function(
    platformCode,
    keyword,
    sellerUuid,
    parentCategoryId,
    cb
  ) {
    let s = new SEngine();
    let result = new Results();

    if (
      !(
        (keyword && keyword !== "undefined" && /\S/.test(keyword)) ||
        (sellerUuid && /\S/.test(sellerUuid)) ||
        (parentCategoryId && parentCategoryId > 0)
      )
    ) {
      let result = new Results();
      let error = ResultsCodes.VALIDATION_ERROR;
      error.message = "Ít nhất một tham số đầu vào phải có giá trị";
      result.setError(ResultsCodes.VALIDATION_ERROR);
      return cb(null, result);
    }

    const makeRequest = async () => {
      let categoryIds = [];
      if (parentCategoryId && parentCategoryId > 0) {
        let parentCategory = await Products.app.models.Category.findOne({
          fields: ["arrChildId"],
          where: {
            id: parentCategoryId
          }
        });

        if (parentCategory.arrChildId) {
          categoryIds = parentCategory.arrChildId.split(",");
        }
      }

      // let sellerId = 0;
      // if (sellerUuid && /\S/.test(sellerUuid)) {

      //     let seller = await Products.app.models.Sellers.findOne({
      //         fields: ['id'],
      //         where: {
      //             "uuid": sellerUuid
      //         }
      //     });

      //     if (seller && seller.id > 0) {
      //         sellerId = seller.id;
      //     }
      // }

      let aggreations = {};
      let resp = await s.SearchAggreation(
        "products",
        "_doc",
        platformCode,
        keyword,
        sellerUuid,
        categoryIds
      );

      let categoryArr = [];
      aggreations.groupByCategory = categoryArr;

      let brandArr = [];
      aggreations.groupByBrand = brandArr;

      let sellerArr = [];
      aggreations.groupBySeller = sellerArr;

      let categoriyBuckets = resp.aggregations.group_by_category.buckets;
      let categories = [];
      if (categoriyBuckets && categoriyBuckets.length > 0) {
        let categoryIdsResult = categoriyBuckets.map(x => x.key);
        categories = await Products.app.models.Category.find({
          fields: ["id", "title"],
          where: { id: { inq: categoryIdsResult } },
          order: "title asc"
        });

        // categories.forEach((item, i) => {
        //     categoryArr.push(Object.assign({}, item, categoriyBuckets[i]));
        // });
        categoryArr = categories.map(x =>
          Object.assign(
            { id: x.id },
            { title: x.title },
            { docCount: categoriyBuckets.find(y => y.key == x.id).doc_count }
          )
        );
        aggreations.groupByCategory = categoryArr;
      }

      let brandBuckets = resp.aggregations.group_by_brand.buckets;
      let brands = [];
      if (brandBuckets && brandBuckets.length > 0) {
        let brandIds = brandBuckets.map(x => x.key);

        brands = await Products.app.models.Brands.find({
          fields: ["id", "brandName"],
          where: { id: { inq: brandIds } },
          order: "brandName asc"
        });

        // brands.forEach((item, i) => {
        //     brandArr.push(Object.assign({}, item, brandBuckets[i]));
        // });
        brandArr = brands.map(x =>
          Object.assign(
            { id: x.id },
            { brandName: x.brandName },
            { docCount: brandBuckets.find(y => y.key == x.id).doc_count }
          )
        );
        aggreations.groupByBrand = brandArr;
      }

      let sellerBuckets = resp.aggregations.group_by_seller.buckets;
      let sellers = [];
      if (sellerBuckets && sellerBuckets.length > 0) {
        let sellerIds = sellerBuckets.map(x => x.key);

        sellers = await Products.app.models.Sellers.find({
          fields: ["id", "shopName"],
          where: { id: { inq: sellerIds } },
          order: "shopName asc"
        });

        // sellers.forEach((item, i) => {
        //     sellerArr.push(Object.assign({}, item, sellerBuckets[i]));
        // });

        sellerArr = sellers.map(x =>
          Object.assign(
            { id: x.id },
            { shopName: x.shopName },
            { docCount: sellerBuckets.find(y => y.key == x.id).doc_count }
          )
        );
        aggreations.groupBySeller = sellerArr;
      }

      result.setSuccess(aggreations);
      return cb(null, result);
    };

    makeRequest().catch(function(e) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  Products.SearchPaging = function(
    platformCode,
    locationLevel1,
    locationLevel2,
    locationLevel3,
    locationLevel4,
    keyword,
    sellerUuid,
    brandIds,
    parentCategoryId,
    categoryIds,
    sellerIds,
    fromPrice,
    toPrice,
    pageIndex,
    pageSize,
    pageOrder,
    cb
  ) {
    let s = new SEngine();
    let result = new Results();

    const makeRequest = async () => {
      let sql = `SELECT
            GROUP_CONCAT(CONCAT(sl.id,sl.status)
            SEPARATOR ';') s_arr,
            GROUP_CONCAT(CONCAT(ps.id,':', ps.seller_id, ':', ps.base_price, ':', psp.id)
                SEPARATOR ';') ps_arr,
            GROUP_CONCAT(CONCAT(pp.product_seller_id ,':' , pp.id, ':' , pp.price, ':' , pp.from, ':' , pp.to)
                SEPARATOR ';') pp_arr,
            GROUP_CONCAT(CONCAT(pst.id , ':' , pst.product_seller_id , ':' , pst.available, ':' , pst.seller_store_id, ':', CASE
                    WHEN ps.service_area_level = 1 and lo1.code = ? THEN '1'
                    WHEN ps.service_area_level = 2 and lo2.code = ? THEN '1'
                    WHEN ps.service_area_level = 3 and lo3.code = ? THEN '1'
                    WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
                        else '0'
                    END)
                SEPARATOR ';') pst_arr,
            pd.id,
            pd.category_id categoryId,
            pd.weight weight,
            pd.unit unit,
            pd.brand_id brandId,
            pd.product_name productName,
            pd.product_image productImage,      
            
            IFNULL(CAST(IFNULL(MIN(pp.price), MIN(ps.base_price)) AS UNSIGNED), 0) minPrice,
            IFNULL(CAST(IFNULL(MAX(pp.price), MAX(ps.base_price)) AS UNSIGNED), 0) maxPrice,
            IFNULL(AVG(rating),0) AS ratingAverage, COUNT(DISTINCT pr.id) AS ratingCount
           
            FROM
                products pd
                    LEFT JOIN
                product_seller ps ON ps.product_id = pd.id
                     AND ps.status =1
                    LEFT JOIN
                product_seller_platform psp ON psp.product_seller_id = ps.id
                    AND psp.status = 1
                    AND psp.platform_code = ?
                    AND psp.display_on_shop = 1
                    LEFT  JOIN
                sellers sl ON ps.seller_id = sl.id
                    AND sl.status IN (1,2)
                    LEFT JOIN
                product_price pp ON pp.product_seller_id = ps.id
                    AND pp.from <= ?
                    AND pp.to >= ?
                    LEFT JOIN
                product_store pst ON pst.product_seller_id = ps.id
                    AND pst.available > 0
                    LEFT JOIN
                seller_store ss ON pst.seller_store_id = ss.id
                    LEFT JOIN
                location lo4 ON lo4.code = ss.location_code
                    LEFT JOIN
                location lo3 ON lo3.code = lo4.parent_code
                    LEFT JOIN
                location lo2 ON lo2.code = lo3.parent_code
                    LEFT JOIN
                location lo1 ON lo1.code = lo2.parent_code
                    LEFT JOIN
                product_rating pr ON pr.product_id = pd.id
            WHERE (ps.id IS NOT NULL && psp.id IS NOT NULL || ps.id IS NULL || psp.id IS NULL )  `;

      // let isNeedAndInWhereClause = false;
      let currentTime = parseInt(new Date().getTime() / 1000.0);
      let params = [
        locationLevel1,
        locationLevel2,
        locationLevel3,
        locationLevel4,
        platformCode,
        currentTime,
        currentTime
      ];

      let sellerId = 0;
      if (sellerUuid && /\S/.test(sellerUuid)) {
        let seller = await Products.app.models.Sellers.findOne({
          fields: ["id"],
          where: {
            uuid: sellerUuid
          }
        });

        if (seller && seller.id > 0) {
          sellerId = seller.id;

          sql = sql + " AND sl.uuid = ? ";
          // isNeedAndInWhereClause = true
          params.push(sellerUuid);
        } else {
          let result = new Results();
          result.setError(ResultsCodes.DATA_NOT_FOUND);
          return cb(null, result);
        }
      }

      if (!categoryIds) {
        categoryIds = [];
        let categoryIdArr = [];
        if (parentCategoryId && parentCategoryId > 0) {
          let parentCategory = await Products.app.models.Category.findOne({
            fields: ["arrChildId"],
            where: {
              id: parentCategoryId
            }
          });

          if (parentCategory.arrChildId) {
            categoryIdArr = parentCategory.arrChildId.split(",");
          }

          categoryIds = categoryIdArr.concat(categoryIds);
        }
      }

      let resp = await s.SearchPaging(
        "products",
        "_doc",
        platformCode,
        keyword,
        sellerId,
        brandIds,
        categoryIds,
        sellerIds,
        fromPrice,
        toPrice,
        pageIndex,
        pageSize,
        pageOrder
      );
      let idList = resp.hits.hits.map(x => x._source.id).join();
      let total = resp.hits.total;

      if (idList && idList.length > 0) {
        // let s = new SEngine();
        // let hits =  await  s.Search('smartlife-eco','Products',keySearch);
        // idList = hits.map(x => x._source.id).join();

        // if (isNeedAndInWhereClause) {
        //     sql = sql + " AND";
        // }

        sql =
          sql +
          " AND FIND_IN_SET(pd.id, ? ) > 0  GROUP BY pd.id ORDER BY FIND_IN_SET(pd.id, ?)";
        params.push(idList);
        params.push(idList);

        let connector = Products.dataSource.connector;
        connector.query(sql, params, function(err, resultObjects) {
          console.log(err);
          if (err) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
          } else {
            // let result = new Results();
            // result.setSuccess({ totalRecords: total, pageData: resultObjects });
            // return cb(null, result);

            let products = [];

            if (resultObjects.length > 0) {
              resultObjects.map(productRaw => {
                let product = {
                  saleable: 0,
                  id: productRaw.id,
                  categoryId: productRaw.categoryId,
                  brandId: productRaw.brandId,
                  productName: productRaw.productName,
                  productImage: productRaw.productImage,
                  minPrice: productRaw.minPrice,
                  maxPrice: productRaw.maxPrice,
                  ratingAverage: productRaw.ratingAverage,
                  ratingCount: productRaw.ratingCount
                };

                // let saleables = productRaw.saleable_arr != null ? productRaw.saleable_arr.split(";").map(x => {
                //     let item = x.split(":");
                //     if (item != null && item.length >= 2) {
                //         return {productSellerId: parseNumber(item[0]), saleable: parseNumber(item[1])}
                //     }
                // }) : null;

                let productPrices =
                  productRaw.pp_arr != null
                    ? productRaw.pp_arr.split(";").map(x => {
                        let item = x.split(":");
                        if (item != null && item.length >= 5) {
                          return {
                            productSellerId: parseNumber(item[0]),
                            id: parseNumber(item[1]),
                            price: parseNumber(item[2]),
                            from: parseNumber(item[3]),
                            to: parseNumber(item[4])
                          };
                        }
                      })
                    : null;

                let productStores =
                  productRaw.pst_arr != null
                    ? productRaw.pst_arr.split(";").map(x => {
                        let item = x.split(":");
                        if (item != null && item.length >= 5) {
                          return {
                            id: parseNumber(item[0]),
                            productSellerId: parseNumber(item[1]),
                            available: parseNumber(item[2]),
                            sellerStoreId: parseNumber(item[3]),
                            saleable: parseNumber(item[4])
                          };
                        }
                      })
                    : null;

                let productSellers = [];

                if (productRaw.ps_arr != null) {
                  productRaw.ps_arr.split(";").map(item => {
                    let productSellerArr = item.split(":");

                    if (productSellerArr.length == 4) {
                      // productSeller has productSellerPlatform
                      let productSeller = {
                        id: parseNumber(productSellerArr[0]),
                        sellerId: parseNumber(productSellerArr[1]),
                        basePrice: parseNumber(productSellerArr[2])
                      };

                      if (productPrices != null && productPrices.length > 0) {
                        let productPrice = productPrices.find(
                          x => x.productSellerId == productSeller.id
                        );
                        productSeller.productPrice = productPrice;
                      }

                      if (productStores != null && productStores.length > 0) {
                        let productStoresByProductSellerId = productStores.filter(
                          x =>
                            x.productSellerId == productSeller.id &&
                            x.saleable == 1
                        );
                        if (
                          productStoresByProductSellerId != null &&
                          productStoresByProductSellerId.length > 0
                        ) {
                          productStoresByProductSellerId = productStoresByProductSellerId.sort(
                            compare
                          );
                          productSeller.productStore =
                            productStoresByProductSellerId[0];
                        }
                      }

                      if (sellerId > 0) {
                        if (productSeller.sellerId == sellerId) {
                          addProductSeller(
                            product,
                            productSellers,
                            productSeller
                          );
                        }
                      } else {
                        addProductSeller(
                          product,
                          productSellers,
                          productSeller
                        );
                      }
                    }
                  });
                }

                product.productSellers = productSellers;
                products.push(product);
              });
            }

            let result = new Results();
            result.setSuccess({ totalRecords: total, pageData: products });
            return cb(null, result);
          }
        });
      } else {
        let result = new Results();
        result.setSuccess({ totalRecords: 0, pageData: [] });
        return cb(null, result);
      }
    };

    makeRequest().catch(function(e) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  /**
   * Search products by keywords
   * Created by Hung Doan
   * Date: 24/08/2018
   * @param {*} keyword
   * @param {*} cb
   */
  Products.SearchPagingold = function(
    platformCode,
    locationLevel1,
    locationLevel2,
    locationLevel3,
    locationLevel4,
    keyword,
    sellerUuid,
    brandIds,
    parentCategoryId,
    categoryIds,
    sellerIds,
    fromPrice,
    toPrice,
    pageIndex,
    pageSize,
    pageOrder,
    cb
  ) {
    let s = new SEngine();
    let result = new Results();

    const makeRequest = async () => {
      let sql = `SELECT 
            GROUP_CONCAT(CONCAT(ps.id,':', ps.seller_id, ':', ps.base_price, ':', psp.id)
                SEPARATOR ';') ps_arr,
            GROUP_CONCAT(CONCAT(pp.product_seller_id ,':' , pp.id, ':' , pp.price, ':' , pp.from, ':' , pp.to)
                SEPARATOR ';') pp_arr,
            GROUP_CONCAT(CONCAT(pst.id , ':' , pst.product_seller_id , ':' , pst.available, ':' , pst.seller_store_id, ':', CASE
                    WHEN ps.service_area_level = 1 and lo1.code = ? THEN '1'
                    WHEN ps.service_area_level = 2 and lo2.code = ? THEN '1'
                    WHEN ps.service_area_level = 3 and lo3.code = ? THEN '1'
                    WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
                        else '0'
                    END)
                SEPARATOR ';') pst_arr,
            pd.id,
            pd.category_id categoryId,
            pd.weight weight,
            pd.unit unit,
            pd.brand_id brandId,
            pd.product_name productName,
            pd.product_image productImage,
            IFNULL(CAST(IFNULL(MIN(pp.price), MIN(ps.base_price)) AS UNSIGNED), 0) minPrice,
            IFNULL(CAST(IFNULL(MAX(pp.price), MAX(ps.base_price)) AS UNSIGNED), 0) maxPrice,
            IFNULL(AVG(rating),0) AS ratingAverage, COUNT(DISTINCT pr.id) AS ratingCount
            FROM
                products pd
                   INNER JOIN
                product_seller ps ON ps.product_id = pd.id
                    AND ps.status IN (1,2)
                    LEFT JOIN
                product_seller_platform psp ON psp.product_seller_id = ps.id
                    AND psp.status IN (1,2)
                    AND psp.platform_code = ?
                    AND psp.display_on_shop = 1
                    LEFT  JOIN
                sellers sl ON ps.seller_id = sl.id  
                    AND    
                sl.status IN (2)     
                    LEFT JOIN
                product_price pp ON pp.product_seller_id = ps.id
                    AND pp.from <= ?
                    AND pp.to >= ?
                    LEFT JOIN
                product_store pst ON pst.product_seller_id = ps.id
                    AND pst.available > 0
                    LEFT JOIN
                seller_store ss ON pst.seller_store_id = ss.id
                    LEFT JOIN
                location lo4 ON lo4.code = ss.location_code
                    LEFT JOIN
                location lo3 ON lo3.code = lo4.parent_code
                    LEFT JOIN
                location lo2 ON lo2.code = lo3.parent_code
                    LEFT JOIN
                location lo1 ON lo1.code = lo2.parent_code
                    LEFT JOIN
                product_rating pr ON pr.product_id = pd.id
            WHERE (ps.id IS NOT NULL && psp.id IS NOT NULL || ps.id IS NULL || psp.id IS NULL ) `;
            // thienvd 05-12-2019 thêm dòng 1202 ps.status =1 , dòng 1182 bỏ AND sl.status IN (1,2) thay  AND sl.status =2 vào dòng 1202
        
            //  
      // let isNeedAndInWhereClause = false;
      let currentTime = parseInt(new Date().getTime() / 1000.0);
      let params = [
        locationLevel1,
        locationLevel2,
        locationLevel3,
        locationLevel4,
        platformCode,
        currentTime,
        currentTime
      ];

      let sellerId = 0;
      if (sellerUuid && /\S/.test(sellerUuid)) {
        let seller = await Products.app.models.Sellers.findOne({
          fields: ["id"],
          where: {
            uuid: sellerUuid
            //,
            //thienvd 05-12-2019
            // Chỉ lấy sellers đã được duyệt status =2
           // status:2
          }
        });

        if (seller && seller.id > 0) {
          sellerId = seller.id;

          sql = sql + " AND sl.uuid = ? ";
          // isNeedAndInWhereClause = true
          params.push(sellerUuid);
        } else {
          let result = new Results();
          result.setError(ResultsCodes.DATA_NOT_FOUND);
          return cb(null, result);
        }
      }

      if (!categoryIds) {
        categoryIds = [];
        let categoryIdArr = [];
        if (parentCategoryId && parentCategoryId > 0) {
          let parentCategory = await Products.app.models.Category.findOne({
            fields: ["arrChildId"],
            where: {
              id: parentCategoryId
            }
          });

          if (parentCategory.arrChildId) {
            categoryIdArr = parentCategory.arrChildId.split(",");
          }

          categoryIds = categoryIdArr.concat(categoryIds);
        }
      }

      let resp = await s.SearchPaging(
        "products",
        "_doc",
        platformCode,
        keyword,
        sellerId,
        brandIds,
        categoryIds,
        sellerIds,
        fromPrice,
        toPrice,
        pageIndex,
        pageSize,
        pageOrder
      );
      let idList = resp.hits.hits.map(x => x._source.id).join();
      let total = resp.hits.total;
      console.log(idList)
      if (idList && idList.length > 0) {
        // let s = new SEngine();
        // let hits =  await  s.Search('smartlife-eco','Products',keySearch);
        // idList = hits.map(x => x._source.id).join();

        // if (isNeedAndInWhereClause) {
        //     sql = sql + " AND";
        // }

        sql =
          sql +
          " AND FIND_IN_SET(pd.id, ? ) > 0  GROUP BY pd.id ORDER BY FIND_IN_SET(pd.id, ?)";
        params.push(idList);
        params.push(idList);

        let connector = Products.dataSource.connector;
        //console.log(sql)
        connector.query(sql, params, function(err, resultObjects) {
          if (err) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
          } else {
            // let result = new Results();
            // result.setSuccess({ totalRecords: total, pageData: resultObjects });
            // return cb(null, result);

            let products = [];
            //console.log(resultObjects);
            if (resultObjects.length > 0) {
              resultObjects.map(productRaw => {
                let product = {
                  saleable: 0,
                  id: productRaw.id,
                  categoryId: productRaw.categoryId,
                  brandId: productRaw.brandId,
                  productName: productRaw.productName,
                  productImage: productRaw.productImage,
                  minPrice: productRaw.minPrice,
                  maxPrice: productRaw.maxPrice,
                  ratingAverage: productRaw.ratingAverage,
                  ratingCount: productRaw.ratingCount
                };

                // let saleables = productRaw.saleable_arr != null ? productRaw.saleable_arr.split(";").map(x => {
                //     let item = x.split(":");
                //     if (item != null && item.length >= 2) {
                //         return {productSellerId: parseNumber(item[0]), saleable: parseNumber(item[1])}
                //     }
                // }) : null;

                let productPrices =
                  productRaw.pp_arr != null
                    ? productRaw.pp_arr.split(";").map(x => {
                        let item = x.split(":");
                        if (item != null && item.length >= 5) {
                          return {
                            productSellerId: parseNumber(item[0]),
                            id: parseNumber(item[1]),
                            price: parseNumber(item[2]),
                            from: parseNumber(item[3]),
                            to: parseNumber(item[4])
                          };
                        }
                      })
                    : null;

                let productStores =
                  productRaw.pst_arr != null
                    ? productRaw.pst_arr.split(";").map(x => {
                        let item = x.split(":");
                        if (item != null && item.length >= 5) {
                          return {
                            id: parseNumber(item[0]),
                            productSellerId: parseNumber(item[1]),
                            available: parseNumber(item[2]),
                            sellerStoreId: parseNumber(item[3]),
                            saleable: parseNumber(item[4])
                          };
                        }
                      })
                    : null;

                let productSellers = [];

                if (productRaw.ps_arr != null) {
                  productRaw.ps_arr.split(";").map(item => {
                    let productSellerArr = item.split(":");

                    if (productSellerArr.length == 4) {
                      // productSeller has productSellerPlatform
                      let productSeller = {
                        id: parseNumber(productSellerArr[0]),
                        sellerId: parseNumber(productSellerArr[1]),
                        basePrice: parseNumber(productSellerArr[2])
                      };

                      if (productPrices != null && productPrices.length > 0) {
                        let productPrice = productPrices.find(
                          x => x.productSellerId == productSeller.id
                        );
                        productSeller.productPrice = productPrice;
                      }

                      if (productStores != null && productStores.length > 0) {
                        let productStoresByProductSellerId = productStores.filter(
                          x =>
                            x.productSellerId == productSeller.id &&
                            x.saleable == 1
                        );
                        if (
                          productStoresByProductSellerId != null &&
                          productStoresByProductSellerId.length > 0
                        ) {
                          productStoresByProductSellerId = productStoresByProductSellerId.sort(
                            compare
                          );
                          productSeller.productStore =
                            productStoresByProductSellerId[0];
                        }
                      }

                      if (sellerId > 0) {
                        if (productSeller.sellerId == sellerId) {
                          addProductSeller(
                            product,
                            productSellers,
                            productSeller
                          );
                        }
                      } else {
                        addProductSeller(
                          product,
                          productSellers,
                          productSeller
                        );
                      }
                    }
                  });
                }

                product.productSellers = productSellers;
                products.push(product);
              });
            }

            let result = new Results();
            result.setSuccess({ totalRecords: total, pageData: products });
            return cb(null, result);
          }
        });
      } else {
        let result = new Results();
        result.setSuccess({ totalRecords: 0, pageData: [] });
        return cb(null, result);
      }
    };

    makeRequest().catch(function(e) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  };

  function addProductSeller(product, productSellers, productSeller) {
    if (productSeller.productStore) {
      product.saleable = 1;
    }

    productSellers.push(productSeller);
  }

  /**
   * Create Search Index for products
   * Created by Thiep Wong
   * Date: 13/05/2018
   * @param {*} cb
   */
  Products.CreateSearchIndex = function(cb) {
    const makeRequest = async () => {
      let result = new Results();
      let _sEngine = new SEngine();

      let products = await this.GetDocuments(0, null);

      if (products) {
        // products.forEach(function(item,index){
        await _sEngine.Index("products", products);
        // });
      }

      result.setSuccess("Create index completed!");
      return cb(null, result);
    };

    makeRequest().catch(function(e) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
    // try {

    //     let product =  Products.find(function(err, instance){
    //         if(instance)
    //         {
    //             instance.forEach(function(item,index){
    //                  _sEngine.Index('smartlife-eco',item);
    //             });
    //         }

    //         result.setSuccess('Create index completed!')
    //         cb(null,result);
    //     })
    // } catch (e) {
    //     console.log(e);
    // }
  };

  Products.UpdateDocumentByProductSellerId = async function(
    productSellerId,
    models
  ) {
    let productSeller = await Products.app.models.ProductSeller.findById(
      productSellerId
    );
    let products = await this.GetDocuments(productSeller.productId, models);
    let _sEngine = new SEngine();

    // only return one record
    if (products && products.length > 0) {
      await _sEngine.UpdateDocument("products", products[0]);
    }
  };

  Products.UpdateDocumentBySellerId = async function(sellerId, models) {
    let productIds = await Products.app.models.ProductSeller.find({
      fields: ["productId"],
      where: { sellerId: sellerId }
    });

    let products = await this.GetDocuments(productIds, models);
    let _sEngine = new SEngine();
    if (products && products.length > 0) {
      for (var index in products) {
        await _sEngine.UpdateDocument("products", products[index]);
      }
    }
  };

  Products.UpdateDocumentByProductId = async function(productId, models) {
    let products = await this.GetDocuments(productId, models);
    let _sEngine = new SEngine();

    // only return one record
    if (products && products.length > 0) {
      await _sEngine.UpdateDocument("products", products[0]);
    }
  };

  Products.UpdateDocumentByProduct = async function(product) {
    let _sEngine = new SEngine();

    let _product = {
      id: product.id,
      productName: product.productName,
      status: product.status,
      brandId: product.brandId,
      categoryId: product.categoryId
    };

    await _sEngine.UpdateDocument("products", _product);
  };

  Products.GetDocuments = async function(id, models) {
    let products = {};
    let whereClause = id > 0 ? { id: id } : { id: { gt: 0 } };

    if (models) {
      const { Products } = models;
      products = await Products.find({
        fields: ["id", "productName", "status", "brandId", "categoryId"],
        include: [
          {
            relation: "productSellers",
            scope: {
              fields: ["id", "basePrice", "sellerId"],
              include: [
                {
                  relation: "seller",
                  scope: {
                    fields: ["id", "uuid", "shopName", "status"],
                    include: [
                      {
                        relation: "sellerPlatform",
                        scope: {
                          fields: ["id", "platformCode", "status"]
                        }
                      }
                    ]
                  }
                },
                {
                  relation: "productSellerPlatform",
                  scope: {
                    fields: ["id", "platformCode", "displayOnShop", "status"]
                  }
                }
              ]
            }
          }
        ],
        where: whereClause
      });
    } else {
      products = await Products.find({
        fields: ["id", "productName", "status", "brandId", "categoryId"],
        include: [
          {
            relation: "productSellers",
            scope: {
              fields: ["id", "basePrice", "sellerId"],
              include: [
                {
                  relation: "seller",
                  scope: {
                    fields: ["id", "uuid", "shopName", "status"],
                    include: [
                      {
                        relation: "sellerPlatform",
                        scope: {
                          fields: ["id", "platformCode", "status"]
                        }
                      }
                    ]
                  }
                },
                {
                  relation: "productSellerPlatform",
                  scope: {
                    fields: ["id", "platformCode", "displayOnShop", "status"]
                  }
                }
              ]
            }
          }
        ],
        where: whereClause
      });
    }

    return products;
  };

  /**
   * GetListForYou
   * @param {*} platformCode platform code
   * @param {*} locationLevel1 country lelve
   * @param {*} locationLevel2 city level
   * @param {*} locationLevel3 district level
   * @param {*} locationLevel4 ward level
   * @param {*} pageSize pageSize from frontend (default is 10)
   * @param {*} pageIndex pageIndex from frontend (default is 0)
   * @param {*} cb Callback function for result returns
   */
  Products.GetListForYou = function(
    platformCode,
    locationLevel1,
    locationLevel2,
    locationLevel3,
    locationLevel4,
    pageSize,
    pageIndex,
    cb
  ) {
    let sql = `SELECT 
        FIND_IN_SET ('1',  GROUP_CONCAT(CASE
            WHEN ps.service_area_level = 1 and  lo1.code = ? THEN '1'
            WHEN ps.service_area_level = 2 and  lo2.code = ? THEN '1'
            WHEN ps.service_area_level = 3 and  lo3.code = ? THEN '1'
            WHEN ps.service_area_level = 4 and lo4.code = ? THEN '1'
            else '0'
        END 
        SEPARATOR ',')) > 0 saleable,
        pd.id,
        pd.category_id categoryId,
        pd.weight weight,
        pd.unit unit,
        pd.brand_id brandId,
        pd.product_name productName,
        pd.product_image productImage,
        IFNULL(MIN(pp.price), MIN(ps.base_price)) minPrice,
        IFNULL(MAX(pp.price), MAX(ps.base_price)) maxPrice,
        IFNULL(AVG(rating),0) AS ratingAverage, COUNT(DISTINCT pr.id) AS ratingCount
        FROM
            products pd
                JOIN
            product_seller ps ON ps.product_id = pd.id
                JOIN
            product_seller_platform psp ON psp.product_seller_id = ps.id
                JOIN
            sellers sl ON ps.seller_id = sl.id
            LEFT JOIN
            product_price pp ON pp.product_seller_id = ps.id
                AND pp.from <= ?
                AND pp.to >= ?
            JOIN
            product_store pst ON pst.product_seller_id = ps.id
                JOIN
            seller_store ss ON pst.seller_store_id = ss.id
                JOIN
            location lo4 ON lo4.code = ss.location_code
                JOIN
            location lo3 ON lo3.code = lo4.parent_code
                JOIN
            location lo2 ON lo2.code = lo3.parent_code
                JOIN
            location lo1 ON lo1.code = lo2.parent_code
                LEFT JOIN
            product_rating pr ON pr.product_id = pd.id
        WHERE
        ps.status = '1'
        AND pd.status = '2'
        AND psp.platform_code = ?
        AND psp.display_on_shop = '1'`;

    let currentTime = parseInt(new Date().getTime() / 1000.0);
    let params = [
      locationLevel1,
      locationLevel2,
      locationLevel3,
      locationLevel4,
      currentTime,
      currentTime,
      platformCode
    ];

    sql = sql + ` GROUP BY pd.id `;
    sql = sql + ` ORDER BY RAND() LIMIT ? OFFSET ?`;

    if (pageSize) {
      if (pageSize <= 0) {
        pageSize = 10;
      }
    } else {
      pageSize = 10;
    }

    if (pageIndex) {
      if (pageIndex < 0) {
        pageIndex = 0;
      }
    } else {
      pageIndex = 0;
    }

    params.push(pageSize, pageIndex);

    let connector = Products.dataSource.connector;
    connector.query(sql, params, function(err, resultObjects) {
      if (err) {
        let result = new Results();
        result.setError(ResultsCodes.DATA_NOT_FOUND);
        return cb(null, result);
      } else {
        let result = new Results();
        result.setSuccess(resultObjects);
        return cb(null, result);
      }
    });
  };

  /**
   *
   */
  Products.RegisterNewProduct = (ctx, cb) => {
    let brandId = ctx.body.brandId || 0;
    let category = ctx.body.category || 0;
    let productName = ctx.body.productName || null;
    let productImages = ctx.body.productImages || null;
    let shortDescription = ctx.body.shortDescription || null;
    let description = ctx.body.description || null;
    let weight = ctx.body.weight || 0;
    let unit = ctx.body.unit || null;
    let dimensions = ctx.body.dimension || null;
    let certificateImages = ctx.body.certificateImages || null;
    let youtubeVideoLink = ctx.body.youtubeVideoLink || null;
    app.dataSources.mysqld
      .transaction(async models => {
        const { Passport, Products, Sellers } = models;
        let result = new Results();
        // let Passport = Products.app.models.Passport;
        let token = await Passport.GetSellerToken(ctx);
        if (!token) {
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }
        if (weight <= 0) {
          result.setError(ResultsCodes.PRO_WEIGHT_ERR);
          return cb(null, result);
        }

        // let Seller = Products.app.models.Sellers;
        let _seller = await Sellers.findById(token.userId);

        let _product = await Products.create({
          productName: productName,
          categoryId: category,
          brandId: brandId,
          productImage: productImages,
          shortDescription: shortDescription,
          description: description,
          unit: unit,
          weight: weight > 0 ? weight : null,
          dimension: dimensions,
          status: 1,
          createTime: new Date().getTime() / 1000,
          createBy: _seller.id,
          createRole: 2, // 2 as seller role; 1 as admin of smartlife; 3 as original seeker
          certificateImages: certificateImages,
          youtubeVideoLink: youtubeVideoLink
        });

        // update ES document
        await Products.UpdateDocumentByProduct(_product);

        result.setSuccess(true);
        return cb ? cb(null, result) : result;
      })
      .catch(function(e) {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });
  };

  // 07-12-2018
  // Thien VD
  // Set status =1: Chờ duyệt khi seller bấm cập nhật
  Products.UpdateRegisterProduct = (id, ctx, cb) => {
    let brandId = ctx.body.brandId || 0;
    let category = ctx.body.category || 0;
    let productName = ctx.body.productName || null;
    let productImages = ctx.body.productImages || null;
    let shortDescription = ctx.body.shortDescription || null;
    let description = ctx.body.description || null;
    let weight = ctx.body.weight || 0;
    let unit = ctx.body.unit || null;
    let dimensions = ctx.body.dimension || null;
    let certificateImages = ctx.body.certificateImages || null;
    let youtubeVideoLink = ctx.body.youtubeVideoLink || null;

    app.dataSources.mysqld
      .transaction(async models => {
        const { Passport, Products, Seller } = models;

        // let Passport = Products.app.models.Passport;
        let token = await Passport.GetSellerToken(ctx);
        if (!token.userId) {
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }

        if (weight <= 0) {
          result.setError(ResultsCodes.PRO_WEIGHT_ERR);
          return cb(null, result);
        }

        // let Seller = Products.app.models.Sellers;

        let _product = await Products.findById(id);

        if (!_product) {
          result.setError(ResultsCodes.ITEM_NOT_FOUND);
          return cb(null, result);
        }
        if (_product.status == 0) {
          await _product.updateAttributes({
            brandId: brandId,
            categoryId: category,
            productName: productName,
            productImage: productImages,
            shortDescription: shortDescription,
            description: description,
            weight: weight > 0 ? weight : null,
            unit: unit,
            dimension: dimensions,
            modifyTime: new Date().getTime() / 1000,
            modifyBy: token.userId,
            //status: 0
            status: 1,
            certificateImages: certificateImages,
            youtubeVideoLink: youtubeVideoLink
          });
        }

        await _product.updateAttributes({
          brandId: brandId,
          categoryId: category,
          productName: productName,
          productImage: productImages,
          shortDescription: shortDescription,
          description: description,
          weight: weight > 0 ? weight : null,
          unit: unit,
          dimension: dimensions,
          modifyTime: new Date().getTime() / 1000,
          modifyBy: token.userId,
          certificateImages: certificateImages,
          youtubeVideoLink: youtubeVideoLink
          //status: 0
          //status:1
        });

        // update ES document
        await Products.UpdateDocumentByProductId(_product.id, models);

        let result = new Results();
        result.setSuccess(true);
        return cb ? cb(null, result) : result;
      })
      .catch(function(e) {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });
  };

  Products.SetStatus = (id, ctx, cb) => {
    let result = new Results();
    app.dataSources.mysqld
      .transaction(async models => {
        const { Passport, Products } = models;
        let token = await Passport.GetSellerToken(ctx);
        if (!token.userId) {
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }

        let _product = await Products.findById(id);

        if (!_product) {
          result.setError(ResultsCodes.ITEM_NOT_FOUND);
          return cb(null, result);
        }

        await _product.updateAttributes({
          modifyTime: new Date().getTime() / 1000,
          modifyBy: token.userId,
          status: -1
        });

        // update ES document
        await Products.UpdateDocumentByProductId(_product.id, models);
      })
      .catch(reject => {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });
  };

  Products.SetStatusProduct = (id, ctx, cb) => {
    let status = ctx.body.status || 0;
    let result = new Results();
    app.dataSources.mysqld
      .transaction(async models => {
        const { Passport, Products } = models;
        let token = await Passport.GetSellerToken(ctx);
        if (!token.userId) {
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }

        let _product = await Products.findById(id);

        if (!_product) {
          result.setError(ResultsCodes.ITEM_NOT_FOUND);
          return cb(null, result);
        }

        await _product.updateAttributes({
          modifyTime: new Date().getTime() / 1000,
          modifyBy: token.userId,
          status: status
        });

        // update ES document
        await Products.UpdateDocumentByProductId(_product.id, models);
      })
      .catch(reject => {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });
  };

  Products.GetListBySeller1 = (ctx, cb) => {
    let pageIndex = ctx.query.pageIndex || 0;
    let pageSize = ctx.query.pageSize || 10;
    let pageOrder = ctx.query.pageOrder || null;
    let pageFilter = ctx.query.pageFilter || null;
    let result = new Results();

    const makeRequest = async () => {
      let Passport = Products.app.models.Passport;
      let token = await Passport.GetSellerToken(ctx);
      if (!token.userId) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }
      if (!pageOrder || pageOrder == "undefined" || pageOrder == "null")
        pageOrder = ["id desc"];
      let filter = new Filter().Where(
        [{ status: { gte: 0 }, createBy: token.userId, createRole: 2 }],
        pageFilter
      );

      let totalRecords = await Products.count(filter);
      Products.find(
        {
          fields: [
            "id",
            "productName",
            "categoryId",
            "productImage",
            "brandId",
            "shortDescription",
            "description",
            "unit",
            "weight",
            "dimension",
            "status",
            "createTime",
            "createBy",
            "certificateImages",
            "youtubeVideoLink"
          ],
          include: [
            {
              relation: "brand",
              scope: {
                fields: ["id", "brandName", "brandGCP", "logo"]
              }
            },
            {
              relation: "category",
              scope: {
                fields: ["id", "title", "icon", "level"]
              }
            }
          ],
          where: filter,
          limit: pageSize,
          skip: pageIndex * pageSize,
          order: pageOrder
        },
        function(err, instance) {
          if (err) {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
          }

          result.setSuccess({
            totalRecords: totalRecords,
            pageSize: pageSize,
            pageIndex: pageIndex,
            pageData: instance
          });
          cb(null, result);
        }
      );
    };
    makeRequest().catch(err => {
      ResultsCodes.Detail = err;
      result.setError(ResultsCodes.DATA_NOT_FOUND);
      return cbn(null, result);
    });
  };

  // ThienVD
  // Code phân trang GetListBySeller
  Products.GetListBySeller = (ctx, cb) => {
    let pageIndex = ctx.query.pageIndex || 0;
    let pageSize = ctx.query.pageSize || 10;
    let pageOrder = ctx.query.pageOrder || null;
    let pageFilter = ctx.query.pageFilter || null; 
    let productName = ctx.query.productName;

    app.dataSources.mysqld
      .transaction(async models => {
        const { ProductSeller, Passport, Products } = models;
        let token = await Passport.GetSellerToken(ctx);
        if (!token) {
          let result = new Results();
          result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
          return cb(null, result);
        }
      

        if (pageSize) {
            if (pageSize <= 0) {
                pageSize = 10;
            }
        } else {
            pageSize = 10;
        }

        if (pageIndex) {
            if (pageIndex < 1) {
                pageIndex = 1;
            }
        } else {
            pageIndex = 1;
        }

        let offset = (pageIndex - 1) * pageSize;
        //let limit = pageSize;           
    
        let sql = `SELECT  p.id, p.product_name AS productName,p.product_image AS productImage, p.brand_id AS brandId, p.short_description AS shortDescription, p.description AS description, p.unit, p.weight,p.dimension,p.status,p.create_time AS createTime, p.create_by AS createBy, p.certificate_images AS certificateImages, p.youtube_video_link AS youtubeVideoLink,
                          c.id AS cateId, c.title, c.icon, c.level, b.brand_name AS brandName                      
                          FROM                   
                          products p                     
                          JOIN brands b ON p.brand_id = b.id
                          JOIN
                          category c ON  p.category_id  = c.id 
                    
                    WHERE  1=1  AND p.create_by= '${token.userId}'`

        if (productName != "undefined" && productName != null) {
            sql += ` AND p.product_name like  '%${productName}%' `

        }
         let count = await Products.Count(sql,null);
      
         sql += `  GROUP BY p.id ORDER BY p.id DESC  limit ${pageSize} offset ${offset} `;
        
        var params = [pageSize, offset];
        // var params =[platformCode, sellerUuid, pageSize, offset];

      

        let connector = Products.dataSource.connector;
        connector.query(sql, params, function (err, resultObjects) {
            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {
                let result = new Results();
                result.setSuccess({ itemCount: count, rows: resultObjects });
                console.log(result);
                return cb(null, result);
            }
        });

    }

    )
      .catch(function(e) {
        let result = new Results();

        if (e instanceof ApplicationError) {
          result.setError(e.error);
        } else {
          result.setError({
            message: e.message
          });
        }

        return cb ? cb(null, result) : result;
      });  

    // const makeRequest = async () => {
    //   let Passport = Products.app.models.Passport;
    //   let token = await Passport.GetSellerToken(ctx);
    //   if (!token.userId) {
    //     result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
    //     return cb(null, result);
    //   }
    //   if (!pageOrder || pageOrder == "undefined" || pageOrder == "null")
    //     pageOrder = ["id desc"];
    //   let filter = new Filter().Where(
    //     [{ status: { gte: 0 }, createBy: token.userId, createRole: 2 }],
    //     pageFilter
    //   );

    //   let totalRecords = await Products.count(filter);
    //   Products.find(
    //     {
    //       fields: [
    //         "id",
    //         "productName",
    //         "categoryId",
    //         "productImage",
    //         "brandId",
    //         "shortDescription",
    //         "description",
    //         "unit",
    //         "weight",
    //         "dimension",
    //         "status",
    //         "createTime",
    //         "createBy",
    //         "certificateImages",
    //         "youtubeVideoLink"
    //       ],
    //       include: [
    //         {
    //           relation: "brand",
    //           scope: {
    //             fields: ["id", "brandName", "brandGCP", "logo"]
    //           }
    //         },
    //         {
    //           relation: "category",
    //           scope: {
    //             fields: ["id", "title", "icon", "level"]
    //           }
    //         }
    //       ],
    //       where: filter,
    //       limit: pageSize,
    //       skip: pageIndex * pageSize,
    //       order: pageOrder
    //     },
    //     function(err, instance) {
    //       if (err) {
    //         result.setError(ResultsCodes.SYSTEM_ERROR);
    //         return cb(null, result);
    //       }

    //       result.setSuccess({
    //         totalRecords: totalRecords,
    //         pageSize: pageSize,
    //         pageIndex: pageIndex,
    //         pageData: instance
    //       });
    //       cb(null, result);
    //     }
    //   );
    // };
    // makeRequest().catch(err => {
    //   ResultsCodes.Detail = err;
    //   result.setError(ResultsCodes.DATA_NOT_FOUND);
    //   return cbn(null, result);
    // });
  };

  Products.GetRegisteredProductBySeller = (ctx, cb) => {
    let result = new Results();
    let t = ctx.query.t || new Date().getTime();

    const makeRequest = async () => {
      let Passport = Products.app.models.Passport;
      let token = await Passport.GetSellerToken(ctx);
      if (!token.userId) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      let _products = await Products.find({
        where: { createBy: token.userId, createRole: 2 }
      });

      let _productStatus = {
        cancel: 0,
        registered: 0,
        pending: 0,
        ative: 0,
        remove: 0,
        approved: 0,
        refusedByQuality: 0,
        refusedByContent: 0,
        refusedByImage: 0
      };

      _productStatus.registered = _products.length;

      _products.forEach(item => {
        switch (item.status) {
          case 0:
            _productStatus.cancel++;
            break;
          case 1:
            _productStatus.pending++;
            break;
          case 2:
            _productStatus.approved++;
            break;
          case -1:
            _productStatus.remove++;
            break;
          case 3:
            _productStatus.refusedByContent++;
            break;
        }
      });
      result.setSuccess(_productStatus);
      cb(null, result);
    };

    makeRequest().catch(err => {
      result.setError(err);
      return cb(null, result);
    });
  };
  //#endregion
  
   Products.CheckSellerProduct =(ctx,cb) => { 
    let _productSeller =  Products.app.models.ProductSeller;  
    let mID = ctx.body.id ;
    let mSellerID =  ctx.body.sellerId;
    let result = new Results();          
        let sql = `SELECT a.id,a.seller_id,a.product_id,a.status,b.status FROM  product_seller a, sellers b WHERE a.seller_id = b.id AND a.status =1 AND b.status in (1,2)`;
                        let params = [];
                        if(mID != undefined && mID !=null && mSellerID !=null && mSellerID != undefined) 
                        {
                            sql = sql + ` AND a.seller_id !=  '${mSellerID}'   AND a.product_id = '${mID}'`;
                        }                
        let connector = _productSeller.dataSource.connector;
        connector.query(sql, params, function (err, res) {
            if (err) {
              result.setSuccess(false);
              return cb(null, result);;
            }
            else {
              if(res.length >0)
              {
                result.setSuccess(true);
                return cb(null,result);
              }
              else
              {
                result.setSuccess(false);
                return cb(null,result);
              }
              
            }
        });   
    }

    // ThienVD 09-12-2019
    // Kiểm tra có shop nào đang bán sản phẩm bị set ngừng kinh doanh
    Products.remoteMethod("CheckSellerProduct", {
      description:
        "@ThienVD kiểm tra còn seller nào phân phối sản phẩm đang set ngừng kinh doanh.",
      http: {
        verb: "POST",
        path: "/checkSellerProduct"
      },
      accepts: [
        {
          arg: "ctx",
          type: "object",
          http: {
            source: "req"
          }
        }
      ],
      returns: {
        arg: "result",
        type: "string",
        root: true
      }
    });





  //#region Expose Parameters
  Products.remoteMethod("GetDetails", {
    http: {
      verb: "GET"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http:{
          source:'req'
        }
      } 
    ],
    returns: 
      {
        arg: "result",
        type: "string",
        root: true
      }
    
  });

  Products.remoteMethod("GetList", {
    http: {
      verb: "get"
    },
    accepts: [
      {
        arg: "uuid",
        type: "string"
      },
      {
        arg: "productName",
        type: "string"
      },
      {
        arg: "brandIds",
        type: "array"
      },
      {
        arg: "categoryId",
        type: "number"
      },
      {
        arg: "platformCode",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel1",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel2",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel3",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel4",
        type: "string",
        required: true
      },
      {
        arg: "keySearch",
        type: "string"
      },
      {
        arg: "fromPrice",
        type: "number"
      },
      {
        arg: "toPrice",
        type: "number"
      },
      {
        arg: "pageSize",
        type: "number"
      },
      {
        arg: "pageIndex",
        type: "number"
      },
      {
        arg: "pageOrder",
        type: "array"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("GetProductsByCategories", {
    description: "@Thiep Wong, get product list by multiple categories",
    http: {
      verb: "GET",
      path: "/categories/:categories"
    },
    accepts: [
      {
        arg: "categories",
        type: "string"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("GetListHotSale", {
    http: {
      verb: "GET"
    },
    accepts: [
      {
        arg: "platformCode",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel1",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel2",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel3",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel4",
        type: "string",
        required: true
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("SearchSuggestion", {
    description:
      "@Thiep Wong, search product with any word, in developping, do not change until clear it",
    http: {
      verb: "post",
      path: "/search-suggestion"
    },
    accepts: [
      {
        arg: "platformCode",
        type: "string",
        required: true
      },
      {
        arg: "keyword",
        type: "string"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("SearchAggreation", {
    description:
      "@Hung Doan, search product with any word, in developping, do not change until clear it",
    http: {
      verb: "post",
      path: "/search-aggreation"
    },
    accepts: [
      {
        arg: "platformCode",
        type: "string",
        required: true
      },
      {
        arg: "keyword",
        type: "string"
      },
      {
        arg: "sellerUuid",
        type: "string"
      },
      {
        arg: "parentCategoryId",
        type: "number"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("SearchPaging", {
    description:
      "@Hung Doan, search product with any word, in developping, do not change until clear it",
    http: {
      verb: "get",
      path: "/search-paging"
    },
    accepts: [
      {
        arg: "platformCode",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel1",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel2",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel3",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel4",
        type: "string",
        required: true
      },
      {
        arg: "keyword",
        type: "string"
      },
      {
        arg: "sellerUuid",
        type: "string"
      },
      {
        arg: "brandIds",
        type: "array"
      },
      {
        arg: "parentCategoryId",
        type: "number"
      },
      {
        arg: "categoryIds",
        type: "array"
      },
      {
        arg: "sellerIds",
        type: "array"
      },
      {
        arg: "fromPrice",
        type: "number"
      },
      {
        arg: "toPrice",
        type: "number"
      },
      {
        arg: "pageIndex",
        type: "number"
      },
      {
        arg: "pageSize",
        type: "number"
      },
      {
        arg: "pageOrder",
        type: "array"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("CreateSearchIndex", {
    description:
      "@Thiep Wong, create the index for products, then can using Search funtion to search products with some keyword. This function is privated use only",
    http: {
      verb: "GET",
      path: "/index"
    },
    accepts: [],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("GetListForYou", {
    http: {
      verb: "GET"
    },
    accepts: [
      {
        arg: "platformCode",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel1",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel2",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel3",
        type: "string",
        required: true
      },
      {
        arg: "locationCodeLevel4",
        type: "string",
        required: true
      },
      {
        arg: "pageSize",
        type: "number"
      },
      {
        arg: "pageIndex",
        type: "number"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("RegisterNewProduct", {
    description:
      "@Thiep Wong, seller can register a new product with this method.",
    http: {
      verb: "POST",
      path: "/register-product"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("UpdateRegisterProduct", {
    description:
      "@Thiep Wong, seller can edit registered product with this method.",
    http: {
      verb: "POST",
      path: "/update-register-product/:id"
    },
    accepts: [
      {
        arg: "id",
        type: "number"
      },
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("GetListBySeller", {
    description: "@Thiep Wong, get registered products by seller id ",
    http: {
      verb: "GET",
      path: "/registered-product-list"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("GetRegisteredProductBySeller", {
    description: "@Thiep Wong, get registered products by seller id",
    http: {
      verb: "GET",
      path: "/registered-products-by-seller"
    },
    accepts: [
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("SetStatus", {
    description:
      "@Thiep Wong, set status of product, using to approve from admin or cancel from seller owner",
    http: {
      verb: "POST",
      path: "/state-update/:id"
    },
    accepts: [
      {
        arg: "id",
        type: "number"
      },
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Products.remoteMethod("SetStatusProduct", {
    description: "@Thien VD, set status of product",
    http: {
      verb: "POST",
      path: "/update-status/:id"
    },
    accepts: [
      {
        arg: "id",
        type: "number"
      },
      {
        arg: "ctx",
        type: "object",
        http: {
          source: "req"
        }
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  //#endregion

  Products.setStatus = (id, status, cb) => {
    app.dataSources.mysqld.transaction(async models => {
        const { Passport, Products, Seller, ProductSeller } = models;
        let result = new Results();
        // let _sEngine = new SEngine();
        if (!id) {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        }

        let _product = await Products.findById(id);

        if (_product) {  

            //await _product.updateAttributes({ 'status': status, 'approvedMsg': approvedMsg });
            //await Products.UpdateDocumentByProductId(id, models);       

            await Products.UpdateListStatus(id, status);

            result.setSuccess(true);
            // if (result.setSuccess && status == 2) {
            //     //Create one record Product_Seller                    
            //     let _productSeller = ProductSeller;                   
            //     //_productSeller.Add(models, _product.id, null, 1, 1, _product.shortDescription, null, null, null, null, null, 0, 0);
            //     _productSeller.NewProduct(_product.id, null, 1, 1, _product.shortDescription, null, null, null, null, null, 0, 0);                
            // }
            return cb ? cb(null, result) : result;

        } else {
            result.setError(Results.DATA_NOT_FOUND);
            return cb ? cb(null, result) : result;

        }
    });
}

  Products.remoteMethod('setStatus', {
    description: '@Thien VD, set status of product',
    http: {
        verb: 'POST',
        path: '/product-approve/:id'
    },
    accepts: [
        {
            arg: 'id',
            type: 'number'
        },
        {
            arg: 'status',
            type: 'number'
        }
        
    ],
    returns: {
        arg: 'result',
        type: 'string',
        root: true
    }
})




  Products.UpdateListStatus = async function (id, status) {
    let _sEngine = new SEngine();
    await _sEngine.UpdateProductStatus('products', id, status);
}

Products.UpdateDocumentByProductId = async function (productId, models) {


    let products = await this.GetDocuments(productId, models);
    let _sEngine = new SEngine();

    // only return one record
    if (products && products.length > 0) {
        await _sEngine.UpdateDocument('products', products[0]);
    }
}

// Products.UpdateDocumentByProduct = async function (product) {
//     let mid = product.id;
//     let products = await this.GetDocuments(product.id, null);
//     let _sEngine = new SEngine();
//     let _product = {
//         id: product.id,
//         productName: product.productName,
//         status: product.status,
//         brandId: product.brandId,
//         categoryId: product.categoryId,
//         status: 2
//     }

//     await _sEngine.UpdateDocument('products', _product);
// }


//ThienVD
//Count Product
// 12-12-2019
Products.Count = function (sql, params) {
  let promise = new Promise((resolve, reject) => {
      var ds = Products.dataSource;
      ds.connector.query(sql, params, function (err, data) {
          if (err) {
              reject(err);
          } else {
              resolve(data.length);
          }
      });
  });
  return promise;
}

};
