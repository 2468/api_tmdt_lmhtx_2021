module.exports =  function(cb) {
 
 cb(null,   {  
    isStatic:true,
       "http": {
          "verb":"get"
        },
          "accepts": [{
            "arg": "username",
            "type":"string"
          },
          { 

               "arg": "fullname",
               "type":"string"
          }
          ],
          "returns": [
            {
              "arg":"response",
              "type":"string"
            },
            {
              "arg":"errors",
              "type":"string"
            }]
       }
  );
}
 