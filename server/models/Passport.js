/**
 * Model Passport
 * Authencation for other system login, TXNG, Facebook, Google.
 * Author: Thiep Wong
 * Last update: 13.3.2018 9:21
 * Updated 25/10/2018
 * Update content: Using jwt instate of old token verify method;
 * There are 2 types of token, as access token and refresh token
 */
var Results = require("../middlewares/ResultHandle");
var ResultsCode = require("../middlewares/message_constant");
var crypto = require('crypto');
var http = require("request");
var SysConfig = require('./../middlewares/SystemConfig');
var LoopBackContext = require('loopback-context');
var Promise = require('bluebird');
var app = require('../../server/server');
var SmartLifeTradeVerify = app.get('SmartLifeTradeVerify');
var Facebook = app.get('Facebook');
/**
 * 
 * @param {*} Passport 
 */
module.exports = function (Passport) {

    /** 
     * Get the token of current login seller
     */
    // Passport.GetSellerToken = function () {
    //     let ctx = LoopBackContext.getCurrentContext();
    //     let accessToken = ctx && ctx.get('current_token');
    //     let sellerToken = Passport.app.models.SellerToken;
    //     if (!accessToken) return new Passport.app.models.SellerToken;
    //     return sellerToken.findById(accessToken);
    // }

    /**
     * Get Seller Token from context and return seller info
     */
    // Passport.GetSellerToken = function () {
    //     let ctx = LoopBackContext.getCurrentContext();
    //     let accessToken = ctx && ctx.get('current_token');
    //     let sellerToken = Passport.app.models.SellerToken;
    //     if (!accessToken) return new Passport.app.models.SellerToken;
    //     return sellerToken.findById(accessToken);
    // }

    Passport.GetSellerToken = async  (ctx) => {
      
        let _token = await Passport.verifyToken(ctx);
        if(_token.name == 'TokenExpiredError') {
            return null;
        }
        if(_token.typ !='sel')  {
            return null;
        }
        return {
             id: _token.iss,
             userId:_token.aid,
             created:_token.iat,
             ttl:12900 
        };  
    }

    /**
     * Get the token of current login member
     * Update by Thiep Wong, using jwt instead of old access token
     */
    Passport.GetMemberToken = async   (ctx)=> { 
            let _token = await Passport.verifyToken(ctx);
            if(_token.name == 'TokenExpiredError') {
                return null;
            } 
            if(_token.typ!='mem') {
                return null;
            }
            return {
                 id: _token.iss,
                 userId:_token.aid,
                 created:_token.iat,
                 ttl:12900 
            }; 
    }

    // Passport.verifyToken =    () => {
    //     let ctx =  LoopBackContext.getCurrentContext();
    //     let accessToken = ctx && ctx.get('current_token');
    //     let JToken = app.models.JToken;
    //     return JToken.verify(accessToken) 
    // }

    Passport.verifyToken =    (req=null) => { 
        if(!req) {
            let ctx =  LoopBackContext.getCurrentContext();
            let accessToken = ctx && ctx.get('current_token');
            let JToken = app.models.JToken;
            return JToken.verify(accessToken) 
        }
        var accessToken = req.query.access_token != undefined ? req.query.access_token : null ||
                req.body ? req.body.access_token ? req.body.access_token : null : null ||
                req.headers.access_token ? req.headers.access_token : null; 
        let JToken = app.models.JToken;
        return JToken.verify(accessToken) 
    }


    // Passport.GetMemberToken = function () {
    //     let ctx = LoopBackContext.getCurrentContext();
    //     let accessToken = ctx && ctx.get('current_token');
    //     let memberToken = Passport.app.models.MemberToken;
    //     if (!accessToken) return new Passport.app.models.MemberToken;
    //     return memberToken.findById(accessToken);
    // }

    /**
     * 
     * @param {Object} req  The context from a request from client. 
     */
    Passport.TokenVerify = function (req) {
  
        var _token = req.query.api_token != undefined ? req.query.api_token : null ||
            req.body ? req.body.api_token ? req.body.api_token : null : null ||
                req.headers.api_token ? req.headers.api_token : null;

        if (!_token) {
            return false;
        }
        else {

            var h = crypto.createHash('RSA-SHA256');

            h.update(req.connection.remoteAddress, "ascii");
            h.update("smartlife"); // Secret keyword
            var _t = h.digest('hex');
            if (_token == _t || _token == SysConfig.api_token.android || _token == SysConfig.api_token.ios || _token == SysConfig.api_token.web) {
                // console.log(_token);
                return true;
            }

            else {
                // console.log(_t);
                return false;
            }

        }

    }

    /**
     * 
     * @param {*} fb_access_token 
     * @param {*} cb 
     */
    Passport.facebookAuth = function (fb_access_token, cb) {
        if (fb_access_token) {
            let url = Facebook.url + "me?fields=picture,name,email,gender,link,cover,verified,first_name,last_name&access_token=" + fb_access_token;
            http({
                url: url,
                json: true
            }, function (error, response, body) {

                if (!error && response.statusCode === 200) {
                    cb(null, body);
                }
                else {
                    let result = new Results();
                    result.setError(error);
                    return cb(null, result);
                }
            })
        }
        else {

            let result = new Results();
            result.setError(error);
            return cb(null, result);
        }

    };


    /**
     * 
     * @param {*} fb_access_token 
     * @param {*} cb 
     */
    Passport.facebookProfile = function (fb_access_token, cb) {

        if (cb == undefined || cb == null) {

            let profile = new Promise((resolve, reject) => {

                if (fb_access_token) {
                    var url = Facebook.url + "me?fields=birthday,email,first_name,id,cover,last_name,political,gender,picture&access_token=";
                    url += fb_access_token;
                    http({
                        url: url,
                        json: true
                    }, function (error, response, body) {

                        if (!error && response.statusCode === 200) {

                            return resolve(body)

                        }
                        else {

                            return reject(error);
                        }

                    })
                }
                else {
                    return reject('Error!');
                }

            });

            return profile;

        }



        if (fb_access_token) {
            var url = Facebook.url + "me?fields=birthday,email,first_name,id,cover,last_name,political,gender,picture&access_token=";
            url += fb_access_token;
            http({
                url: url,
                json: true
            }, function (error, response, body) {

                if (!error && response.statusCode === 200) {
                    cb(null, body);

                }
                else {

                    cb(null, "Loi roi");
                }

            })
        }
        else {
            cb(null, "Khong co token facebook");
        }

    };


    /**
     * 
     * @param {*} commonToken 
     * @param {*} cb 
     */
    Passport.CommonAuth = function (commonToken, cb) {
        http({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'app_version':'1.0',
                'api_token': commonToken
            },
            url: SmartLifeTradeVerify.url,
            json: true
        }, function (error, response, body) {

            if (!error && response.statusCode === 200) {
                cb(null, body);
            }
            else {
                let result = new Results();
                result.setError(error);
                return cb(null, result);
            }
        })


    }


}
