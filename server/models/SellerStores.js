/**
 * Model SellerStore
 * Author: LuongPD
 * Last update: 15.3.2018 13:49
 */
var Results = require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var Filter = require('../middlewares/DataFilterHandle');

module.exports = function (SellerStores) {

    SellerStores.sharedClass.methods().forEach(function (method) {
        SellerStores.disableRemoteMethodByName(method.name, method.isStatic);
    });

    //#region Functions
    /**
     * add new warehouse
     * Created by Thiep Wong
     * @param {*} storeName 
     * @param {*} managerName 
     * @param {*} managerPhone 
     * @param {*} address 
     * @param {*} locationCode 
     * @param {*} cb 
     */
    SellerStores.newWarehouse = function (ctx, cb) {
        let storeName = ctx.body.storeName || null; 
        let managerName = ctx.body.managerName || null; 
        let  managerPhone = ctx.body.managerPhone || null; 
        let  address = ctx.body.address || null; 
        let  locationCode = ctx.body.locationCode || null; 
        let  locationLevel = ctx.body.locationLevel || 0;
        let result = new Results();

        if (!storeName || !address || !locationCode) { 
            result.setError(
                ResultsCodes.SELLER_STORE_INFO_INVALID
            )
            return cb(null, result);
        }

        const makeRequest = async () => {
            let Passport = SellerStores.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) { 
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            var sellerStore = new SellerStores;
            sellerStore.sellerId = token.userId;
            sellerStore.storeName = storeName;
            sellerStore.managerName = managerName;
            sellerStore.managerPhone = managerPhone;
            sellerStore.address = address;
            sellerStore.locationCode = locationCode;
            sellerStore.locationLevel = locationLevel?locationLevel:4
            sellerStore.createTime = parseInt(new Date().getTime() / 1000.0);
            sellerStore.createBy = token.userId;
            sellerStore.status = 1;
            SellerStores.create(sellerStore, function (err, instanceSellerStore) {
                if (err) { 
                    result.setError(ResultsCodes.ADD_ITEM_FAILED); 
                    return cb(null, result);
                }
                
                    result.setSuccess(true);
                    cb(null, result); 
            }); 
        }

        makeRequest().catch(err=>{
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null,result);
        });

    }

    /**
     * Edit warehouse infomation
     * Created by Thiep Wong
     * @param {*} id 
     * @param {*} storeName 
     * @param {*} managerName 
     * @param {*} managerPhone 
     * @param {*} address 
     * @param {*} locationCode 
     * @param {*} cb 
     */
    SellerStores.editWarehouse = function (id, ctx, cb) {
        let storeName = ctx.body.storeName || null; 
        let  managerName= ctx.body.managerName || null; 
        let  managerPhone = ctx.body.managerPhone || null; 
        let  address = ctx.body.address || null; 
        let locationCode = ctx.body.locationCode || null; 

        let result = new Results();
        const makeEditRequest = async () => {
            let Passport = SellerStores.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            if (id < 1) { 
                result.setError(ResultsCodes.SELLER_STORE_INFO_INVALID);
                return cb(null, result);
            }

            let sellerStore = await SellerStores.findById(id);
            if (!sellerStore.id) { 
                result.setError(ResultsCodes.SELLER_STORE_INFO_INVALID);
                return cb(null, result);
            }

            sellerStore.storeName = storeName;
            sellerStore.managerName = managerName;
            sellerStore.managerPhone = managerPhone;
            sellerStore.address = address;
            sellerStore.locationCode = locationCode;

            sellerStore.save(function (err) {
                if (err) { 
                    result.setError(ResultsCodes.EDIT_ITEM_FAILS);
                    return cb(null, result);
                }
 
                result.setSuccess(ResultsCodes.ITEM_EDITED);
                cb(null, result);
            })
        }

        makeEditRequest();
    }


    /**
     * Delete a warehouse record
     * Created by Thiep Wong
     * @param {*} id 
     * @param {*} cb 
     */
    SellerStores.deleteWarehouse = function (id,ctx, cb) {
 
        let result = new Results();
        const makeDeleteRequest = async () => {
            let Passport = SellerStores.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) { 
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            let _wareHouse = await SellerStores.findById(id);

            if (!_wareHouse.id) { 
                result.setError(ResultsCodes.SELLER_STORE_INFO_INVALID);
                return cb(null, result);
            }
            let _productStore = await SellerStores.app.models.ProductStore.findOne({
                fields: ["id","available"],
                where: {
                    sellerStoreId: _wareHouse.id
                }
              });
      
              if (_productStore && _productStore.id > 0) {
                result.setError(ResultsCodes.DELETE_FAILS);
                return cb(null, result);
              }

            _wareHouse.updateAttribute('status',-1,(err,_w)=>{
                if (err) { 
                    result.setError(ResultsCodes.DELETE_ITEM_FAILS);
                    return cb(null, result);
                } 
                result.setSuccess(ResultsCodes.ITEM_DELETED);
                cb(null, result);
            }) 
        }

        makeDeleteRequest().catch(err=>{
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });


    }

    /**
     * Deprecated Method, use newWarehouse instead
     * @param {*} storeName 
     * @param {*} managerName 
     * @param {*} managerPhone 
     * @param {*} address 
     * @param {*} locationCode 
     * @param {*} cb 
     */
    SellerStores.addNew = function (storeName, managerName, managerPhone, address, locationCode, cb) {
        if (!storeName || !address || !locationCode) {
            let result = new Results();
            result.setError(
                ResultsCodes.SELLER_STORE_INFO_INVALID
            )
            return cb(null, result);
        }

        var sellerStore = new SellerStores;
        sellerStore.sellerId = 3;
        sellerStore.storeName = storeName;
        sellerStore.managerName = managerName;
        sellerStore.managerPhone = managerPhone;
        sellerStore.address = address;
        sellerStore.locationCode = locationCode;
        sellerStore.createTime = parseInt(new Date().getTime() / 1000.0);
        sellerStore.createBy = 0;
        SellerStore.create(sellerStore, function (err, instanceSellerStore) {
            if (err) {
                Results.success = null;
                Results.warning = null;
                Results.errors = ResultsCodes.ADD_ITEM_FAILED;
                cb(null, Results);
            }
            else {
                Results.success = true;
                Results.warning = null;
                Results.errors = null;
                cb(null, Results);
            }
        });
    }


    /**
     * GetStoreby SellerId
     * Modified by Thiep Wong
     * @param {*} pageIndex 
     * @param {*} pageSize 
     * @param {*} pageOrder 
     * @param {*} pageFilter 
     * @param {*} cb 
     */
    SellerStores.GetBySeller = function (ctx, cb) {
    let   pageIndex = ctx.query.pageIndex || 0
    let  pageSize = ctx.query.pageSize || 10 
    let  pageOrder = ctx.query.pageOrder || null;
    let  pageFilter = ctx.query.pageFilter || null;
        if (!pageSize) {
            pageSize = 10;
            pageIndex = 0;
        }
        
        let result = new Results();
        const makeRequestStoreList = async () => {
            let Passport = SellerStores.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            if (!pageOrder || pageOrder == 'undefined' || pageOrder == 'null') pageOrder = ['id asc'];

            let filter = new Filter().Where([{ "sellerId": token.userId }, { "status": { gte: 1 } }], pageFilter);

            let totalRecords = await SellerStores.count(filter);

            SellerStores.find({
                fields: ['id', 'storeName', 'sellerId', 'managerName', 'managerPhone', 'address', 'locationCode','status'],
                include: {
                    relation: 'location',
                    scope: {
                        fields: ['code', 'path', 'name']
                    }
                },
                where: filter,
                limit: pageSize,
                skip: pageIndex * pageSize,
                order: pageOrder
            }, function (err, instance) { 
                if(err) {
                    result.setError(ResultsCodes.DATA_NOT_FOUND);
                    return cbn(null,result);
                }
                result.setSuccess({ totalRecords: totalRecords, pageSize: pageSize, pageIndex: pageIndex, pageData: instance , message: 'Thanh cong' });
                cb(null, result);
            })
        }
        makeRequestStoreList().catch(err=>{
            ResultsCodes.Detail = err;
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cbn(null,result);
        });

    }
    //#endregion

      //#region  Get Seller Stores infomation from buyer client by store id and seller id;

      SellerStores.remoteMethod('GetStoreInfoById', {
          description:'@Thiep Wong, get seller store infomation by seller store\'s id and seller id ',
          http:{
              verb:'GET',
              path:'/store-info'
          },
          accepts:[
              {
                  arg:'ctx',
                  type:'object',
                  http:{
                      source:'req'
                  }
              }
          ],
          returns:{
              arg:'result',
              type:'string',
              root:true
          }
      })


      SellerStores.GetStoreInfoById = (ctx,cb)=>{
          let sellerId = ctx.query.sellerId || 0;
          let storeId = ctx.query.storeId || 0;
          let result = new Results();
          if(!sellerId || !storeId) {
            result.setError(ResultsCodes.ARG_NOT_FOUND);
            return cb(null,result);
          }

          let makeRequest = async() =>{

            let _store = await SellerStores.findOne({
                include:{
                    relation: 'location'
                },
                where: {
               id: storeId
            }});
            if(!_store || _store.sellerId != sellerId) {
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null,result);
            }

            result.setSuccess(_store);
            cb(null,result);

          }


          makeRequest().catch(err=>{
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cb(null,result);
          })
          

      }

      //#endregion

    //#region Expose Parameters



    SellerStores.remoteMethod('addNew', {
        description: 'Deprecated method, use newWarehouse to instead',
        http: {
            verb: 'POST',
            path:'/addnew'
        },
        accepts: [
            {
                arg: "ctx",
                type: "object", 
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
        
    })

    SellerStores.remoteMethod('GetBySeller', {
        description: '@Thiep Wong, get seller\'s owner stores with id and filters',
        http: {
            verb: "GET",
            path: '/list'
        },
        accepts: [
           
        {
            arg: 'ctx',
            type: 'object',
            http:{
                source:'req'
            }
        }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
        
    })

    SellerStores.remoteMethod('newWarehouse', {
        description: '@Thiep Wong, to register a new warehouse',
        http: {
            verb: 'POST',
            path: '/add'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            }

        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    SellerStores.remoteMethod('deleteWarehouse', {
        description: '@Thiep Wong, to unregister a warehouse',
        http: {
            path: '/delete/:id',
            verb: 'DELETE'
        },
        accepts: [
            {
                arg: 'id',
                type: 'number',
                required: true
            },
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    })

    SellerStores.remoteMethod('editWarehouse', {
        description: '@Thiep Wong, to edit the warehouse infomation',
        http: {
            path: '/edit/:id',
            verb: 'PUT'
        },
        accepts: [
            {
                arg: 'id',
                type: 'number'
            },
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    //#endregion
};