/**
 * Model Product Key Search
 * Author: LuongPD
 * Last update: 19.4.2018 15:09
 */
var Results = new require('../middlewares/ResultHandle');

 module.exports = function(ProductKeySearch) {

    //#region Functions
    ProductKeySearch.FindKeySearch = function (key, cb) {
        ProductKeySearch.find({
            fields: ['id', 'keySearch'],
            where: {
                keySearch: {like: key + '%'}
            }
        }, function (err, results) {
            let result = new Results();
            result.setSuccess(results);
            return  cb(null, result);
        })
    }
    //#endregion

    //#region Expose Parameters
    ProductKeySearch.remoteMethod('FindKeySearch', {
        http: {
            verb: "GET"
        },
        accepts: [{
                arg: "key",
                type: "string"
            }
        ],
        returns: [{
                arg: 'result',
                type: 'string',
                root: true
            }
        ]
    })
    //#endregion
};