var app = require('../../server/server');
var Results = require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var ApplicationError = require('../middlewares/application-error');

module.exports = function(SellerPlatform) {

    //#region Functions
    SellerPlatform.CreateNew = function (ctx, cb) {

        let sellerId = ctx.body.sellerId || 0;
        let platformCode = ctx.body.platformCode || null; 
        let createBy = ctx.body.createBy || 0 ;
        let modifyBy = ctx.body.modifyBy || 0;
        
        let result = new Results();
        app.dataSources.mysqld.transaction(async models => {
            // const { SellerPlatform } = models;
            // let newDate = parseInt(new Date().getTime() / 1000.0);

            // let instance = await SellerPlatform.findOne({
            //     where: {
            //         'sellerId': sellerId,
            //         'platformCode': platformCode
            //     }
            // });

            // if (instance) { // if exists change status to 1
            //     instance.modifyBy = modifyBy;
            //     instance.modifyTime = newDate;
            //     instance.status = 0;
            //     await instance.save();
            // } else { 

            //     // create new record
            //     let sellerPlatform = new SellerPlatform;
            //     sellerPlatform.sellerId = sellerId;
            //     sellerPlatform.platformCode = platformCode;
            //     sellerPlatform.status = 0;
            //     sellerPlatform.approvedBy = null;
            //     sellerPlatform.approvedTime = null;
            //     sellerPlatform.createBy = createBy;
            //     sellerPlatform.createTime = parseInt(new Date().getTime() / 1000.0);
            //     await SellerPlatform.create(sellerPlatform);
            // }

            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                throw new ApplicationError(ResultsCodes.ACCESS_TOKEN_INVALID);
            }

            let newDate = parseInt(new Date().getTime() / 1000.0);
            
            const {Products} = models;
            await this.Add(models, token.userId, platformCode, 0, token.userId, newDate);

            // update ES document
            await Products.UpdateDocumentBySellerId(token.userId, models);

            result.setSuccess(true);
            return cb ? cb(null, result) : result;
            
        }).catch(function (e) { 

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }

    SellerPlatform.Add = async function(models, sellerId, platformCode, status, createBy, newDate) {
        const { SellerPlatform } = models;

        let sellerPlatform = new SellerPlatform;
        sellerPlatform.sellerId = sellerId;
        sellerPlatform.platformCode = platformCode;
        sellerPlatform.status = status;
        sellerPlatform.approvedBy = null;
        sellerPlatform.approvedTime = null;
        sellerPlatform.createBy = createBy;
        sellerPlatform.createTime = newDate;

        await SellerPlatform.create(sellerPlatform);
    }

    SellerPlatform.Active = function (sellerId, platformCode, modifyBy, cb) {

        app.dataSources.mysqld.transaction(async models => {
            const { SellerPlatform } = models;

            let instance = await SellerPlatform.findOne({
                where: {
                    'sellerId': sellerId,
                    'platformCode': platformCode
                }
            });

            if (instance) {
                instance.modifyBy = modifyBy;
                instance.modifyTime = newDate;
                instance.status = 1;
                await instance.save();

                let result = new Results();
                result.setSuccess(true);
                return cb ? cb(null, result) : result;

            } else {
                let result = new Results();
                result.setError(ResultsCodes.ITEM_NOT_FOUND);
                return cb ? cb(null, result) : result;
            }

        }).catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb ? cb(null, result) : result;
        });
    }

    SellerPlatform.List = function (ctx, cb) {
        let status = ctx.query.status || 1; 
        if(status.length<2) status = [status];
        let result = new Results();
        const makeRequest = async () => {

            let Passport = SellerPlatform.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            let seller = await SellerPlatform.app.models.Sellers.findById(token.userId);
            if (!seller) { 
                result.setError(ResultsCodes.USER_NOT_FOUND);
                return cb(null, result);
            }
            
            let sellerPlatforms = await SellerPlatform.find({
                fields: ['id', 'sellerId', 'platformCode', 'status'],
                include: [{
                    relation: 'platform',
                    scope: {
                        fields: ['id', 'platformCode', 'platformName'],
                    }
                }],
                where: {
                    'sellerId': seller.id,
                    'status': {inq : status}
                },
                order: 'platformCode asc'
            });
 
            result.setSuccess(sellerPlatforms);
            return cb ? cb(null, result) : result;
        }

        makeRequest().catch(function (e) { 
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    SellerPlatform.Save = function (ctx, cb) {

        let platforms =ctx.body.platforms || [];
        
        let result = new Results();
        app.dataSources.mysqld.transaction(async models => {
            const {Passport, Products, SellerPlatform} = models;

            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                throw new ApplicationError(ResultsCodes.ACCESS_TOKEN_INVALID);
            }

            let newDate = new Date().getTime() / 1000.0;

            if (platforms.length > 0) {

                for (var index in platforms) {

                    let platform = platforms[index];

                    if (platform.sellerPlatformStatus != undefined && platform.sellerPlatformStatus != 0 && platform.sellerPlatformStatus != 1 && platform.sellerPlatformStatus != 2) {
                            let error = ResultsCodes.VALIDATION_ERROR;
                            error.message = 'sellerPlatformStatus phải là 0 hoặc 1 hoặc 2';
                            throw new ApplicationError(error);
                    }

                    if (platform.sellerPlatformId > 0) { // if exists => update status
                        let instance = await SellerPlatform.findOne({where: {'id': platform.sellerPlatformId, 'sellerId': token.userId}});
                        if (!instance) {
                            let error = ResultsCodes.DATA_NOT_FOUND;
                            error.message = 'Không tìm thấy SellerPlatform với id = ' + platform.sellerPlatformId + ', sellerId = ' + token.userId;
                            throw new ApplicationError(error);
                        } 

                        // if already exists and status = 1 => set status to 0
                        if (platform.sellerPlatformStatus != undefined) {
                            await instance.updateAttributes({'status': platform.sellerPlatformStatus, 'modifyBy': token.userId, 'modifyTime': newDate});
                        }
                    } else {
                        // if not exists => find by platformCode
                        if (platform.sellerPlatformStatus != undefined && platform.sellerPlatformStatus == 1) {
                        
                            let instance = await SellerPlatform.findOne({where: {'platformCode': platform.platformCode, 'sellerId': token.userId}});
                            if (instance) {
                                // update status to 1
                                await instance.updateAttributes({'status': 1, 'modifyBy': token.userId, 'modifyTime': newDate});
                            
                            }  else {
                                // add new record with status = 1
                                await this.Add(models, token.userId, platform.platformCode, token.userId, 1, newDate);
                            }
                        }
                    }
                }

                // update ES document
                await Products.UpdateDocumentBySellerId(token.userId, models);
            }

            result.setSuccess(true);
            return cb(null, result);
        }).catch(function (e) {

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }
    //#endregion

    //#region Expose Parameters
    SellerPlatform.remoteMethod('CreateNew', {
        http: {
            verb: 'POST',
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    
    SellerPlatform.remoteMethod('Active', {
        http: {
            verb: 'POST',
        },
        accepts: [{
                arg: 'sellerId',
                type: 'number',
                required: true
            }, {
                arg: 'platformCode',
                type: 'string',
                required: true
            }, {
                arg: 'createBy',
                type: 'number'
            }, {
                arg: 'modifyBy',
                type: 'number'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    SellerPlatform.remoteMethod('List', {
        http: {
            verb: 'GET',
            path: '/list'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    SellerPlatform.remoteMethod('Save', {
        http: {
            verb: 'POST',
            path:'/save'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    //#endregion

};