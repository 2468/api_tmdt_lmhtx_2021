var Results = require('./../middlewares/ResultHandle');
var ResultCodes = require('./../middlewares/message_constant');
const Msg = require('../middlewares/message_constant');
'use strict'
module.exports = function(ConfigSys) {

    ConfigSys.Register = (ctx,cb)=>{

        const makeRequest = async() =>{
         let name = ctx.body.name || null;
         let code = ctx.body.code || null;       
         let domain = ctx.body.domain || null;
         let api = ctx.body.api || null;          

            let result = new Results();
            let Passport = ConfigSys.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);

            if(!token.userId) {
                result.setError(ResultCodes.ACCESS_TOKEN_INVALID);
                return cb(null,result);
            }
            ConfigSys.create({
                name:name,
                code:code,
                domain:domain,
                api:api,               
                createTime:new Date().getTime()/1000,
                createBy: token.userId
            },(err,res)=>{
                if(err) {
                    result.setError(ResultCodes.ACCESS_TOKEN_INVALID);
                    return cb(null,result);
                } 
                //result.setSuccess(ResultCodes.ADDED_SUCCESS);
                //return cb(null,result); 
                result.setSuccess(res);
                return cb(null,result); 
            })
        }

        makeRequest();

    }

   

    ConfigSys.GetConfigSysFilter = (configName,cb)=>{

        const makeRequest = async()=>{
            let result = new Results();
           
            let where = configName?{
                where: {'name':{like: '%'+configName+'%'},} 
            }: null;    
            ConfigSys.find(where,(err,res)=>{
                if(err) {
                result.setError(err);
                return cb(null,result);
                }
    
                result.setSuccess(res);
                cb(null,result);
            });
        }
      makeRequest();
    }

    ConfigSys.GetListConfigSys = (cb)=>{
        let result = new Results();
        ConfigSys.find({
            fields: {},
            where: {              
                    
                    status: {gt: 0}
                  
            }          
        },(err,res)=>{
            if(err) {
                result.setError(err);
                return cb(null,result);
            }

            result.setSuccess(res);
            cb(null,result);           
 
        })
    }

  

    ConfigSys.remoteMethod('Register',{
        description:'@ThienVD, create a new config',
        http:{
            verb:'POST',
            path:'/register'
        },
        accepts:[
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns:{
            arg:'result',
            type:'string',
            root:true
        }
    })

    ConfigSys.remoteMethod('GetConfigSysFilter',{
        description:'@ThienVD getlistConfigSysFilter status >-1',
        http:{
            verb:'GET',
            path:'/listfilter'
        },
        accepts:[
            {
                arg:'name',
                type:'string'
            }
        ],
        returns:{
            type:'string',
            root:true
        }
    })

  

    ConfigSys.remoteMethod('GetListConfigSys',{
        description:'@Thien VD, get all ConfigSys as list ',
        http:{
            verb:'GET',
            path:'/get-list'
        },
        accepts:[],
        returns: {
            type:'string',
            root:true
        }

    })


        //#region  Edit  
        ConfigSys.remoteMethod('Edit', {
            description: '@ThienVD, edit',
            http: {
                verb: 'POST',
                path: '/edit/:id'
            },
            accepts: [
                {
                    arg: 'id',
                    type: 'string'
                },
                {
                    arg: 'configData',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }
            ],
            returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        })
    
        ConfigSys.Edit = (id, configData, cb) => {
            let result = new Results();
            if (!id) {
                result.setError(Msg.ARG_NOT_FOUND);
                return cb(null, result);
            }
    
            ConfigSys.findById(id, (err, resConfig) => {
                if (err) {
                    result.setError(Msg.SYSTEM_ERROR);
                    return cb(null, result);
                }
    
                if (!resConfig) {
                    result.setError(Msg.DATA_NOT_FOUND);
                    return cb(null, result);
                }
    
                resConfig.updateAttributes(configData, (err, resData) => {
                    if (err) {
                        result.setError(Msg.SYSTEM_ERROR);
                        return cb(null, result);
                    }
    
                    result.setSuccess(configData);
                    cb(null, result);
                })
    
    
            })
        }
    
        //#endregion
    
        //#region  Delete
        ConfigSys.remoteMethod('Delete', {
            description: '@ThiepVD, delete ',
            http: {
                verb: 'delete',
                path: '/:id'
            },
            accepts: [
                {
                    arg: 'id',
                    type: 'string'
                }
            ],
            returns: {
                arg: 'result',
                type: 'string',
                root: true
            }
        })
    
        ConfigSys.Delete = (id, cb) => {
            let result = new Results();
            if (!id) {
                result.setError(Msg.ARG_NOT_FOUND);
                return cb(null, result);
            }
            ConfigSys.findById(id, (err, resConfig) => {
                if (err) {
                    result.setError(Msg.SYSTEM_ERROR);
                    return cb(null, result);
                }
    
                if (!resConfig) {
                    result.setError(Msg.DATA_NOT_FOUND);
                    return cb(null, result);
                }
    
                resConfig.updateAttribute('status', -1, (err, resData) => {
                    if (err) {
                        result.setError(Msg.SYSTEM_ERROR);
                        return cb(null, result);
                    }
                    result.setSuccess(Msg.ITEM_DELETED);
                    cb(null, result);
                }) 
            }) 
        }
};