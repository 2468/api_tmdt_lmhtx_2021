/**
 * Model Answers
 * Author: LuongPD
 * Last update: 15.5.2018 13:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
'use strict';

module.exports = function(Answers) {
    
    //#region Functions
    Answers.Answer = function(questionId, answer,ctx, cb) {

        const makeAnswer = async () => {
            let Passport = Questions.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var sellerId = token.userId;

            app.dataSources.mysqld.transaction(async models => {
                const {Answers} = models;

                const answers = await Answers.findOne({
                    fields: ['id', 'questionId', 'sellerId', 'answer', 'createTime'],
                    where: {
                        "questionId": questionId,
                        "sellerId": sellerId
                    }
                });

                if (answers == null) {
                    throw new Error(JSON.stringify(ResultsCodes.ITEM_NOT_FOUND));
                }

                answers.answer = answer;
                answers.modifyTime = parseInt(new Date().getTime() / 1000.0);
                answers.modifyBy = sellerId;
                await answers.save();

                let result = new Results();
                result.setSuccess(true);
                return cb(null, result);

            }).catch (function(e) {
                let result = new Results();
                result.setError(ResultsCodes.UPDATE_ITEM_FAILED);
                return cb(null, result);
            });
        }
        makeAnswer().catch (function(e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return  cb(null, result);
        });
    }
    //#endregion

    //#region Expose Parameters
    Answers.remoteMethod('Answer', {
        accepts: [{
            arg: "questionId",
            type: "number",
            required: true
        }, {
            arg: "answer",
            type: "string",
            required: true
        },
        {
            arg:'ctx',
            type:'object',
            http:{
                source:'req'
            }
        }
    ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
};