/**
 * Model Location
 * Author: LuongPD
 * Last update: 13.3.2018 13:49
 */
var Results = require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var locations = require('./tree.json');

module.exports = function (Location) {

    //#region Functions
    Location.GetListFull = function (cb) {
        Location.find({
            fields: ['name', 'slug', 'level', 'nameWithType', 'path', 'pathWithType', 'code', 'parentCode'],
            include: {
                relation: 'subLocations',
                scope: {
                    fields: ['name', 'slug', 'level', 'nameWithType', 'path',  'pathWithType','code', 'parentCode'],
                    include: {
                        relation: 'subLocations',
                        scope: {
                            fields: ['name', 'slug', 'level', 'nameWithType', 'path',  'pathWithType','code', 'parentCode'],
                            include: {
                                relation: 'subLocations',
                                scope: {
                                    fields: ['name', 'slug', 'level', 'nameWithType', 'path', 'pathWithType', 'code', 'parentCode'],
                                }
                            }
                        }
                    }
                }
            },
            where: {
                "parentCode": ""
            }
        }, function (err, instance) {
            let result = new Results();
            result.setSuccess(instance);
            return cb(null, result);
        })
    }

    Location.AddNew = function (json, cb) {

        // json = "";
        // console.log(config.firstName + ' ' + config.lastName);
        var locations = parseListObject(json);
         console.log(location.name);

        for (var item2 in locations) {
            var level2 = locations[item2];

            try {
                if (level2.name != null) {
                    createLocation("vn", level2.name, level2.slug.replace('-', ''), level2.name_with_type, level2.code, level2.path, level2.path_with_type, 2);
                }
            } catch (error) {
                console.log(level2.name);
            }

            for (var item3 in level2.quan_huyen) {
                var level3 = level2.quan_huyen[item3];

                try {
                    if (level3.name != null) {
                        createLocation(level3.parent_code, level3.name, level3.slug.replace('-', ''), level3.name_with_type, level3.code, level3.path, level3.path_with_type, 3);
                    }
                } catch (error) {
                    console.log(level3.name);
                }

                for (var item4 in level3.xa_phuong) {
                    var level4 = level3.xa_phuong[item4];

                    try {
                        if (level4.name != null) {
                            createLocation(level4.parent_code, level4.name, level4.slug.replace('-', ''), level4.name_with_type, level4.code, level4.path, level4.path_with_type, 4);
                        }
                    } catch (error) {
                        console.log(level4.name);
                    }
                }
            }
        }

        let result = new Results();
        result.setSuccess(true);
        return cb(null, result);
    }

    var createLocation = async function(parentCode, name, slug, nameWithType, code, path, pathWithType, level) {

        // console.log(parentCode + "-" + nameWithType);

        var location = new Location;
        location.parentCode = parentCode;
        location.name = name;
        location.slug = slug.replace('-', '');
        location.nameWithType = nameWithType;
        location.code = code;
        location.path = path;
        location.pathWithType = pathWithType;
        location.level = level;
        Location.create(location);
    }

    var parseListObject = function (json) {
        json = "[" +                                    // enclose with []
            json.replace(/(\w+)(?=:)/g, '"$1"')             // search for words followed by a colon
                .replace(/,$/, '')                              // trim the ending comma
                .replace(/:([\w.]+)/g, function (match, value) {  // grab the values
                    return ':' + (                                // ensure preceding colon is in place
                        isNaN(value) || value % 1 !== 0 ?           // is it a non-float number?
                            '"' + value + '"' :                       // enclose with "" if not a non-float number
                            parseFloat(value)                         // parse if is number
                    );
                })
            + "]";                                            // enclose with []
        return JSON.parse(json);
    }

    /**
     * Get location Code with name
     * Created by Thiep Wong
     * Date 28/06/18
     * @param {*} level2 
     * @param {*} level3 
     * @param {*} level4 
     * @param {*} cb 
     */
    Location.FindByName = function (level2, level3, level4, cb) {
        const makeRequest = async () => {

            let _level2 = await Location.findOne({
                fields: ['name', 'level', 'nameWithType', 'path', 'code', 'parentCode'], where: {
                    name: level2,
                    level: 2
                }
            });

            let _level3 = await Location.findOne({
                fields: ['name', 'level', 'nameWithType', 'path', 'code', 'parentCode'], where: {
                    name: level3,
                    parentCode: _level2.code,
                    level: 3
                }
            });

            let _level4 = await Location.findOne({
                fields: ['name', 'level', 'nameWithType', 'path', 'code', 'parentCode'], where: {
                    name: level4,
                    parentCode: _level3.code,
                    level: 4
                }
            });

            if(_level3) {

                let result = new Results();
                result.setSuccess({
                    level2:_level2,
                    level3:_level3,
                    level4:_level4,
                    path: _level4.path
                });

                cb(null,result);
            } else {
                return cb(null, result.setError('Không có dữ liệu'))
            }
 
        }

        makeRequest();


    }

    /**
     * Get children locatons array by parent code and level
     * Created by Thiep Wong
     * Date 02/07/2018
     * @param {*} parentCode 
     * @param {*} level 
     * @param {*} cb 
     */
    Location.GetChildByParentCode =function(parentCode,level,cb) {

        if(!parentCode) {
            let result = new Results();
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cb(null,result);
        }

        let _where ='';
        if(level) {
            _where = {
                parentCode:parentCode,
                level:level
            };
        } else {
            _where = {
                parentCode:parentCode
            }
        }

        Location.find({
            fields:['name', 'level', 'nameWithType', 'path', 'code', 'parentCode'],
            where: _where,
            order: 'name ASC'

        },function(err, instance){
            if(err) {
                let result = new Results();
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null,result);
            }

            let result = new Results();
            result.setSuccess(instance);
            return cb(null,result);

        })

    }

     /**
     * Get locaton by code 
     * Created by Hung Doan
     * Date 04/07/2018
     * @param {*} code 
     * @param {*} level 
     * @param {*} cb 
     */
    Location.GetLocationByCode =function(code,cb) {

        if(!code) {
            let result = new Results();
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cb(null,result);
        }

        Location.findOne({
            fields:['name', 'level', 'nameWithType', 'path', 'code', 'parentCode'],
            where: {code: code},
            order: 'name ASC',
        },function(err, instance){
            if(err) {
                let result = new Results();
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null,result);
            }

            let result = new Results();
            result.setSuccess(instance);
            return cb(null,result);

        })

    }

    //#endregion

    //#region Expose Parameters
    Location.remoteMethod('GetListFull', {
        http: {
            verb: "GET"
        },
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    Location.remoteMethod('FindByName', {
        description: '@Thiep Wong, find location code by name to synchronize from lat lon',
        http: {
            verb: 'POST',
            path: '/find-by-name'
        },
        accepts: [{
            arg: 'level2',
            type: 'string'
        },
        {
            arg: 'level3',
            type: 'string'
        },
        {
            arg: 'level4',
            type: 'string'
        }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
    
    
    Location.remoteMethod('GetChildByParentCode',{
        description:'@Thiep Wong, get children location array by level and parent code',
        http:{
            verb:'GET',
            path:'/sub-locations/:parentCode/:level'
        },
        accepts:[
            {
                arg:'parentCode',
                type:'string'
            },
            {
                arg:'level',
                type:'number'
            }
        ],
        returns :{
            arg:'result',
            type:'string',
            root:true
        }
    })

    Location.remoteMethod('GetLocationByCode', {
        description:'@Thiep Wong, get only one location, without sub-locations',
        http:{
            verb:'GET',
            path:'/:code/:level'
        },
        accepts:[
            {
                arg:'code',
                type:'string'
            }, 
            {
                arg:'level',
                type:'number'
            }
        ],
        returns:{
            arg:'result',
            
        }
    })

    Location.remoteMethod('GetLocationByCode',{
        description:'@Hung doan, get location by code',
        http:{
            verb:'GET',
            path:'/:code'
        },
        accepts:[
            {
                arg:'code',
                type:'string'
            }
        ],
        returns :{
            arg:'result',
            type:'string',
            root:true
        }
    })

    Location.remoteMethod('AddNew', {
        accepts: {
            arg: "json",
            type: "string"
        },
        returns: {
            arg: "result",
            type: "object",
            root: true
        }
    })
    //#endregion
};