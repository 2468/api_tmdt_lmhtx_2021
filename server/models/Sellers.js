/**
 * Model Members
 * Signup, signin, update infomation for Members model.
 * Author: Thiep Wong
 * Last update: 9.3.2018 9:21
 * Last updated: 29/10/2018
 */
'use strict';
var Results = require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var path = require('path');
var crypto = require('crypto');
var loopback = require("loopback");
var LoopBackContext = require('loopback-context');
var EmailContentHandle = require('./../middlewares/EmailContentHandle');
var app = require('../../server/server');
var SMLSellerCenter = app.get('SMLSellerCenter');
var ApplicationError = require('../middlewares/application-error');
const models = require('./../server').models;

module.exports = function (Sellers) { 
    Sellers.sharedClass.methods().forEach(function (method) {
        Sellers.disableRemoteMethodByName(method.name, method.isStatic);
      });

    // email validation regex
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let _mobile_pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

    Sellers.validatesUniquenessOf('username', { message: 'username đã tồn tại, vui lòng chọn username khác!' });
    Sellers.validatesLengthOf('username', { min: 10, message: { min: 'username không ít hơn 10 ký tự' } });
    Sellers.validatesLengthOf('username', { max: 50, message: { max: 'username không quá 50 ký tự' } });
    Sellers.validatesUniquenessOf('email', { message: 'email đã tồn tại, vui lòng chọn username khác!' });
    Sellers.validatesLengthOf('email', { min: 10, message: { min: 'email không ít hơn 10 ký tự' } });
    Sellers.validatesLengthOf('email', { max: 50, message: { max: 'email không quá 50 ký tự' } });
    Sellers.validatesFormatOf('email', { with: re, message: 'Email không chính xác, vui lòng kiểm tra lại' });
    Sellers.validatesLengthOf('fullname', { min: 5, message: { min: 'Tên người dùng không ít hơn 10 ký tự' } });
    Sellers.validatesLengthOf('fullname', { max: 50, message: { max: 'Tên người dùng không quá 50 ký tự' } });
    Sellers.validatesLengthOf('mobile', { min: 10, message: { min: 'Số điện thoại không ít hơn 10 số' } });
    Sellers.validatesLengthOf('mobile', { max: 15, message: { max: 'Số điện thoại không quá 15 ký tự' } });
    Sellers.validatesFormatOf('mobile', { with: _mobile_pattern, message: 'Số điện thoại không chính xác, vui lòng kiểm tra lại' });


    // Sellers.validatesLengthOf('password', { min: 5, message: { min: 'Password quá ngắn, vui lòng chọn password khác' } });
    // Sellers.validatesLengthOf('password', { max: 50, message: { max: 'Password không được dài hơn 50 ký tự' } });
    //#region SignIn from Smart site

    /** 
     * @param {String} email email as username
     * @param {String} password the password of account
     */
    Sellers.SignIn = function (email, password, cb) {
        let result = new Results();
        if (!email && !password) {
            result.setError(ResultsCodes.USER_NOT_FOUND);
            return cb(null, result);

        }
        // Find an member with info as below
        this.findOne({ fields: { id: true, uuid: true, password: true, username: true, status: true, fullname: true, logoImage: true }, where: { username: email, status: { gte: 1 } } }, function (err, _seller_instance) {
            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.LOGIN_FAILED);
                return cb(null, result);
            }
            if (!_seller_instance) {
                result.setError(ResultsCodes.USER_NOT_FOUND);
                return cb(null, result);
            }
            // Compare password if
            _seller_instance.hasPassword(password, function (err, res) {
                if (err) {

                    let result = new Results();
                    result.setError(ResultsCodes.LOGIN_FAILED);
                    return cb(null, result);
                }

                if (!res) {
                    result.setError(ResultsCodes.PASSWORD_NOT_MATCH);
                    return cb(null, result);
                }

                const JToken = models.JToken;
                let __token = JToken.sign(_seller_instance.uuid, _seller_instance.id, 'sel');
                let RefreshToken = models.RefreshToken;
                RefreshToken.sign(_seller_instance.uuid, _seller_instance.id, 'sel', (err, resToken) => {
                    if (err) {
                        result.setError(err);
                        return cb(null, result);
                    }
                    result.setSuccess({
                        token:

                        {
                            id: __token,
                            ttl: 126000,
                            created: Math.floor(new Date().getTime() / 1000),
                            uuid: _seller_instance.uuid,
                            refresh: resToken
                        }
                        , seller: _seller_instance
                    });
                    cb(null, result);

                }); 

            }) 
        });



    }

    //#endregion

    //#region  SignUp as a seller
    /**
     * @param {*} mobile 
     * @param {*} email 
     * @param {*} password 
     * @param {*} seller_type 
     * @param {*} api_token 
     * @param {*} cb callback function
     */
    Sellers.SignUp = function (fullname, mobile, email, password, sellerType, platformCodes,activeUrl, cb) {

        const makeRequest = async () => {

            if (!mobile || !email || !password || !fullname) {
                // Callback an error
                let result = new Results();
                result.setError(ResultsCodes.SELLER_INFO_INVALID);
                return cb(null, result);
            }
            else {

                app.dataSources.mysqld.transaction(async models => {
                    const { Sellers, SellerPlatform } = models;

                    // Save infomation to database
                    let seller = new Sellers;
                    seller.fullname = fullname;
                    seller.username = email;
                    seller.password = password;
                    seller.mobile = mobile;
                    seller.email = email;
                    seller.sellerType = sellerType;
                    seller.createTime = parseInt((new Date().getTime() / 1000.0));
                    seller.sellerSource = 1; // Create from B2C platform
                    seller.status = 0;

                    let _token = crypto.createHash('RSA-SHA512');
                    _token.update(email, "ascii");
                    _token.update("smartlife"); // Secret keyword
                    seller.verificationToken = _token.digest('hex');

                    const sellerInstance = await Sellers.create(seller);

                    // create seller platform
                    for (var index in platformCodes) {
                        await CreateSellerPlatform(models, sellerInstance.id, platformCodes[index], sellerInstance.id, sellerInstance.id);
                    }
                    // await platformCodes.forEach(platformCode => {
                    // });

                    let verify_msg = {

                        heading: EmailContentHandle.REGISTRATION_SUCCESS.heading + seller.fullname,
                        content: EmailContentHandle.REGISTRATION_SUCCESS.content,
                        verify_link: activeUrl + seller.verificationToken + '/' + sellerInstance.id
                    };

                    let renderer = loopback.template(path.resolve(__dirname, '../../server/views/verify.ejs'));
                    let html_body = renderer(verify_msg);

                    let options = {
                        to: email,
                        from: 'no-reply@smartlifevn.com',
                        subject: EmailContentHandle.REGISTRATION_SUCCESS.title,
                        html: html_body
                    };

                    //Verify the signup email
                    // Send the verify code to seller's email.

                    let mail = await new Promise((resolve, reject) => {

                        Sellers.app.models.Email.send(options, function (err, mail) {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(mail);
                            }
                        });
                    });


                    let result = new Results();
                    result.setSuccess(
                        {
                            data: {
                                seller: sellerInstance,
                                email: mail
                            }
                        }
                    );

                    return cb(null, result);

                }).catch(function (err) {
                    let result = new Results();
                    if (err.name === 'ValidationError') {
                        err.code = ResultsCodes.VALIDATION_ERROR.code;
                        err.name = ResultsCodes.VALIDATION_ERROR.name;
                        result.setError(err);
                    }
                    else {
                        result.setError(ResultsCodes.SYSTEM_ERROR);
                    }

                    return cb(null, result);
                });
            }

        } // end of else

        makeRequest().catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
        });
    }

    async function CreateSellerPlatform(models, sellerId, platformCode, createBy, modifyBy) {
        const { SellerPlatform } = models;
        let newDate = parseInt(new Date().getTime() / 1000.0);

        let instance = await SellerPlatform.findOne({
            where: {
                'sellerId': sellerId,
                'platformCode': platformCode
            }
        });

        if (instance) { // if exists change status to 1
            instance.modifyBy = modifyBy;
            instance.modifyTime = newDate;
            instance.status = 0;
            await instance.save();
        } else {

            // create new record
            let sellerPlatform = new SellerPlatform;
            sellerPlatform.sellerId = sellerId;
            sellerPlatform.platformCode = platformCode;
            sellerPlatform.status = 0;
            sellerPlatform.approvedBy = null;
            sellerPlatform.approvedTime = null;
            sellerPlatform.createBy = createBy;
            sellerPlatform.createTime = parseInt(new Date().getTime() / 1000.0);
            await SellerPlatform.create(sellerPlatform);
        }
    }

    async function ActiveSellerPlatforms(models, sellerId, modifyBy, modifyTime) {
        const { SellerPlatform } = models;

        let sellerPlatforms = await SellerPlatform.find({
            where: {
                'sellerId': sellerId
            }
        });

        if (sellerPlatforms && sellerPlatforms.length > 0) {
            for (var index in sellerPlatforms) {
                let sellerPlatform = sellerPlatforms[index];
                sellerPlatform.modifyBy = modifyBy;
                sellerPlatform.modifyTime = modifyTime;
                sellerPlatform.status = 1;
                await sellerPlatform.save();
            }
        }
    }

    //#endregion

    //#region Email Verify
    /**
     * @param {String} EmailToken The token of email
     */
    Sellers.EmailVerify = function (SellerId, EmailToken, cb) {

        app.dataSources.mysqld.transaction(async models => {
            const { Sellers, SellerPlatform, SellerToken, Products } = models;

            if (!EmailToken || !SellerId) {
                let result = new Results();
                result.setError(ResultsCodes.SELLER_INFO_INVALID);
                return cb(null, result);
            }
            else {

                const sellerInstance = await Sellers.findById(SellerId);
                // console.log(sellerInstance);
                if (!sellerInstance) {
                    let result = new Results();
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                } else {
                    if (sellerInstance.verificationToken == EmailToken) {

                        let newDate = parseInt((new Date().getTime()) / 1000);
                        // sellerInstance.status = 1;
                        // sellerInstance.verificationToken = null;
                        await sellerInstance.updateAttributes({ status: 1, verificationToken: null });

                        // var sellerToken = new SellerToken;
                        // sellerToken.userId = sellerInstance.id;
                        // sellerToken.ttl = 126000;
                        // sellerToken.expired = newDate + sellerToken.ttl;
                        // console.log(sellerToken);
                        // Day la cai ham tao token mac dinh cua loopback, da bo ko dung thang nay nua.
                        // const tokenInstance = await sellerInstance.createAccessToken(sellerToken);
                        const JToken = models.JToken;
                        let __token = JToken.sign(sellerInstance.uuid, sellerInstance.id, 'sel');
                        console.log(__token);

                        // let platformCodes = await SellerPlatform.find({
                        //     fields: ['platformCode'],
                        //     where: {
                        //         'sellerId': SellerId
                        //     }
                        // });

                        // console.log(platformCodes);
                        await ActiveSellerPlatforms(models, sellerInstance.id, sellerInstance.id, newDate);

                        // update ES document
                        await Products.UpdateDocumentBySellerId(SellerId, models);

                        let result = new Results();
                        result.setSuccess({
                            token: __token,
                            seller: sellerInstance
                        });
                        return cb(null, result);
                    }
                    else {

                        let result = new Results();
                        result.setError(ResultsCodes.EMAIL_VERIFYCODE_INVALID);
                        return cb(null, result);
                    }
                }
            }
        }).catch(function (e) {
            console.log(e);
            let result = new Results();

            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });;
    }

    //#region Update Seller Status
    /**
     * @param {String} status The status of seller
     */
    Sellers.UpdateStatus = function (ctx, cb) {

        let sellerId = ctx.query.sellerId || 0 ;
        let  status = ctx.query.status || 0;

        let result = new Results();
        app.dataSources.mysqld.transaction(async models => {
            const { Sellers, Passport, Products } = models;

            // let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            let ownerInstance = await Sellers.findById(token.userId);
            if (!ownerInstance) {
                throw new ApplicationError(ResultsCodes.USER_NOT_FOUND);
            }

            if (ownerInstance.username !== 'xuanhungttm@gmail.com') {
                throw new ApplicationError(ResultsCodes.NO_PERMISSTION_ERROR);
            }

            let sellerInstance = await Sellers.findById(sellerId);
            if (!sellerInstance) {
                throw new ApplicationError(ResultsCodes.USER_NOT_FOUND);
            } else if (sellerInstance.status == 0) {// not active yet {
                throw new ApplicationError(ResultsCodes.SELLER_NO_ACTIVE_ERROR);
            } else {

                if (status != 1 && status != 2 && status != 3) {
                    let error = ResultsCodes.VALIDATION_ERROR;
                    error.message = 'status phải là 1 hoặc 2 hoặc 3';
                    throw new ApplicationError(ResultsCodes.VALIDATION_ERROR);
                }

                if (sellerInstance.status == 1) {
                    if (status != 2 && status != 3) {
                        let error = ResultsCodes.VALIDATION_ERROR;
                        error.message = 'Không thể chuyển từ status ' + sellerInstance.status + ' sang ' + status;
                        throw new ApplicationError(ResultsCodes.VALIDATION_ERROR);
                    }
                } else if (sellerInstance.status == 2) {
                    if (status != 1 && status != 3) {
                        let error = ResultsCodes.VALIDATION_ERROR;
                        error.message = 'Không thể chuyển từ status ' + sellerInstance.status + ' sang ' + status;
                        throw new ApplicationError(ResultsCodes.VALIDATION_ERROR);
                    }
                } else if (sellerInstance.status == 3) {
                    if (status != 1 && status != 2) {
                        let error = ResultsCodes.VALIDATION_ERROR;
                        error.message = 'Không thể chuyển từ status ' + sellerInstance.status + ' sang ' + status;
                        throw new ApplicationError(ResultsCodes.VALIDATION_ERROR);
                    }
                }

                await sellerInstance.updateAttributes({ status: status, modifyBy: token.userId, modifyTime: new Date().getTime() / 1000.0 });

                // update ES document
                await Products.UpdateDocumentBySellerId(sellerId, models);
            }
 
            result.setSuccess(true);
            return cb(null, result);

        }).catch(function (e) { 
            if (e instanceof ApplicationError) {
                result.setError(e.error);
            } else {
                result.setError({
                    message: e.message
                });
            }

            return cb ? cb(null, result) : result;
        });
    }

    //#endregion

    //#region  Reset Password
    Sellers.ResetPassword = function (email,resetUrl, cb) {
        if (email) {
            this.findOne({ fields: { id: true, username: true, email: true, mobile: true, password: true, status: true, fullname: true, verificationToken: true }, where: { email: email, status: { gte: 0 } } }, function (err, _sellerInstance) {

                if (err) {
                    let result = new Results();
                    result.setError(ResultsCodes.EMAIL_INVALID)
                    return cb(null, result);
                }
                else {
                    if (!_sellerInstance) {
                        let result = new Results();
                        result.setError(ResultsCodes.USER_NOT_FOUND)
                        return cb(null, result);
                    }

                    var _token = crypto.createHash('RSA-SHA512');
                    _token.update(email, "ascii");
                    _token.update((new Date().getTime()).toString()); // Secret keyword
                    _sellerInstance.updateAttributes({ verificationToken: _token.digest('hex') }, function (err, _sellerInt) {
                        if (err) {
                            let result = new Results();
                            result.setError(ResultsCodes.EMAIL_INVALID)
                            return cb(null, result);
                        }
                        else {

                            var verify_msg = {

                                heading: "Xin chào " + _sellerInstance.fullname,
                                content: "Bạn đã xác nhận reset mật khẩu hệ thống",
                                verify_link: resetUrl + _sellerInstance.verificationToken
                            };

                            var renderer = loopback.template(path.resolve(__dirname, '../../server/views/verify.ejs'));
                            var html_body = renderer(verify_msg);

                            var options = {
                                to: email,
                                from: 'no-reply@smartlifevn.com',
                                subject: 'Thay đổi mật khẩu tại Smartlife',
                                html: html_body
                            };

                            //Verify the signup email
                            // Send the verify code to seller's email.
                            Sellers.app.models.Email.send(options, function (err, mail) {

                                if (err) {
                                    return cb(null, 'error');
                                }
                                let result = new Results();
                                result.setSuccess({
                                    data: {
                                        seller: _sellerInstance,
                                        email: mail
                                    }
                                });
                                return cb(null, result);
                            });

                        }
                    });

                }

            });

        }
        else {
            let result = new Results();
            result.setError(ResultsCodes.EMAIL_INVALID);
            return cb(null, result);
        }
    }
    //#endregion

    Sellers.CreateNewPassword = function (resetToken, newPassword, cb) {
        if (!resetToken) {
            let result = new Results();
            result.setError(ResultsCodes.USER_NOT_FOUND);
            return cb(null, result);
        }

        Sellers.findOne({ where: { verificationToken: resetToken, status: { gte: 0 } } }, function (err, _sellerInstance) {

            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }

            if (!_sellerInstance) {
                let result = new Results();
                result.setError(ResultsCodes.USER_NOT_FOUND);
                return cb(null, result);
            }

            _sellerInstance.updateAttributes({ password: Sellers.hashPassword(newPassword), verificationToken: null, status: 1 }, function (err, _inst) {
                if (err) {
                    let result = new Results();
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }

                let result = new Results();
                result.setSuccess(ResultsCodes.PASSWORD_RESET_SUCCESS);
                cb(null, result);
            });



        });
    }

    Sellers.getBankInfo = function (ctx,cb) {

        const makeInfoRequest = async () => {

            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(token.userId, { fields: { 'bankAccountHolder': true, 'bankAccountNumber': true, 'bankName': true, 'bankBranch': true } }, function (err, _sellerInst) {
                if (err) {
                    let result = new Results();
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }

                let result = new Results();
                result.setSuccess({ data: _sellerInst });
                cb(null, result);
            })


        }
        makeInfoRequest();
    }

    Sellers.updateBankInfo = function (ctx, cb) {
        let holderName = ctx.body.holderName || null; 
        let bankAccount = ctx.body.bankAccount || null; 
        let bankName = ctx.body.bankName || null; 
        let bankBranch = ctx.body.bankBranch || null;

        let result = new Results();
        const makeInfoRequest = async () => {
            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(token.userId, function (err, _sellerInst) {
                if (err) { 
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }

                _sellerInst.updateAttributes({
                    bankAccountHolder: holderName,
                    bankAccountNumber: bankAccount,
                    bankName: bankName,
                    bankBranch: bankBranch
                }, function (err, _inst) {

                    if (err) { 
                        result.setError(ResultsCodes.USER_NOT_FOUND);
                        return cb(null, result);
                    } 
                    result.setSuccess({
                        data: {
                            bankAccountHolder: _sellerInst.bankAccountHolder,
                            bankAccountNumber: _sellerInst.bankAccountNumber,
                            bankName: _sellerInst.bankName,
                            bankBranch: _sellerInst.bankBranch
                        }
                    });
                    cb(null, result);
                });

            })
        }

        makeInfoRequest();

    }

    Sellers.getBusinessInfo = function (ctx,cb) {

        const makeInfoRequest = async () => {

            let result = new Results();
            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(token.userId, { fields: { 'companyName': true, 'companyAddress': true, 'taxCode': true, 'sellerType': true, 'certificateImages': true } }, function (err, _sellerInst) {
                if (err) { 
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }
 
                result.setSuccess({ data: _sellerInst });
                cb(null, result);
            })


        }
        makeInfoRequest();
    }

    Sellers.updateBusinessInfo = function (ctx, cb) {

        let companyName = ctx.body.companyName || null; 
        let taxCode = ctx.body.taxCode || null; 
        let companyAddress = ctx.body.companyAddress || null; 
        let  businessType = ctx.body.businessType || 0;
        let images = ctx.body.images || null;

        let result = new Results();
        const makeInfoRequest = async () => {
            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(token.userId, function (err, _sellerInst) {
                if (err) { 
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }

                _sellerInst.updateAttributes({
                    companyName: companyName,
                    taxCode: taxCode,
                    companyAddress: companyAddress,
                    certificateImages: images,
                    sellerType: businessType
                }, function (err, _inst) {

                    if (err) { 
                        result.setError(ResultsCodes.USER_NOT_FOUND);
                        return cb(null, result);
                    } 
                    result.setSuccess({
                        data: {
                            companyName: _sellerInst.companyName,
                            taxCode: _sellerInst.taxCode,
                            companyAddress: _sellerInst.companyAddress,
                            certificateImages: _sellerInst.certificateImages,
                            businessType: _sellerInst.sellerType
                        }
                    });
                    cb(null, result);
                });

            })
        }

        makeInfoRequest();
    }

    Sellers.updateShopInfo = function (ctx, cb) {
        let shopName = ctx.body.shopName || null; 
        let shopAddress = ctx.body.shopAddress || null; 
        let  shopAvatar = ctx.body.shopAvatar ||null; 
        let shopCover = ctx.body.shopCover || null; 
        let  about = ctx.body.about || null; 
        let  facebook =  ctx.body.facebook || null; 
        let result = new Results();
        const makeInfoRequest = async () => {
            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) { 
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(token.userId, function (err, _sellerInst) {
                if (err) { 
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }

                _sellerInst.updateAttributes({
                    shopName: shopName,
                    shopAddress: shopAddress,
                    coverImage: shopCover,
                    logoImage: shopAvatar,
                    about:about,
                    faceBook:facebook
                }, function (err, _inst) {

                    if (err) { 
                        result.setError(ResultsCodes.USER_NOT_FOUND);
                        return cb(null, result);
                    } 
                    result.setSuccess({ 
                        shopName: _sellerInst.shopName,
                        shopAddress: _sellerInst.shopAddress,
                        shopCover: _sellerInst.coverImage,
                        shopAvatar: _sellerInst.logoImage,
                        about:_sellerInst.about,
                        faceBook:facebook
                    });
                    cb(null, result);
                });

            })


        }
        makeInfoRequest();
    }


    Sellers.updateShopAbout = function (ctx, cb) {
        let about = ctx.body.about || null;  
        let result = new Results(); 
        const makeInfoRequest = async () => {
            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(token.userId, function (err, _sellerInst) {
                if (err) { 
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }

                _sellerInst.updateAttributes({
                    about: about,
                }, function (err, _inst) {

                    if (err) { 
                        result.setError(ResultsCodes.USER_NOT_FOUND);
                        return cb(null, result);
                    } 
                    result.setSuccess({
                        about: about
                    });
                    cb(null, result);
                });
            })
        }
        makeInfoRequest();
    }


    Sellers.getShopInfo = function (ctx,cb) { 

        let result = new Results();
        const makeInfoRequest = async () => {
            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(token.userId, function (err, _sellerInst) {
                if (err) { 
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }
 
                result.setSuccess({
                    shopName: _sellerInst.shopName,
                    shopAddress: _sellerInst.shopAddress,
                    shopAvatar: _sellerInst.logoImage,
                    shopCover: _sellerInst.coverImage,
                    about:_sellerInst.about,
                    facebook:_sellerInst.faceBook
                });
                cb(null, result);

            });
        }

        makeInfoRequest();
    }

    Sellers.GetListShops = function (platformCode, content, cb) {
        let sql = `SELECT 
            s.uuid,
            s.shop_name AS shopName,
            s.logo_image AS logoImage
        FROM 
            sellers s
        JOIN
            seller_platform sp ON sp.seller_id = s.id
        WHERE
            s.status > 1
            AND sp.status in (1, 2, 3)           
            AND sp.platform_code = ? 
            ORDER BY shopName ASC `;
        var params = [platformCode];
        let connector = Sellers.dataSource.connector;
        connector.query(sql, params, function (err, resultObjects) {

            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.DATA_NOT_FOUND);
                return cb(null, result);
            }
            else {
                let result = new Results();
                result.setSuccess(resultObjects);
                return cb(null, result);
            }
        });
    }

    Sellers.GetShopDetails = function (uuid, platformCode, cb) {

        let result = new Results();
        let sellerRaw = {};

        Sellers.findOne({
            fields: ['id', 'uuid', 'shopName', 'fullname', 'mobile', 'email', 'shopAddress', 'shopLocationCode', 'certificateImages', 'logoImage', 'coverImage', 'about', 'createTime', 'status'],
            where: {
                "uuid": uuid
            }
        }, function (err, seller) {

            if (err) {
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }

            if (seller == null) {
                result.setError(ResultsCodes.ITEM_NOT_FOUND);
                return cb(null, result);
            }

            sellerRaw.uuid = seller.uuid;
            sellerRaw.shopName = seller.shopName;
            sellerRaw.fullName = seller.fullname;
            sellerRaw.mobile = seller.mobile;
            sellerRaw.email = seller.email;
            sellerRaw.shopAddress = seller.shopAddress;
            sellerRaw.shopLocationCode = seller.shopLocationCode;
            sellerRaw.certificateImages = seller.certificateImages;
            sellerRaw.logoImage = seller.logoImage;
            sellerRaw.coverImage = seller.coverImage;
            sellerRaw.about = seller.about;
            sellerRaw.createTime = seller.createTime;
            sellerRaw.status = seller.status;

            let SellerRating = Sellers.app.models.SellerRating;
            SellerRating.countRatingByStar(seller.id, function (err, resultsCountRating) {
                sellerRaw.countRatingByStar = resultsCountRating;

                SellerRating.ratingPercent(seller.id, function (err, resultsAverage) {
                    sellerRaw.ratingPercentage = resultsAverage[0].ratingPercent;

                    var defaultFlag = 0;
                    var sql = `SELECT COUNT(1) as CountItem 
                                FROM product_seller ps
                                JOIN
                                product_seller_platform psp ON psp.product_seller_id = ps.id
                                WHERE ps.seller_id = ? 
                                AND ps.status = ?
                                AND psp.platform_code = ?`;
                    var params = [seller.id, 1, platformCode];
                    var connector = Sellers.dataSource.connector;
                    connector.query(sql, params, function (err, resultObjects) {
                        if (resultObjects.length > 0) {
                            sellerRaw.countItem = resultObjects[0].CountItem;

                            result.setSuccess(sellerRaw);
                            cb(null, result);
                        }
                    });
                })
            })

            // let result = new Results();
            // return cb(null, result);
        })
    }

    Sellers.GetShopInfo = function (id, platformCode, cb) {
        let result = new Results();
        let sellerRaw = {};

        Sellers.findOne({
            fields: ['id', 'uuid', 'shopName', 'fullname', 'mobile', 'email', 'shopAddress', 'shopLocationCode', 'certificateImages', 'logoImage', 'coverImage', 'about', 'createTime', 'status','faceBook'],
            where: {
                "id": id
            }
        }, function (err, seller) { 

            if (err) {
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }

            if (seller == null) {
                result.setError(ResultsCodes.ITEM_NOT_FOUND);
                return cb(null, result);
            }

            sellerRaw.uuid = seller.uuid;
            sellerRaw.shopName = seller.shopName;
            sellerRaw.fullName = seller.fullname;
            sellerRaw.mobile = seller.mobile;
            sellerRaw.email = seller.email;
            sellerRaw.shopAddress = seller.shopAddress;
            sellerRaw.shopLocationCode = seller.shopLocationCode;
            sellerRaw.certificateImages = seller.certificateImages;
            sellerRaw.logoImage = seller.logoImage;
            sellerRaw.coverImage = seller.coverImage;
            sellerRaw.about = seller.about;
            sellerRaw.createTime = seller.createTime;
            sellerRaw.status = seller.status;
            sellerRaw.facebook = seller.faceBook;


            let SellerRating = Sellers.app.models.SellerRating;
            SellerRating.countRatingByStar(seller.id, function (err, resultsCountRating) {
                sellerRaw.countRatingByStar = resultsCountRating;

                SellerRating.ratingPercent(seller.id, function (err, resultsAverage) {
                    sellerRaw.ratingPercentage = resultsAverage[0].ratingPercent;
 
                    var sql = `SELECT COUNT(1) as CountItem 
                                FROM product_seller ps
                                JOIN
                                product_seller_platform psp ON psp.product_seller_id = ps.id
                                WHERE ps.seller_id = ? 
                                AND ps.status = ?
                                AND psp.platform_code = ?`;
                    var params = [seller.id, 1, platformCode];
                    var connector = Sellers.dataSource.connector;
                    connector.query(sql, params, function (err, resultObjects) {
                        if (resultObjects.length > 0) {
                            sellerRaw.countItem = resultObjects[0].CountItem;

                            result.setSuccess(sellerRaw);
                            cb(null, result);
                        }
                    });
                })
            })
 
        })
    }

    Sellers.GetListHotShops = function (platformCode, cb) {

        // @Thiep Comment seller status has 3 states: 0 un-verify; 1 verified; 2 approve as sellers
        //11-11-2019  @Thienvd Lay shop đủ thông tin tên,logo,cover mới cho lên trang chủ
        let sql = `SELECT 
            distinct(s.id), 
            s.uuid,
            s.shop_name AS shopName,
            s.logo_image AS logoImage,     
            s.cover_image AS coverImage,
            s.order
            
        FROM 
                sellers s
        JOIN
            seller_platform sp ON sp.seller_id = s.id
        WHERE
            s.status = 2 
            AND s.shop_name is not null
            AND s.logo_image is not null
            AND  s.cover_image is not null
            AND sp.status = ?
            AND sp.platform_code = ? ORDER BY s.order DESC`

            //AND sp.platform_code = ? ORDER BY s.order , s.id DESC`
            
            //thienvd 09/04/2020:  distinct(s.id),   sửa AND sp.platform_code = ? ORDER BY s.order DESC`;


        var params = [2, platformCode];
        let connector = Sellers.dataSource.connector;
        connector.query(sql, params, function (err, resultObjects) {

            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {
                let result = new Results();
                result.setSuccess(resultObjects);
                return cb(null, result);
            }
        });
    }



    Sellers.GetShops = function (platformCode, content, status, pageIndex, pageSize, pageOrder, pageFilter, cb) {

        if (status.length == 0) {
            let result = new Results();
            let error = ResultsCodes.VALIDATION_ERROR;
            error.message = 'status phải là mảng số nguyên có độ dài lớn hơn 0';
            result.setError(error);
            return cb(null, result);
        }

        for (let i = 0; i < status.length; i++) {
            if (isNaN(status[i])) {
                let result = new Results();
                let error = ResultsCodes.VALIDATION_ERROR;
                error.message = 'status phải là mảng số nguyên ';
                result.setError(error);
                return cb(null, result);
            }
        }

        if (pageSize) {
            if (pageSize <= 0) {
                pageSize = 10;
            }
        } else {
            pageSize = 10;
        }

        if (pageIndex) {
            if (pageIndex < 1) {
                pageIndex = 1;
            }
        } 
        else {
            pageIndex = 1;
        }

        let offset = (pageIndex - 1) * pageSize;

        let sql = `SELECT 
                            distinct(s.id), 
                            s.uuid,
                            s.shop_name AS shopName,
                            s.logo_image AS logoImage,     
                            s.cover_image AS coverImage,
                            GROUP_CONCAT(  ps.id 
                                SEPARATOR ';') sellerProductId,
                                
                                                                    GROUP_CONCAT(p.id
                                SEPARATOR ';') productIds,
                                
                                                GROUP_CONCAT(p.product_name
                                SEPARATOR ';') productNames,
                                
                                                GROUP_CONCAT(p.product_image
                                SEPARATOR ';') productImages
                            
                        FROM 
                            sellers s
                        JOIN
                            seller_platform sp ON sp.seller_id = s.id
                            LEFT JOIN product_seller ps on ps.seller_id = s.id
                                LEFT JOIN products p on p.id = ps.product_id
                        WHERE
                            s.status > 1
                            AND sp.status in (?)
                            AND sp.platform_code = ? `;

                            if (content != "undefined" && content != null) 
                            {
                                sql += ` AND s.shop_name like  '%${content}%' GROUP BY s.id ORDER BY s.order DESC `
                    
                            }
                            else
                            sql+=`  GROUP BY s.id ORDER BY s.order DESC limit ? offset ?`

        var params = [status, platformCode, pageSize, offset];
        let connector = Sellers.dataSource.connector;      
        
        connector.query(sql, params, function (err, resultObjects) {
           console.log( offset + " " + status + " " + platformCode + " " + sql);
            if (err) {
                let result = new Results();
                result.setError(ResultsCodes.SYSTEM_ERROR);
                return cb(null, result);
            }
            else {
                let result = new Results();
                result.setSuccess(resultObjects);
                return cb(null, result);
            }
        });

    }


    Sellers.getShopAbout = (ctx,cb) => {
        let result = new Results();
        const makeRequest = async () => {
            const Passport = app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            Sellers.findById(token.userId, (err, res) => {

                if (err) {
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }
                result.setSuccess({
                    about: res.about
                })

                cb(null, result)
            })


        }

        makeRequest().catch(err => {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result)
        })


    }

    //#region  Remote method define
    /** Declare remote method for API */

    // Sellers.remoteMethod('GetShops',{
    //     description:'@Thiep Wong, get a list of shops with platform',
    //     http:{
    //         verb:'GET',
    //         path:'/list'
    //     },
    //     accepts:[
    //         {
    //             arg:'platformCode',
    //             type:'string'
    //         }
    //     ],
    //     returns:{
    //         arg:'result',
    //         type:'string',
    //         root:true
    //     }
    // })

    Sellers.remoteMethod('SignIn', {
        description:'@Thiep Wong, Login to Seller System, update by Thiep Wong',
        http: {
            verb: "POST",
            path:'/signin'
        },
        accepts: [{
            arg: "email",
            type: "string"
        },
        {
            arg: "password",
            type: "string"
        }],
        returns: 
            { arg: 'result', type: 'string', root: true }
        
    });

    Sellers.remoteMethod('SignUp', {
        http: {
            verb: "POST"
        },
        accepts: [{
            arg: "fullname",
            type: "string",
            required: true
        },
        {
            arg: "mobile",
            type: "string",
            required: true
        },
        {
            arg: "email",
            type: "string",
            required: true
        },
        {
            arg: "password",
            type: "string",
            required: true

        },
        {
            arg: "sellerType",
            type: "number",
            required: true
        },
        {
            arg: "platformCodes",
            type: "array",
            required: true
        },
        {
            arg:'activeUrl',
            type:'string',
            require:true
        }
        ],
        returns: [
            { arg: 'result', type: 'string', root: true }
        ]
    });

    Sellers.remoteMethod('EmailVerify', {
        description: '@Thiep Wong, verify the email after register success',
        http: {
            verb: 'POST',
            path: '/email-verify'
        },

        accepts: [
            {
                arg: "SellerId",
                type: "number"
            },
            {
                arg: "EmailToken",
                type: "string"
            }],

        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    })

    Sellers.remoteMethod('ResetPassword', {
        description: '@Thiep Wong, send a token of password reset to seller email',
        http: {
            verb: "POST",
            path: '/password-reset'
        },
        accepts: [{
            arg: "email",
            type: "string"
        }, {
            arg:'resetUrl',
            type:'string'
        }],
        returns: {
            arg: "result",
            type: "string",
            root: true
        }
    })

    Sellers.remoteMethod('CreateNewPassword', {
        description: '@Thiep Wong, create a new password after active route link from email.',
        http: {
            verb: 'POST',
            path: '/create-new-password'
        },
        accepts: [
            {
                arg: 'resetToken',
                type: 'string'
            },
            {
                arg: 'newPassword',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('getBankInfo', {
        description: '@Thiep Wong, get bank infomation of seller',
        http: {
            verb: 'GET',
            path: '/bank-info'
        },
        accepts: [
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: "string",
            root: true
        }
    })

    Sellers.remoteMethod('UpdateStatus', {
        description: '@Hung Doan, admin change status',
        http: {
            verb: 'GET',
            path: '/update-status'
        },
        accepts: [
            {
            arg: 'ctx',
            type: 'object',
            http:{
                source:'req'
            }
        } ],
        returns: {
            arg: 'result',
            type: "string",
            root: true
        }
    })


    Sellers.remoteMethod('updateBankInfo', {
        description: '@Thiep Wong, update bank infomation of the seller',
        http: {
            verb: 'POST',
            path: '/bank-info/edit'
        },
        accepts: [

            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            }
     
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('getBusinessInfo', {
        description: '@Thiep Wong, get the business infomation from seller',
        http: {
            verb: 'GET',
            path: '/business-info'
        },
        accepts: [
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('updateBusinessInfo', {
        description: '@Thiep Wong, update business infomation of seller',
        http: {
            verb: 'POST',
            path: '/business-info/edit'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('GetListShops', {
        http: {
            verb: "GET"
        },
        accepts: [{
            arg: 'platformCode',
            type: 'string',
            required: true
        },
    {
            arg: 'content',
            type: 'string',
          
        
    }], returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('GetShopDetails', {
        description:'@Thiep Wong, get detail from uuid of seller',
        http: {
            verb: "GET",
            path: '/detail/:uuid'
        },
        accepts: [{
            arg: 'uuid',
            type: 'string'
        },
        {
            arg: "platformCode",
            type: "string",
            required: true
        }],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('GetShopInfo', {
        description:'@Thiep Wong, get infomation of show, GetShopDetails\' will be predecated next version',
        http: {
            verb: "GET",
            path: '/detailed/:id'
        },
        accepts: [{
            arg: 'id',
            type: 'number'
        },
        {
            arg: "platformCode",
            type: "string",
            required: true
        }],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
 
    Sellers.remoteMethod('GetListHotShops', {
        http: {
            verb: "GET"
        },
        accepts: [{
            arg: 'platformCode',
            type: 'string'
        }],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('GetShops', {
        description: '@Thiep Wong, get all shops by page index, page size and filters',
        http: {
            verb: 'GET',
            path: '/shops'
        },
        accepts: [
            {
                arg: 'platformCode',
                type: 'string',
                required: true
            },
            {
                arg:'content',
                type:'string'
            }
            ,
            {
                arg: 'status',
                type: 'array',
                required: true
            },
            {
                arg: 'pageIndex',
                type: 'number'
            },
            {
                arg: 'pageSize',
                type: 'number'
            },
            {
                arg: 'pageOrder',
                type: 'string'
            },
            {
                arg: 'pageFilter',
                type: 'string'
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Sellers.remoteMethod('updateShopInfo', {
        description: '@Thiep Wong, update the shop infomation',
        http: {
            verb: 'POST',
            path: '/shop-info/edit'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            } 

        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
 
    Sellers.remoteMethod('updateShopAbout', {
        description: '@Thien VD, update the shop about',
        http: {
            verb: 'POST',
            path: '/shop-info/edit-about'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            }

        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })
 
    Sellers.remoteMethod('getShopAbout', {
        description: '@Thien VD get About Shop',
        http: {
            verb: "GET",
            path: '/shop-about'
        },
        accepts: [
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }

    }) 

    Sellers.remoteMethod('getShopInfo', {
        description: '@Thiep Wong, update the shop infomation',
        http: {
            verb: 'GET',
            path: '/shop-info'
        },
        accepts: [
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }

        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })



    // #endregion

    // # Add by HungNV for update new version

    Sellers.remoteMethod('getBankInfoBySellerId', {
        description: '@HungNV, get bank infomation of seller',
        http: {
            verb: 'GET',
            path: '/bank-info-by-sellerId'
        },
        accepts: [
            {
                arg: 'sellerId',
                type: 'string'
            },
            {
                arg:'ctx',
                type:'object',
                http:{
                    source:'req'
                }
            }
        ],
        returns: {
            arg: 'result',
            type: "string",
            root: true
        }
    })

    Sellers.getBankInfoBySellerId = function (sellerId, ctx,cb) {

        const makeInfoRequest = async () => {

            let Passport = Sellers.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                let result = new Results();
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }

            Sellers.findById(sellerId, { fields: { 'bankAccountHolder': true, 'bankAccountNumber': true, 'bankName': true, 'bankBranch': true } }, function (err, _sellerInst) {
                if (err) {
                    let result = new Results();
                    result.setError(ResultsCodes.USER_NOT_FOUND);
                    return cb(null, result);
                }

                let result = new Results();
                result.setSuccess({ data: _sellerInst });
                cb(null, result);
            })


        }
        makeInfoRequest();
    }
}