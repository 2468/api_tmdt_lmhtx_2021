/**
 * Model Members
 * Signup, signin, update infomation for Members model.
 * Author: Thiep Wong
 * Last update: 9.3.2018 9:21
 * Last update: 26/10/2018 5:08PM Update JToken and Refresh token
 */
'use strict';
var Results = require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var path = require('path');
const models = require('./../server').models;

module.exports = function (Members) {

  Members.sharedClass.methods().forEach(function (method) {
    Members.disableRemoteMethodByName(method.name, method.isStatic);
  });


  Members.CommonSignIn = function (commonToken, cb) {

    let result = new Results();
    if (!commonToken) {
      result.setError(ResultsCodes.MEMBER_INFO_INVALID);
      return cb(null, result);
    }
    Members.app.models.Passport.CommonAuth(commonToken, function (err, resInfo) {
      if (err) {
        result.setError(err);
        return cb(null, result);
      }

      //Check the member in db
      if (!resInfo.data) {
        let result = new Results();
        result.setError(ResultsCodes.LOGIN_FAILED);
        return cb(null, result);
      }

      Members.findOne({
        fields: ['id', 'uuid', 'password', 'username', 'status', 'facebookId',
          'googleId', 'email', 'fullname', 'idNumber', 'idCardImg', 'gender', 'birthday', 'locationId', 'address', 'avatar',
          'chatId', 'receiveNewsFlag', 'verificationToken', 'emailVerified'],
        where: { phone: resInfo.data.phone }
      }, function (err, _mem_instance) {

        if (err) {
          result.setError(err);
          return cb(null, result);
        }

        if (_mem_instance) // Already in Db
        {

          const JToken = models.JToken;
          let __token = JToken.sign(_mem_instance.uuid, _mem_instance.id, 'mem');
          let RefreshToken = models.RefreshToken;
          RefreshToken.sign(_mem_instance.uuid, _mem_instance.id, 'mem', (err, resToken) => {
            if (err) {
              result.setError(err);
              return cb(null, result);
            }

            result.setSuccess({
              token:
              {
                id: __token,
                ttl: 126000,
                created: Math.floor(new Date().getTime() / 1000),
                uuid: _mem_instance.uuid,
                refresh: resToken
              }
              , member: _mem_instance
            });
            cb(null, result);

          });

        }
        else  // Addnew member
        {

          let _member = new Members;
          _member.username = resInfo.data.phone;
          _member.phone = resInfo.data.phone;
          _member.fullname = resInfo.data.fullname;
          _member.avatar = resInfo.data.avatar;
          _member.status = 1;
          _member.password = '!@#$%^&';
          _member.facebookId = null;
          _member.googleId = null;
          _member.email = null;
          _member.idNumber = null;
          _member.idCardImg = null;
          _member.gender = null;
          _member.birthday = null;
          _member.locationId = null;
          _member.address = null;
          _member.chatId = null;
          _member.receiveNewsFlag = 0;
          _member.emailVerified = null;

          _member.createTime = parseInt(new Date().getTime() / 1000);
          Members.create(_member, function (err, _mem_instance) {

            if (err) {
              result.setError(err);
              return cb(null, result);
            }

            // Create an token; 
            const JToken = models.JToken;
            let __token = JToken.sign(_mem_instance.uuid, _mem_instance.id, 'mem');
            let RefreshToken = models.RefreshToken;
            RefreshToken.sign(_mem_instance.uuid, _mem_instance.id, 'mem', (err, resToken) => {
              if (err) {
                result.setError(err);
                return cb(null, result);
              }
              result.setSuccess({
                token:
                {
                  id: __token,
                  ttl: 126000,
                  created: Math.floor(new Date().getTime() / 1000),
                  uuid: _mem_instance.uuid,
                  refresh: resToken
                }
                , member: _mem_instance
              });
              cb(null, result);
            });

          });

        }

      })



    })

  }

  //#region Login by Facebook token
  /**
   *  return access token from FB auth;
   */
  Members.FacebookSignIn = function (fbAccessToken, cb) {
    var Passport = Members.app.models.Passport;
    let result = new Results();
    Passport.facebookAuth(fbAccessToken, function (err, _instance) {
      if (err) {
        result.setError(ResultsCodes.LOGIN_FAILED);
        return cb(null, result);
      }

      if (_instance.id) {
        // Check facebookId in db 
        Members.findOne({
          where: {
            facebookId: _instance.id,
            status: 1
          }
        }, function (err, _uinstance) {

          if (err) {
            result.setError(ResultsCodes.LOGIN_FAILED);
            return cb(null, result);
          }
          if (_uinstance) {
            // Create an access token. 
            const JToken = models.JToken;
            let __token = JToken.sign(_uinstance.uuid, _uinstance.id, 'mem');
            let RefreshToken = models.RefreshToken;
            RefreshToken.sign(_uinstance.uuid, _uinstance.id, 'mem', (err, resToken) => {
              if (err) {
                result.setError(err);
                return cb(null, result);
              }

              result.setSuccess({
                token:
                {
                  id: __token,
                  ttl: 126000,
                  created: Math.floor(new Date().getTime() / 1000),
                  uuid: _uinstance.uuid,
                  refresh: resToken
                }
                , member: _uinstance
              });
              cb(null, result);

            });
            // var memberToken = new Members.app.models.MemberToken;
            // memberToken.userId = _uinstance.id;
            // memberToken.ttl = 126000;
            // memberToken.expired = parseInt((new Date().getTime()) / 1000) + memberToken.ttl;
            // _uinstance.createAccessToken(memberToken, function (err, _tokeninstance) {
            //   if (err) {
            //     let result = new Results();
            //     result.setError(ResultsCodes.LOGIN_FAILED);
            //     return cb(null, result);
            //   }
            //   else {
            //     let result = new Results();
            //     result.setSuccess({ token: _tokeninstance, member: _uinstance });
            //     cb(null, result);
            //   }

            // });
          }
          else {
            // Unregistered account, add new account  
            result.setError(ResultsCodes.MOBILE_UNVERIFY);
            cb(null, result);
          }

        });

      }
      else {
        let result = new Results();
        result.setError(ResultsCodes.LOGIN_FAILED);
        cb(null, result);
      }
    });
  }
  //#endregion
  //#region  Sigin by Smartlife site
  /**
   * 
   * @param {*} mobile for sign in  
   * @param {*} password 
   * @param {*} cb 
   */
  Members.SignIn = function (mobile, password, cb) {
    let result = new Results();
    if (mobile && password) {
      // Find an member with info as below
      Members.findOne({
        fields: { id: true, uuid: true, password: true, username: true, fullname: true, avatar: true, phone: true, status: true },
        where: { username: mobile, status: { gte: 1 } }
      }, function (err, _user_instance) {
        if (err) {

          result.setError(err);
          return cb(null, result);
        }
        else {
          if (_user_instance) {
            // Compare password if
            _user_instance.hasPassword(password, function (err, res) {
              if (err) {

                result.setError(err);
                return cb(null, result);
              }
              else {
                if (res) {
                  // Bg tahng seller cung can su dung phuong thuc nay cho dong bo. Khong dung Access Token mac dinh cua loopback nua!
                  const JToken = models.JToken;
                  let __token = JToken.sign(_user_instance.uuid, _user_instance.id, 'mem');
                  let RefreshToken = models.RefreshToken;
                  RefreshToken.sign(_user_instance.uuid, _user_instance.id, 'mem', (err, resToken) => {
                    if (err) {
                      result.setError(err);
                      return cb(null, result);
                    }
                    result.setSuccess({
                      token:

                      {
                        id: __token,
                        ttl: 126000,
                        created: Math.floor(new Date().getTime() / 1000),
                        uuid: _user_instance.uuid,
                        refresh: resToken
                      }
                      , member: _user_instance
                    });
                    cb(null, result);

                  });
                }
                else {
                  result.setError(ResultsCodes.PASSWORD_NOT_MATCH);
                  cb(null, result);

                }
              }
            })

          }
          else {
            result.setError(ResultsCodes.USER_NOT_FOUND);
            cb(null, result);

          }

        }
      });

    }
    else {
      result.setError(ResultsCodes.MEMBER_INFO_INVALID);
      cb(null, result);
    }

  }
  //#endregion
  //#region ChangePassword 

  Members.ChangePassword = function (memberId, oldPassword, password, cb) {
    if (memberId && oldPassword && password) {
      // Check member id and old password
      this.findById(memberId, function (err, _memInstance) {
        if (err) {
          let result = new Results();
          result.setSuccess(ResultsCodes.MEMBER_INFO_INVALID);
          return cb(null, result);
        }
        else {
          if (_memInstance) {

            _memInstance.hasPassword(password, function (err, isMatch) {
              // Check password
              if (isMatch) {
                let result = new Results();
                result.setSuccess(ResultsCodes);
                return cb(null, result);
              }
              else {


              }
            })

          }
          else {
            // No members
            let result = new Results();
            result.setSuccess(ResultsCodes.MEMBER_INFO_INVALID);
            return cb(null, result);
          }

        }
      })
    }
    else {
      let result = new Results();
      result.setSuccess(ResultsCodes.MEMBER_INFO_INVALID);
      return cb(null, result);
    }
  }
  //#endregion

  //#region ResetPassword

  Members.ResetPassword = function (otpMobile, otpCode, password, cb) {
    if (otpMobile && otpCode && password) {
      // Check OTP, if OTP found and 
      var Otp = Members.app.models.OTP;
      Otp.findById(otpCode, function (err, _otpInstance) {
        if (err) {
          let result = new Results();
          result.setError(ResultsCodes.OTP_NOT_FOUND);
          return cb(null, result);
        }
        else {
          if (_otpInstance && _otpInstance.phone == otpMobile) {
            var _now = parseInt(new Date().getTime() / 1000);
            if (_otpInstance.expired < _now) {
              let result = new Results();
              result.setSuccess(ResultsCodes.OTP_EXPIRED);
              return cb(null, result);

            }
            else {
              Members.findOne({ fields: { uuid: true, id: true, username: true, fullname: true, phone: true }, where: { phone: _otpInstance.phone, username: _otpInstance.phone } }, function (err, _memInstance) {

                if (err) {
                  let result = new Results();
                  result.setError(ResultsCodes.MEMBER_INFO_INVALID);
                  return cb(null, result);

                }
                else {
                  _memInstance.updateAttribute('password', Members.hashPassword(password), function (err, OK) {
                    if (err) {
                      let result = new Results();
                      result.setError(ResultsCodes.MEMBER_INFO_INVALID);
                      return cb(null, result);
                    }

                    let result = new Results();
                    result.setSuccess(ResultsCodes.PASSWORD_RESET_SUCCESS);
                    cb(null, result);
                  });


                }

              });
            }

          }
          else {
            let result = new Results();
            result.setError(ResultsCodes.OTP_NOT_FOUND);
            return cb(null, result);
          }
        }

      });

    }
    else {
      let result = new Results();
      result.setError(ResultsCodes.OTP_NOT_FOUND);
      return cb(null, result);
    }

  }
  //#endregion
  //#region  SignUp from smart megaMall
  /**
   * Members Signup
   * Modified by Thiep Wong 04/06/2018
   * @param {*} fullname 
   * @param {*} mobile 
   * @param {*} email 
   * @param {*} subscribe 
   * @param {*} gender 
   * @param {*} birthday 
   * @param {*} cb 
   */
  Members.SignUp = function (fullname, mobile, email, password, subscribe, cb) {
    let result = new Results();

    const makeRequest = async () => {

      if (!mobile) {
        result.setError(ResultsCodes.MEMBER_MOBILE_INVALID);
        return cb(null, result);
      }
      let _member = await Members.findOne({ where: { username: mobile } });

      if (_member) {
        result.setError(ResultsCodes.MEMBER_FOUND);
        return cb(null, result);
      }


      var member = new Members;
      member.username = mobile
      member.phone = mobile;
      member.email = email;
      member.password = password;
      member.fullname = fullname;
      member.receiveNewsFlag = subscribe;
      member.createTime =  parseInt(new Date().getTime() / 1000.0);
      Members.create(member, function (err, memberInstance) {

        var Otp = Members.app.models.OTP;
        if (err) {

          result.setError(ResultsCodes.SIGNUP_FAILED);
          return cb(null, result);
        }
        /**
         * Create and send an otp to member to active account.
         */
        Otp.createNewOTP(memberInstance, function (err, otpInstance) {

          if (err) {
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb(null, result);
          }

          Otp.IRISSend({
            mobile: mobile,
            otp: otpInstance.id
          }, (err, res) => {
            if (err) {
              result.setError(err);
              return cb(null, result);
            }

            if (res['Code'] == 201) {
              result.setSuccess(res);
            } else {
              result.setError(res);
            }

            cb(null, result);
          });

        });

      });
    }

    makeRequest().catch(err => {
      result.setError(err);
      return cb(null, result);
    })


  }
  //#endregion
  /**
   * Members Edit
   * Modified by Hung Doan 31/07/2018
   * @param {*} fullname 
   * @param {*} cb 
   */
  Members.Edit = function (fullname, birthday, receiveNewsFlag, cb) {
    let result = new Results();
    const makeRequest = async () => {
      var Passport = Members.app.models.Passport;
      let token = await Passport.GetMemberToken(cb);
      if (!token || !token.userId) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      let member = await Members.findOne({
        where: {
          id: token.userId
        }
      });

      if (!member || !member.id) {
        result.setError(ResultsCodes.DATA_NOT_FOUND);
        return cb(null, result);
      } else {

        if (!isNullOrBlankOrUndefined(fullname) && !isNullOrBlankOrUndefined(birthday)
          && !(receiveNewsFlag === undefined)) {
          member.fullname = fullname;
          member.birthday = birthday;
          member.receiveNewsFlag = receiveNewsFlag;
          await member.save();

          result.setSuccess(true);
          return cb(null, result);
        } else {
          result.setError(ResultsCodes.VALIDATION_ERROR);
          return cb(null, result);
        }
      }
    };

    makeRequest().catch(e => {
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  }
  Members.Edit2 = function(ctx, cb) {
   let uuid = ctx.body.uuid ;
   let  fullName = ctx.body.fullname || null;
   let  birthday = ctx.body.birthday || null; 
   let  receiveNewsFlag = ctx.body.receiveNewsFlag || null;

     
    let result = new Results();
    if(!fullName){
        result.setError(ResultsCodes.ARG_NOT_FOUND);
        return cb(null,result);
    }
    const makeEdit = async () => {
        let Passport = Members.app.models.Passport;
        let token = await Passport.GetMemberToken(ctx);
        if (!token) {
            result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
            return cb(null, result);
        }

        var memberId = token.userId;

        let MemberInfo = await Members.findOne({
            where: {
                "uuid": uuid
            }
        });

        if (!MemberInfo) { 
            result.setError(ResultsCodes.DATA_NOT_FOUND);
            return cb(null, result);
        }       
        MemberInfo.fullname = fullName;
        MemberInfo.birthday = birthday;
        MemberInfo.receiveNewsFlag = receiveNewsFlag;  
        MemberInfo.save(); 
        result.setSuccess(true);
        return cb(null,result);
    }

    makeEdit().catch (function(e) { 
        result.setError(ResultsCodes.SYSTEM_ERROR);
        return  cb(null, result);
    });
}
  Members.remoteMethod('Edit2', {
    description:'@ThienVD, edit member address information',
    http:{
        verb:'POST',
        path:'/edit2'
    },
    accepts: [
       {
            arg: "ctx",
            type: "object",
            http:{
                source:'req'
            }
        } 
    ],
    returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    
})
  function isNullOrBlankOrUndefined(value) {
    return (value || /^\s*$/.test(value)) ? false : true;
  }
  /**
   * Members Details
   * Modified by Hung Doan 31/07/2018
   * @param {*} cb 
   */
  Members.Details = function (ctx, cb) {

    let result = new Results();
    const makeRequest = async () => {
      var Passport = Members.app.models.Passport;
      let token = await Passport.GetMemberToken(ctx);
      if (!token || !token.userId) {
        result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        return cb(null, result);
      }

      let member = await Members.findOne({
        where: {
          id: token.userId
        }
      });

      if (!member || !member.id) {
        result.setError(ResultsCodes.DATA_NOT_FOUND);
        return cb(null, result);
      } else {
        result.setSuccess(member);
        return cb(null, result);
      }
    };

    makeRequest().catch(function (e) {
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    });
  }

  //#region  OTP Verify with mobile number
  /**
   * 
   * @param {String} otpCode Otp code from frontend
   * @param {*} otpMobile Otp mobile from frontend
   * @param {*} fbAccessToken facebook token
   * @param {*} cb Callback function for result returns
   */
  Members.OTPVerify = function (otpCode, otpMobile, fbAccessToken, cb) {
    let result = new Results();
    if (!otpCode || !otpMobile) {
      result.setError(ResultsCodes.MEMBER_MOBILE_INVALID);
      return cb(null, result);
    }

    Members.app.models.OTP.findById(otpCode, function (err, otps) {
      if (err) {
        result.setError(err);
        return cb(null, result);
      }

      if (!otps.id || otps.phone != otpMobile) {
        result.setError(ResultsCodes.OTP_NOT_FOUND);
        return cb(null, result);
      }

      if (otps.created + otps.ttl < (Math.floor(new Date().getTime() / 1000))) {
        Members.app.models.OTP.deleteById(otps.id, (err) => {
          if (err) {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(result);
          }
          result.setError(ResultsCodes.OTP_EXPIRED);
          return cb(null, result);
        });

      }

      if (fbAccessToken) {
        // Sign In with facebook access token key
        var Passport = Members.app.models.Passport;
        Passport.facebookProfile(fbAccessToken, function (err, res) {
          if (err) {
            result.setError(ResultsCodes.SYSTEM_ERROR);
            return cb(null, result);
          }
          // Insert into members tables if do not exist in database
          var _fb_member = new Members;
          _fb_member.fullname = res.first_name + ' ' + res.last_name;
          _fb_member.username = otpMobile;
          _fb_member.avatar = res.picture.data.url;
          _fb_member.gender = res.gender = "male" ? 1 : 0;
          _fb_member.phone = otpMobile;
          _fb_member.facebookId = res.id;
          _fb_member.status = true;
          _fb_member.password = "!@#$%^&";
          _fb_member.createTime = Math.floor(parseInt(new Date().getTime() / 1000));
          Members.create(_fb_member, function (err, resInstance) {
            if (err) {
              if (err.name = "ValidationError") {
                result.setError(ResultsCodes.FACEBOOK_MOBILE_LINKED)
              } else {
                result.setError(ResultsCodes.SYSTEM_ERROR)
              }

              return cb(null, result);
            }
            const JToken = models.JToken;
            let __token = JToken.sign(resInstance.uuid, resInstance.id, 'mem');
            let RefreshToken = models.RefreshToken;
            RefreshToken.sign(resInstance.uuid, resInstance.id, 'mem', (err, resToken) => {
              if (err) {
                result.setError(err);
                return cb(null, result);
              }
              result.setSuccess({
                token:
                {
                  id: __token,
                  ttl: 126000,
                  created: Math.floor(new Date().getTime() / 1000),
                  uuid: resInstance.uuid,
                  refresh: resToken
                }
                , member: resInstance
              });

              cb(null, result);
            });

          });
        });

      }
      else {
        // Create an accesstoken and auto login
        Members.findOne({ fields: { id: true, uuid: true, password: true, username: true, fullname: true, avatar: true, phone: true, status: true }, where: { username: otpMobile } }, (err, resMember) => {
          if (err) {
            result.setError(err);
            return cb(null, result);
          }
          const JToken = models.JToken;
          let __token = JToken.sign(resMember.uuid, resMember.id, 'mem');
          let RefreshToken = models.RefreshToken;
          RefreshToken.sign(resMember.uuid, resMember.id, 'mem', (err, resToken) => {

            if (err) {
              result.setError(ResultsCodes.VALIDATION_ERROR);
              return cb(null, result);
            }
            result.setSuccess({
              token:
              {
                id: __token,
                ttl: 126000,
                created: Math.floor(new Date().getTime() / 1000),
                uuid: resMember.uuid,
                refresh: resToken
              }
              , member: resMember
            });

            cb(null, result);
          });

        });

        // var _member = new Members;
        // _member.id = otps.userid;
        // var memberToken = new Members.app.models.MemberToken;
        // memberToken.userId = otps.userid;
        // memberToken.ttl = 126000;
        // memberToken.expired = parseInt((new Date().getTime()) / 1000) + memberToken.ttl;
        // _member.createAccessToken(memberToken, function (err, _instance) {
        //   if (err) {
        //     result.setError(err);
        //     return cb(null, result);
        //   }
        //   else {

        //     Results.success = {
        //       data: otps,
        //       token: _instance,
        //       message: ResultsCodes.OTP_FOUND
        //     };
        //     Results.errors = null;
        //     Results.warning = null;
        //     //  Members.app.models.OTP.deleteById(otps.id, cb);
        //     cb(null, Results);
        //   }
        // });

      }

    });


  }
  //#endregion


  //#region  Send an OTP to mobile

  /**
   * Thiep Wong
   * Send OTP to user mobile
   */
  Members.SendOTP = function (mobile, cb) {
    let result = new Results();

    const makeRequest = async () => {

      if (!mobile) {
        result.setError(ResultsCodes.MEMBER_MOBILE_INVALID);
        return cb(null, result);
      }

      let _member = await Members.findOne({ where: { phone: mobile } });

      if (!_member) {
        result.setError(ResultsCodes.USER_NOT_FOUND);
        return cb(null, result);
      }
      var Otp = Members.app.models.OTP;
      var _newmember = new Members;
      _newmember.id = -1;
      _newmember.phone = mobile;

      Otp.createNewOTP(_newmember, function (err, _otp) {
        if (err) {
          result.setSuccess(ResultsCodes.OTP_CREATE_ERR);
          return cb(null, result);
        }

        if (_otp.id) {
          Otp.IRISSend({
            mobile: mobile,
            otp: _otp.id
          }, (err, _res) => {
            if (err) {
              result.setError(ResultsCodes.OTP_CREATE_ERR);
              return cb(result);
            }

            result.setSuccess(ResultsCodes.OTP_SENT);
            cb(null, result);

          });

        }
        else {
          result.setError(ResultsCodes.OTP_CREATE_ERR);
          return cb(null, result);

        }


      })


    }

    makeRequest().catch((err) => {
      result.setError(err);
      return cb(null, result);
    })



  }

  /**
   * SignUp by facebook token
   * Modified by Thiep Wong 04/06/2018
   * @param {*} token 
   * @param {*} mobile 
   * @param {*} otpCode 
   * @param {*} cb 
   */
  Members.FacebookSignUp = function (token, mobile, otpCode, cb) {
    let result = new Results();
    if (!token || !mobile || !otpCode) {
      result.setError(ResultsCodes.FACEBOOK_TOKEN_INVALID);
      return cb(null, result);
    }
    let Passport = Members.app.models.Passport;
    let OTP = Members.app.models.OTP;
    const makeRequest = async () => {

      // Check OTP return if false
      let otp = await OTP.findById(otpCode);
      if (!otp.id || otp.phone != mobile) {
        result.setError(ResultsCodes.OTP_NOT_FOUND);
        return cb(null, result);
      }

      let info = await Passport.facebookProfile(token);
      if (!info.id) {
        result.setError(ResultsCodes.FACEBOOK_TOKEN_INVALID);
        return cb(null, result);
      }

      let _mem = await Members.findOne({
        where: {
          username: mobile,
          phone: mobile
        }
      });

      if (!_mem) {
        // Create new account from fb infomation

        let _member = new Members();
        _member.facebookId = info.id;
        _member.phone = mobile;
        _member.username = mobile;
        _member.password = 'SML001002@#$'; // Default password!
        _member.email = info.email;
        _member.gender = info.gender == 'male' ? 1 : 0;
        _member.fullname = info.first_name + ' ' + info.last_name;
        _member.avatar = info.picture.data.url;
        _member.createTime = new Date().getDate() / 1000;
        _member.status = 1;
        Members.create(_member, (err, memRes) => {
          if (err) {
            result.setError(err);
            return cb(null, result);
          }

          // Create the token
          let memberToken = new Members.app.models.MemberToken;
          memberToken.userId = memRes.id;
          memberToken.ttl = 126000;
          memberToken.expired = parseInt((new Date().getTime()) / 1000) + memberToken.ttl;
          memRes.createAccessToken(memberToken, function (err, _tokeninstance) {
            if (err) {
              let result = new Results();
              result.setError(err);
              return cb(null, result);
            }

            let result = new Results();
            result.setSuccess({
              token: _tokeninstance, member: {
                avatar: memRes.avatar,
                fullname: memRes.fullname,
                phone: memRes.phone,
                status: memRes.status,
                username: memRes.username,
                uuid: memRes.uuid
              }
            });

            cb(null, result);

          });

        });

      } else {

        // Assign fb id to exits account info
        _mem.facebookId = info.id;
        _mem.updateAttributes({ 'facebookId': _mem.facebookId, 'avatar': info.picture.data.url }, (err, resMember) => {
          if (err) {
            result.setError(ResultsCodes.MEMBER_INFO_INVALID);
            return cb(null, result);
          }

          let memberToken = new Members.app.models.MemberToken;
          memberToken.userId = _mem.id;
          memberToken.ttl = 126000;
          memberToken.expired = parseInt((new Date().getTime()) / 1000) + memberToken.ttl;
          _mem.createAccessToken(memberToken, function (err, _tokeninstance) {
            if (err) {

              result.setError(err);
              return cb(null, result);
            }


            result.setSuccess({
              token: _tokeninstance, member: {
                avatar: _mem.avatar,
                fullname: _mem.fullname,
                phone: _mem.phone,
                status: _mem.status,
                username: _mem.username,
                uuid: _mem.uuid
              }
            });

            cb(null, result);

          });

        });
      }
    }

    makeRequest().catch(err => {
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(result);
    });
  }

  /**
   * 
   * @param {*} uuid 
   * @param {*} t 
   * @param {*} cb 
   */
  Members.SignOut = function (uuid, t, cb) {
    let _time = new Date().getTime();
    if (_time - t > 15000) {
      let result = new Results();
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    }
    const Passport = Members.app.models.Passport;
    const makeRequest = async () => {
      let _memberInfo = await Members.findOne({
        fields: ['id', 'uuid'],
        where: {
          uuid: uuid
        }
      });

      if (_memberInfo) {
        // Clear the token 
        let result = new Results();
        result.setSuccess(ResultsCodes.LOGOUT_SUCCESS);
        cb(null, result);

      } else {
        let result = new Results();
        result.setSuccess(ResultsCodes.LOGOUT_SUCCESS);
        cb(null, result);
      }
    }

    makeRequest();
  }


  Members.AccountVerify = (mobile, otpCode, otpTime, cb) => {

    let result = new Results();
    if (!mobile || !otpCode || !otpTime) {
      result.setError(ResultsCodes.VALIDATION_ERROR);
      return cb(null, result);
    }

    let OTP = Members.app.models.OTP;
    OTP.findOne({
      where: {
        id: otpCode,
        phone: mobile
      }

    }, (err, res) => {
      if (err) {
        result.setError(ResultsCodes.OTP_NOT_FOUND);
        return cb(null, result);
      }

      if (!res) {
        result.setError(ResultsCodes.OTP_NOT_FOUND);
        return cb(null, result);
      }


      if (res.expired >= (new Date().getTime() / 1000) > 0 && res.expired >= otpTime) {
        // Ok, update id, and create an token;
        Members.findById(res.userid, (err, memRes) => {
          if (err) {
            result.setError(ResultsCodes.USER_NOT_FOUND);
            return cb(null, result);
          }

          memRes.updateAttribute('status', 1, (err, memUpRes) => {
            if (err) {
              result.setError(ResultsCodes.SYSTEM_ERROR);
              return cb(null, result);
            }

            const JToken = models.JToken;
            let __token = JToken.sign(memUpRes.uuid, memUpRes.id, 'mem');
            let RefreshToken = models.RefreshToken;
            RefreshToken.sign(memUpRes.uuid, memUpRes.id, 'mem', (err, resToken) => {

              if (err) {
                result.setError(ResultsCodes.VALIDATION_ERROR);
                return cb(null, result);
              }
              result.setSuccess({
                token:
                {
                  id: __token,
                  ttl: 126000,
                  created: Math.floor(new Date().getTime() / 1000),
                  uuid: memUpRes.uuid,
                  refresh: resToken
                }
                , member: memUpRes
              });

              cb(null, result);
            });


            // let memberToken = new Members.app.models.MemberToken;
            // memberToken.userId = memUpRes.id;
            // memberToken.ttl = 126000;
            // memberToken.expired = parseInt((new Date().getTime()) / 1000) + memberToken.ttl;
            // memUpRes.createAccessToken(memberToken, function (err, _tokeninstance) {
            //   if (err) {
            //     let result = new Results();
            //     result.setError(err);
            //     return cb(null, result);
            //   }

            //   let result = new Results();
            //   result.setSuccess({
            //     token: _tokeninstance, member: {
            //       avatar: memUpRes.avatar,
            //       fullname: memUpRes.fullname,
            //       phone: memUpRes.phone,
            //       status: memUpRes.status,
            //       username: memUpRes.username,
            //       uuid: memUpRes.uuid
            //     }
            //   });

            //   // OTP.destroyById(res.id);
            //   cb(null, result);
            // });

          })

        });



      } else {

        result.setError(ResultsCodes.OTP_EXPIRED);
        return cb(null, result);

      }



    })



  }


  Members.RefreshToken = (reToken, cb) => {
    let result = new Results();
    if (!reToken) {
      result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
      return cb(null, result);
    }

    const makeRequest = async () => {

      let RefreshToken = models.RefreshToken;
      let _refreshToken = await RefreshToken.getToken(reToken);
      if (!_refreshToken || !_refreshToken.aid) {
        result.setError(ResultsCodes.REFRESH_TOKEN_EXPIRED);
        return cb(null, result);
      }

      let _member = await Members.findById(_refreshToken.aid, {
        fields: ['id', 'uuid', 'password', 'username', 'status', 'facebookId',
          'googleId', 'email', 'fullname', 'idNumber', 'idCardImg', 'gender', 'birthday', 'locationId', 'address', 'avatar',
          'chatId', 'receiveNewsFlag', 'verificationToken', 'emailVerified']
      });

      if (_member) {
        const JToken = models.JToken;
        let __token = JToken.sign(_member.uuid, _member.id, 'mem');
        let RefreshToken = models.RefreshToken;
        RefreshToken.sign(_member.uuid, _member.id, 'mem', (err, resToken) => {
          if (err) {
            result.setError(err);
            return cb(null, result);
          }

          RefreshToken.destroyById(reToken);
          result.setSuccess({
            token:
            {
              id: __token,
              ttl: 126000,
              created: Math.floor(new Date().getTime() / 1000),
              uuid: _member.uuid,
              refresh: resToken
            }
            , member: _member
          });
          cb(null, result);

        });
      }


    }
    makeRequest().catch(err => {
      result.setError(ResultsCodes.SYSTEM_ERROR);
      return cb(null, result);
    })
  }
  //#endregion

  //#region  Declare the remote method
  /**
   * Remote method defined
   * Using accepts:[] to get request parametter
   * Using returns:[] to response result
   */
  Members.remoteMethod('SignIn', {
    http: {
      verb: "post",
      path: '/signin'
    },
    accepts: [{
      arg: "mobile",
      type: "string"
    },
    {
      arg: "password",
      type: "string"
    }],
    returns: [
      { arg: 'result', type: 'string', root: true }
    ]
  });


  Members.remoteMethod('OTPVerify', {
    description: '@Thiep Wong, verify receivered OTP code by sms',
    http: {
      verb: 'POST',
      path: '/otp-code-verify'
    },
    accepts: [{
      arg: "otpCode",
      type: "string"
    },
    {
      arg: "otpMobile",
      type: "string"
    },
    {
      arg: "fbAccessToken",
      type: "string"
    }],
    returns: [
      { arg: 'result', type: 'string', root: true }
    ]
  });

  Members.remoteMethod('FacebookSignIn', {
    http: {
      verb: "post",
      path: '/facebook-signin'
    },
    accepts: {
      arg: "fbAccessToken",
      type: "string"
    },
    returns: [
      { arg: 'result', type: 'string', root: true }
    ]
  });

  Members.remoteMethod('FacebookSignUp', {
    description: '@Thiep Wong, signup from facebook by token',
    http: {
      verb: 'POST',
      path: '/facebook-signup'
    },
    accepts: [{
      arg: 'token',
      type: 'string'
    }, {
      arg: 'mobile',
      type: 'string'
    }, {
      arg: 'otpCode',
      type: 'string'
    }],
    returns: {
      arg: 'result',
      type: 'string',
      root: true
    }
  })

  Members.remoteMethod('SignUp', {
    description: '@Thiep Wong, signup member role with basic infomation, other infomation will be updated after',
    http: {
      verb: "post",
      path: '/signup'
    },
    accepts: [{
      arg: "fullname",
      type: "string"
    },
    {
      arg: "mobile",
      type: "string"
    },
    {
      arg: "email",
      type: "string"

    },
    {
      arg: "password",
      type: "string"
    },
    {
      arg: "subscribe",
      type: "number"
    }
    ],
    returns: [
      { arg: 'result', type: 'string', root: true }
    ]
  });

  Members.remoteMethod('SendOTP', {
    http: {
      verb: "post",
      path: '/send-otp'
    },

    accepts: [
      {
        arg: "mobile",
        type: "string"
      }
    ],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  });

  Members.remoteMethod('ChangePassword', {
    description: '@Thiep Wong, change the password of member ',
    http: {
      verb: "POST",
      path: 'change-password'
    },
    accepts: [{
      arg: "memberId",
      type: "number"
    },
    {
      arg: "oldPassword",
      type: "string"

    },
    {
      arg: "password",
      type: "string"

    }],

    returns: {
      arg: "result",
      root: true
    }
  });

  Members.remoteMethod('CommonSignIn', {
    description: '@Thiep Wong, sign in from traceability app',
    http: {
      verb: "POST",
      path: '/cross-platform-signin'
    },
    accepts: [{
      arg: "commonToken",
      type: "string"
    }],
    returns: {
      arg: "result",
      type: "string",
      root: true
    }
  }
  )

  Members.remoteMethod('ResetPassword', {
    http: {
      verb: "POST",
      path: '/reset-password'
    },

    accepts: [{
      arg: "otpMobile",
      type: "string"
    },
    {
      arg: "otpCode",
      type: "string"
    },
    {
      arg: "password",
      type: "string"
    }],
    returns: {
      arg: "result",
      root: true
    }
  })

  Members.remoteMethod('SignOut', {
    description: '@Thiep Wong, signout system with access token key and time',
    http: {
      verb: 'POST',
      path: '/signout/:uuid'
    },
    accepts: [
      {
        arg: 'uuid',
        type: 'string'
      },
      {
        arg: 't',
        type: 'number'
      }
    ],
    returns: {
      arg: 'result',
      type: 'string',
      root: true
    }
  })

  Members.remoteMethod('Edit', {
    description: '@Hung Doan, to edit the member infomation',
    http: {
      path: '/edit',
      verb: 'PUT'
    },
    accepts: [
      {
        arg: 'fullname',
        type: 'string',
        required: true
      },
      {
        arg: 'birthday',
        type: 'number',
        required: true
      },
      {
        arg: 'receiveNewsFlag',
        type: 'Boolean',
        required: true
      }
    ],
    returns: {
      arg: 'result',
      type: 'string',
      root: true
    }
  })

  Members.remoteMethod('Details', {
    description: '@Hung Doan, to view the member information, updated by Thiep Wong ',
    http: {
      path: '/details',
      verb: 'GET'
    },
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'req'
        }
      }
    ],
    returns: {
      arg: 'result',
      type: 'string',
      root: true
    }
  })
  Members.remoteMethod('AccountVerify', {
    description: '@Thiep Wong, verify account after registered',
    http: {
      verb: 'POST',
      path: '/account-verify'
    },
    accepts: [
      {
        arg: 'mobile',
        type: 'string'
      },
      {
        arg: 'otpCode',
        type: 'string'
      },
      {
        arg: 'otpTime',
        type: 'number'
      }
    ],
    returns: {
      type: 'string',
      root: true
    }
  })
  Members.remoteMethod('RefreshToken', {
    description: '@Thiep Wong, to refresh a new token after access token expired. if receivered refresh token expired too, return to login method',
    http: {
      verb: 'GET',
      path: '/refresh-token'
    },
    accepts: [
      {
        arg: 'reToken',
        type: 'string'
      }
    ],
    returns: {
      arg: 'result',
      type: 'string',
      root: true
    }
  })

  Members.remoteMethod('MemberInfo', {
    description: '@Thien VD, lấy danh sách thành viên có đặt hàng tại shop',
    http: {
      verb: 'GET',
      path: '/getInfoMemberOrders'
    },
    accepts: [
     
      {
        arg: 'content',
        type: 'string'

      }, 
      {
        arg: 'uuid',
        type: 'string'
      },
      {
        arg: 'pageIndex',
        type: 'number'
    },
    {
        arg: 'pageSize',
        type: 'number'
    },
 
      {
          arg: 'ctx',
          type: 'object',
          http: {
              source: 'req'
          }
      }    
    ],
    returns: {
      arg: 'result',
      type: 'string',
      root: true
    }
  })

  Members.MemberInfo = function ( content,uuid,inPageIndex,inPageSize, ctx, cb) {

    //let pageIndex = ctx.query.pageIndex || 1;
    let pageIndex = inPageIndex || 0;
    let pageSize = inPageSize || 10;
    // let pageOrder = ctx.query.pageOrder || null; 
    

    let result = new Results();
    const makeRequest = async () => {
        let Passport = Members.app.models.Passport;
        // let token = await Passport.GetMemberToken(ctx);
        // if (!token) {
        //     let result = new Results();
        //     result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
        //     return cb(null, result);
        // }  
        if (pageSize) {
            if (pageSize <= 0) {
                pageSize = 10;
            }
        } else {
            pageSize = 10;
        }
        if (pageIndex) {
            if (pageIndex < 1) {
                pageIndex = 1;
            }
        } else {
            pageIndex = 1;
        }
        let orderBy = "";
        let offset = (pageIndex - 1) * pageSize;

      

        let sql = `Select Distinct(a.id), a.fullname, c.address,a.phone,a.birthday,a.create_time
               From members a INNER JOIN orders b ON b.member_id = a.id 
               INNER JOIN orders_details d ON b.id =  d.orders_id
               INNER JOIN product_seller e ON e.id =  d.product_seller_id
               INNER JOIN sellers f ON f.id = e.seller_id
               LEFT JOIN member_address c ON c.member_id = a.id               
               WHERE c.default_flag =1 and f.uuid = '${uuid}'`;   
        if (content != "undefined") {
            sql += ` and a.fullname like '%${content}%' `;          
        }      
        sql += ` group by a.id,address order by a.fullname DESC  limit ${pageSize} offset ${offset}`;

        let count = await Members.Count(sql,''); 

        console.log(count);

        var ds = Members.dataSource;       
            ds.connector.query(sql, '', function (err, instance) {
                if (err) {
                    result.setError(ResultsCodes.DATA_NOT_FOUND);
                    return cb(null, result);
                }               
                result.setSuccess({ itemCount: count.length? count.length:0, rows: instance });              
                cb(null, result);
            });     
    }
    makeRequest().catch(function (e) {
        let result = new Results();
        result.setError(ResultsCodes.SYSTEM_ERROR);
        return cb(null, result);
    });
}
    /**
     * Count
     * @param {*} sql 
     * @param {*} params 
     */
  Members.Count = function (sql, params) {
      let promise = new Promise((resolve, reject) => {
          var ds = Members.dataSource;
          ds.connector.query(sql, params, function (err, data) {
              if (err) {
                  reject(err);
              } else {
                  resolve(data);
              }
          });
      });
      return promise;
  }

//#endregion
}