var Results = new require('../middlewares/ResultHandle');

module.exports = function (Notification) {

    /**
     * @function createNotification
     * @param {number} orderId
     * @param {number} shippingId
     * @param {string} orderCode
     * @param {status} status
     */
    Notification.createNotification = function (memberId, orderId, shippingBillId, orderCode, status) {
        if (!memberId || !orderId || !status) {
            var e = new Error('Data Input invalid!');
            return e;
        }


        // Created a random otp code
        var token = ("" + Math.random()).substring(2, 8);
        var res = {
            id: token,
            memberId: memberId,
            orderId: orderId,
            shippingId: shippingBillId,
            orderCode: orderCode,
            status: status,
            created: parseInt(new Date().getTime() / 1000),
            expired: parseInt((new Date().getTime() / 1000) + 3000),  // 5 mins from now
            ttl: 1 // 5 mins
        }
        Notification.create(res, function (err, otpInstance) {

            if (err) {
                return err;
            }
            return otpInstance;
        });
    }

    Notification.removeNotification = function (id) {
        if (!id) {
            Results.success = {};
            Results.errors = {
                code: "E0004",
                name: "ID_NOT_VALID",
                message: "Mã Notification không đúng"
            }
            Results.warning = {};
            return Results;
        }
        else {
            Notification.deleteById(id, function (err) {
                if (err) {
                    Results.success = {};
                    Results.errors = {
                        code: "E0005",
                        name: "CANNOT_DESTROY_NOTIFICATION",
                        message: "Không thể hủy mã Notification này"
                    }
                    Results.warning = {}; t
                    return Results;

                }
                else {
                    Results.success = true;
                    return Results;
                }
            });
        }
    }
}