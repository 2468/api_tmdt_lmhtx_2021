/**
 * Model StoreHistory
 * Author: LuongPD
 * Last update: 14.3.2018 13:49
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
'use strict';

module.exports = function(StoreHistory) {
    
    //#region Functions
    StoreHistory.CreateNew = function (productStoreId, orderDetailsId, itemAvailable, itemBooked, cb) {
        app.dataSources.mysqld.transaction(async models => {
            const { StoreHistory } = models;

            var storeHistory = new StoreHistory;
            storeHistory.productStoreId = productStoreId;
            storeHistory.orderDetailsId = orderDetailsId;
            storeHistory.itemAvailable = itemAvailable;
            storeHistory.itemBooked = itemBooked;
            storeHistory.createTime = parseInt(new Date().getTime() / 1000.0);
            await StoreHistory.create(storeHistory);

            let result = new Results();
            result.setSuccess(true);
            return cb ? cb(null, result) : result;
            
        }).catch(function (e) {
            let result = new Results();
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb ? cb(null, result) : result;
        });
    }
    //#endregion

    //#region Expose Parameters
    //#endregion
};