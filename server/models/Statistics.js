/**
 * Model Statistics
 * Author: LuongPD
 * Last update: 3.7.2018 10:38
 */
var Results = new require('../middlewares/ResultHandle');
var ResultsCodes = require('../middlewares/message_constant');
var app = require('../../server/server');
'use strict';

module.exports = function(Statistics) {

    //#region Functions
    Statistics.DailyRunning = function(cb) {

        app.dataSources.mysqld.transaction(async models => {
            const {Statistics, StatisticsShipping, Sellers, Orders, ShippingBill} = models;

            var sellers = await Sellers.find({
                fields: ['id'],
                where: {
                    "status": { gte: 0 }
                }
            });

            var preDate = new Date(new Date().getTime() - 0 * 86400000);
            var preStartDate = new Date(preDate.getFullYear(), preDate.getMonth(), preDate.getDate(), 0, 0, 0, 0);
            var preEndDate = new Date(preDate.getFullYear(), preDate.getMonth(), preDate.getDate(), 23, 59, 59, 0);
            var preStartTime = parseInt(preStartDate.getTime() / 1000.0);
            var preEndTime = parseInt(preEndDate.getTime() / 1000.0);
            var currentDate = new Date(new Date().getTime() + 1 * 86400000);
            var currentDate2 = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
            var currentTime = parseInt(currentDate2.getTime() / 1000.0);

            for (var index in sellers) {
                var seller = sellers[index];

                var preStatistics = await Statistics.findOne({
                    where: { 
                        "sellerId": seller.id,
                        and: [{ "createTime": { lte: preEndTime } }, { "createTime": { gte: preStartTime } }] 
                    }
                });

                let shippingBills = await ShippingBill.find({
                    fields: ['id', 'sellerId', 'orderId', 'billPrice', 'shipUnitId', 'shipFee', 'createTime'],
                    include: [
                        {
                            relation:'orders',
                            scope: {
                                fields:['id', 'paymentType', 'buyerLocationCode']
                            }
                        }
                    ],
                    where: {
                        "sellerId": seller.id,
                        "status": 7,
                        and: [{ "createTime": { lte: preEndTime } }, { "createTime": { gte: preStartTime } }]
                    }
                });

                var totalBillPrice = 0;
                var totalShipFee = 0;
                if (shippingBills.length > 0) {
                    for (var index in shippingBills) {

                        var shippingBill = shippingBills[index].toJSON();
                        var newStatisticsShipping = new StatisticsShipping();
                        newStatisticsShipping.sellerId = seller.id;
                        newStatisticsShipping.orderId = shippingBill.orders.id;
                        newStatisticsShipping.billPrice = shippingBill.billPrice;
                        newStatisticsShipping.shipUnitId = shippingBill.shipUnitId;
                        newStatisticsShipping.shipFee = shippingBill.shipFee;
                        newStatisticsShipping.paymentType = shippingBill.orders.paymentType;
                        newStatisticsShipping.buyerLocationCode = shippingBill.orders.buyerLocationCode;
                        newStatisticsShipping.createTime = currentTime;
                        await newStatisticsShipping.save();

                        totalBillPrice += shippingBill.billPrice;
                        totalShipFee += shippingBill.shipFee;
                    }
                } else {

                    var newStatisticsShipping = new StatisticsShipping();
                    newStatisticsShipping.sellerId = seller.id;
                    newStatisticsShipping.orderId = 0;
                    newStatisticsShipping.billPrice = 0;
                    newStatisticsShipping.shipUnitId = 0;
                    newStatisticsShipping.shipFee = 0;
                    newStatisticsShipping.paymentType = 0;
                    newStatisticsShipping.buyerLocationCode = 0;
                    newStatisticsShipping.createTime = currentTime;
                    await newStatisticsShipping.save();
                }

                var beginningBalance = 0;
                if (preStatistics != null) {
                    beginningBalance = preStatistics.finalBalance;
                }

                var newStatistics = new Statistics();
                newStatistics.sellerId = seller.id;
                newStatistics.beginningBalance = beginningBalance;
                newStatistics.totalBillPrice = totalBillPrice;
                newStatistics.totalShipFee = totalShipFee;
                newStatistics.finalBalance = beginningBalance + totalBillPrice;
                newStatistics.smartlifeFee = 0;
                newStatistics.smartlifeForfeit = 0;
                newStatistics.castOnShip = 0;
                newStatistics.castOnSmartlife = 0;
                newStatistics.createTime = currentTime;
                await newStatistics.save();
            }

            let result = new Results();
            result.setSuccess(sellers.length);
            return cb(null, result);

        }).catch (function(e) {
            let result = new Results();
            result.setError(ResultsCodes.ADD_ITEM_FAILED);
            return cb(null,result);
        });
    }

    Statistics.ReportBusiness = function (ctx, cb) {

        let from = ctx.query.from || 0;
        let to = ctx.query.to || new Date().getTime()/1000; 
        let type = ctx.query.type || 0;

        let result = new Results();
        const makeRequest = async () => {
            let Passport = Statistics.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var sellerId = token.userId; 

            let sql = `SELECT 
                SUM(total_bill_price) AS total_bill_price, 
                DATE_FORMAT(DATE_ADD('1970-1-1',INTERVAL create_time SECOND), ?) as group_case
            FROM 
                statistics
            WHERE 
                seller_id = ?
            AND 
                create_time between ? AND ?
            GROUP BY group_case
            ORDER BY group_case ASC;`

            let params = [];

            if (type == 0) {
                params.push("%d/%m/%Y");
            } else {
                params.push("%m/%Y");
            }

            params.push(sellerId);
            params.push(from);
            params.push(to);

            let connector = Statistics.dataSource.connector
            connector.query(sql, params, function (err, resultObjects) {

                if (err) { 
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }
                else { 
                    result.setSuccess(resultObjects);
                    return cb(null, result);
                }

            });
        }
        makeRequest();
    }

    Statistics.ReportOrders = function (ctx, cb) {
        let from = ctx.query.from || 0;
        let to = ctx.query.to || new Date().getTime()/1000;
        let type = ctx.query.type || 0;

        let result = new Results();
        const makeRequest = async () => {
            let Passport = Statistics.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var sellerId = token.userId; 
            let sql = `SELECT DATE_FORMAT(DATE_ADD('1970-1-1',INTERVAL ss1.create_time SECOND), ?) AS group_case,
                (
                    SELECT COUNT(*) 
                    FROM statistics_shipping ss2 
                    WHERE DATE_FORMAT(DATE_ADD('1970-1-1',INTERVAL ss2.create_time SECOND), ?) = group_case
                    AND ss2.seller_id = ss1.seller_id
                    AND ss2.order_id > 0
                ) AS total_count
            FROM statistics_shipping ss1
            WHERE ss1.seller_id = ?
            AND ss1.create_time between ? AND ?
            GROUP BY group_case
            ORDER BY group_case ASC;`

            let params = [];

            if (type == 0) {
                params.push("%d/%m/%Y");
                params.push("%d/%m/%Y");
            } else {
                params.push("%m/%Y");
                params.push("%m/%Y");
            }

            params.push(sellerId);
            params.push(from);
            params.push(to);

            let connector = Statistics.dataSource.connector
            connector.query(sql, params, function (err, resultObjects) {

                if (err) { 
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }
                else { 
                    result.setSuccess(resultObjects);
                    cb(null, result);
                }

            });
        }
        makeRequest();
    }

    Statistics.ReportArea = function (ctx, cb) {
        let from = ctx.query.from || 0;
        let to = ctx.query.to ||  new Date().getTime()/1000; 
        let type = ctx.query.type || 0; 
        let  parentCode = ctx.query.parentCode || null;

        let result = new Results();
        const makeRequest = async () => {
            let Passport = Statistics.app.models.Passport;
            let token = await Passport.GetSellerToken(ctx);
            if (!token) {
                result.setError(ResultsCodes.ACCESS_TOKEN_INVALID);
                return cb(null, result);
            }
            var sellerId = token.userId;
 
            let params = [];
            params.push(sellerId);
            params.push(from);
            params.push(to);

            let sql = `SELECT`;

            if (type == 0) {
                sql += ` l2.code AS location_code, l2.name_with_type AS location_name,`;
            } else if (type == 1) {
                sql += ` l3.code AS location_code, l3.name_with_type AS location_name,`;
            } else if (type == 2) {
                sql += ` l4.code AS location_code, l4.name_with_type AS location_name,`;
            }

            sql += `COUNT(ss1.buyer_location_code) AS total_count
            FROM statistics_shipping ss1
            INNER JOIN location l4
                ON ss1.buyer_location_code = l4.code
            INNER JOIN location l3
                ON l4.parent_code = l3.code
            INNER JOIN location l2
                ON l3.parent_code = l2.code
            WHERE ss1.seller_id = ?
                AND ss1.order_id > 0
                AND ss1.create_time between ? AND ?`;

            if (parentCode != null) {
                if (type == 0) {
                    //
                } else if (type == 1) {
                    sql += ` AND l2.code = ?`;
                    params.push(parentCode);
                } else if (type == 2) {
                    sql += ` AND l3.code = ?`;
                    params.push(parentCode);
                }
            }
            
            sql += ` GROUP BY location_code, location_name;`

            let connector = Statistics.dataSource.connector
            connector.query(sql, params, function (err, resultObjects) {

                if (err) { 
                    result.setError(ResultsCodes.SYSTEM_ERROR);
                    return cb(null, result);
                }
                else { 
                    result.setSuccess(resultObjects);
                    return cb(null, result);
                }

            });
        }
        makeRequest();
    }
    //#endregion

    //#region Expose Parameters
    Statistics.remoteMethod('DailyRunning', {
        http: {
            verb: "GET"
        },
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    Statistics.remoteMethod('ReportBusiness', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns: {
            arg: 'result',
            type: 'string',
            root: true
        }
    })

    Statistics.remoteMethod('ReportOrders', {
        http: {
            verb: "GET"
        },
        accepts: [
            {
                arg: 'from',
                type: 'number',
                required: true
            },
            {
                arg: 'to',
                type: 'number',
                required: true
            },
            {
                arg: 'type',
                type: 'number',
                required: true
            }
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })

    Statistics.remoteMethod('ReportArea', {
        http: {
            verb: "GET",
            path:'/reportarea'
        },
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http:{
                    source:'req'
                }
            } 
        ],
        returns: [{
            arg: 'result',
            type: 'string',
            root: true
        }]
    })
    //#endregion
};