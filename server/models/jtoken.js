'user strict';

const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const privateCert = fs.readFileSync(path.resolve(__dirname, `../../private.key`));
const publicCert = fs.readFileSync(path.resolve(__dirname, `../../public.key`));

module.exports = function (JToken) {
   // Bg thang member doi sang jtoken roi, va thang seller cung can dung thang nay de dam bao
    JToken.sign = (issUUID, uid, type) => {
        let token = jwt.sign({ iss: issUUID, aid: uid, typ: type, exp: Math.floor(Date.now() / 1000) + (60 * 3600) }, privateCert, { algorithm: 'RS512' });
        return token ? token : '';
    }

    JToken.verify = (token) => {
        let promise = new Promise((res, rej) => {
            jwt.verify(token, publicCert, { algorithm: 'RS512', ignoreExpiration: false }, (err, decoded) => {
                if (err) {
                    return res(err);
                }

                return res(decoded);
            });
        })

        return promise;
    }

    JToken.decode = (token, option = null) => {

    }

}